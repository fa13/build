package gradle.plugin.jre

import org.junit.Test

import static org.junit.Assert.assertEquals

class JreVersionTest {

    @Test
    void getMajor() throws Exception {
        assertEquals(7, new JreVersion(7, 5, 1).getMajor())
    }

    @Test
    void getUpdate() throws Exception {
        assertEquals(5, new JreVersion(7, 5, 1).getUpdate())
    }

    @Test
    void getBuild() throws Exception {
        assertEquals('01', new JreVersion(7, 5, 1).getBuild())
    }
}
