package gradle.plugin.jre

import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import javax.inject.Inject

class DownloadOracleJavaTask extends DefaultTask  {
    private static final BASE_URI = 'http://repository.fa13.info/jdk'

    JreVersion version
    Platform platform
    Architecture arch

    @Inject
    DownloadOracleJavaTask() {
    }

    @OutputFile
    File getOutputFile() {
        return project.file("$temporaryDir/$jreFileName")
    }

    @Input
    String getSourceUri() {
        "${BASE_URI}/${jreFileName}"
    }

    String getJreFileName() {
        "jre-${version.major}u${version.update}-${platform.name}-${arch.name}.tar.gz"
    }

    @TaskAction
    void downloadJre() {
        logger.info("Downloading $sourceUri to ${outputFile.absolutePath}")
        httpClient().withCloseable { httpClient ->
            httpClient.execute(new HttpGet(sourceUri)).withCloseable { response ->
                outputFile.withOutputStream { out ->
                    response.entity.writeTo(out)
                }
            }
        }
    }

    private static CloseableHttpClient httpClient() {
        HttpClientBuilder.create().build()
    }
}