package com.fa13.build.gui;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import com.fa13.build.view.MainWindow;

public class MainGUI {

    public static final String CRASH_REPORT_FILE = "crash.log";

    public static void main(String[] args) throws IOException {
        try {
            new MainWindow();
        } catch (Exception e) {
            e.printStackTrace();
            createCrashReport(e);
        }
    }

    private static void createCrashReport(Exception e) {
        File file = new File(CRASH_REPORT_FILE);
        try {
            PrintStream ps = new PrintStream(file);
            e.printStackTrace(ps);
            ps.close();
        } catch (Exception ex) {
        }
    }

}