package com.fa13.build.gui.cocoa;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.internal.C;
import org.eclipse.swt.internal.Callback;
import org.eclipse.swt.widgets.Display;

/**
 * <p>
 * Provide a hook to connecting the Preferences, About and Quit menu items of the Mac OS X
 * Application menu when using the SWT Cocoa bindings.</p>
 * <p>
 * This code does not require the Cocoa SWT JAR in order to be compiled as it uses reflection to
 * access the Cocoa specific API methods. It does, however, depend on JFace (for IAction), but you
 * could easily modify the code to use SWT Listeners instead in order to use this class in SWT only
 * applications.</p>
 * <p>
 * This code was influenced by the
 * <a href="http://www.simidude.com/blog/2008/macify-a-swt-application-in-a-cross-platform-way/">
 * CarbonUIEnhancer from Agynami</a>
 * with the implementation being modified from the
 * <a href="http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.ui.cocoa/src/org/eclipse/ui/internal/cocoa/CocoaUIEnhancer.java">
 * org.eclipse.ui.internal.cocoa.CocoaUIEnhancer</a>.</p>
 */
public class CocoaUIEnhancer {

    private static final int kAboutMenuItem = 0;
    private static final int kPreferencesMenuItem = 2;
    // private static final int kServicesMenuItem = 4;
    // private static final int kHideApplicationMenuItem = 6;
    private static final int kQuitMenuItem = 10;

    final private String appName;

    /**
     * Construct a new CocoaUIEnhancer.
     *
     * @param appName The name of the application. It will be used to customize the About and Quit menu
     *                items. If you do not wish to customize the About and Quit menu items, just pass
     *                <tt>null</tt> here.
     */
    public CocoaUIEnhancer(String appName) {
        this.appName = appName;
    }

    /**
     * Hook the given Listener to the Mac OS X application Quit menu and the IActions to the About
     * and Preferences menus.
     *  @param display           The Display to use.
     * @param aboutAction       The action to run when the About menu is invoked.
     * @param preferencesAction The action to run when the Preferences menu is invoked.
     */
    public void hookApplicationMenu(Display display, final Runnable aboutAction, final Runnable preferencesAction) {
        try {
            final Callback proc3Args = initialize(aboutAction, preferencesAction);
            display.disposeExec(new Runnable() {
                public void run() {
                    invoke(proc3Args, "dispose");
                }
            });
        } catch (Exception e) {
            System.err.println("Could not initialize application menu for OS X. Please report stacktrace:");
            e.printStackTrace(System.err);
        }
    }

    private Callback initialize(Runnable aboutAction, Runnable preferencesAction) throws Exception {
        // Load SWT cocoa classes
        Class<?> osClass = Class.forName("org.eclipse.swt.internal.cocoa.OS");
        Class<?> nsmenuClass = Class.forName("org.eclipse.swt.internal.cocoa.NSMenu");
        Class<?> nsmenuitemClass = Class.forName("org.eclipse.swt.internal.cocoa.NSMenuItem");
        Class<?> nsstringClass = Class.forName("org.eclipse.swt.internal.cocoa.NSString");
        Class<?> nsapplicationClass = Class.forName("org.eclipse.swt.internal.cocoa.NSApplication");

        // Register names in objective-c.
//        long sel_toolbarButtonClicked = registerName(osClass, "toolbarButtonClicked:"); //$NON-NLS-1$
        long sel_preferencesMenuItemSelected = registerName(osClass, "preferencesMenuItemSelected:"); //$NON-NLS-1$
        long sel_aboutMenuItemSelected = registerName(osClass, "aboutMenuItemSelected:"); //$NON-NLS-1$

        // Build menu action handler
        HashMap<Long, Runnable> actions = new HashMap<>();
        actions.put(sel_preferencesMenuItemSelected, preferencesAction);
        actions.put(sel_aboutMenuItemSelected, aboutAction);
        ActionHandler handler = new ActionHandler(actions);

        Callback menuCallback = new Callback(handler, "actionProc", 3); //$NON-NLS-1$

        Method getAddress = Callback.class.getMethod("getAddress");
        long callbackAddress = convertToLong(getAddress.invoke(menuCallback, (Object[]) null));
        if (callbackAddress == 0) {
            SWT.error(SWT.ERROR_NO_MORE_CALLBACKS);
        }

        // Instead of creating a new delegate class in objective-c, just use the current SWTApplicationDelegate.
        // An instance of this is a field of the Cocoa Display object and is already the target for the menuItems.
        // So just get this class and add the new methods to it.
        long swtApplicationDelegateClass = convertToLong(invoke(osClass, "objc_lookUpClass", new Object[] {"SWTApplicationDelegate"}));

        // Add the action callbacks for Preferences and About menu items.
        invoke(osClass, "class_addMethod", new Object[] {
            wrapPointer(swtApplicationDelegateClass),
            wrapPointer(sel_preferencesMenuItemSelected),
            wrapPointer(callbackAddress),
            "@:@"}); //$NON-NLS-1$
        invoke(osClass, "class_addMethod", new Object[] {
            wrapPointer(swtApplicationDelegateClass),
            wrapPointer(sel_aboutMenuItemSelected),
            wrapPointer(callbackAddress),
            "@:@"}); //$NON-NLS-1$

        // Get the Mac OS X Application menu.
        Object sharedApplication = invoke(nsapplicationClass, "sharedApplication");
        Object mainMenu = invoke(sharedApplication, "mainMenu");
        Object mainMenuItem = invoke(nsmenuClass, mainMenu, "itemAtIndex", new Object[] {wrapPointer(0)});
        Object appMenu = invoke(mainMenuItem, "submenu");

        // Create the About <application-name> menu command
        Object aboutMenuItem =
            invoke(nsmenuClass, appMenu, "itemAtIndex", new Object[] {wrapPointer(kAboutMenuItem)});
        if (appName != null) {
            Object nsStr = invoke(nsstringClass, "stringWith", new Object[] {"About " + appName});
            invoke(nsmenuitemClass, aboutMenuItem, "setTitle", new Object[] {nsStr});
        }
        // Rename the quit action.
        if (appName != null) {
            Object quitMenuItem =
                invoke(nsmenuClass, appMenu, "itemAtIndex", new Object[] {wrapPointer(kQuitMenuItem)});
            Object nsStr = invoke(nsstringClass, "stringWith", new Object[] {"Quit " + appName});
            invoke(nsmenuitemClass, quitMenuItem, "setTitle", new Object[] {nsStr});
        }

        // Enable the Preferences menuItem.
        Object prefMenuItem =
            invoke(nsmenuClass, appMenu, "itemAtIndex", new Object[] {wrapPointer(kPreferencesMenuItem)});
        invoke(nsmenuitemClass, prefMenuItem, "setEnabled", new Object[] {true});

        // Set the action to execute when the About or Preferences menuItem is invoked.
        //
        // We don't need to set the target here as the current target is the SWTApplicationDelegate
        // and we have registerd the new selectors on it. So just set the new action to invoke the
        // selector.
        invoke(nsmenuitemClass, prefMenuItem, "setAction",
            new Object[] {wrapPointer(sel_preferencesMenuItemSelected)});
        invoke(nsmenuitemClass, aboutMenuItem, "setAction",
            new Object[] {wrapPointer(sel_aboutMenuItemSelected)});

        return menuCallback;
    }

    private long registerName(Class<?> osCls, String name)
        throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object object = invoke(osCls, "sel_registerName", new Object[] {name});
        return convertToLong(object);
    }

    private long convertToLong(Object object) {
        if (object instanceof Integer) {
            Integer i = (Integer) object;
            return i.longValue();
        }
        if (object instanceof Long) {
            return (Long) object;
        }
        return 0;
    }

    private static Object wrapPointer(long value) {
        Class<?> PTR_CLASS = C.PTR_SIZEOF == 8 ? long.class : int.class;
        if (PTR_CLASS == long.class)
            return value;
        else
            return (int) value;
    }

    private static Object invoke(Class<?> clazz, String methodName, Object[] args) {
        return invoke(clazz, null, methodName, args);
    }

    private static Object invoke(Class<?> clazz, Object target, String methodName, Object[] args) {
        try {
            Class<?>[] signature = new Class<?>[args.length];
            for (int i = 0; i < args.length; i++) {
                Class<?> thisClass = args[i].getClass();
                if (thisClass == Integer.class)
                    signature[i] = int.class;
                else if (thisClass == Long.class)
                    signature[i] = long.class;
                else if (thisClass == Byte.class)
                    signature[i] = byte.class;
                else if (thisClass == Boolean.class)
                    signature[i] = boolean.class;
                else
                    signature[i] = thisClass;
            }
            Method method = clazz.getMethod(methodName, signature);
            return method.invoke(target, args);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private Object invoke(Class<?> cls, String methodName) {
        return invoke(cls, methodName, (Class<?>[]) null, (Object[]) null);
    }

    private Object invoke(Class<?> cls, String methodName, Class<?>[] paramTypes, Object... arguments) {
        try {
            Method m = cls.getDeclaredMethod(methodName, paramTypes);
            return m.invoke(null, arguments);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private Object invoke(Object obj, String methodName) {
        return invoke(obj, methodName, (Class<?>[]) null, (Object[]) null);
    }

    private Object invoke(Object obj, String methodName, Class<?>[] paramTypes, Object... arguments) {
        try {
            Method m = obj.getClass().getDeclaredMethod(methodName, paramTypes);
            return m.invoke(obj, arguments);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private static class ActionHandler {
        private static final int RETURN_VALUE = 99;

        private final Map<Long, Runnable> actions;

        private ActionHandler(Map<Long, Runnable> actions) {
            this.actions = actions;
        }

        public int actionProc(int id, int sel, int arg0) {
            return (int) actionProc((long) id, (long) sel, (long) arg0);
        }

        public long actionProc(long id, long sel, long arg0) {
            Runnable action = actions.get(sel);
            if (action != null) {
                action.run();
            }
            return RETURN_VALUE;
        }
    }
}
