package com.fa13.build.view.game;


public enum SchemeViewType {
    ATTACK, DEFENCE, FREEKICK
}