package com.fa13.build.view.calc;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.model.All;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.model.TransferPlayerFilter;
import com.fa13.build.model.calc.PlayersExchangeVariant;
import com.fa13.build.utils.GameUtils;
import com.fa13.build.utils.GuiLongTask;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.PlayerAbilitiesFilterView;
import com.fa13.build.view.transfer.TransferView.FilterSpinner;

public class PlayerExchangesView extends Composite {

    final private TabFolder parentTabFolder;
    private final TabItem mainItem;
    //choosen team/player
    private Team choosenTeam;
    private List<Player> listChoosenPlayers;
    private All all;

    private Table exchangeVariantsTable;
    private ScrolledComposite scrollPanel;
    private Composite mainComposite;
    private Group choosenTeamGroup;
    private Composite leftPanel;
    private Composite rightPanel;
    private Label teamLabel;
    private Combo teamCombo;
    private Table playersTable;
    private Label playersLabel;
    private Group filterPanel;
    //private ArrayList<Player> listAllPlayers;
    private PlayerAbilitiesFilterView abilitiesFilterView;
    private boolean filterReseting;

    private TransferPlayerFilter transferPlayerFilter;
    private Label ageLabel;
    private Spinner ageSpinner1;
    private Spinner ageSpinner2;
    private Label strengthLabel;
    private Spinner strengthSpinner1;
    private Spinner strengthSpinner2;
    private Label wageLabel;
    private Spinner wageSpinner1;
    private Spinner wageSpinner2;
    private Label talentLabel;
    private Spinner talentSpinner1;
    private Spinner talentSpinner2;
    private Label healthLabel;
    private Spinner healthSpinner1;
    private Spinner healthSpinner2;
    private Label expLabel;
    private Spinner expSpinner1;
    private Spinner expSpinner2;
    private Spinner ticketsSpinner1;
    private Spinner ticketsSpinner2;
    private Label positionLabel;
    private Text positionText;
    private Spinner priceSpinner1;
    private Spinner priceSpinner2;
    private Label teamFilterLabel;
    private Text teamFilterText;
    private Label countryLabel;
    private Text countryText;
    private Composite filterControlPanel;
    private Button applyFiltersButton;
    private Button showHideFilterButton;
    private Image imageFilterWindowShow;
    private Image imageFilterWindowHide;
    private Button checkButtonUseFilter;
    private Button checkButtonUseAbilityFilter;
    private List<PlayersExchangeVariant> listExchanges;
    private Button bTeamsWithManager;
    private static final NumberFormat nf = NumberFormat.getInstance();

    public PlayerExchangesView(TabFolder parent, int style) {
        super(parent, style);
        transferPlayerFilter = new TransferPlayerFilter();
        //the manager's name that means there is no actual manager in team
        //used in filterА
        transferPlayerFilter.setNoManagerName(getRes("NoManagerName"));//!!!
        parentTabFolder = parent;
        mainItem = new TabItem(parentTabFolder, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("PlayerExchanges.tab"));
        loadImages();
        mainItem.setImage(MainWindow.getSharedImage(ImageType.EXCHANGE));

        initComponents();
        addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                imageFilterWindowHide.dispose();
                imageFilterWindowShow.dispose();

            }
        });

    }

    private void loadImages() {

        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_find_16x16.png"));
        imageFilterWindowShow = new Image(getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_remove_16x16.png"));
        imageFilterWindowHide = new Image(getDisplay(), imgData);

    }

    private void initComponents() {

        scrollPanel = new ScrolledComposite(parentTabFolder, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        scrollPanel.setExpandHorizontal(true);
        scrollPanel.setExpandVertical(true);
        mainComposite = new Composite(scrollPanel, SWT.NONE);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(new FormLayout());

        leftPanel = new Composite(mainComposite, SWT.NONE);
        //leftPanel.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_CYAN));
        FormData fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(0, 0);
        fd.bottom = new FormAttachment(100, 0);
        leftPanel.setLayout(new GridLayout(1, false));
        leftPanel.setLayoutData(fd);

        rightPanel = new Composite(mainComposite, SWT.NONE);
        //rightPanel.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_GREEN));
        fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(leftPanel, 0);
        fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        rightPanel.setLayout(new GridLayout(2, false));
        rightPanel.setLayoutData(fd);

        createChoosenTeamGroup();
        createFilterGroup();
        createAbilitiesFilterGroup();
        createExchangesTable();

        mainComposite.setRedraw(true);
        mainItem.setControl(scrollPanel);
        scrollPanel.setContent(mainComposite);

    }

    private void createAbilitiesFilterGroup() {

        abilitiesFilterView = new PlayerAbilitiesFilterView(rightPanel, SWT.NONE);
        GridData gd = new GridData();
        abilitiesFilterView.setLayoutData(gd);
        abilitiesFilterView.setPlayerFilter(transferPlayerFilter);//!!!

    }

    private void createExchangesTable() {

        exchangeVariantsTable = new Table(rightPanel, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION);
        exchangeVariantsTable.setHeaderVisible(true);
        exchangeVariantsTable.setLinesVisible(true);
        GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
        exchangeVariantsTable.setLayoutData(gd);
        createExchangeVariantsTableColumns();

    }

    private void createFilterGroup() {

        filterPanel = new Group(rightPanel, SWT.NONE);

        filterPanel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        filterPanel.setText(MainWindow.getMessage("filter"));
        filterPanel.setLayout(new GridLayout(6, false));

        GridData data = new GridData();//SWT.FILL, SWT.FILL, true, true);
        filterPanel.setLayoutData(data);
        filterPanel.addMenuDetectListener(new MenuDetectListener() {
            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(filterPanel);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("ResetFilter"));
                item.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        resetFilter();
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });
        // age        
        ageLabel = new Label(filterPanel, SWT.NONE);
        ageLabel.setText(MainWindow.getMessage("filter.age"));
        GridData gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageLabel.setLayoutData(gridData);

        ageSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        ageSpinner1.setValues(15, 15, 99, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageSpinner1.setLayoutData(gridData);
        ageSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.AGE, 1));

        ageSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        ageSpinner2.setValues(99, 15, 99, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageSpinner2.setLayoutData(gridData);
        ageSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.AGE, 2));
        //strength
        strengthLabel = new Label(filterPanel, SWT.NONE);
        strengthLabel.setText(MainWindow.getMessage("filter.strength"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthLabel.setLayoutData(gridData);

        strengthSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        strengthSpinner1.setValues(1, 1, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthSpinner1.setLayoutData(gridData);
        strengthSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STRENGTH, 1));

        strengthSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        strengthSpinner2.setValues(100, 1, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthSpinner2.setLayoutData(gridData);
        strengthSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STRENGTH, 2));

        //wage
        wageLabel = new Label(filterPanel, SWT.NONE);
        wageLabel.setText(MainWindow.getMessage("filter.wage"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageLabel.setLayoutData(gridData);

        wageSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        wageSpinner1.setValues(0, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageSpinner1.setLayoutData(gridData);
        wageSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.WAGE, 1));

        wageSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        wageSpinner2.setValues(999, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageSpinner2.setLayoutData(gridData);
        wageSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.WAGE, 2));

        //talent
        talentLabel = new Label(filterPanel, SWT.NONE);
        talentLabel.setText(MainWindow.getMessage("filter.talent"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentLabel.setLayoutData(gridData);

        talentSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        talentSpinner1.setValues(20, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentSpinner1.setLayoutData(gridData);
        talentSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TALENT, 1));

        talentSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        talentSpinner2.setValues(100, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentSpinner2.setLayoutData(gridData);
        talentSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TALENT, 2));
        //health
        healthLabel = new Label(filterPanel, SWT.NONE);
        healthLabel.setText(MainWindow.getMessage("filter.health"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthLabel.setLayoutData(gridData);

        healthSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        healthSpinner1.setValues(20, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthSpinner1.setLayoutData(gridData);
        healthSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEALTH, 1));

        healthSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        healthSpinner2.setValues(100, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthSpinner2.setLayoutData(gridData);
        healthSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEALTH, 2));
        //Experience
        expLabel = new Label(filterPanel, SWT.NONE);
        expLabel.setText(MainWindow.getMessage("filter.experiance"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expLabel.setLayoutData(gridData);

        expSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        expSpinner1.setValues(0, 0, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expSpinner1.setLayoutData(gridData);
        expSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.EXPERIANCE, 1));

        expSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        expSpinner2.setValues(100, 0, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expSpinner2.setLayoutData(gridData);
        expSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.EXPERIANCE, 2));

        //position
        positionLabel = new Label(filterPanel, SWT.NONE);
        positionLabel.setText(MainWindow.getMessage("filter.position"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        positionLabel.setLayoutData(gridData);

        positionText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        positionText.setLayoutData(gridData);
        positionText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setPositionNamePart(positionText.getText());
            }
        });

        //player
        teamFilterLabel = new Label(filterPanel, SWT.NONE);
        teamFilterLabel.setText(MainWindow.getMessage("global.team"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        teamFilterLabel.setLayoutData(gridData);

        teamFilterText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.FILL, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        teamFilterText.setLayoutData(gridData);
        teamFilterText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setTeamPart(teamFilterText.getText());
            }
        });
        //country
        countryLabel = new Label(filterPanel, SWT.NONE);
        countryLabel.setText(MainWindow.getMessage("filter.country"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        countryLabel.setLayoutData(gridData);

        countryText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.FILL, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        countryText.setLayoutData(gridData);
        countryText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setCountryPart(countryText.getText());
            }
        });

        bTeamsWithManager = new Button(filterPanel, SWT.CHECK);
        gridData = new GridData(SWT.FILL, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        bTeamsWithManager.setLayoutData(gridData);
        bTeamsWithManager.setText(getRes("TeamsWithManager"));
        bTeamsWithManager.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                transferPlayerFilter.setTeamsWithManagerOnly(bTeamsWithManager.getSelection());
            }
        });
    }

    private void createChoosenTeamGroup() {

        choosenTeamGroup = new Group(leftPanel, SWT.NONE);
        choosenTeamGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        choosenTeamGroup.setText(MainWindow.getMessage("ChoosePlayerForExchanges"));
        choosenTeamGroup.setLayout(new GridLayout(1, false));

        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        choosenTeamGroup.setLayoutData(gd);

        teamLabel = new Label(choosenTeamGroup, SWT.NONE);
        teamLabel.setText(MainWindow.getMessage("global.team"));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        teamLabel.setLayoutData(gd);

        teamCombo = new Combo(choosenTeamGroup, SWT.READ_ONLY | SWT.BORDER);
        if (!SystemUtils.IS_OS_LINUX) {
            teamCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        teamCombo.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.FILL;
        teamCombo.setLayoutData(gd);
        teamCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                onTeamChanged();
            }
        });
        playersLabel = new Label(choosenTeamGroup, SWT.NONE);
        playersLabel.setText(MainWindow.getMessage("saleEditorTabName"));
        gd = new GridData();
        gd.horizontalAlignment = SWT.CENTER;
        playersLabel.setLayoutData(gd);

        playersTable = new Table(choosenTeamGroup, SWT.BORDER | SWT.READ_ONLY | SWT.CHECK);
        gd = new GridData(GridData.FILL_BOTH);
        gd.grabExcessHorizontalSpace = true;
        gd.grabExcessVerticalSpace = true;
        //gd.horizontalAlignment = SWT.FILL;
        //gd.verticalAlignment = SWT.FILL;
        playersTable.setLayoutData(gd);

        playersTable.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(playersTable);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(getRes("SelectAll"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        for (TableItem ti : playersTable.getItems()) {
                            ti.setChecked(true);
                        }
                    }
                });

                item = new MenuItem(menu, SWT.NONE);
                item.setText(getRes("DeselectAll"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        for (TableItem ti : playersTable.getItems()) {
                            ti.setChecked(false);
                        }
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });
        playersTable.addListener(SWT.MouseHover, new Listener() {

            @Override
            public void handleEvent(Event event) {

                if (event.type == SWT.MouseHover) {
                    TableItem ti = playersTable.getItem(new Point(event.x, event.y));
                }

            }
        });
        createFilterControlPanel();

    }

    private void createFilterControlPanel() {
        filterControlPanel = new Composite(choosenTeamGroup, SWT.NONE);
        GridData gd = new GridData();
        filterControlPanel.setLayoutData(gd);
        filterControlPanel.setLayout(new GridLayout(2, false));

        applyFiltersButton = new Button(filterControlPanel, SWT.PUSH);
        applyFiltersButton.setToolTipText(MainWindow.getMessage("applyFilter"));
        //applyFiltersButton.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData();
        applyFiltersButton.setLayoutData(gd);
        applyFiltersButton.setImage(MainWindow.getSharedImage(ImageType.OK));
        //applyFiltersButton.setText(MainWindow.getMessage("filter"));
        applyFiltersButton.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                GuiLongTask task = new GuiLongTask() {

                    @Override
                    protected void execute() {
                        recalcExchangeVariants();
                        fillExchangeVariantsTable();
                    }
                };
                GuiUtils.showModalProgressWindow(getShell(), task, MainWindow.getMessage("global.pleaseWait"), false);

            }

        });

        checkButtonUseFilter = new Button(filterControlPanel, SWT.CHECK);
        checkButtonUseFilter.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
        checkButtonUseFilter.setLayoutData(gd);
        checkButtonUseFilter.setText(MainWindow.getMessage("filter"));
        checkButtonUseFilter.setSelection(false);
        checkButtonUseFilter.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                boolean flag = checkButtonUseFilter.getSelection() || checkButtonUseAbilityFilter.getSelection();
                applyFiltersButton.setEnabled(flag);
                transferPlayerFilter.setUseMainFilter(checkButtonUseFilter.getSelection());
                GuiLongTask task = new GuiLongTask() {

                    @Override
                    protected void execute() {
                        recalcExchangeVariants();
                        fillExchangeVariantsTable();
                    }
                };
                GuiUtils.showModalProgressWindow(getShell(), task, MainWindow.getMessage("global.pleaseWait"), false);
            }

        });

        showHideFilterButton = new Button(filterControlPanel, SWT.TOGGLE);
        showHideFilterButton.setSelection(true);
        gd = new GridData();
        showHideFilterButton.setLayoutData(gd);
        showHideFilterButton.setImage(imageFilterWindowHide);
        //applyFiltersButton.setText(MainWindow.getMessage("filter"));
        showHideFilterButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                if (showHideFilterButton.getSelection()) {
                    showHideFilterButton.setImage(imageFilterWindowHide);
                    showHideFilterButton.setToolTipText(MainWindow.getMessage("hideFilter"));
                } else {
                    showHideFilterButton.setImage(imageFilterWindowShow);
                    showHideFilterButton.setToolTipText(MainWindow.getMessage("showFilter"));
                }
                showFilterPanel(showHideFilterButton.getSelection());
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        checkButtonUseAbilityFilter = new Button(filterControlPanel, SWT.CHECK);
        checkButtonUseAbilityFilter.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
        checkButtonUseAbilityFilter.setLayoutData(gd);
        checkButtonUseAbilityFilter.setText(MainWindow.getMessage("filter.ability"));
        checkButtonUseAbilityFilter.setSelection(false);
        checkButtonUseAbilityFilter.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                boolean flag = checkButtonUseFilter.getSelection() || checkButtonUseAbilityFilter.getSelection();
                applyFiltersButton.setEnabled(flag);
                transferPlayerFilter.setUseAbilityFilter(checkButtonUseAbilityFilter.getSelection());
                GuiLongTask task = new GuiLongTask() {

                    @Override
                    protected void execute() {
                        recalcExchangeVariants();
                        fillExchangeVariantsTable();
                    }
                };
                GuiUtils.showModalProgressWindow(getShell(), task, MainWindow.getMessage("global.pleaseWait"), false);
            }

        });
    }

    private void showFilterPanel(boolean flag) {

        //        if (flag) {
        //            Point computedSize = middlePanel.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        //            middlePanelFormData.height = computedSize.y;
        //        } else {
        //            middlePanelFormData.height = 0;
        //        }
        //        topPanel.layout();
        filterPanel.setVisible(flag);
        abilitiesFilterView.setVisible(flag);
        GridData gd = (GridData) filterPanel.getLayoutData();
        gd.exclude = !flag;
        gd = (GridData) abilitiesFilterView.getLayoutData();
        gd.exclude = !flag;
        filterPanel.getParent().layout();
    }

    private void resetFilter() {
        //to avoid reaction on spinner's selection events
        filterReseting = true;
        transferPlayerFilter.resetMainFilter();

        ageSpinner1.setValues(transferPlayerFilter.getAge1(), 15, 99, 0, 1, 10);
        ageSpinner2.setValues(transferPlayerFilter.getAge2(), 15, 99, 0, 1, 10);

        wageSpinner1.setValues(transferPlayerFilter.getWage1(), 0, 999, 0, 1, 10);
        wageSpinner2.setValues(transferPlayerFilter.getWage2(), 0, 999, 0, 1, 10);

        healthSpinner1.setValues(transferPlayerFilter.getHealth1(), 20, 100, 0, 1, 10);
        healthSpinner2.setValues(transferPlayerFilter.getHealth2(), 20, 100, 0, 1, 10);

        strengthSpinner1.setValues(transferPlayerFilter.getStrength1(), 1, 100, 0, 1, 10);
        strengthSpinner2.setValues(transferPlayerFilter.getStrength2(), 1, 100, 0, 1, 10);

        talentSpinner1.setValues(transferPlayerFilter.getTalent1(), 20, 100, 0, 1, 10);
        talentSpinner2.setValues(transferPlayerFilter.getTalent2(), 20, 100, 0, 1, 10);

        expSpinner1.setValues(transferPlayerFilter.getExperiance1(), 1, 100, 0, 1, 10);
        expSpinner2.setValues(transferPlayerFilter.getExperiance2(), 1, 100, 0, 1, 10);

        teamFilterText.setText(transferPlayerFilter.getTeamPart());
        positionText.setText(transferPlayerFilter.getPositionNamePart());
        countryText.setText(transferPlayerFilter.getCountryPart());
        bTeamsWithManager.setSelection(transferPlayerFilter.isTeamsWithManagerOnly());
        filterReseting = false;
    }

    protected void onTeamChanged() {

        fillPlayersTable();

    }

    private void createExchangeVariantsTableColumns() {
        //        String[] titles = { "#1", getRes("PlayerName") + " 1", getRes("PlayerAge") + " 1", getRes("PlayerTalant") + " 1",
        //                getRes("PlayerStrength") + " 1", getRes("PlayerAbilities") + " 1", getRes("PlayerSalary") + " 1", "#2", getRes("PlayerName") + " 2",
        //                getRes("PlayerAge") + " 2", getRes("PlayerTalant") + " 2", getRes("PlayerStrength") + " 2", getRes("PlayerAbilities") + " 2",
        //                getRes("PlayerSalary") + " 2", getRes("Surcharge"), getRes("global.team") + " 2", getRes("PlayerCountry") + " 2", getRes("Manager"),
        //                getRes("manager.email") };
        //        String[] toolTips = { getRes("playerNumberHint"), "", getRes("tooltip.Age"), getRes("tooltip.Talent"), getRes("tooltip.Strength"),
        //                getRes("tooltip.Skills"), getRes("tooltip.Salary"), getRes("playerNumberHint"), "", getRes("tooltip.Age"), getRes("tooltip.Talent"),
        //                getRes("tooltip.Strength"), getRes("tooltip.Skills"), getRes("tooltip.Salary"), getRes("global.thousand_short_FC"), "", "", "", "" };
        //        
        String[] titles = { "#1", getRes("PlayerName") + " 1", "#2", getRes("PlayerPosition") + " 2", getRes("PlayerName") + " 2",
                getRes("PlayerPrice") + " 2", getRes("PlayerHealth") + " 2", getRes("PlayerAge") + " 2", getRes("PlayerTalant") + " 2",
                getRes("PlayerStrength") + " 2", getRes("PlayerAbilities") + " 2", getRes("PlayerSalary") + " 2", getRes("Surcharge"),
                getRes("global.team") + " 2", getRes("PlayerCountry") + " 2", getRes("Manager"), getRes("manager.email") };
        String[] toolTips = { getRes("playerNumberHint"), "", getRes("playerNumberHint"), getRes("tooltip.Position"), "", "",
                getRes("tooltip.Health"), getRes("tooltip.Age"), getRes("tooltip.Talent"), getRes("tooltip.Strength"), getRes("tooltip.Skills"),
                getRes("tooltip.Salary"), getRes("global.thousand_short_FC"), "", "", "", "" };

        for (int i = 0; i < titles.length; i++) {
            TableColumn column = new TableColumn(exchangeVariantsTable, SWT.NONE);
            // column.setText("1234");
            // column.pack();
            column.setText(titles[i]);
            column.setToolTipText(toolTips[i]);
            column.pack();
            column.setResizable(true);
        }

        exchangeVariantsTable.pack();
    }

    public Team getTeam() {
        return choosenTeam;
    }

    public void setTeam(Team team) {
        this.choosenTeam = team;
    }

    public List<Player> getListChoosenPlayers() {
        return listChoosenPlayers;
    }

    public void setListChoosenPlayers(List<Player> list) {
        this.listChoosenPlayers = list;
    }

    public All getAll() {
        return all;
    }

    public void setAll(All all) {
        if (this.all != all) {
            this.all = all;
            //            listAllPlayers = new ArrayList<Player>();
            //            for (Team t : all.getTeams()) {
            //                listAllPlayers.addAll(t.getPlayers());
            //            }
            int selectionIndex = teamCombo.getSelectionIndex();
            String prevTeam = selectionIndex == -1 ? null : teamCombo.getItem(selectionIndex);

            fillTeamCombo();

            if (prevTeam != null) {
                int teamIndex = GuiUtils.getComboItemIndex(teamCombo, prevTeam);
                if (teamIndex != -1) {
                    teamCombo.select(teamIndex);
                }
            } else {
                if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getCurrentTeam() != null) {
                    Team t = MainWindow.getAllInstance().getCurrentTeam();
                    GuiUtils.selectComboItem(teamCombo, t.getName());
                } else {
                    teamCombo.select(0);
                }
            }

            onTeamChanged();
        }
    }

    private void fillTeamCombo() {

        teamCombo.removeAll();

        if (all == null || all.getTeams() == null) {
            return;
        }

        for (Team t : all.getTeams()) {
            teamCombo.add(t.getName());
            teamCombo.setData(t.getName(), t);
        }

    }

    public void fillExchangeVariantsTable() {
        exchangeVariantsTable.removeAll();
        int counter = 0;
        Color clr = Display.getDefault().getSystemColor(SWT.COLOR_GRAY);
        for (PlayersExchangeVariant pev : listExchanges) {
            TableItem ti = new TableItem(exchangeVariantsTable, SWT.NONE);
            if (counter++ % 2 == 0) {
                ti.setBackground(clr);
            }
            Player p1 = pev.getPlayer1();
            Player p2 = pev.getPlayer2();

            ti.setText(0, String.valueOf(p1.getNumber()));
            ti.setText(1, String.valueOf(p1.getName()));

            ti.setText(2, String.valueOf(p2.getNumber()));
            ti.setText(3, String.valueOf(p2.getPosition().toString()));
            ti.setText(4, String.valueOf(p2.getName()));
            ti.setText(5, String.valueOf(p2.getPrice()));
            ti.setText(6, String.valueOf(p2.getHealth()));
            ti.setText(7, String.valueOf(p2.getAge()));
            ti.setText(8, String.valueOf(p2.getTalent()));
            ti.setText(9, String.valueOf(p2.getStrength()));
            ti.setText(10, String.valueOf(GameUtils.getPlayerAbilitiesAsString(p2)));
            ti.setText(11, String.valueOf(p2.getSalary()));
            int diff = p2.getPrice() - p1.getPrice();
            double diffPercent = 100.0 * (p2.getPrice() - p1.getPrice()) / (double) Math.min(p1.getPrice(), p2.getPrice());
            nf.setMaximumFractionDigits(1);
            ti.setText(12, String.valueOf(diff + " ( " + nf.format(diffPercent) + "% )"));
            Team t2 = pev.getTeam2();
            ti.setText(13, String.valueOf(t2.getName()));
            ti.setText(14, String.valueOf(Player.getNationalityNameByCode(p2.getNationalityCode())));
            ti.setText(15, String.valueOf(t2.getManagerName()));
            ti.setText(16, String.valueOf(t2.getEmail()));

            //            ti.setText(0, String.valueOf(p1.getNumber()));
            //            ti.setText(1, String.valueOf(p1.getName()));
            //            ti.setText(2, String.valueOf(p1.getAge()));
            //            ti.setText(3, String.valueOf(p1.getTalent()));
            //            ti.setText(4, String.valueOf(p1.getStrength()));
            //            ti.setText(5, String.valueOf(GameUtils.getPlayerAbilitiesAsString(p1)));
            //            ti.setText(6, String.valueOf(p1.getSalary()));
            //
            //            ti.setText(7, String.valueOf(p2.getNumber()));
            //            ti.setText(8, String.valueOf(p2.getName()));
            //            ti.setText(9, String.valueOf(p2.getAge()));
            //            ti.setText(10, String.valueOf(p2.getTalent()));
            //            ti.setText(11, String.valueOf(p2.getStrength()));
            //            ti.setText(12, String.valueOf(GameUtils.getPlayerAbilitiesAsString(p2)));
            //            ti.setText(13, String.valueOf(p2.getSalary()));
            //            int diff = p2.getPrice() - p1.getPrice();
            //            double diffPercent = 100.0 * (p2.getPrice() - p1.getPrice()) / (double) Math.min(p1.getPrice(), p2.getPrice());
            //            nf.setMaximumFractionDigits(1);
            //            ti.setText(14, String.valueOf(diff + " ( " + nf.format(diffPercent) + "% )"));
            //            Team t2 = pev.getTeam2();
            //            ti.setText(15, String.valueOf(t2.getName()));
            //            ti.setText(16, String.valueOf(Player.getNationalityNameByCode(p2.getNationalityCode())));
            //            ti.setText(17, String.valueOf(t2.getManagerName()));
            //            ti.setText(18, String.valueOf(t2.getEmail()));
        }

        exchangeVariantsTable.setRedraw(false);
        for (int i = 0; i < exchangeVariantsTable.getColumnCount(); i++) {
            exchangeVariantsTable.getColumn(i).pack();
        }
        exchangeVariantsTable.setRedraw(true);
    }

    private void fillPlayersTable() {
        playersTable.removeAll();
        int selTeamIndex = teamCombo.getSelectionIndex();
        if (selTeamIndex != -1) {
            String teamName = teamCombo.getItem(selTeamIndex);
            Team team = (Team) teamCombo.getData(teamName);
            if (team != null) {
                for (Player p : team.getPlayers()) {
                    TableItem ti = new TableItem(playersTable, SWT.NONE);
                    ti.setText(0, p.getName());
                    ti.setChecked(true);
                    ti.setData(p);
                }
            }
        }
    }

    private void recalcExchangeVariants() {
        Team team = null;
        int selTeamIndex = teamCombo.getSelectionIndex();
        if (selTeamIndex != -1) {
            String key = teamCombo.getItem(selTeamIndex);
            team = (Team) teamCombo.getData(key);
        }
        listExchanges = new ArrayList<PlayersExchangeVariant>();
        if (team == null) {
            return;
        }
        for (TableItem ti : playersTable.getItems()) {
            if (ti.getChecked()) {
                Player p = (Player) ti.getData();
                for (Team t2 : all.getTeams()) {
                    if (t2 == team) {
                        continue;
                    }
                    for (Player p2 : t2.getPlayers()) {
                        if (p != p2 && isPlayerFiltered(t2, p2)) {
                            double a = Math.min(p.getPrice(), p2.getPrice());
                            double b = Math.max(p.getPrice(), p2.getPrice());
                            //5 percents
                            if ((b - a) / a <= 0.05) {

                                PlayersExchangeVariant ev = new PlayersExchangeVariant();
                                ev.setTeam1(team);
                                ev.setPlayer1(p);
                                ev.setTeam2(t2);
                                ev.setPlayer2(p2);
                                if (listExchanges.indexOf(ev) == -1) {
                                    listExchanges.add(ev);
                                }

                            }

                        }
                    }
                }
            }
        }
    }

    private boolean isPlayerFiltered(Team team, Player player) {
        return transferPlayerFilter.validate(team, player);
    }

    public void updateView() {
    	scrollPanel.setContent(mainComposite);
    	scrollPanel.setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
    	scrollPanel.update();
    }

    public TabItem asTabItem() {
        return mainItem;
    }
    
    private String getRes(String key) {
        return MainWindow.getMessage(key);
    }

    public class FilterSpinnerSelectionListener extends SelectionAdapter {

        private FilterSpinner filterSpinner;
        private int spinnerNumber;

        public FilterSpinnerSelectionListener(FilterSpinner filterSpinner, int spinnerNumber) {
            this.filterSpinner = filterSpinner;
            this.spinnerNumber = spinnerNumber;
        }

        @Override
        public void widgetSelected(SelectionEvent e) {
            if (filterReseting) {
                return;
            }
            switch (filterSpinner) {
                case AGE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setAge1(ageSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setAge2(ageSpinner2.getSelection());
                    }
                    break;

                case EXPERIANCE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setExperiance1(expSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setExperiance2(expSpinner2.getSelection());
                    }
                    break;

                case HEALTH:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setHealth1(healthSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setHealth2(healthSpinner2.getSelection());
                    }
                    break;

                case PRICE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setPrice1(priceSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setPrice2(priceSpinner2.getSelection());
                    }
                    break;

                case STRENGTH:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setStrength1(strengthSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setStrength2(strengthSpinner2.getSelection());
                    }
                    break;

                case TALENT:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setTalent1(talentSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setTalent2(talentSpinner2.getSelection());
                    }
                    break;
                case TICKETS:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setTickets1(ticketsSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setTickets2(ticketsSpinner2.getSelection());
                    }
                    break;
                case WAGE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setWage1(wageSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setWage2(wageSpinner2.getSelection());
                    }
                    break;
                default:
                    break;
            }

        }

    }

}