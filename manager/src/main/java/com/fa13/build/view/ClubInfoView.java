package com.fa13.build.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.ClubReader;
import com.fa13.build.controller.io.ClubWriter;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.model.Club;
import com.fa13.build.model.Club.TriAction;
import com.fa13.build.model.Team;
import com.fa13.build.model.TeamStats;
import com.fa13.build.utils.GuiLongTask;
import com.fa13.build.utils.GuiLongTask.IOnFinish;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;

public class ClubInfoView implements UIItem, IDisposable, IStorable {

    final private TabFolder parentTabFolder;
    private final TabItem mainItem;
    private ScrolledComposite scrollPanel;
    private final Composite mainComposite;
    private Composite managerPanel;
    private Composite clubInfoPanel;
    private Composite clubWeekCostsPanel;
    private Composite teamUniformPanel;
    private Composite clubInfraPanel;
    private Composite clubStaffPanel;
    private Composite clubBuyCostsPanel;
    private Composite buttonsPanel;

    private Button openButton;
    private Button saveButton;

    //private String password;

    private Club club, clubBid;
    //club stores initial club data
    //clubBid stores club bid (opened/to save), calculated when user changes something
    //private Manager manager;
    private Label lManager;
    private Label lManagerData;
    private Label lManagerCity;
    private Label lManagerCityData;
    private Label lManagerCountry;
    private Label lManagerCountryData;
    private Label lManagerEmail;
    private Label lManagerEmailData;
    //really unused = deprecated
    //private Label lManagerIcq;
    //private Label lManagerIcqData;
    private Label lManagerPlayed;
    private Label lManagerPlayedData;
    private Label lManagerFinances;
    private Label lManagerFinancesData;
    private Label lManagerProfile;
    private Link lManagerProfileData;
    private Label lClubCity;
    private Label lClubCityData;
    private Label lClubCountry;
    private Label lClubCountryData;
    private Label lClubFinances;
    private Label lClubFinancesData;
    private Label lClubCapital;
    private Label lClubCapitalData;
    private Label lClubRating;
    private Label lClubRatingData;
    private Label lClubBoom;
    private Label lClubBoomData;
    private Label lClubAllZip;
    private Link lClubAllZipData;
    private Label lClubProfile;
    private Link lClubProfileData;
    private Label lClubHistory;
    private Link lClubHistoryData;
    private Label lWeekCostsPlayers;
    private Label lWeekCostsPlayersData;
    private Label lWeekCostsCoaches;
    private Label lWeekCostsCoachesData;
    private Label lWeekCostsTotal;
    private Label lWeekCostsTotalData;
    private Label lUniform;
    private Image imgHomeUniform;
    private Image imgAwayUniform;
    private Button bToggleUniformAction;
    private Label lStadiumName;
    private Label lStadiumNameData;
    private Label lStadiumCapacity;
    private Spinner sStadiumCapacityData;
    private Label lStadiumDestruction;
    private Label lStadiumDestructionData;
    private Button bToggleStadiumAction;
    private Label lSportbase;
    private Spinner sSportbaseData;
    private Label lSportbaseDestruction;
    private Label lSportbaseDestructionData;
    private Button bToggleSportbaseAction;
    private Label lSportSchool;
    private Label lSportSchoolData;
    private Label lSportSchoolDestruction;
    private Label lSportSchoolDestructionData;
    private Button bToggleSportSchoolAction;
    private Label lMainCoach;
    private Spinner sMainCoachData;
    private Label lMainCoachCostsData;
    private Button bToggleMainCoachAction;
    private Label lGkCoach;
    private Spinner sGkCoachData;
    private Label lGkCoachCostsData;
    private Button bToggleGkCoachAction;
    private Label lDefCoach;
    private Spinner sDefCoachData;
    private Label lDefCoachCostsData;
    private Button bToggleDefCoachAction;
    private Label lMfCoach;
    private Spinner sMfCoachData;
    private Label lMfCoachCostsData;
    private Button bToggleMfCoachAction;
    private Label lFwCoach;
    private Spinner sFwCoachData;
    private Label lFwCoachCostsData;
    private Button bToggleFwCoachAction;
    private Label lFitnessCoach;
    private Spinner sFitnessCoachData;
    private Label lFitnessCoachCostsData;
    private Button bToggleFitnessCoachAction;
    private Label lMoraleCoach;
    private Spinner sMoraleCoachData;
    private Label lMoraleCoachCostsData;
    private Button bToggleMoraleCoachAction;
    private Label lScoutCoach;
    private Spinner sScoutData;
    private Label lScoutCostsData;
    private Button bToggleScoutAction;
    private Label lDoctorCoach;
    private Spinner sDoctorPlayers;
    private Spinner sDoctorLevel;
    private Label lDoctorData;
    private Button bToggleDoctorAction;
    private Label lTotalCosts;
    private Label lTotalCostsData;
    private Label lBalanceChange;
    private Label lBalanceChangeData;
    private ImageData imgDataUniformOriginal;
    private Label lHomeUnifornImgContainer;
    private Label lAwayUnifornImgContainer;

    private Image imgClubLogo;
    private Image imgClub;

    static Map<Club.TriAction, String> mapTriActionToString = new TreeMap<Club.TriAction, String>();

    public static RGB[] rgbColors = new RGB[47];
    private static RGB UNIFORM_TOP_ORIGINAL_COLOR;
    private static RGB UNIFORM_BOTTOM_ORIGINAL_COLOR;
    static {
        //colors init

        rgbColors[0] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("80", 16), Integer.parseInt("80", 16));//1
        rgbColors[1] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("00", 16), Integer.parseInt("00", 16));//2 RED
        rgbColors[2] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("40", 16), Integer.parseInt("40", 16));//3
        rgbColors[3] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("00", 16), Integer.parseInt("00", 16));//4
        rgbColors[4] = new RGB(Integer.parseInt("40", 16), Integer.parseInt("00", 16), Integer.parseInt("00", 16));//5
        rgbColors[5] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("00", 16), Integer.parseInt("00", 16));//6
        rgbColors[6] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("FF", 16), Integer.parseInt("80", 16));//7
        rgbColors[7] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("FF", 16), Integer.parseInt("00", 16));//8
        rgbColors[8] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("80", 16), Integer.parseInt("40", 16));//9
        rgbColors[9] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("80", 16), Integer.parseInt("00", 16));//10
        rgbColors[10] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("40", 16), Integer.parseInt("00", 16));//11
        rgbColors[11] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("80", 16), Integer.parseInt("00", 16));//12
        rgbColors[12] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("FF", 16), Integer.parseInt("80", 16));//13
        rgbColors[13] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("FF", 16), Integer.parseInt("00", 16));//14
        rgbColors[14] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("FF", 16), Integer.parseInt("00", 16));//15
        rgbColors[15] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("80", 16), Integer.parseInt("00", 16));//16
        rgbColors[16] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("40", 16), Integer.parseInt("00", 16));//17
        rgbColors[17] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("80", 16), Integer.parseInt("40", 16));//18
        rgbColors[18] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("FF", 16), Integer.parseInt("80", 16));//19
        rgbColors[19] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("FF", 16), Integer.parseInt("40", 16));//20
        rgbColors[20] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("80", 16), Integer.parseInt("80", 16));//21
        rgbColors[21] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("80", 16), Integer.parseInt("40", 16));//22
        rgbColors[22] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("40", 16), Integer.parseInt("40", 16));//23
        rgbColors[23] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("80", 16), Integer.parseInt("80", 16));//24
        rgbColors[24] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("FF", 16), Integer.parseInt("FF", 16));//25
        rgbColors[25] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("FF", 16), Integer.parseInt("FF", 16));//26
        rgbColors[26] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("40", 16), Integer.parseInt("80", 16));//27
        rgbColors[27] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("00", 16), Integer.parseInt("FF", 16));//28 blue
        rgbColors[28] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("00", 16), Integer.parseInt("80", 16));//29
        rgbColors[29] = new RGB(Integer.parseInt("40", 16), Integer.parseInt("80", 16), Integer.parseInt("80", 16));//30
        rgbColors[30] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("80", 16), Integer.parseInt("FF", 16));//31
        rgbColors[31] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("80", 16), Integer.parseInt("C0", 16));//32
        rgbColors[32] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("80", 16), Integer.parseInt("FF", 16));//33
        rgbColors[33] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("00", 16), Integer.parseInt("A0", 16));//34
        rgbColors[34] = new RGB(Integer.parseInt("00", 16), Integer.parseInt("00", 16), Integer.parseInt("36", 16));//35
        rgbColors[35] = new RGB(Integer.parseInt("C0", 16), Integer.parseInt("C0", 16), Integer.parseInt("C0", 16));//36
        rgbColors[36] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("80", 16), Integer.parseInt("C0", 16));//37
        rgbColors[37] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("80", 16), Integer.parseInt("C0", 16));//38
        rgbColors[38] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("00", 16), Integer.parseInt("40", 16));//39
        rgbColors[39] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("00", 16), Integer.parseInt("80", 16));//40
        rgbColors[40] = new RGB(Integer.parseInt("40", 16), Integer.parseInt("00", 16), Integer.parseInt("40", 16));//41
        rgbColors[41] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("80", 16), Integer.parseInt("FF", 16));//42
        rgbColors[42] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("00", 16), Integer.parseInt("FF", 16));//43
        rgbColors[43] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("00", 16), Integer.parseInt("80", 16));//44
        rgbColors[44] = new RGB(Integer.parseInt("80", 16), Integer.parseInt("00", 16), Integer.parseInt("FF", 16));//45
        rgbColors[45] = new RGB(Integer.parseInt("40", 16), Integer.parseInt("00", 16), Integer.parseInt("80", 16));//46
        rgbColors[46] = new RGB(Integer.parseInt("FF", 16), Integer.parseInt("FF", 16), Integer.parseInt("FF", 16));//47

        UNIFORM_TOP_ORIGINAL_COLOR = rgbColors[27];
        UNIFORM_BOTTOM_ORIGINAL_COLOR = rgbColors[1];
    }

    //private Club.TriAction uniformAction,stadiumAction,sportBaseAction;

    private final Map<Object, String> textControls = new HashMap<Object, String>();
    private Label lClubLogoContainer;

    private final String CLUB_LOGO_DIRECTORY = "clubLogo";
    private final String CLUB_LOGO_IMAGE_EXTENSION = "png";
    private final String CLUB_LOGO_DOWNLOAD_URL = "https://repository.fa13.info/site/team/";
    private Image imgBlankClubLogo;

    public ClubInfoView(TabFolder parent) {
        parentTabFolder = parent;
        loadImages();
        mainItem = new TabItem(parentTabFolder, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("clubTabName"));
        mainItem.setImage(imgClub);
        textControls.put(mainItem, "clubTabName");
        scrollPanel = new ScrolledComposite(parentTabFolder, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        scrollPanel.setExpandHorizontal(true);
        scrollPanel.setExpandVertical(true);

        mainComposite = new Composite(scrollPanel, SWT.NONE);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(new FormLayout());
        mainItem.setControl(scrollPanel);
        scrollPanel.setContent(mainComposite);
        //**** managerPanel *************************************
        managerPanel = new Composite(mainComposite, SWT.NONE);
        managerPanel.setLayout(new FillLayout());

        FormData data;
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(50, 0);
        data.top = new FormAttachment(0, 0);
        managerPanel.setLayoutData(data);
        createManagerPanelContent();
        managerPanel.setSize(managerPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** clubInfoPanel *************************************
        clubInfoPanel = new Composite(mainComposite, SWT.NONE);
        clubInfoPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(50, 0);
        data.top = new FormAttachment(managerPanel, 0);

        clubInfoPanel.setLayoutData(data);
        createClubInfoPanelContent();
        clubInfoPanel.setSize(clubInfoPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** clubCostsPanel *************************************
        clubWeekCostsPanel = new Composite(mainComposite, SWT.NONE);
        clubWeekCostsPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(50, 0);
        data.top = new FormAttachment(clubInfoPanel, 0);

        clubWeekCostsPanel.setLayoutData(data);
        createWeekCostsPanelContent();
        clubWeekCostsPanel.setSize(clubWeekCostsPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** teamUniformPanel ************************************* 
        teamUniformPanel = new Composite(mainComposite, SWT.NONE);
        teamUniformPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(50, 0);
        data.top = new FormAttachment(clubWeekCostsPanel, 0);

        teamUniformPanel.setLayoutData(data);
        createTeamUniformPanelContents();
        teamUniformPanel.setSize(teamUniformPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** clubInfraPanel *************************************
        clubInfraPanel = new Composite(mainComposite, SWT.NONE);
        clubInfraPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(50, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);

        clubInfraPanel.setLayoutData(data);
        createClubInfraPanelContent();
        clubInfraPanel.setSize(clubInfraPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** clubStaffPanel *************************************
        clubStaffPanel = new Composite(mainComposite, SWT.NONE);
        clubStaffPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(50, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(clubInfraPanel, 0);

        clubStaffPanel.setLayoutData(data);
        createClubStaffPanelContent();
        clubStaffPanel.setSize(clubStaffPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** clubBuyCostsPanel *************************************
        clubBuyCostsPanel = new Composite(mainComposite, SWT.NONE);
        clubBuyCostsPanel.setLayout(new FillLayout());

        data = new FormData();
        data.left = new FormAttachment(50, 0);
        data.right = new FormAttachment(90, 0);
        data.top = new FormAttachment(clubStaffPanel, 0);

        clubBuyCostsPanel.setLayoutData(data);
        createClubBuyCostsPanelContent();
        clubBuyCostsPanel.setSize(clubBuyCostsPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        //**** buttonsPanel *************************************
        buttonsPanel = new Composite(mainComposite, SWT.NONE);
        buttonsPanel.setLayout(new GridLayout(1, false));

        data = new FormData();
        data.left = new FormAttachment(clubBuyCostsPanel, 10);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(clubStaffPanel, 10);
        data.bottom = new FormAttachment(100, 0);

        buttonsPanel.setLayoutData(data);

        openButton = new Button(buttonsPanel, SWT.PUSH);
        openButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openButton.setText(MainWindow.getMessage("global.open"));
        openButton.setImage(MainWindow.getSharedImage(ImageType.OPEN));
        textControls.put(openButton, "global.open");

        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        openButton.setLayoutData(gridData);

        openButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                openClubBidDlg();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        saveButton = new Button(buttonsPanel, SWT.PUSH);
        saveButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saveButton.setText(MainWindow.getMessage("global.save"));
        saveButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        textControls.put(saveButton, "global.save");

        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.minimumWidth = 80;
        saveButton.setLayoutData(gridData);

        saveButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                saveClubBidDlg();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        updateMessages();
        mainComposite.setRedraw(true);

        clubBid = new Club();
        club = new Club();

        scrollPanel.setMinSize(mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        saveButton.setVisible(false);
    }

    private void loadImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/BlankClubLogo.gif"));
        imgBlankClubLogo = new Image(parentTabFolder.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/club_16x16.png"));
        imgClub = new Image(parentTabFolder.getDisplay(), imgData);
    }

    private Club.TriAction nextTriAction(Club.TriAction triAction, boolean useRepair) {
        if (triAction.equals(Club.TriAction.TR_NOTHING)) {
            return Club.TriAction.TR_BUILD;
        } else if (triAction.equals(Club.TriAction.TR_BUILD)) {
            return useRepair ? Club.TriAction.TR_REPAIR : Club.TriAction.TR_NOTHING;
        } else if (triAction.equals(Club.TriAction.TR_REPAIR)) {
            return Club.TriAction.TR_NOTHING;
        } else {
            return Club.TriAction.TR_NOTHING;
        }
    }

    private void createClubBuyCostsPanelContent() {
        Group group = new Group(clubBuyCostsPanel, SWT.DEFAULT);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(2, false));
        group.setText(MainWindow.getMessage("club.buyCosts"));
        textControls.put(group, "club.buyCosts");

        //********************* totalCosts
        lTotalCosts = new Label(group, SWT.NONE);
        lTotalCosts.setText(MainWindow.getMessage("club.totalCosts"));
        textControls.put(lTotalCosts, "club.totalCosts");

        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lTotalCosts.setLayoutData(gridData);
        lTotalCostsData = new Label(group, SWT.NONE);
        lTotalCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lTotalCostsData.setLayoutData(gridData);

        //********************* balanceChange
        lBalanceChange = new Label(group, SWT.NONE);
        lBalanceChange.setText(MainWindow.getMessage("club.balanceChanges"));
        textControls.put(lBalanceChange, "club.balanceChanges");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lBalanceChange.setLayoutData(gridData);

        lBalanceChangeData = new Label(group, SWT.NONE);
        lBalanceChangeData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lBalanceChangeData.setLayoutData(gridData);

        //set bold+italic font
        FontData[] fontData = lTotalCosts.getFont().getFontData();
        fontData[0].setHeight(10);
        fontData[0].setStyle(SWT.BOLD + SWT.ITALIC);
        Font font = new Font(parentTabFolder.getDisplay(), fontData[0]);
        lTotalCostsData.setFont(font);
        lBalanceChangeData.setFont(font);
    }

    private void createClubStaffPanelContent() {

        final NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);

        Group group = new Group(clubStaffPanel, SWT.NONE);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(4, false));
        group.setText(MainWindow.getMessage("club.staff"));
        textControls.put(group, "club.staff");

        //********************* mainCoach
        lMainCoach = new Label(group, SWT.NONE);
        lMainCoach.setText(MainWindow.getMessage("club.mainCoachLevel"));
        textControls.put(lMainCoach, "club.mainCoachLevel");

        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lMainCoach.setLayoutData(gridData);

        sMainCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sMainCoachData.setLayoutData(gridData);

        sMainCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sMainCoachData.getSelection();
                clubBid.setCoachCoef(value);
                boolean changed = (value != club.getCoachCoef()) && bToggleMainCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachCoef(changed);
                clubBid.setCoachCoefDiff(value - club.getCoachCoef());
                lMainCoachCostsData.setText(nf.format(32000 + (int) Math.round(1600 * value)));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lMainCoachCostsData = new Label(group, SWT.NONE);
        lMainCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lMainCoachCostsData.setLayoutData(gridData);

        bToggleMainCoachAction = new Button(group, SWT.PUSH);
        bToggleMainCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        //bToggleMainCoachAction.setLayoutData(gridData);
        //textControls.put(bToggleMainCoachAction, "club.action.doNothing");

        bToggleMainCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleMainCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleMainCoachAction.getData(), false);
                boolean changed = (clubBid.getCoachCoef() != club.getCoachCoef()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachCoef(changed);
                bToggleMainCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleMainCoachAction.setSize(bToggleMainCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleMainCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* gkCoach
        lGkCoach = new Label(group, SWT.NONE);
        lGkCoach.setText(MainWindow.getMessage("club.gkCoachLevel"));
        textControls.put(lGkCoach, "club.gkCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lGkCoach.setLayoutData(gridData);

        sGkCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sGkCoachData.setLayoutData(gridData);

        sGkCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sGkCoachData.getSelection();
                clubBid.setCoachGK(value);
                boolean changed = (value != club.getCoachGK()) && bToggleGkCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachGK(changed);
                lGkCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lGkCoachCostsData = new Label(group, SWT.NONE);
        lGkCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lGkCoachCostsData.setLayoutData(gridData);

        bToggleGkCoachAction = new Button(group, SWT.PUSH);
        bToggleGkCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleGkCoachAction, "club.action.doNothing");

        bToggleGkCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleGkCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleGkCoachAction.getData(), false);
                boolean changed = (sGkCoachData.getSelection() != club.getCoachGK()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachGK(changed);
                bToggleGkCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleGkCoachAction.setSize(bToggleGkCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleGkCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* defCoach
        lDefCoach = new Label(group, SWT.NONE);
        lDefCoach.setText(MainWindow.getMessage("club.dfCoachLevel"));
        textControls.put(lDefCoach, "club.dfCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lDefCoach.setLayoutData(gridData);

        sDefCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sDefCoachData.setLayoutData(gridData);
        sDefCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sDefCoachData.getSelection();
                clubBid.setCoachDef(value);
                boolean changed = (value != club.getCoachDef()) && bToggleDefCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachDef(changed);
                lDefCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lDefCoachCostsData = new Label(group, SWT.NONE);
        lDefCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lDefCoachCostsData.setLayoutData(gridData);

        bToggleDefCoachAction = new Button(group, SWT.PUSH);
        bToggleDefCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleDefCoachAction, "club.action.doNothing");

        bToggleDefCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleDefCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleDefCoachAction.getData(), false);
                boolean changed = (sDefCoachData.getSelection() != club.getCoachDef()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachDef(changed);
                bToggleDefCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleDefCoachAction.setSize(bToggleDefCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleDefCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* mfCoach
        lMfCoach = new Label(group, SWT.NONE);
        lMfCoach.setText(MainWindow.getMessage("club.mfCoachLevel"));
        textControls.put(lMfCoach, "club.mfCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lMfCoach.setLayoutData(gridData);

        sMfCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sMfCoachData.setLayoutData(gridData);
        sMfCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sMfCoachData.getSelection();
                clubBid.setCoachMid(value);
                boolean changed = (value != club.getCoachMid()) && bToggleMfCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachMid(changed);
                lMfCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lMfCoachCostsData = new Label(group, SWT.NONE);
        lMfCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lMfCoachCostsData.setLayoutData(gridData);

        bToggleMfCoachAction = new Button(group, SWT.PUSH);
        bToggleMfCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleMfCoachAction, "club.action.doNothing");

        bToggleMfCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleMfCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleMfCoachAction.getData(), false);
                boolean changed = (sMfCoachData.getSelection() != club.getCoachMid()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachMid(changed);
                bToggleMfCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleMfCoachAction.setSize(bToggleMfCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleMfCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* fwCoach
        lFwCoach = new Label(group, SWT.NONE);
        lFwCoach.setText(MainWindow.getMessage("club.fwCoachLevel"));
        textControls.put(lFwCoach, "club.fwCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lFwCoach.setLayoutData(gridData);

        sFwCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sFwCoachData.setLayoutData(gridData);
        sFwCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sFwCoachData.getSelection();
                clubBid.setCoachFw(value);
                boolean changed = (value != club.getCoachFw()) && bToggleFwCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachFw(changed);
                lFwCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lFwCoachCostsData = new Label(group, SWT.NONE);
        lFwCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lFwCoachCostsData.setLayoutData(gridData);

        bToggleFwCoachAction = new Button(group, SWT.PUSH);
        bToggleFwCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleFwCoachAction, "club.action.doNothing");

        bToggleFwCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleFwCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleFwCoachAction.getData(), false);
                boolean changed = (sFwCoachData.getSelection() != club.getCoachFw()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachFw(changed);
                bToggleFwCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleFwCoachAction.setSize(bToggleFwCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleFwCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* physicsCoach
        lFitnessCoach = new Label(group, SWT.NONE);
        lFitnessCoach.setText(MainWindow.getMessage("club.physicsCoachLevel"));
        textControls.put(lFitnessCoach, "club.physicsCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lFitnessCoach.setLayoutData(gridData);

        sFitnessCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sFitnessCoachData.setLayoutData(gridData);
        sFitnessCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sFitnessCoachData.getSelection();
                clubBid.setCoachFitness(value);
                boolean changed = (value != club.getCoachFitness()) && bToggleFitnessCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachFitness(changed);
                lFitnessCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lFitnessCoachCostsData = new Label(group, SWT.NONE);
        lFitnessCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lFitnessCoachCostsData.setLayoutData(gridData);

        bToggleFitnessCoachAction = new Button(group, SWT.PUSH);
        bToggleFitnessCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleFitnessCoachAction, "club.action.doNothing");

        bToggleFitnessCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleFitnessCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleFitnessCoachAction.getData(), false);
                boolean changed = (sFitnessCoachData.getSelection() != club.getCoachFitness()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachFitness(changed);
                bToggleFitnessCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleFitnessCoachAction.setSize(bToggleFitnessCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleFitnessCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* moraleCoach
        lMoraleCoach = new Label(group, SWT.NONE);
        lMoraleCoach.setText(MainWindow.getMessage("club.psychoCoachLevel"));
        textControls.put(lMoraleCoach, "club.psychoCoachLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lMoraleCoach.setLayoutData(gridData);

        sMoraleCoachData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sMoraleCoachData.setLayoutData(gridData);
        sMoraleCoachData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sMoraleCoachData.getSelection();
                clubBid.setCoachMorale(value);
                boolean changed = (value != club.getCoachMorale()) && bToggleMoraleCoachAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeCoachMorale(changed);
                lMoraleCoachCostsData.setText(nf.format(8000 * value));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lMoraleCoachCostsData = new Label(group, SWT.NONE);
        lMoraleCoachCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lMoraleCoachCostsData.setLayoutData(gridData);

        bToggleMoraleCoachAction = new Button(group, SWT.PUSH);
        bToggleMoraleCoachAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleMoraleCoachAction, "club.action.doNothing");

        bToggleMoraleCoachAction.setData(Club.TriAction.TR_NOTHING);
        bToggleMoraleCoachAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleMoraleCoachAction.getData(), false);
                boolean changed = (sMoraleCoachData.getSelection() != club.getCoachMorale()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeCoachMorale(changed);
                bToggleMoraleCoachAction.setText(mapTriActionToString.get(newAction));
                bToggleMoraleCoachAction.setSize(bToggleMoraleCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleMoraleCoachAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* doctor
        lDoctorCoach = new Label(group, SWT.NONE);
        lDoctorCoach.setText(MainWindow.getMessage("club.doctorLevel"));
        textControls.put(lDoctorCoach, "club.doctorLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lDoctorCoach.setLayoutData(gridData);

        Composite doctorPanel = new Composite(group, SWT.NONE);
        doctorPanel.setLayout(new FillLayout());
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        doctorPanel.setLayoutData(gridData);

        sDoctorLevel = new Spinner(doctorPanel, SWT.BORDER);
        sDoctorPlayers = new Spinner(doctorPanel, SWT.BORDER);
        sDoctorPlayers.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                clubBid.setDoctorCount(sDoctorPlayers.getSelection());
                int value = sDoctorPlayers.getSelection();
                boolean changed = (value != club.getDoctorCount()) && bToggleDoctorAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeDoctor(changed);
                lDoctorData.setText(nf.format(8000 * (clubBid.getDoctorCount() + clubBid.getDoctorLevel())));
                recalcBuyCosts();

            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        sDoctorLevel.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                clubBid.setDoctorLevel(sDoctorLevel.getSelection());
                int value = sDoctorLevel.getSelection();
                boolean changed = (value != club.getDoctorLevel()) && bToggleDoctorAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeDoctor(changed);
                lDoctorData.setText(nf.format(8000 * (clubBid.getDoctorCount() + clubBid.getDoctorLevel())));
                recalcBuyCosts();

            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lDoctorData = new Label(group, SWT.NONE);
        lDoctorData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lDoctorData.setLayoutData(gridData);

        bToggleDoctorAction = new Button(group, SWT.PUSH);
        bToggleDoctorAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleDoctorAction, "club.action.doNothing");

        bToggleDoctorAction.setData(Club.TriAction.TR_NOTHING);
        bToggleDoctorAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleDoctorAction.getData(), false);
                boolean changed = (sDoctorLevel.getSelection() != club.getDoctorLevel() || sDoctorPlayers.getSelection() != club.getDoctorCount())
                        && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeDoctor(changed);
                bToggleDoctorAction.setText(mapTriActionToString.get(newAction));
                bToggleDoctorAction.setSize(bToggleDoctorAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleDoctorAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* scout
        lScoutCoach = new Label(group, SWT.NONE);
        lScoutCoach.setText(MainWindow.getMessage("club.scoutLevel"));
        textControls.put(lScoutCoach, "club.scoutLevel");

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lScoutCoach.setLayoutData(gridData);

        sScoutData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sScoutData.setLayoutData(gridData);
        sScoutData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sScoutData.getSelection();
                clubBid.setScoutLevel(value);
                boolean changed = (value != club.getScoutLevel()) && bToggleScoutAction.getData() == TriAction.TR_BUILD;
                clubBid.setChangeScout(changed);
                lScoutCostsData.setText(nf.format(24000 * (value)));
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lScoutCostsData = new Label(group, SWT.NONE);
        lScoutCostsData.setText("Value");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lScoutCostsData.setLayoutData(gridData);

        bToggleScoutAction = new Button(group, SWT.PUSH);
        bToggleScoutAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleScoutAction, "club.action.doNothing");

        bToggleScoutAction.setData(Club.TriAction.TR_NOTHING);
        bToggleScoutAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                if (resetUiClubActionsInProgress) {
                    return;
                }
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleScoutAction.getData(), false);
                boolean changed = (sScoutData.getSelection() != club.getScoutLevel()) && (newAction == TriAction.TR_BUILD);
                clubBid.setChangeScout(changed);
                bToggleScoutAction.setText(mapTriActionToString.get(newAction));
                bToggleScoutAction.setSize(bToggleScoutAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleScoutAction.setData(newAction);
                recalcBuyCosts();
            }
        });

    }

    private void createClubInfraPanelContent() {

        Group group = new Group(clubInfraPanel, SWT.NONE);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(5, false));
        group.setText(MainWindow.getMessage("club.infrastructure"));
        textControls.put(group, "club.infrastructure");

        lStadiumName = new Label(group, SWT.NONE);
        lStadiumName.setText(MainWindow.getMessage("club.stadiumName"));
        textControls.put(lStadiumName, "club.stadiumName");
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lStadiumName.setLayoutData(gridData);

        lStadiumNameData = new Label(group, SWT.NONE);
        lStadiumNameData.setText("Club stadium name data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.horizontalSpan = 4;
        lStadiumNameData.setLayoutData(gridData);

        lStadiumCapacity = new Label(group, SWT.NONE);
        lStadiumCapacity.setText(MainWindow.getMessage("club.stadiumCapacity"));
        textControls.put(lStadiumCapacity, "club.stadiumCapacity");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lStadiumCapacity.setLayoutData(gridData);

        sStadiumCapacityData = new Spinner(group, SWT.BORDER);
        //trick to correctly calculate space - overwise widget width is not set correctly
        sStadiumCapacityData.setValues(199999, 0, 200000, 0, 1000, 3000);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sStadiumCapacityData.setLayoutData(gridData);

        sStadiumCapacityData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int value = sStadiumCapacityData.getSelection();
                clubBid.setStadium(value);
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lStadiumDestruction = new Label(group, SWT.NONE);
        lStadiumDestruction.setText(MainWindow.getMessage("club.destruction"));
        textControls.put(lStadiumDestruction, "club.destruction");
        gridData = new GridData(SWT.END, SWT.CENTER, true, false);
        lStadiumDestruction.setLayoutData(gridData);

        lStadiumDestructionData = new Label(group, SWT.NONE);
        lStadiumDestructionData.setText("Value");
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        lStadiumDestructionData.setLayoutData(gridData);

        bToggleStadiumAction = new Button(group, SWT.PUSH);
        bToggleStadiumAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleStadiumAction, "club.action.doNothing");
        bToggleStadiumAction.setData(Club.TriAction.TR_NOTHING);
        bToggleStadiumAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleStadiumAction.getData(), true);
                clubBid.setStadiumAction(newAction);
                bToggleStadiumAction.setText(mapTriActionToString.get(newAction));
                bToggleStadiumAction.setSize(bToggleStadiumAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleStadiumAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* sportBase
        lSportbase = new Label(group, SWT.NONE);
        lSportbase.setText(MainWindow.getMessage("club.sportBaseLevel"));
        textControls.put(lSportbase, "club.sportBaseLevel");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lSportbase.setLayoutData(gridData);

        sSportbaseData = new Spinner(group, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        sSportbaseData.setLayoutData(gridData);
        sSportbaseData.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                clubBid.setSportBase(sSportbaseData.getSelection());
                recalcBuyCosts();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        lSportbaseDestruction = new Label(group, SWT.NONE);
        lSportbaseDestruction.setText(MainWindow.getMessage("club.destruction"));
        textControls.put(lSportbaseDestruction, "club.destruction");
        gridData = new GridData(SWT.END, SWT.CENTER, true, false);
        lSportbaseDestruction.setLayoutData(gridData);

        lSportbaseDestructionData = new Label(group, SWT.NONE);
        lSportbaseDestructionData.setText("Value");
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        lSportbaseDestructionData.setLayoutData(gridData);

        bToggleSportbaseAction = new Button(group, SWT.PUSH);
        bToggleSportbaseAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleSportbaseAction, "club.action.doNothing");
        bToggleSportbaseAction.setData(Club.TriAction.TR_NOTHING);
        bToggleSportbaseAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleSportbaseAction.getData(), club.getSportBase() > 0);
                clubBid.setSportBaseAction(newAction);
                bToggleSportbaseAction.setText(mapTriActionToString.get(newAction));
                bToggleSportbaseAction.setSize(bToggleSportbaseAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleSportbaseAction.setData(newAction);
                recalcBuyCosts();
            }
        });

        //********************* sportSchool
        lSportSchool = new Label(group, SWT.NONE);
        lSportSchool.setText(MainWindow.getMessage("club.sportSchool"));
        textControls.put(lSportSchool, "club.sportSchool");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lSportSchool.setLayoutData(gridData);

        lSportSchoolData = new Label(group, SWT.NONE);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lSportSchoolData.setLayoutData(gridData);

        lSportSchoolDestruction = new Label(group, SWT.NONE);
        lSportSchoolDestruction.setText(MainWindow.getMessage("club.destruction"));
        textControls.put(lSportSchoolDestruction, "club.destruction");
        gridData = new GridData(SWT.END, SWT.CENTER, true, false);
        lSportSchoolDestruction.setLayoutData(gridData);

        lSportSchoolDestructionData = new Label(group, SWT.NONE);
        lSportSchoolDestructionData.setText("Value");
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        lSportSchoolDestructionData.setLayoutData(gridData);

        bToggleSportSchoolAction = new Button(group, SWT.PUSH);
        bToggleSportSchoolAction.setText(MainWindow.getMessage("club.action.doNothing"));
        //textControls.put(bToggleSportSchoolAction, "club.action.doNothing");
        bToggleSportSchoolAction.setData(Club.TriAction.TR_NOTHING);
        bToggleSportSchoolAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                Club.TriAction newAction = nextTriAction((Club.TriAction) bToggleSportSchoolAction.getData(), club.isSchool());
                clubBid.setSportSchoolAction(newAction);
                clubBid.setSchool(newAction.equals(TriAction.TR_BUILD) && !club.isSchool());
                bToggleSportSchoolAction.setText(mapTriActionToString.get(newAction));
                bToggleSportSchoolAction.setSize(bToggleSportSchoolAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                bToggleSportSchoolAction.setData(newAction);
                recalcBuyCosts();
            }
        });

    }

    private void createTeamUniformPanelContents() {
        Group group = new Group(teamUniformPanel, SWT.DEFAULT);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(3, false));
        group.setText(MainWindow.getMessage("club.uniform"));
        textControls.put(group, "club.uniform");

        lUniform = new Label(group, SWT.NONE);
        lUniform.setText(MainWindow.getMessage("club.allUniform"));
        textControls.put(lUniform, "club.allUniform");
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lUniform.setLayoutData(gridData);

        Composite uniformsPanel = new Composite(group, SWT.NONE);
        uniformsPanel.setLayout(new FillLayout());
        lHomeUnifornImgContainer = new Label(uniformsPanel, SWT.NONE);
        InputStream imageStream = getClass().getResourceAsStream("/com/fa13/build/resources/images/uniform.png");
        imgDataUniformOriginal = new ImageData(imageStream);
        imgHomeUniform = new Image(parentTabFolder.getDisplay(), imgDataUniformOriginal);
        lHomeUnifornImgContainer.setImage(imgHomeUniform);
        lHomeUnifornImgContainer.addListener(SWT.MouseDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                handleUniformClick(event, true);
            }
        });

        lAwayUnifornImgContainer = new Label(uniformsPanel, SWT.NONE);
        //InputStream imageStream2 = getClass().getResourceAsStream("/com/fa13/build/resources/images/uniform.png");
        imgAwayUniform = new Image(parentTabFolder.getDisplay(), imgDataUniformOriginal);
        lAwayUnifornImgContainer.setImage(imgAwayUniform);
        lAwayUnifornImgContainer.addListener(SWT.MouseDown, new Listener() {
            @Override
            public void handleEvent(Event event) {
                handleUniformClick(event, false);
            }

        });

        bToggleUniformAction = new Button(group, SWT.TOGGLE);
        bToggleUniformAction.setText(MainWindow.getMessage("club.action.doNothing"));
        bToggleUniformAction.setData(TriAction.TR_NOTHING);
        //textControls.put(bToggleUniformAction, "club.action.doNothing");
        bToggleUniformAction.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                boolean flag = bToggleUniformAction.getSelection();
                clubBid.setChangeUniformColors(!flag);
                if (!flag) {
                    bToggleUniformAction.setText(MainWindow.getMessage("club.action.change"));
                } else {
                    bToggleUniformAction.setText(MainWindow.getMessage("club.action.doNothing"));
                }
                bToggleUniformAction.setSize(bToggleUniformAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
                checkSaveButton();
            }
        });

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        uniformsPanel.setLayoutData(gridData);

    }

    private void handleUniformClick(Event event, boolean isHome) {
        //event.x event.y 
        //imgHomeUniform.getBounds()
        //uniform image height 30
        //uniform top height 17
        //uniform bottom height 13

        boolean up = event.y < 17;
        ColorDialog dlgColor = new ColorDialog(parentTabFolder.getShell());
        //home uniform up=color1,down=color3
        int colorIndex = -1;
        if (up) {
            //System.out.println("choosen home up");
            colorIndex = isHome ? clubBid.getColor1() : clubBid.getColor3();
        } else {
            //System.out.println("choosen home bottom");
            colorIndex = isHome ? clubBid.getColor2() : clubBid.getColor4();
        }
        //choose rgb color according to current color or 
        dlgColor.setRGB(rgbColors[colorIndex - 1]);
        RGB rgbColor = dlgColor.open();
        if (rgbColor != null) {
            //convert rgb to index
            int selectedColorIndex = getColorIndex(rgbColor);
            if (selectedColorIndex != -1) {

                int color1, color2;

                if (isHome) {
                    if (up) {
                        color1 = selectedColorIndex;
                        color2 = clubBid.getColor2() - 1;
                        clubBid.setColor1(selectedColorIndex + 1);
                        clubBid.setChangeColor1(club.getColor1() != clubBid.getColor1());
                    } else {
                        color1 = clubBid.getColor1() - 1;
                        color2 = selectedColorIndex;
                        clubBid.setColor2(selectedColorIndex + 1);
                        clubBid.setChangeColor2(club.getColor2() != clubBid.getColor2());
                    }
                } else {//away
                    if (up) {
                        color1 = selectedColorIndex;
                        color2 = clubBid.getColor4() - 1;
                        clubBid.setColor3(selectedColorIndex + 1);
                        clubBid.setChangeColor3(club.getColor3() != clubBid.getColor3());
                    } else {
                        color1 = clubBid.getColor3() - 1;
                        color2 = selectedColorIndex;
                        clubBid.setColor4(selectedColorIndex + 1);
                        clubBid.setChangeColor4(club.getColor4() != clubBid.getColor4());
                    }
                }

                setUniformColors(isHome, color1, color2);
            } else {
                //System.out.println("colorIndex = -1 !!!");
                GuiUtils.showErrorMessage(parentTabFolder.getShell(), MainWindow.getMessage("error"),
                        MainWindow.getMessage("club.unsupportedUniformColor"));
            }
            checkSaveButton();
        }

    }

    private int getColorIndex(RGB rgb) {
        if (rgb == null) {
            return -1;
        }
        for (int i = 0; i < rgbColors.length; i++) {
            if (rgbColors[i].equals(rgb)) {
                return i;
            }
        }
        return -1;
    }

    private void checkSaveButton() {
        boolean needSave = clubBid.isChangeCoachCoef() || clubBid.isChangeCoachGK() || clubBid.isChangeCoachDef() || clubBid.isChangeCoachMid()
                || clubBid.isChangeCoachFw() || clubBid.isChangeCoachFitness() || clubBid.isChangeCoachMorale() || clubBid.isChangeDoctor()
                || clubBid.isChangeScout()
                || (clubBid.isChangeUniformColors()
                        && (clubBid.isChangeColor1() || clubBid.isChangeColor2() || clubBid.isChangeColor3() || clubBid.isChangeColor4()))
                || clubBid.getStadiumAction().equals(TriAction.TR_REPAIR) || clubBid.getSportBaseAction().equals(TriAction.TR_REPAIR)
                || clubBid.getSportSchoolAction().equals(TriAction.TR_REPAIR)
                || (clubBid.getStadiumAction().equals(TriAction.TR_BUILD) && (club.getStadium() != clubBid.getStadium()))
                || (clubBid.getSportBaseAction().equals(TriAction.TR_BUILD) && (club.getSportBase() != clubBid.getSportBase()))
                || (clubBid.getSportSchoolAction().equals(TriAction.TR_BUILD) && !club.isSchool() && clubBid.isSchool());
        saveButton.setVisible(needSave);
    }

    private void createWeekCostsPanelContent() {

        Group group = new Group(clubWeekCostsPanel, SWT.DEFAULT);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(2, false));
        group.setText(MainWindow.getMessage("club.weekCosts"));
        textControls.put(group, "club.weekCosts");
        group.setLayout(new GridLayout(2, false));

        lWeekCostsPlayers = new Label(group, SWT.NONE);
        lWeekCostsPlayers.setText(MainWindow.getMessage("club.playersWeekCosts"));
        textControls.put(lWeekCostsPlayers, "club.playersWeekCosts");
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lWeekCostsPlayers.setLayoutData(gridData);

        lWeekCostsPlayersData = new Label(group, SWT.NONE);
        lWeekCostsPlayersData.setText("Club player week const data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lWeekCostsPlayersData.setLayoutData(gridData);

        lWeekCostsCoaches = new Label(group, SWT.NONE);
        lWeekCostsCoaches.setText(MainWindow.getMessage("club.coachesWeekCosts"));
        textControls.put(lWeekCostsCoaches, "club.coachesWeekCosts");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lWeekCostsCoaches.setLayoutData(gridData);

        lWeekCostsCoachesData = new Label(group, SWT.NONE);
        lWeekCostsCoachesData.setText("Club coach week const data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lWeekCostsCoachesData.setLayoutData(gridData);

        lWeekCostsTotal = new Label(group, SWT.NONE);
        lWeekCostsTotal.setText(MainWindow.getMessage("club.totalWeekCosts"));
        textControls.put(lWeekCostsTotal, "club.totalWeekCosts");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lWeekCostsTotal.setLayoutData(gridData);

        lWeekCostsTotalData = new Label(group, SWT.NONE);
        lWeekCostsTotalData.setText("Club city data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lWeekCostsTotalData.setLayoutData(gridData);

    }

    private void createClubInfoPanelContent() {
        Group group = new Group(clubInfoPanel, SWT.DEFAULT);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(3, false));
        group.setText(MainWindow.getMessage("club.overallInfo"));
        textControls.put(group, "club.overallInfo");

        lClubCity = new Label(group, SWT.NONE);
        lClubCity.setText(MainWindow.getMessage("club.city"));
        textControls.put(lClubCity, "club.city");
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubCity.setLayoutData(gridData);

        lClubCityData = new Label(group, SWT.NONE);
        lClubCityData.setText("Club city data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubCityData.setLayoutData(gridData);

        lClubLogoContainer = new Label(group, SWT.NONE);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.verticalSpan = 8;
        lClubLogoContainer.setLayoutData(gridData);

        lClubCountry = new Label(group, SWT.NONE);
        lClubCountry.setText(MainWindow.getMessage("club.country"));
        textControls.put(lClubCountry, "club.country");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubCountry.setLayoutData(gridData);

        lClubCountryData = new Label(group, SWT.NONE);
        lClubCountryData.setText("Club country data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubCountryData.setLayoutData(gridData);

        lClubFinances = new Label(group, SWT.NONE);
        lClubFinances.setText(MainWindow.getMessage("club.finances"));
        textControls.put(lClubFinances, "club.finances");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubFinances.setLayoutData(gridData);

        lClubFinancesData = new Label(group, SWT.NONE);
        lClubFinancesData.setText("Club finances data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubFinancesData.setLayoutData(gridData);

        lClubCapital = new Label(group, SWT.NONE);
        lClubCapital.setText(MainWindow.getMessage("club.finances"));
        textControls.put(lClubCapital, "statistics.team.price.capitalization");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubCapital.setLayoutData(gridData);
        
        lClubCapitalData = new Label(group, SWT.NONE);
        lClubCapitalData.setText("Club capital data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubCapitalData.setLayoutData(gridData);

        
        lClubRating = new Label(group, SWT.NONE);
        lClubRating.setText(MainWindow.getMessage("club.rating"));
        textControls.put(lClubRating, "club.rating");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubRating.setLayoutData(gridData);

        lClubRatingData = new Label(group, SWT.NONE);
        lClubRatingData.setText("Club rating data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubRatingData.setLayoutData(gridData);

        lClubBoom = new Label(group, SWT.NONE);
        lClubBoom.setText(MainWindow.getMessage("club.boom"));
        textControls.put(lClubBoom, "club.boom");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubBoom.setLayoutData(gridData);

        lClubBoomData = new Label(group, SWT.NONE);
        lClubBoomData.setText("Club boom data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubBoomData.setLayoutData(gridData);

        lClubAllZip = new Label(group, SWT.NONE);
        lClubAllZip.setText(MainWindow.getMessage("club.allzip"));
        textControls.put(lClubAllZip, "club.allzip");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubAllZip.setLayoutData(gridData);

        lClubAllZipData = new Link(group, SWT.NONE);
        lClubAllZipData.setText("Club all.zip data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubAllZipData.setLayoutData(gridData);
        lClubAllZipData.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                GuiUtils.openWebpage(e.text);
            }

        });

        lClubProfile = new Label(group, SWT.NONE);
        lClubProfile.setText(MainWindow.getMessage("club.profile"));
        textControls.put(lClubProfile, "club.profile");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lClubProfile.setLayoutData(gridData);

        lClubProfileData = new Link(group, SWT.NONE);
        lClubProfileData.setText("Club profile data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lClubProfileData.setLayoutData(gridData);
        lClubProfileData.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                GuiUtils.openWebpage(e.text);
            }

        });

        //        lClubHistory = new Label(group, SWT.BORDER);
        //        lClubHistory.setText(MainWindow.getMessage("club.history"));
        //        textControls.put(lClubHistory, "club.history");
        //        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        //        lClubHistory.setLayoutData(gridData);
        //
        //        lClubHistoryData = new Link(group, SWT.BORDER);
        //        lClubHistoryData.setText("Club history data");
        //        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        //        lClubHistoryData.setLayoutData(gridData);
        //        lClubHistoryData.addSelectionListener(new SelectionAdapter() {
        //
        //            @Override
        //            public void widgetSelected(SelectionEvent e) {
        //                GuiUtils.openWebpage(e.text);
        //            }
        //
        //        });

    }

    private void createManagerPanelContent() {

        Group group = new Group(managerPanel, SWT.DEFAULT);
        group.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        group.setLayout(new GridLayout(2, true));
        group.setText(MainWindow.getMessage("club.manager"));
        textControls.put(group, "club.manager");

        lManager = new Label(group, SWT.NONE);
        lManager.setText(MainWindow.getMessage("manager.manager"));
        textControls.put(lManager, "manager.manager");
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManager.setLayoutData(gridData);

        lManagerData = new Label(group, SWT.NONE);
        lManagerData.setText("Manager data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerData.setLayoutData(gridData);

        lManagerCity = new Label(group, SWT.NONE);
        lManagerCity.setText(MainWindow.getMessage("manager.city"));
        textControls.put(lManagerCity, "manager.city");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerCity.setLayoutData(gridData);

        lManagerCityData = new Label(group, SWT.NONE);
        lManagerCityData.setText("Manager city data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerCityData.setLayoutData(gridData);

        lManagerCountry = new Label(group, SWT.NONE);
        lManagerCountry.setText(MainWindow.getMessage("manager.country"));
        textControls.put(lManagerCountry, "manager.country");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerCountry.setLayoutData(gridData);

        lManagerCountryData = new Label(group, SWT.NONE);
        lManagerCountryData.setText("Manager country data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerCountryData.setLayoutData(gridData);

        lManagerEmail = new Label(group, SWT.NONE);
        lManagerEmail.setText(MainWindow.getMessage("manager.email"));
        textControls.put(lManagerEmail, "manager.email");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerEmail.setLayoutData(gridData);

        lManagerEmailData = new Label(group, SWT.NONE);
        lManagerEmailData.setText("Manager e-mail data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerEmailData.setLayoutData(gridData);
        /*
                lManagerIcq = new Label(group, SWT.BORDER);
                lManagerIcq.setText("icq");
                gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
                lManagerIcq.setLayoutData(gridData);
        
                lManagerIcqData = new Label(group, SWT.NONE);
                lManagerIcqData.setText("icq");
                gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
                lManagerIcqData.setLayoutData(gridData);
        */
        lManagerPlayed = new Label(group, SWT.NONE);
        lManagerPlayed.setText(MainWindow.getMessage("manager.played"));
        textControls.put(lManagerPlayed, "manager.played");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerPlayed.setLayoutData(gridData);

        lManagerPlayedData = new Label(group, SWT.NONE);
        lManagerPlayedData.setText("Manager played data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerPlayedData.setLayoutData(gridData);

        lManagerFinances = new Label(group, SWT.NONE);
        lManagerFinances.setText(MainWindow.getMessage("manager.finances"));
        textControls.put(lManagerFinances, "manager.finances");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerFinances.setLayoutData(gridData);

        lManagerFinancesData = new Label(group, SWT.NONE);
        lManagerFinancesData.setText("Manager finances data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerFinancesData.setLayoutData(gridData);

        lManagerProfile = new Label(group, SWT.NONE);
        lManagerProfile.setText(MainWindow.getMessage("manager.profile"));
        textControls.put(lManagerProfile, "manager.profile");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        lManagerProfile.setLayoutData(gridData);

        lManagerProfileData = new Link(group, SWT.NONE);
        lManagerProfileData.setText("Manager profile data");
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        lManagerProfileData.setLayoutData(gridData);
        lManagerProfileData.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                GuiUtils.openWebpage(e.text);
            }

        });
        ;
    }

    public void updateAll() {
        if (!needUpdate) {
            scrollPanel.setContent(mainComposite);
            scrollPanel.update();
            return;
        }
        if (MainWindow.getAllInstance() == null) {
            return;
        }
        redraw();
        needUpdate = false;
    }

    public void updatePassword(String password) {
        //this.password = password;
        club.setPassword(password);//!!! don't remove - password'll init later from here
        clubBid.setPassword(password);
    }

    public void updateMessages() {

        mapTriActionToString.put(Club.TriAction.TR_BUILD, MainWindow.getMessage("club.action.change"));
        mapTriActionToString.put(Club.TriAction.TR_NOTHING, MainWindow.getMessage("club.action.doNothing"));
        mapTriActionToString.put(Club.TriAction.TR_REPAIR, MainWindow.getMessage("club.action.repair"));

        GuiUtils.updateTextControls(textControls);
        fillLanguageDependedUIforCurrentClub();

        //update toggles with current values

        bToggleUniformAction.setText(mapTriActionToString.get(bToggleUniformAction.getData()));
        bToggleStadiumAction.setText(mapTriActionToString.get(bToggleStadiumAction.getData()));
        bToggleSportbaseAction.setText(mapTriActionToString.get(bToggleSportbaseAction.getData()));
        bToggleSportSchoolAction.setText(mapTriActionToString.get(bToggleSportSchoolAction.getData()));
        bToggleMainCoachAction.setText(mapTriActionToString.get(bToggleMainCoachAction.getData()));
        bToggleGkCoachAction.setText(mapTriActionToString.get(bToggleGkCoachAction.getData()));
        bToggleDefCoachAction.setText(mapTriActionToString.get(bToggleDefCoachAction.getData()));
        bToggleMfCoachAction.setText(mapTriActionToString.get(bToggleMfCoachAction.getData()));
        bToggleFwCoachAction.setText(mapTriActionToString.get(bToggleFwCoachAction.getData()));
        bToggleFitnessCoachAction.setText(mapTriActionToString.get(bToggleFitnessCoachAction.getData()));
        bToggleMoraleCoachAction.setText(mapTriActionToString.get(bToggleMoraleCoachAction.getData()));
        bToggleDoctorAction.setText(mapTriActionToString.get(bToggleDoctorAction.getData()));
        bToggleScoutAction.setText(mapTriActionToString.get(bToggleScoutAction.getData()));

        mainComposite.layout(true, true);
    }

    //redraw() is used only when team/all changed (updateTab comes)
    public void redraw() {
        //System.out.println("ClubInfoView.redraw...");
        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                resetUiClubActions();
                club.initFromTeam(currentTeam);
                //manager = Manager.extractManager(currentTeam);
                clubBid.copyFrom(club);
                fillUiFromClub(club);
                recalcBuyCosts();
            }
        }

        mainComposite.layout();
    }

    boolean resetUiClubActionsInProgress = false;

    private void resetUiClubActions() {
        resetUiClubActionsInProgress = true;

        bToggleUniformAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleUniformAction.setData(TriAction.TR_NOTHING);

        bToggleStadiumAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleStadiumAction.setData(TriAction.TR_NOTHING);

        bToggleSportbaseAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleSportbaseAction.setData(TriAction.TR_NOTHING);

        bToggleSportSchoolAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleSportSchoolAction.setData(TriAction.TR_NOTHING);

        bToggleMainCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleMainCoachAction.setData(TriAction.TR_NOTHING);

        bToggleGkCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleGkCoachAction.setData(TriAction.TR_NOTHING);

        bToggleDefCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleDefCoachAction.setData(TriAction.TR_NOTHING);

        bToggleMfCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleMfCoachAction.setData(TriAction.TR_NOTHING);

        bToggleFwCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleFwCoachAction.setData(TriAction.TR_NOTHING);

        bToggleFitnessCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleFitnessCoachAction.setData(TriAction.TR_NOTHING);

        bToggleMoraleCoachAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleMoraleCoachAction.setData(TriAction.TR_NOTHING);

        bToggleDoctorAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleDoctorAction.setData(TriAction.TR_NOTHING);

        bToggleScoutAction.setText(mapTriActionToString.get(TriAction.TR_NOTHING));
        bToggleScoutAction.setData(TriAction.TR_NOTHING);

        recalcBuyCosts();
        resetUiClubActionsInProgress = false;
    }

    //redraw club
    public void fillUiFromClub(Club club) {
        if (club == null) {
            return;
        }

        if (MainWindow.getAllInstance() == null) {
            return;
        }

        Team team = MainWindow.getAllInstance().getCurrentTeam();

        if (team == null) {
            return;
        }

        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);

        TeamStats teamStats = new TeamStats(team);

        lManagerData.setText(team.getManagerName());
        lManagerCityData.setText(team.getManagerTown());
        lManagerCountryData.setText(team.getManagerCountry());
        lManagerEmailData.setText(team.getEmail());
        //lManagerIcqData.setText(String.valueOf(team.getUIN()));
        lManagerPlayedData.setText(nf.format(team.getGames()));
        lManagerFinancesData.setText(nf.format(team.getManagerFinance()));

        lClubCityData.setText(team.getTown());

        setClubLogoImage(team.getId());

        lClubFinancesData.setText(nf.format(team.getTeamFinance()));
        lClubCapitalData.setText(nf.format(getTeamCapit1al(team)));
        lClubRatingData.setText(nf.format(team.getRating()));
        lClubBoomData.setText(String.valueOf(team.getBoom()));

        fillLanguageDependedUIforCurrentClub();//to update them easy then language changes

        lWeekCostsCoachesData.setText(nf.format(teamStats.getTrainersSalary() * 1000));
        lWeekCostsPlayersData.setText(nf.format(teamStats.getPlayersSalary() * 1000));
        lWeekCostsTotalData.setText(nf.format((teamStats.getTrainersSalary() + teamStats.getPlayersSalary()) * 1000));

        lStadiumNameData.setText(team.getStadium());
        sStadiumCapacityData.setValues(team.getStadiumCapacity().intValue(), 0, 200000, 0, 1000, 3000);
        int sportBaseMaxLevel = team.getSportbase().intValue() + 1 > 5 ? 5 : team.getSportbase().intValue() + 1;
        sSportbaseData.setValues(team.getSportbase().intValue(), 0, sportBaseMaxLevel, 0, 1, 1);

        lStadiumDestructionData.setText(team.getStadiumState().toString());
        lSportbaseDestructionData.setText(team.getSportbaseState().toString());
        lSportSchoolDestructionData.setText(team.getSportschoolState().toString());

        //String s2 = " " + MainWindow.getMessage("global.thousand_short_FC");

        sMainCoachData.setValues(team.getCoach(), 0, 999, 0, 10, 1);
        lMainCoachCostsData.setText(nf.format(32000 + (int) Math.round(1600 * team.getCoach())));// + s2);

        sGkCoachData.setValues(team.getGoalkeepersCoach(), 0, 99, 0, 1, 1);
        lGkCoachCostsData.setText(nf.format(8000 * team.getGoalkeepersCoach()));// + s2);

        sDefCoachData.setValues(team.getDefendersCoach(), 0, 99, 0, 1, 1);
        lDefCoachCostsData.setText(nf.format(8000 * team.getDefendersCoach()));// + s2);

        sMfCoachData.setValues(team.getMidfieldersCoach(), 0, 99, 0, 1, 1);
        lMfCoachCostsData.setText(nf.format(8000 * team.getMidfieldersCoach()));// + s2);

        sFwCoachData.setValues(team.getForwardsCoach(), 0, 99, 0, 1, 1);
        lFwCoachCostsData.setText(nf.format(8000 * team.getForwardsCoach()));// + s2);

        sFitnessCoachData.setValues(team.getFitnessCoach(), 0, 99, 0, 1, 1);
        lFitnessCoachCostsData.setText(nf.format(8000 * team.getFitnessCoach()));// + s2);

        sMoraleCoachData.setValues(team.getMoraleCoach(), 0, 99, 0, 1, 1);
        lMoraleCoachCostsData.setText(nf.format(8000 * team.getMoraleCoach()));// + s2);

        sDoctorPlayers.setValues(team.getDoctorPlayers(), 0, 99, 0, 1, 1);
        sDoctorLevel.setValues(team.getDoctorQualification(), 0, 99, 0, 1, 1);
        lDoctorData.setText(nf.format(8000 * (team.getDoctorPlayers() + team.getDoctorQualification())));// + s2);

        sScoutData.setValues(team.getScout(), 0, 99, 0, 1, 1);
        lScoutCostsData.setText(nf.format(24000 * team.getScout()));// + s2);
        //only default init
        lTotalCostsData.setText("0");
        lBalanceChangeData.setText("0");

        //uniform
        setUniformColors(true, club.getColor1() - 1, club.getColor2() - 1);
        setUniformColors(false, club.getColor3() - 1, club.getColor4() - 1);

        mainComposite.layout();
    }

    private int getTeamCapit1al(Team team) {
        TeamStats teamStats = new TeamStats(team);
        return teamStats.getTeamPrice() * 1000 + teamStats.getFinance() * 1000;
    }

    private void setClubLogoImage(final String clubCode) {

        final String localLogoFileName = CLUB_LOGO_DIRECTORY + File.separatorChar + clubCode + "." + CLUB_LOGO_IMAGE_EXTENSION;
        File file = new File(localLogoFileName);
        if (file.exists() && file.length() > 0) {
            //System.out.println("loading club logo from local...");
            if (imgClubLogo != null && !imgClubLogo.isDisposed()) {
                imgClubLogo.dispose();
            }
            try {
                ImageData imgData = new ImageData(localLogoFileName);
                imgClubLogo = new Image(parentTabFolder.getDisplay(), imgData);
                lClubLogoContainer.setImage(imgClubLogo);
            } catch (Exception e) {
                //System.out.println("error creating logo from file: " + localLogoFileName);
                lClubLogoContainer.setImage(imgBlankClubLogo);
            }
        } else {
            //System.out.println("downloading club logo from url...");
            GuiLongTask task = new GuiLongTask() {

                @Override
                protected void execute() {
                    String url = CLUB_LOGO_DOWNLOAD_URL + clubCode + "." + CLUB_LOGO_IMAGE_EXTENSION;
                    try {
                        GuiUtils.downloadUrlResource(url, localLogoFileName);
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                }
            };
            task.setOnFinishExecute(new IOnFinish() {

                @Override
                public void execute() {
                    File file = new File(localLogoFileName);
                    if (file.exists() && file.length() > 0) {
                        if (imgClubLogo != null && !imgClubLogo.isDisposed()) {
                            imgClubLogo.dispose();
                        }
                        try {
                            ImageData imgData = new ImageData(localLogoFileName);
                            imgClubLogo = new Image(parentTabFolder.getDisplay(), imgData);
                            lClubLogoContainer.setImage(imgClubLogo);
                        } catch (Exception e) {
                            //System.out.println("error creating logo from file: " + localLogoFileName);
                            lClubLogoContainer.setImage(imgBlankClubLogo);
                        }

                    } else {
                        if (file.exists() && file.length() == 0) {
                            //System.out.println("delete corrupted club logo... " + localLogoFileName);
                            file.delete();
                        }
                        lClubLogoContainer.setImage(imgBlankClubLogo);
                    }
                }

            });
            task.start();
        }

    }

    private void fillUiFromClubBid(Club clubBid) {
        resetUiClubActions();
        fillUiFromClub(this.club);//to refresh for current team

        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);

        //next toggle changes on UI and recalcCosts
        //make UI to not react on next toggles
        //while we fill toggles UI
        resetUiClubActionsInProgress = true;

        //uniform changed?
        if (clubBid.isChangeUniformColors()) {

            bToggleUniformAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleUniformAction.setData(TriAction.TR_BUILD);
            //uniform

            int color1 = clubBid.isChangeColor1() ? clubBid.getColor1() : club.getColor1();
            int color2 = clubBid.isChangeColor2() ? clubBid.getColor2() : club.getColor2();
            int color3 = clubBid.isChangeColor3() ? clubBid.getColor3() : club.getColor3();
            int color4 = clubBid.isChangeColor4() ? clubBid.getColor4() : club.getColor4();

            setUniformColors(true, color1 - 1, color2 - 1);
            setUniformColors(false, color3 - 1, color4 - 1);
        }
        bToggleUniformAction.setSize(bToggleUniformAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));
        //stadium changed?
        if (!clubBid.getStadiumAction().equals(TriAction.TR_NOTHING)) {

            bToggleStadiumAction.setText(mapTriActionToString.get(clubBid.getStadiumAction()));
            bToggleStadiumAction.setData(clubBid.getStadiumAction());
            if (clubBid.getStadiumAction().equals(TriAction.TR_BUILD)) {
                sStadiumCapacityData.setValues(clubBid.getStadium(), 0, 200000, 0, 1000, 3000);
            }
        }
        bToggleStadiumAction.setSize(bToggleStadiumAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        //sportbase changed?
        if (!clubBid.getSportBaseAction().equals(TriAction.TR_NOTHING)) {

            bToggleSportbaseAction.setText(mapTriActionToString.get(clubBid.getSportBaseAction()));
            bToggleSportbaseAction.setData(clubBid.getSportBaseAction());
            int sportBaseMaxLevel = clubBid.getSportBase() + 1 > 5 ? 5 : clubBid.getSportBase() + 1;
            sSportbaseData.setValues(clubBid.getSportBase(), 0, sportBaseMaxLevel, 0, 1, 1);
        }
        bToggleSportbaseAction.setSize(bToggleSportbaseAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        //sportschool changed?
        if (!clubBid.getSportSchoolAction().equals(TriAction.TR_NOTHING)) {

            bToggleSportSchoolAction.setText(mapTriActionToString.get(clubBid.getSportSchoolAction()));
            bToggleSportSchoolAction.setData(clubBid.getSportSchoolAction());

        }
        bToggleSportSchoolAction.setSize(bToggleSportSchoolAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        //coaches changed?
        if (clubBid.isChangeCoachCoef()) {

            bToggleMainCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleMainCoachAction.setData(TriAction.TR_BUILD);
            sMainCoachData.setValues(clubBid.getCoachCoef(), 0, 199, 0, 10, 1);
            lMainCoachCostsData.setText(nf.format(32000 + (int) Math.round(1600 * clubBid.getCoachCoef())));// + s2);
        }
        bToggleMainCoachAction.setSize(bToggleMainCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachGK()) {

            bToggleGkCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleGkCoachAction.setData(TriAction.TR_BUILD);
            sGkCoachData.setValues(clubBid.getCoachGK(), 0, 99, 0, 1, 1);
            lGkCoachCostsData.setText(nf.format(8000 * clubBid.getCoachGK()));
        }
        bToggleGkCoachAction.setSize(bToggleGkCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachDef()) {

            bToggleDefCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleDefCoachAction.setData(TriAction.TR_BUILD);
            sDefCoachData.setValues(clubBid.getCoachDef(), 0, 99, 0, 1, 1);
            lDefCoachCostsData.setText(nf.format(8000 * clubBid.getCoachDef()));

        }
        bToggleDefCoachAction.setSize(bToggleDefCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachMid()) {

            bToggleMfCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleMfCoachAction.setData(TriAction.TR_BUILD);
            sMfCoachData.setValues(clubBid.getCoachMid(), 0, 99, 0, 1, 1);
            lMfCoachCostsData.setText(nf.format(8000 * clubBid.getCoachMid()));
        }
        bToggleMfCoachAction.setSize(bToggleMfCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachFw()) {

            bToggleFwCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleFwCoachAction.setData(TriAction.TR_BUILD);
            sFwCoachData.setValues(clubBid.getCoachFw(), 0, 99, 0, 1, 1);
            lFwCoachCostsData.setText(nf.format(8000 * clubBid.getCoachFw()));
        }
        bToggleFwCoachAction.setSize(bToggleFwCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachFitness()) {

            bToggleFitnessCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleFitnessCoachAction.setData(TriAction.TR_BUILD);
            sFitnessCoachData.setValues(clubBid.getCoachFitness(), 0, 99, 0, 1, 1);
            lFitnessCoachCostsData.setText(nf.format(8000 * clubBid.getCoachFitness()));
        }
        bToggleFitnessCoachAction.setSize(bToggleFitnessCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeCoachMorale()) {

            bToggleMoraleCoachAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleMoraleCoachAction.setData(TriAction.TR_BUILD);
            sMoraleCoachData.setValues(clubBid.getCoachMorale(), 0, 99, 0, 1, 1);
            lMoraleCoachCostsData.setText(nf.format(8000 * clubBid.getCoachMorale()));
        }
        bToggleMoraleCoachAction.setSize(bToggleMoraleCoachAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeDoctor()) {

            bToggleDoctorAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleDoctorAction.setData(TriAction.TR_BUILD);
            sDoctorPlayers.setValues(clubBid.getDoctorCount(), 0, 99, 0, 1, 1);
            sDoctorLevel.setValues(clubBid.getDoctorLevel(), 0, 99, 0, 1, 1);
            lDoctorData.setText(nf.format(8000 * (clubBid.getDoctorCount() + clubBid.getDoctorLevel())));
        }
        bToggleDoctorAction.setSize(bToggleDoctorAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        if (clubBid.isChangeScout()) {

            bToggleScoutAction.setText(mapTriActionToString.get(TriAction.TR_BUILD));
            bToggleScoutAction.setData(TriAction.TR_BUILD);
            sScoutData.setValues(clubBid.getScoutLevel(), 0, 99, 0, 1, 1);
            lScoutCostsData.setText(nf.format(24000 * clubBid.getScoutLevel()));
        }
        bToggleScoutAction.setSize(bToggleScoutAction.computeSize(SWT.DEFAULT, SWT.DEFAULT, true));

        recalcBuyCosts();
        resetUiClubActionsInProgress = false;
    }

    private void fillLanguageDependedUIforCurrentClub() {

        if (MainWindow.getAllInstance() == null) {
            return;
        }

        Team team = MainWindow.getAllInstance().getCurrentTeam();

        if (team == null) {
            return;
        }

        String s = MainWindow.getMessage("country." + team.getCountryCode());
        lClubCountryData.setText(s);

        s = "<A href=\"http://www.fa13.info/manager/" + team.getManagerId() + "\">" + MainWindow.getMessage("global.open") + "</A>";
        lManagerProfileData.setText(s);

        s = "<A href=\"http://repository.fa13.info/site/build/mini/all13" + team.getId() + ".zip" + "\">" + MainWindow.getMessage("global.open")
                + "</A>";
        lClubAllZipData.setText(s);

        s = "<A href=\"http://www.fa13.info/club/" + team.getId() + "\">" + MainWindow.getMessage("global.open") + "</A>";
        lClubProfileData.setText(s);

        //        s = "<A href=\"http://www.fa13.info/club/" + team.getId() + "\">" + MainWindow.getMessage("global.open") + "</A>";
        //        lClubHistoryData.setText(s);

        s = team.isSportschool() ? MainWindow.getMessage("global.yes") : MainWindow.getMessage("global.no");
        lSportSchoolData.setText(s);

    }

    private void setUniformColors(boolean isHome, int topColorIndex, int bottomColorIndex) {

        ImageData imgData = (ImageData) imgDataUniformOriginal.clone();
        PaletteData palette = new PaletteData(0xff0000, 0x00ff00, 0x0000ff);
        for (int x = 0; x < imgData.width; x++) {
            for (int y = 0; y < imgData.height; y++) {

                RGB rgb = palette.getRGB(imgData.getPixel(x, y));

                if (rgb.equals(UNIFORM_TOP_ORIGINAL_COLOR) && topColorIndex > -1 && topColorIndex < rgbColors.length) {

                    imgData.setPixel(x, y, palette.getPixel(rgbColors[topColorIndex]));

                } else if (rgb.equals(UNIFORM_BOTTOM_ORIGINAL_COLOR) && bottomColorIndex > -1 && bottomColorIndex < rgbColors.length) {

                    imgData.setPixel(x, y, palette.getPixel(rgbColors[bottomColorIndex]));

                }

            }
        }
        //transformed uniform image
        Image newImg = new Image(parentTabFolder.getDisplay(), imgData);

        if (isHome) {
            if (lHomeUnifornImgContainer.getImage() != null) {
                lHomeUnifornImgContainer.getImage().dispose();
            }
            lHomeUnifornImgContainer.setImage(newImg);
        } else {
            if (lAwayUnifornImgContainer.getImage() != null) {
                lAwayUnifornImgContainer.getImage().dispose();
            }
            lAwayUnifornImgContainer.setImage(newImg);
        }

    }

    private int calcStadiumCosts(int startCapacity, int endCapacity) {

        if (endCapacity - startCapacity <= 0) {
            return 0;
        }

        float costs = 0;

        for (int capacity = startCapacity / 1000; capacity < endCapacity / 1000 && capacity < 50; capacity++) {
            costs += (20.0 / (63.0 - capacity));
        }

        if (endCapacity > 49000) {
            costs += (endCapacity / 1000 - 50) * 1.5;
        }

        return (int) (1000000 * costs);

    }
    /**
     * Added with boom1 cancelling 24.06.2018
     * @param startCapacity
     * @param endCapacity
     * @return
     */
    private int calcNewStadiumCosts(int startCapacity, int endCapacity, double damagePercents) {
        
        int buildConsts = calcStadiumCosts(startCapacity, endCapacity);
        
        return (int)((0.15 + 0.9*damagePercents/100)*buildConsts/2.0);
        
    }

    public void recalcBuyCosts() {
        if (clubBid == null) {
            return;
        }
        long diff = 0;
        buyCosts = 0;
        balanceChange = 0;
        //stadiumCapacity
        if (clubBid.getStadiumAction().equals(TriAction.TR_BUILD) && club.getStadium() < clubBid.getStadium()) {
            buyCosts += calcStadiumCosts(club.getStadium(), clubBid.getStadium());
        } else if (clubBid.getStadiumAction().equals(TriAction.TR_REPAIR)) {
            //buyCosts += calcStadiumCosts(club.getStadium() - 3000, club.getStadium()) / 2;
            int damage = getStadiumDamage();
            buyCosts += calcNewStadiumCosts(club.getStadium() - 3000, club.getStadium(), damage);
        }
        //sportBaseLevel

        if (clubBid.getSportBaseAction().equals(TriAction.TR_BUILD) && clubBid.getSportBase() > club.getSportBase()) {
            if (club.getSportBase() > 0) {
                buyCosts += 3000000;
            } else {
                buyCosts += 5000000;
            }
        } else if (clubBid.getSportBaseAction().equals(TriAction.TR_REPAIR)) {
            buyCosts += 1500000;
        }

        //sportSchool
        if (!club.isSchool() && clubBid.getSportSchoolAction().equals(TriAction.TR_BUILD)) {
            buyCosts += 5000000;
        } else if (club.isSchool() && clubBid.getSportSchoolAction().equals(TriAction.TR_REPAIR)) {
            buyCosts += 2500000;
        }

        //mainCoach
        diff = clubBid.getCoachCoef() - club.getCoachCoef();
        if (clubBid.isChangeCoachCoef() && diff != 0) {
            if (diff > 0) {
                buyCosts += 1000000 + 100000 * diff;
            }
            balanceChange += 1600 * diff;
        }

        //gkCoach
        diff = clubBid.getCoachGK() - club.getCoachGK();
        if (clubBid.isChangeCoachGK() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachGK() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachGK() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //defCoach
        diff = clubBid.getCoachDef() - club.getCoachDef();
        if (clubBid.isChangeCoachDef() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachDef() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachDef() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //midCoach
        diff = clubBid.getCoachMid() - club.getCoachMid();
        if (clubBid.isChangeCoachMid() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachMid() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachMid() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //fwCoach
        diff = clubBid.getCoachFw() - club.getCoachFw();
        if (clubBid.isChangeCoachFw() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachFw() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachFw() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //fitnessCoach
        diff = clubBid.getCoachFitness() - club.getCoachFitness();
        if (clubBid.isChangeCoachFitness() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachFitness() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachFitness() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //moraleCoach
        diff = clubBid.getCoachMorale() - club.getCoachMorale();
        if (clubBid.isChangeCoachMorale() && diff != 0) {
            if (diff > 0) {
                if (club.getCoachMorale() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getCoachMorale() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //doctorCoach
        diff = clubBid.getDoctorCount() + clubBid.getDoctorLevel() - club.getDoctorCount() - club.getDoctorLevel();
        if (clubBid.isChangeDoctor() && diff != 0) {
            if (diff > 0) {
                if (club.getDoctorCount() + club.getDoctorLevel() == 0) {
                    buyCosts += 100000 + 250000 * (clubBid.getDoctorCount() + clubBid.getDoctorLevel() - 1);
                } else {
                    buyCosts += 250000 * diff;
                }
            }
            balanceChange += 8000 * diff;
        }

        //scout
        diff = clubBid.getScoutLevel() - club.getScoutLevel();
        if (clubBid.isChangeScout() && diff != 0) {
            if (diff > 0) {
                if (club.getScoutLevel() == 0) {
                    buyCosts += 100000 + 500000 * (clubBid.getScoutLevel() - 1);
                } else {
                    buyCosts += 500000 * diff;
                }
            }
            balanceChange += 24000 * diff;
        }

        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(true);

        lTotalCostsData.setText(nf.format(buyCosts));
        lBalanceChangeData.setText(nf.format(-balanceChange));

        checkSaveButton();
    }

    private int getStadiumDamage() {
        if (MainWindow.getAllInstance() == null) {
            return 0;
        }

        Team team = MainWindow.getAllInstance().getCurrentTeam();

        if (team == null) {
            return 0;
        }
        
        
        return team.getStadiumState();
    }

    //prepare clubBid for use after it was loaded
    private void initClubBidForUse(Club clubBid) {

        if (!clubBid.isChangeColor1()) {
            clubBid.setColor1(club.getColor1());
        }

        if (!clubBid.isChangeColor2()) {
            clubBid.setColor2(club.getColor2());
        }

        if (!clubBid.isChangeColor3()) {
            clubBid.setColor3(club.getColor3());
        }

        if (!clubBid.isChangeColor4()) {
            clubBid.setColor4(club.getColor4());
        }

        if (clubBid.getStadiumAction().equals(TriAction.TR_NOTHING)) {
            clubBid.setStadium(club.getStadium());
        }

        if (clubBid.getSportBaseAction().equals(TriAction.TR_NOTHING)) {
            clubBid.setSportBase(club.getSportBase());
        }

        if (clubBid.getSportSchoolAction().equals(TriAction.TR_NOTHING)) {
            clubBid.setSchool(club.isSchool());
        }

        //fix main coach value
        clubBid.setCoachCoef(club.getCoachCoef() + clubBid.getCoachCoefDiff());
        if (!clubBid.isChangeCoachGK()) {
            clubBid.setCoachGK(club.getCoachGK());
        }

        if (!clubBid.isChangeCoachDef()) {
            clubBid.setCoachDef(club.getCoachDef());
        }

        if (!clubBid.isChangeCoachMid()) {
            clubBid.setCoachMid(club.getCoachMid());
        }

        if (!clubBid.isChangeCoachFw()) {
            clubBid.setCoachFw(club.getCoachFw());
        }

        if (!clubBid.isChangeCoachFitness()) {
            clubBid.setCoachFitness(club.getCoachFitness());
        }

        if (!clubBid.isChangeCoachMorale()) {
            clubBid.setCoachMorale(club.getCoachMorale());
        }

        if (!clubBid.isChangeDoctor()) {
            clubBid.setDoctorLevel(club.getDoctorLevel());
            clubBid.setDoctorCount(club.getDoctorCount());
        }

        if (!clubBid.isChangeScout()) {
            clubBid.setScoutLevel(club.getScoutLevel());
        }

    }

    private long buyCosts = 0, balanceChange = 0;
    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        if (!needUpdate) {
            updateAll();
        }
        redraw();
        needUpdate = false;
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    private String bidFileName = null;

    public void openClubBidDlg() {
        FileDialog dlg = new FileDialog(parentTabFolder.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterNames(extNames);
        dlg.setFileName(bidFileName == null ? getBidDefaultFileName() : bidFileName);
        String result = dlg.open();
        if (result != null) {
            openClubBid(result);
        }
    }

    public void openClubBid(String fname) {
        if (fname == null) {
            return;
        }
        try {

            clubBid = ClubReader.readClubFile(fname);
            initClubBidForUse(clubBid);
            fillUiFromClubBid(clubBid);
            bidFileName = fname;
        } catch (ReaderException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentTabFolder.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.open_bid_error"));
        }
    }

    public void saveClubBidDlg() {

        Team team = MainWindow.getAllInstance().getCurrentTeam();
        if (buyCosts > team.getTeamFinance()) {
            MessageBox dialog = new MessageBox(parentTabFolder.getShell(), SWT.ICON_ERROR | SWT.OK | SWT.CANCEL);
            dialog.setText(MainWindow.getMessage("error"));
            dialog.setMessage(MainWindow.getMessage("club.bid.costsExceededFinances"));

            if (dialog.open() != SWT.OK) {
                return;
            }
        }

        ClubBidDialog confirmDialog = new ClubBidDialog(parentTabFolder.getShell());
        confirmDialog.setText(MainWindow.getMessage("club.bid.contents"));
        confirmDialog.setClubBid(clubBid);
        if (confirmDialog.open() == SWT.CANCEL) {
            return;
        }

        FileDialog dlg = new FileDialog(parentTabFolder.getShell(), SWT.SAVE);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterNames(extNames);
        dlg.setFileName(getBidDefaultFileName());
        String result = dlg.open();
        if (result != null) {
            saveClubBid(result);
        }
    }

    public void saveClubBid(String fname) {
        if (fname == null) {
            return;
        }
        try {
            if (MainWindow.getAllInstance() != null) {
                Team team = MainWindow.getAllInstance().getCurrentTeam();
                if (team != null)
                    clubBid.setTeamID(team.getId());
            }

            ClubWriter.writeClubFile(fname, clubBid);
            bidFileName = fname;
            GuiUtils.showSuccessMessage(parentTabFolder.getShell(), MainWindow.getMessage("info"), MainWindow.getMessage("global.bidSaveSuccess"));
        } catch (IOException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentTabFolder.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.bidSaveError"));
        }
    }

    private String getBidDefaultFileName() {
        String bidFileName = "";

        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                bidFileName = "Club" + currentTeam.getId() + ".f13";
            }
        }

        return bidFileName;
    }

    //release resources here!
    @Override
    public void dispose() {
        imgHomeUniform.dispose();
        imgAwayUniform.dispose();
        imgBlankClubLogo.dispose();
        if (imgClubLogo != null && !imgClubLogo.isDisposed()) {
            imgClubLogo.dispose();
        }
        imgClub.dispose();
    }

    public class ClubBidDialog extends Dialog {

        private Club clubBid;
        private int result = SWT.OK;
        Table table;

        public ClubBidDialog(Shell parent) {
            this(parent, SWT.BORDER | SWT.APPLICATION_MODAL | SWT.TITLE);
        }

        public ClubBidDialog(Shell parent, int style) {
            super(parent, style);
        }

        public int open() {
            Shell shell = new Shell(getParent(), getStyle());
            shell.setText(getText());
            createContents(shell);

            Rectangle ca = getParent().getShell().getClientArea();
            int dlgW = shell.getBounds().width;
            int dlgH = shell.getBounds().height;
            int x = (ca.width - dlgW) / 2;
            int y = (ca.height - dlgH) / 2;
            shell.setBounds(x, y, dlgW, dlgH);

            shell.pack();
            shell.open();
            Display display = getParent().getDisplay();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
            return result;
        }

        private void createContents(final Shell shell) {
            shell.setLayout(new GridLayout(2, false));
            table = new Table(shell, SWT.BORDER);
            table.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            table.setVisible(false);
            table.setCapture(false);
            table.setHeaderVisible(false);
            table.setLinesVisible(true);

            GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
            table.setLayoutData(gd);

            TableColumn tableColumn = new TableColumn(table, SWT.NONE);
            //tableColumn.setText(MainWindow.getMessage("global.action"));
            tableColumn.setResizable(true);
            tableColumn.setAlignment(SWT.FILL);

            Button bOk = new Button(shell, SWT.PUSH);
            bOk.setFont(MainWindow.getSharedFont(FontType.BUTTON));
            bOk.setText(MainWindow.getMessage("global.continue"));
            gd = new GridData(GridData.FILL_HORIZONTAL);
            bOk.setLayoutData(gd);
            bOk.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent event) {
                    result = SWT.OK;
                    shell.close();
                }
            });

            Button bCancel = new Button(shell, SWT.PUSH);
            bCancel.setFont(MainWindow.getSharedFont(FontType.BUTTON));
            bCancel.setText(MainWindow.getMessage("global.cancel"));
            gd = new GridData(GridData.FILL_HORIZONTAL);
            bCancel.setLayoutData(gd);
            bCancel.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent event) {
                    result = SWT.CANCEL;
                    shell.close();
                }
            });

            shell.setDefaultButton(bOk);

            table.setFont(MainWindow.getSharedFont(FontType.VIEW_IMPORTANT));
            fillUiFromBid();
            shell.pack();
        }

        public Club getClubBid() {
            return clubBid;
        }

        public void setClubBid(Club clubBid) {
            this.clubBid = clubBid;

        }

        private void fillUiFromBid() {
            if (clubBid.isChangeUniformColors()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("club.bid.uniformChange"));
            }

            if (!clubBid.getStadiumAction().equals(TriAction.TR_NOTHING)) {

                String text = "";
                if (clubBid.getStadiumAction().equals(TriAction.TR_REPAIR)) {
                    text = MainWindow.getMessage("club.bid.stadiumRepair");
                } else if (clubBid.getStadiumAction().equals(TriAction.TR_BUILD)) {
                    text = MainWindow.getMessage("club.bid.stadiumCapacityChange") + " : " + clubBid.getStadium();
                }
                if (!text.isEmpty()) {
                    TableItem ti = new TableItem(table, SWT.NONE);
                    ti.setText(0, text);
                }
            }

            if (!clubBid.getSportBaseAction().equals(TriAction.TR_NOTHING)) {

                String text = "";
                if (clubBid.getSportBaseAction().equals(TriAction.TR_REPAIR)) {
                    text = MainWindow.getMessage("club.bid.sportBaseRepair");
                } else if (clubBid.getSportBaseAction().equals(TriAction.TR_BUILD)) {
                    text = MainWindow.getMessage("club.bid.sportBaseBuild") + " : " + clubBid.getSportBase();
                }
                if (!text.isEmpty()) {
                    TableItem ti = new TableItem(table, SWT.NONE);
                    ti.setText(0, text);
                }
            }

            if (!clubBid.getSportSchoolAction().equals(TriAction.TR_NOTHING)) {

                String text = "";
                if (clubBid.getSportSchoolAction().equals(TriAction.TR_REPAIR)) {
                    text = MainWindow.getMessage("club.bid.sportSchoolRepair");
                } else if (clubBid.getSportSchoolAction().equals(TriAction.TR_BUILD) && clubBid.isSchool()) {
                    text = MainWindow.getMessage("club.bid.sportSchoolBuild");
                }
                if (!text.isEmpty()) {
                    TableItem ti = new TableItem(table, SWT.NONE);
                    ti.setText(0, text);
                }
            }

            if (clubBid.isChangeCoachCoef()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.mainCoachInvite") + " : " + clubBid.getCoachCoef();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachGK()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.gkCoachInvite") + " : " + clubBid.getCoachGK();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachDef()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.dfCoachInvite") + " : " + clubBid.getCoachDef();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachMid()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.mfCoachInvite") + " : " + clubBid.getCoachMid();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachFw()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.fwCoachInvite") + " : " + clubBid.getCoachFw();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachFitness()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.fitnessCoachInvite") + " : " + clubBid.getCoachFitness();
                ti.setText(0, text);
            }

            if (clubBid.isChangeCoachMorale()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.moraleCoachInvite") + " : " + clubBid.getCoachMorale();
                ti.setText(0, text);
            }

            if (clubBid.isChangeDoctor()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.doctorInvite") + " : ( " + clubBid.getDoctorLevel() + " / " + clubBid.getDoctorCount()
                        + " )";
                ti.setText(0, text);
            }

            if (clubBid.isChangeScout()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                String text = MainWindow.getMessage("club.bid.scoutInvite") + " : " + clubBid.getScoutLevel();
                ti.setText(0, text);
            }
            table.layout();
            table.setVisible(true);
            table.getColumn(0).pack();
        }

    }

    @Override
    public void store(Properties props) {
        // TODO Auto-generated method stub

    }

}
