package com.fa13.build.view.transfer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.view.MainWindow;

public class TBidsTableListener implements Listener {
    private static final int MIN_PRICE_BID = -99000;
    private static final int MAX_PRICE_BID = 99000;

    private final Table table;
    private final TableEditor tableEditor;
    private final TransferView transferView;

    private int bidNumber;

    TBidsTableListener(Table table, TransferView transferView) {
        this.table = table;
        this.tableEditor = createTableEditor(table);
        this.transferView = transferView;
    }

    private TableEditor createTableEditor(Table table) {
        TableEditor tableEditor = new TableEditor(table);
        tableEditor.horizontalAlignment = SWT.LEFT;
        tableEditor.grabHorizontal = true;
        tableEditor.minimumWidth = 35;
        return tableEditor;
    }

    @Override
    public void handleEvent(Event event) {
        Control oldEditor = tableEditor.getEditor();
        if (oldEditor != null) {
            oldEditor.dispose();
            transferView.redrawBids();
        }

        TableCell tableCell = getEventTableCell(table, event);
        if (tableCell != null) {
            bidNumber = (Integer) tableCell.tableItem.getData("bidNumber");
            if (transferView.bidList[bidNumber] != null) {
                if (tableCell.columnIndex == 1) {
                    Spinner spinner = createPriceSpinner();
                    spinner.setValues(transferView.bidList[bidNumber].getPrice(), MIN_PRICE_BID, MAX_PRICE_BID, 0, 1, 10);
                    tableEditor.setEditor(spinner, tableCell.tableItem, tableCell.columnIndex);
                    tableEditor.minimumWidth = spinner.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                    spinner.setFocus();

                } else if (tableCell.columnIndex == 2) {
                    Combo combo = createTradeInCombo();
                    combo.setText(tableCell.tableItem.getText(tableCell.columnIndex));
                    tableEditor.setEditor(combo, tableCell.tableItem, tableCell.columnIndex);
                    tableEditor.minimumWidth = combo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                    combo.setFocus();
                }
            }
        }
    }

    private TableCell getEventTableCell(Table table, Event event) {
        int rowCount = table.getItemCount();
        int columnCount = table.getColumnCount();

        for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            TableItem tmpItem = table.getItem(rowIndex);
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                if (tmpItem.getBounds(columnIndex).contains(event.x, event.y)) {
                    return new TableCell(tmpItem, columnIndex);
                }
            }
        }
        return null;
    }

    private Spinner createPriceSpinner() {
        final Spinner spinner = new Spinner(table, SWT.NONE);

        spinner.addModifyListener(new ModifyListener() {
            @Override
            public void modifyText(ModifyEvent e) {
                transferView.bidList[bidNumber].setPrice(spinner.getSelection());
            }
        });

        return spinner;
    }

    private Combo createTradeInCombo() {
        final Combo combo = new Combo(table, SWT.DROP_DOWN | SWT.READ_ONLY);

        combo.addSelectionListener(new SelectionListener() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                int index = combo.getSelectionIndex();
                if (index != -1) {
                    String playerName = combo.getItem(index);
                    int number = Integer.valueOf(combo.getData(playerName).toString());
                    transferView.bidList[bidNumber].setTradeIn(number);
                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        combo.addListener(SWT.MouseVerticalWheel, new Listener() {

            @Override
            public void handleEvent(Event arg0) {
                arg0.doit = false;

            }
            
        });
        if (MainWindow.getAllInstance() != null) {
            Team team = MainWindow.getAllInstance().getCurrentTeam();
            if (team != null) {
                combo.add("");
                combo.setData("", 0);
                for (Player player : team.getPlayers()) {
                    combo.add(player.getName());
                    combo.setData(player.getName(), player.getNumber());
                }
            }
        }

        return combo;
    }

    private static class TableCell {
        private final TableItem tableItem;
        private final int columnIndex;

        private TableCell(TableItem tableItem, int columnIndex) {
            this.tableItem = tableItem;
            this.columnIndex = columnIndex;
        }
    }

}
