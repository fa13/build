package com.fa13.build.view.calc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxis.Position;
import org.swtchart.IAxisSet;
import org.swtchart.ILineSeries;
import org.swtchart.ISeries;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.internal.series.BarSeries;

import com.fa13.build.model.All;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.model.calc.PlayerCalcInfo;
import com.fa13.build.model.calc.PlayerPrice;
import com.fa13.build.utils.GameUtils;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;

public class PlayerPriceView extends Composite {

    final private TabFolder parentTabFolder;
    private final TabItem mainItem;
    private Composite mainComposite;
    private Composite leftPanel;
    private Composite rightPanel;
    private Chart chart;
    private Label ageLabel;
    private Spinner ageSpinner;
    private Group likePlayersGroup;
    private Label talentLabel;
    private Spinner talentSpinner;
    private Label devStrengthLabel;
    private Spinner devStrengthSpinner;
    private Label expLabel;
    private Spinner expSpinner;
    private Group playerDevGroup;
    private Label skillsLabel;
    private Spinner devSkillsSpinner;
    private Label devExpLabel;
    private Spinner devExpSpinner;
    private Group playerChooseGroup;
    private Label teamLabel;
    private Combo teamCombo;
    private Label playerLabel;
    private Combo playerCombo;
    //private Button bApplyPlayer;

    private All all;

    private double[] age;
    private double[] price;
    private double[] likePlayers;
    private double[] strength;

    final private List<Player> listCalcPlayers = new ArrayList<Player>();

    private PlayerCalcInfo choosenPlayerInfo = new PlayerCalcInfo(null, 0);
    //private List<Player> listAllPlayers;
    private Label strengthLabel;
    private Spinner strengthSpinner;

    public static final String SERIE_PLAYER_PRICE = "PlayerPriceSerie";
    public static final String SERIE_LIKE_PLAYERS = "LikePlayersSerie";
    public static final String SERIE_STRENGTH = "StrengthSerie";
    private static Color BLACK = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);

    private boolean recalcSuspended = false;

    public PlayerPriceView(TabFolder parent, int style) {
        super(parent, style);
        parentTabFolder = parent;
        mainItem = new TabItem(parentTabFolder, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("PlayerPrice.tab"));
        loadImages();
        mainItem.setImage(MainWindow.getSharedImage(ImageType.SALE));

        initComponents();

    }

    private void loadImages() {

    }

    private void initComponents() {

        ScrolledComposite scrollPanel = new ScrolledComposite(parentTabFolder, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        scrollPanel.setExpandHorizontal(true);
        scrollPanel.setExpandVertical(true);
        mainComposite = new Composite(scrollPanel, SWT.NONE);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(new FormLayout());
        mainItem.setControl(scrollPanel);
        scrollPanel.setContent(mainComposite);
        //left panel
        createLeftPanel();
        createRightPanel();

        mainComposite.setRedraw(true);
    }

    private void createLeftPanel() {

        leftPanel = new Composite(mainComposite, SWT.NONE);
        FormData fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(0, 0);
        //fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        leftPanel.setLayoutData(fd);
        leftPanel.setLayout(new GridLayout(1, false));

        createPlayerChoosePanel();
        //createLikePlayersGroup();
        createPlayerDevGroup();

    }

    private void createPlayerChoosePanel() {

        playerChooseGroup = new Group(leftPanel, SWT.NONE);
        playerChooseGroup.setText(MainWindow.getMessage("PlayerChoose"));
        playerChooseGroup.setLayout(new GridLayout(1, false));
        GridData gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.FILL;
        playerChooseGroup.setLayoutData(gd);

        teamLabel = new Label(playerChooseGroup, SWT.NONE);
        teamLabel.setText(MainWindow.getMessage("global.team"));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        teamLabel.setLayoutData(gd);

        teamCombo = new Combo(playerChooseGroup, SWT.READ_ONLY | SWT.BORDER);
        if (!SystemUtils.IS_OS_LINUX) {
            teamCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        teamCombo.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.FILL;
        teamCombo.setLayoutData(gd);
        teamCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                onTeamChanged();
            }
        });

        playerLabel = new Label(playerChooseGroup, SWT.NONE);
        playerLabel.setText(MainWindow.getMessage("PlayerName"));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        playerLabel.setLayoutData(gd);

        playerCombo = new Combo(playerChooseGroup, SWT.READ_ONLY | SWT.BORDER);
        playerCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        playerCombo.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.FILL;
        playerCombo.setLayoutData(gd);
        playerCombo.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                onPlayerChanged();
            }
        });

        //        bApplyPlayer = new Button(playerChooseGroup, SWT.PUSH);
        //        bApplyPlayer.setText(MainWindow.getMessage("global.Apply"));
        //        bApplyPlayer.setImage(MainWindow.getSharedImage(ImageType.OK));
        //        gd = new GridData();
        //        gd.horizontalAlignment = SWT.CENTER;
        //        bApplyPlayer.setLayoutData(gd);
    }

    protected void onPlayerChanged() {

        int index = playerCombo.getSelectionIndex();
        Player p = null;
        if (index != -1) {
            String item = playerCombo.getItem(index);
            p = (Player) playerCombo.getData(item);
        }
        choosenPlayerInfo.setPlayer(p);
        recalcPriceDynamic();
    }

    protected void onTeamChanged() {

        int index = teamCombo.getSelectionIndex();
        if (index != -1) {
            fillPlayerCombo();
        }
        playerCombo.select(0);
        onPlayerChanged();
    }

    private void createPlayerDevGroup() {

        playerDevGroup = new Group(leftPanel, SWT.NONE);
        playerDevGroup.setText(MainWindow.getMessage("PlayerDevelopmentPlan"));
        //playerDevGroup.setToolTipText(MainWindow.getMessage("PlayersDiff.tooltip"));
        playerDevGroup.setLayout(new GridLayout(2, false));
        GridData gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        playerDevGroup.setLayoutData(gd);

        skillsLabel = new Label(playerDevGroup, SWT.NONE);
        skillsLabel.setText(MainWindow.getMessage("PlayerAbilities"));
        gd = new GridData();
        skillsLabel.setLayoutData(gd);

        devSkillsSpinner = new Spinner(playerDevGroup, SWT.BORDER);
        devSkillsSpinner.setToolTipText(MainWindow.getMessage("tooltip.Skills"));
        devSkillsSpinner.setValues(15, 0, 50, 0, 1, 1);
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        devSkillsSpinner.setLayoutData(gd);
        devSkillsSpinner.addSelectionListener(saRecalcDynamics);

        devExpLabel = new Label(playerDevGroup, SWT.NONE);
        devExpLabel.setText(MainWindow.getMessage("filter.experiance"));
        gd = new GridData();
        devExpLabel.setLayoutData(gd);

        devExpSpinner = new Spinner(playerDevGroup, SWT.BORDER);
        devExpSpinner.setValues(5, 0, 10, 0, 1, 1);
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        devExpSpinner.setLayoutData(gd);
        devExpSpinner.addSelectionListener(saRecalcDynamics);

        devStrengthLabel = new Label(playerDevGroup, SWT.NONE);
        devStrengthLabel.setText(MainWindow.getMessage("filter.strength"));
        gd = new GridData();
        devStrengthLabel.setLayoutData(gd);

        devStrengthSpinner = new Spinner(playerDevGroup, SWT.BORDER);
        devStrengthSpinner.setValues(5, 0, 10, 0, 1, 1);
        gd = new GridData();
        gd.grabExcessHorizontalSpace = true;
        gd.horizontalAlignment = SWT.CENTER;
        devStrengthSpinner.setLayoutData(gd);
        devStrengthSpinner.addSelectionListener(saRecalcDynamics);

    }

    /*
        private void createLikePlayersGroup() {

            likePlayersGroup = new Group(leftPanel, SWT.NONE);
            likePlayersGroup.setText(MainWindow.getMessage("LikePlayers"));
            likePlayersGroup.setToolTipText(MainWindow.getMessage("PlayersDiff.tooltip"));
            likePlayersGroup.setLayout(new GridLayout(2, false));
            GridData gd = new GridData();
            gd.grabExcessHorizontalSpace = true;
            gd.horizontalAlignment = SWT.FILL;
            likePlayersGroup.setLayoutData(gd);

            ageLabel = new Label(likePlayersGroup, SWT.NONE);
            ageLabel.setText(MainWindow.getMessage("filter.age"));
            gd = new GridData();
            ageLabel.setLayoutData(gd);

            ageSpinner = new Spinner(likePlayersGroup, SWT.BORDER);
            ageSpinner.setValues(1, 0, 10, 0, 1, 1);
            gd = new GridData();
            gd.grabExcessHorizontalSpace = true;
            gd.horizontalAlignment = SWT.CENTER;
            ageSpinner.setLayoutData(gd);
            ageSpinner.addSelectionListener(saRecalcDynamics);

            talentLabel = new Label(likePlayersGroup, SWT.NONE);
            talentLabel.setText(MainWindow.getMessage("filter.talent"));
            gd = new GridData();
            talentLabel.setLayoutData(gd);

            talentSpinner = new Spinner(likePlayersGroup, SWT.BORDER);
            talentSpinner.setValues(10, 0, 20, 0, 1, 1);
            gd = new GridData();
            gd.grabExcessHorizontalSpace = true;
            gd.horizontalAlignment = SWT.CENTER;
            talentSpinner.setLayoutData(gd);
            talentSpinner.addSelectionListener(saRecalcDynamics);

            strengthLabel = new Label(likePlayersGroup, SWT.NONE);
            strengthLabel.setText(MainWindow.getMessage("filter.strength"));
            gd = new GridData();
            strengthLabel.setLayoutData(gd);

            strengthSpinner = new Spinner(likePlayersGroup, SWT.BORDER);
            strengthSpinner.setValues(3, 0, 10, 0, 1, 1);
            gd = new GridData();
            gd.grabExcessHorizontalSpace = true;
            gd.horizontalAlignment = SWT.CENTER;
            strengthSpinner.setLayoutData(gd);
            strengthSpinner.addSelectionListener(saRecalcDynamics);

            expLabel = new Label(likePlayersGroup, SWT.NONE);
            expLabel.setText(MainWindow.getMessage("filter.experiance"));
            gd = new GridData();
            expLabel.setLayoutData(gd);

            expSpinner = new Spinner(likePlayersGroup, SWT.BORDER);
            expSpinner.setValues(50, 0, 50, 0, 1, 1);
            gd = new GridData();
            gd.grabExcessHorizontalSpace = true;
            gd.horizontalAlignment = SWT.CENTER;
            expSpinner.setLayoutData(gd);
            expSpinner.addSelectionListener(saRecalcDynamics);
            //likePlayersGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        }
    */
    private void createRightPanel() {
        rightPanel = new Composite(mainComposite, SWT.NONE);
        FormData fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(leftPanel, 0);
        fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        rightPanel.setLayoutData(fd);
        rightPanel.setLayout(new FormLayout());

        chart = new Chart(rightPanel, SWT.NONE);
        fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(0, 0);
        fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        chart.setLayoutData(fd);
        initChart();
    }

    private void initChart() {

        chart.getLegend().setPosition(SWT.BOTTOM);
        chart.getTitle().setForeground(BLACK);

        IAxisSet axisSet = chart.getAxisSet();

        IAxis xAxis = axisSet.getXAxis(0);
        xAxis.getTitle().setText(MainWindow.getMessage("filter.age"));
        xAxis.getTitle().setForeground(BLACK);

        IAxis yAxis = axisSet.getYAxis(0);
        yAxis.getTitle().setText(MainWindow.getMessage("filter.price"));
        yAxis.getTitle().setForeground(BLACK);

        int y2axisId = axisSet.createYAxis();
        IAxis yAxis2 = axisSet.getYAxis(y2axisId);
        yAxis2.setPosition(Position.Secondary);
        yAxis2.getTitle().setForeground(BLACK);
        yAxis2.getTitle().setText(MainWindow.getMessage("Quantity"));

        int y3axisId = axisSet.createYAxis();
        IAxis yAxis3 = axisSet.getYAxis(y3axisId);
        yAxis3.setPosition(Position.Secondary);
        yAxis3.getTitle().setForeground(BLACK);
        yAxis3.getTitle().setText(MainWindow.getMessage("filter.strength"));

        //series
        ILineSeries serie = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, SERIE_PLAYER_PRICE);
        serie.setDescription(MainWindow.getMessage("PricePrediction"));
        serie.setXAxisId(0);
        serie.setYAxisId(0);
        Color clr = Display.getDefault().getSystemColor(SWT.COLOR_RED);
        serie.setSymbolColor(clr);
        serie.setLineColor(clr);
        serie.setLineWidth(2);

        BarSeries barSerie = (BarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, SERIE_LIKE_PLAYERS);
        barSerie.setDescription(MainWindow.getMessage("LikePlayers"));
        barSerie.setXAxisId(0);
        barSerie.setYAxisId(y2axisId);

        serie = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, SERIE_STRENGTH);
        serie.setDescription(MainWindow.getMessage("filter.strength"));
        serie.setXAxisId(0);
        serie.setYAxisId(y3axisId);
        clr = Display.getDefault().getSystemColor(SWT.COLOR_BLUE);
        serie.setSymbolColor(clr);
        serie.setLineColor(clr);
    }

    public void updateView() {
        if (recalcSuspended) {
            recalcPriceDynamic();
        }
    }

    private int calcMethod = 1;

    private void recalcPriceDynamic() {
        if (!this.isVisible()) {
            recalcSuspended = true;
            return;
        }
        recalcSuspended = false;
        if (all == null || choosenPlayerInfo == null || choosenPlayerInfo.getPlayer() == null) {
            age = new double[0];
            price = new double[0];
            updateSeries();
            return;
        }
        //initSettingsFromUI();
        Player p = choosenPlayerInfo.getPlayer();
        int abilitiesTotal = calcMethod == 0 ? PlayerPrice.getPlayerAbilitiesTotal(choosenPlayerInfo.getPlayer()) : PlayerPrice
                .getPlayerAbilitiesTotalMinus20(choosenPlayerInfo.getPlayer());
        choosenPlayerInfo.setAbilitiesTotal(abilitiesTotal);
        String s = MainWindow.getMessage("PricePrediction") + " [" + p.getName() + "(" + p.getPosition().toString() + ") -> " + p.getAge() + "/"
                + p.getTalent() + "/" + p.getStrength() + "/" + GameUtils.getPlayerAbilitiesAsString(p) + "] " + MainWindow.getMessage("Round") + " "
                + p.getBirthtour();
        chart.getTitle().setText(s);
        int originalAge = p.getAge();
        int originalExp = p.getExperience();
        int originalStr = p.getStrength();
        int max_year = 41;
        int size = max_year - originalAge;
        if (size < 0) {
            max_year = max_year - size + 1;
            size = max_year - originalAge;//second try
        }
        double price1;
        double str;
        age = new double[size];
        price = new double[size];
        likePlayers = new double[size];
        strength = new double[size];
        int add_sv = devSkillsSpinner.getSelection();
        int add_exp = devExpSpinner.getSelection();
        int add_str = devStrengthSpinner.getSelection();
        //        int ageDiff = ageSpinner.getSelection();
        //        int talentDiff = talentSpinner.getSelection();
        //        int strengthDiff = strengthSpinner.getSelection();
        //        int expDiff = expSpinner.getSelection();
        int ageDiff = 3;
        int talentDiff = 15;
        int strengthDiff = 3;
        int expDiff = 20;

        double playersCount = 0;
        //debug
        //System.out.println(p.getName());
        for (int age1 = originalAge; age1 < max_year; age1++) {

            listCalcPlayers.clear();

            if (age1 == originalAge) {
                price1 = p.getPrice();
                str = p.getStrength();
            } else {

                p.setAge(age1);

                choosenPlayerInfo.setAbilitiesTotal(add_sv + choosenPlayerInfo.getAbilitiesTotal());
                p.setExperience(p.getExperience() + add_exp);
                double p_loss_str = 0.05 * (p.getStrength() + add_str) * (age1 - 26.0) / (double) p.getTalent();
                double loss_str = 30 * p_loss_str;
                if (loss_str < 0) {
                    loss_str = 0;
                }
                //debug
                //System.out.println("age,p,loss=" + age1 + "," + p_loss_str + "," + loss_str);
                str = p.getStrength() + add_str - Math.round(loss_str);
                p.setStrength((int) str);
                //limit strength with talent!!!
                if (p.getStrength() > p.getTalent() - 3) {
                    p.setStrength(p.getTalent() - 3);
                }
                if (calcMethod == 0) {
                    for (Team t: all.getTeams()) {
                        for (Player p2 : t.getPlayers()) {
                            if ((p != p2) && (PlayerPrice.isPlayersAreSimilar(p, p2, ageDiff, talentDiff, strengthDiff, expDiff))) {
                                listCalcPlayers.add(p2);
                            }
                        }
                    }
                    price1 = PlayerPrice.getPredictionPrice(choosenPlayerInfo, listCalcPlayers);
                    playersCount = listCalcPlayers.size();
                } else {
                    double[] res = PlayerPrice.getPredictionPriceFa13(choosenPlayerInfo, all, 1);
                    if (res != null) {
                        price1 = res[0];
                        playersCount = res[1];
                    } else {
                        price1 = playersCount = 0;
                    }
                }
            }
            age[age1 - originalAge] = age1;
            price[age1 - originalAge] = price1;
            likePlayers[age1 - originalAge] = playersCount;
            strength[age1 - originalAge] = p.getStrength();
        }
        //restore original values
        p.setAge(originalAge);
        p.setExperience(originalExp);
        p.setStrength(originalStr);
        choosenPlayerInfo.setAbilitiesTotal(PlayerPrice.getPlayerAbilitiesTotal(choosenPlayerInfo.getPlayer()));
        preProcessChartData();
        updateSeries();
    }

    private void preProcessChartData() {
        int zeroPriceCount = 0;
        for (double val : price) {
            if (val < 0.1) {
                zeroPriceCount++;
            }
        }
        int newSize = price.length - zeroPriceCount;
        if (newSize >= 0) {
            double newAge[] = new double[newSize];
            double newPrice[] = new double[newSize];
            double newLikeP[] = new double[newSize];
            double newStrength[] = new double[newSize];
            int j = 0;
            for (int i = 0; i < price.length; i++) {
                if (price[i] > 0.1) {
                    newAge[j] = age[i];
                    newPrice[j] = price[i];
                    newLikeP[j] = likePlayers[i];
                    newStrength[j] = strength[i];
                    j++;
                }
            }
            age = newAge;
            price = newPrice;
            likePlayers = newLikeP;
            strength = newStrength;
        }

    }

    private void updateSeries() {
        ISeries series = chart.getSeriesSet().getSeries(SERIE_PLAYER_PRICE);
        series.setXSeries(age);
        series.setYSeries(price);

        series = chart.getSeriesSet().getSeries(SERIE_LIKE_PLAYERS);
        series.setXSeries(age);
        series.setYSeries(likePlayers);

        series = chart.getSeriesSet().getSeries(SERIE_STRENGTH);
        series.setXSeries(age);
        series.setYSeries(strength);

        if (age.length > 0) {
            chart.setVisible(true);
            //            chart.getAxisSet().getXAxis(0).setRange(new Range(age[0], age[age.length - 1]));
            //            chart.getAxisSet().getYAxis(0).adjustRange();
            //            chart.getAxisSet().getYAxis(1).adjustRange();
            //            chart.getAxisSet().getYAxis(2).adjustRange();
            chart.getAxisSet().adjustRange();
            chart.redraw();
        } else {
            chart.setVisible(false);
        }

    }

    public TabItem asTabItem() {
        return mainItem;
    }

    public All getAll() {
        return all;
    }

    public void setAll(All all) {
        long start = System.currentTimeMillis();
        if (this.all != all) {
            this.all = all;
//            int initCapacity = all.getTeams() == null ? 0 : all.getTeams().size() * 18;
//            listAllPlayers = new ArrayList<Player>( initCapacity );
//            for (Team t : all.getTeams()) {
//                listAllPlayers.addAll(t.getPlayers());
//            }
            int selectionIndex = teamCombo.getSelectionIndex();
            String prevTeam = selectionIndex == -1 ? null : teamCombo.getItem(selectionIndex);

            selectionIndex = playerCombo.getSelectionIndex();
            String prevPlayer = selectionIndex == -1 ? null : playerCombo.getItem(selectionIndex);

            fillTeamCombo();
            //fillPlayerCombo();
            //restore previous selection
            if (prevTeam != null) {
                int teamIndex = GuiUtils.getComboItemIndex(teamCombo, prevTeam);
                if (teamIndex != -1) {
                    teamCombo.select(teamIndex);
                }
            } else {
                if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getCurrentTeam() != null) {
                    Team t = MainWindow.getAllInstance().getCurrentTeam();
                    GuiUtils.selectComboItem(teamCombo, t.getName());
                } else {
                    teamCombo.select(0);
                }
            }

            onTeamChanged();

            if (prevPlayer != null) {
                int playerIndex = GuiUtils.getComboItemIndex(playerCombo, prevPlayer);
                if (playerIndex != -1) {
                    playerCombo.select(playerIndex);
                }
            }

        }
        //System.out.println("playerPrice.setAll done in " + (System.currentTimeMillis() - start) );
    }

    private void fillPlayerCombo() {

        playerCombo.removeAll();
        int teamIndex = teamCombo.getSelectionIndex();
        if (teamIndex == -1 || all == null || all.getTeams() == null) {
            return;
        }

        String teamName = teamCombo.getItem(teamIndex);
        for (Team t : all.getTeams()) {
            if (t.getName().equalsIgnoreCase(teamName)) {
                for (Player p : t.getPlayers()) {
                    playerCombo.add(p.getName());
                    playerCombo.setData(p.getName(), p);
                }
            }
        }

    }

    private void fillTeamCombo() {

        teamCombo.removeAll();

        if (all == null || all.getTeams() == null) {
            return;
        }

        for (Team t : all.getTeams()) {
            teamCombo.add(t.getName());
            teamCombo.setData(t.getName(), t);
        }

    }

    private SelectionAdapter saRecalcDynamics = new SelectionAdapter() {
        public void widgetSelected(SelectionEvent e) {
            recalcPriceDynamic();
        };
    };

}