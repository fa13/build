package com.fa13.build.view.game;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.utils.GameUtils;
import com.fa13.build.view.DaysOfRestComposite;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.SpecialRoleEditor;
import com.fa13.build.view.TeamUIItem;
import com.fa13.build.view.UIItem;

public class GameFormEditView implements TeamUIItem, UIItem, IDisposable, IStorable {

    public static final int SQUARD_CAPACITY = 11;
    public static final int SUBSTITUTION_SQUARD_CAPACITY = 7;
    public static double TEAMWORK_STRENGTH_MAX_PERCENT = 12.5d;
    public static String PROP_COUNTRY_CODE = "countryCode";
    public static String PROP_FLAG = "flag";
    public static String PROP_CARD = "card";

    Composite parentInstance;
    Composite leftPanel;
    ScrolledComposite rightPanelScrl;
    Composite rightPanel;
    Table playersTable;
    Table matchPlayersTable;
    Table matchPlayersTable2;
    boolean homeBonus;
    boolean isSchemeUpdate = true;
    static Color colors[] = null;
    static Font fonts[] = null;
    public static Image yellowCards[] = null;
    public static Image redCards[] = null;
    int daysOfRest;
    TableEditor matchPlayersTableEditor;
    public static Map<String, Image> flags = null;
    SpecialRoleEditor specialRoleEditor;
    DaysOfRestComposite daysOfRestComposite;
    Button homeBonusButton;
    Label homeBonusLabel;

    static Listener paintListener;
    static String[] titles;

    private SchemeControlPanel schemeControlPanel;
    private SchemesModel schemesModel;
    private Label squardRealStrengthLabel;
    private Label squardRealStrengthValue;
    private Label lAvgTeamWork;
    private Label lAvgTeamWorkValue;
    private Properties props;

    public GameFormEditView(Composite parent, SchemesModel schemesModel, Properties props) {

        this.schemesModel = schemesModel;
        parentInstance = parent;
        parent.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                // store settings before widgets disposed
                //name;height;style
                FontData fd = fonts[0].getFontData()[0];
                squardFontData = fd.getName();
                squardFontData += ";" + fd.getHeight();
                squardFontData += ";" + fd.getStyle();

            }
        });
        this.props = props;
        if (colors == null) {
            colors = new Color[11];
            colors[0] = new Color(parent.getDisplay(), 0, 0, 0);
            colors[1] = new Color(parent.getDisplay(), 0, 0, 192);
            colors[2] = new Color(parent.getDisplay(), 0, 192, 0);
            colors[3] = new Color(parent.getDisplay(), 0, 0, 0);
            colors[4] = new Color(parent.getDisplay(), 0, 0, 192);
            colors[5] = new Color(parent.getDisplay(), 0, 192, 0);
            colors[6] = new Color(parent.getDisplay(), 50, 50, 50);
            colors[7] = new Color(parent.getDisplay(), 50, 50, 128);
            colors[8] = new Color(parent.getDisplay(), 50, 128, 50);
            colors[9] = new Color(parent.getDisplay(), 255, 0, 0);
            colors[10] = new Color(parent.getDisplay(), 250, 250, 0);
        }
        String storedFontData = props.getProperty(MainWindow.PROP_SQUARD_FONT_DATA);
        FontData fd;
        if (storedFontData == null || storedFontData.isEmpty()) {
            fd = parentInstance.getFont().getFontData()[0];
        } else {
            try {
                String ss[] = storedFontData.split(";");
                //name;height;style
                fd = new FontData(ss[0], Integer.parseInt(ss[1]), Integer.parseInt(ss[2]));
            } catch (Exception e) {
                fd = parentInstance.getFont().getFontData()[0];
            }
        }
        createFonts(fd);

        parent.setLayout(new FormLayout());

        leftPanel = new Composite(parent, SWT.NONE);

        leftPanel.setLayout(new FillLayout());

        rightPanelScrl = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
        rightPanelScrl.setExpandHorizontal(false);
        rightPanelScrl.setExpandVertical(true);
        rightPanel = new Composite(rightPanelScrl, SWT.NONE);
        rightPanelScrl.setContent(rightPanel);

        FormData data = new FormData();

        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(rightPanelScrl, -5);
        leftPanel.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(100, 0);
        rightPanelScrl.setLayoutData(data);

        data = new FormData();
        rightPanel.setLayoutData(data);
        rightPanel.setLayout(new FormLayout());

        playersTable = new Table(leftPanel, SWT.BORDER | SWT.FULL_SELECTION);
        playersTable.setHeaderVisible(true);
        playersTable.setLinesVisible(true);
        playersTable.setHeaderVisible(true);

        String[] defaultTitles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerAge"),
                MainWindow.getMessage("PlayerTalant"), MainWindow.getMessage("PlayerExperience"), MainWindow.getMessage("PlayerFitness"),
                MainWindow.getMessage("PlayerMorale"), MainWindow.getMessage("PlayerStrength"), MainWindow.getMessage("PlayerRealStrength"),
                MainWindow.getMessage("PlayerHealth"), MainWindow.getMessage("PlayerRest"), MainWindow.getMessage("PlayerCards"),
                MainWindow.getMessage("PlayerAbilities"), MainWindow.getMessage("PlayerTeamwork") };

        String[] defaultTooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("tooltip.Age"),
                MainWindow.getMessage("tooltip.Talent"), MainWindow.getMessage("tooltip.Experience"), MainWindow.getMessage("tooltip.Physics"),
                MainWindow.getMessage("tooltip.Morale"), MainWindow.getMessage("tooltip.Strength"), MainWindow.getMessage("tooltip.RealStrength"),
                MainWindow.getMessage("tooltip.Health"), MainWindow.getMessage("tooltip.RestDays"), MainWindow.getMessage("PlayerCards"),
                MainWindow.getMessage("tooltip.Skills"), MainWindow.getMessage("tooltip.Teamwork"), };

        titles = defaultTitles;

        for (int i = 0; i < titles.length; i++) {
            TableColumn column = new TableColumn(playersTable, SWT.NONE);
            column.setText(titles[i]);
            column.setToolTipText(defaultTooltips[i]);
            column.pack();
            column.setResizable(true);
        }

        playersTable.getColumn(3).pack();

        schemeControlPanel = new SchemeControlPanel(rightPanel, schemesModel);

        data = new FormData();
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        schemeControlPanel.setLayoutData(data);

        schemeControlPanel.getSchemeCombo().addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                Combo combo = (Combo) e.widget;
                if (isSchemeUpdate) {
                    GameEditView.copyScheme(GameForm.getInstance().getFirstTeam(), (PlayerPosition[]) combo.getData(combo.getText()));
                }
                GameForm.getInstance().setSelectedSchemeIndex(combo.getSelectionIndex());
                redrawForm();
            }
        });

        matchPlayersTable = new Table(rightPanel, SWT.BORDER | SWT.FULL_SELECTION);
        matchPlayersTable.setHeaderVisible(true);
        matchPlayersTable.setLinesVisible(true);
        matchPlayersTable.setHeaderVisible(true);

        String[] titlesMatch = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("Standards") };

        String[] titlesBench = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                "   " };

        for (int i = 0; i < titlesMatch.length; i++) {
            TableColumn column = new TableColumn(matchPlayersTable, SWT.NONE);
            column.setText("1234");
            column.pack();
            column.setText(titlesMatch[i]);
            column.setResizable(false);
        }
        matchPlayersTable.setItemCount(SQUARD_CAPACITY);

        for (int i = 0; i < matchPlayersTable.getColumnCount(); i++) {
            matchPlayersTable.getColumn(i).pack();
        }

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        Point size = matchPlayersTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.right = new FormAttachment(matchPlayersTable, size.x);
        data.top = new FormAttachment(schemeControlPanel, 4);
        matchPlayersTable.setLayoutData(data);

        matchPlayersTable2 = new Table(rightPanel, SWT.BORDER | SWT.FULL_SELECTION);
        matchPlayersTable2.setHeaderVisible(true);
        matchPlayersTable2.setLinesVisible(true);
        matchPlayersTable2.setHeaderVisible(true);

        for (int i = 0; i < titlesBench.length; i++) {
            TableColumn column = new TableColumn(matchPlayersTable2, SWT.NONE);
            column.setText("1234");
            column.pack();
            column.setText(titlesBench[i]);
            column.setResizable(false);
        }
        matchPlayersTable2.setItemCount(SUBSTITUTION_SQUARD_CAPACITY);

        for (int i = 0; i < matchPlayersTable2.getColumnCount(); i++) {
            matchPlayersTable2.getColumn(i).pack();
        }

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(matchPlayersTable, 0);
        matchPlayersTable2.setLayoutData(data);

        setDragDropSource(playersTable);
        setDragDropSource(matchPlayersTable);
        setDragDropSource(matchPlayersTable2);
        setDragDropTarget(matchPlayersTable);
        setDragDropTarget(matchPlayersTable2);

        daysOfRestComposite = new DaysOfRestComposite(rightPanel, SWT.NONE, this);
        data = new FormData();
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.right = new FormAttachment(100, -rightPanelScrl.getVerticalBar().getSize().x);
        data.top = new FormAttachment(matchPlayersTable2, 10);
        daysOfRestComposite.setLayoutData(data);

        homeBonusButton = new Button(rightPanel, SWT.CHECK);
        data = new FormData();
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.top = new FormAttachment(daysOfRestComposite, 10);
        homeBonusButton.setLayoutData(data);

        homeBonusButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                homeBonus = homeBonusButton.getSelection();
                redraw();
            }
        });

        homeBonusLabel = new Label(rightPanel, SWT.NONE);
        data = new FormData();
        data.left = new FormAttachment(homeBonusButton, 5);
        data.top = new FormAttachment(daysOfRestComposite, 10);
        homeBonusLabel.setLayoutData(data);
        homeBonusLabel.setText(MainWindow.getMessage("homeBonus"));

        squardRealStrengthLabel = new Label(rightPanel, SWT.NONE);
        data = new FormData();
        data.left = new FormAttachment(homeBonusButton, -10);
        data.top = new FormAttachment(homeBonusLabel, 10);
        squardRealStrengthLabel.setLayoutData(data);
        squardRealStrengthLabel.setText(MainWindow.getMessage("strength.average"));
        squardRealStrengthLabel.setToolTipText(MainWindow.getMessage("strength.average.tooltip"));

        squardRealStrengthValue = new Label(rightPanel, SWT.NONE);
        squardRealStrengthValue.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        data = new FormData();
        data.left = new FormAttachment(squardRealStrengthLabel, 5);
        data.top = new FormAttachment(homeBonusLabel, 10);
        data.width = 70;
        squardRealStrengthValue.setLayoutData(data);
        squardRealStrengthValue.setAlignment(SWT.CENTER);
        //squardRealStrengthValue.setText("56.7");
        squardRealStrengthValue.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        squardRealStrengthValue.setToolTipText(MainWindow.getMessage("strength.average.tooltip"));

        lAvgTeamWork = new Label(rightPanel, SWT.NONE);
        lAvgTeamWork.setText(MainWindow.getMessage("teamwork.average"));
        data = new FormData();
        data.left = new FormAttachment(homeBonusButton, -10);
        data.top = new FormAttachment(squardRealStrengthLabel, 5);
        lAvgTeamWork.setLayoutData(data);

        lAvgTeamWorkValue = new Label(rightPanel, SWT.NONE);
        data = new FormData();
        data.left = new FormAttachment(squardRealStrengthLabel, 5);
        data.top = new FormAttachment(squardRealStrengthLabel, 5);
        data.width = 70;
        lAvgTeamWorkValue.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        lAvgTeamWorkValue.setLayoutData(data);
        lAvgTeamWorkValue.setAlignment(SWT.CENTER);

        Map<String, Image> tmp = new HashMap<String, Image>();
        Set<Map.Entry<String, String>> nations = Player.NATIONALITIES.entrySet();

        if (flags == null) {
            for (Iterator<Map.Entry<String, String>> iterator = nations.iterator(); iterator.hasNext();) {
                Map.Entry<String, String> currEntry = iterator.next();
                String curr = currEntry.getValue();
                Image img = new Image(parent.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/flags/" + curr + ".png"));
                tmp.put(curr, img);
            }
            flags = Collections.unmodifiableMap(tmp);
        }

        final Image img = new Image(parent.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/flags/not.png"));
        if (yellowCards == null) {
            yellowCards = new Image[3];
            for (int i = 0; i < 3; i++) {
                yellowCards[i] = new Image(parent.getDisplay(),
                        this.getClass().getResourceAsStream("/com/fa13/build/resources/cards/yellow" + String.valueOf(i) + ".png"));
            }
        }

        if (redCards == null) {
            redCards = new Image[5];
            for (int i = 0; i < 5; i++) {
                redCards[i] = new Image(parent.getDisplay(),
                        this.getClass().getResourceAsStream("/com/fa13/build/resources/cards/red" + String.valueOf(i) + ".png"));
            }
        }
        paintListener = new Listener() {
            public void handleEvent(Event event) {
                switch (event.type) {
                    case SWT.MeasureItem: {
                        if (event.index == 3) {
                            Rectangle rect = img.getBounds();
                            event.width = rect.width;
                            event.height = Math.max(event.height, rect.height);
                        } else if (event.index == 13) {
                            Rectangle rect = yellowCards[0].getBounds();
                            event.width = rect.width;
                            event.height = Math.max(event.height, rect.height);
                        } else {
                            TableItem item = (TableItem) event.item;
                            String text = item.getText(event.index);
                            int size = text.length();
                            text = "";
                            for (int i = 0; i < size + 1; i++) {
                                text += "A";
                            }
                            Point extent = event.gc.stringExtent(text);
                            event.width = Math.max(event.width, extent.x + 6);
                        }
                        break;
                    }
                    case SWT.PaintItem: {
                        if (event.index == 3) {

                            Rectangle rect = img.getBounds();
                            //int w = Math.max(event.width, rect.width);
                            //int xoffset = Math.max(0, (rect.width - w) / 2);
                            int yoffset = Math.max(0, (rect.height - event.height) / 2);
                            Image flag = (Image) event.item.getData(PROP_FLAG);
                            if (flag != null) {
                                event.gc.drawImage(flag, event.x + (playersTable.getColumn(3).getWidth() - rect.width) / 2, event.y + yoffset);
                                //event.gc.drawImage(flag, xoffset, yoffset, w, Math.min(rect.height, event.height),
                                //        event.x + (playersTable.getColumn(3).getWidth() - rect.width) / 2, event.y, w, event.height);
                            }
                        }
                        if (event.index == 13) {
                            Rectangle rect = yellowCards[0].getBounds();
                            //int w = Math.max(event.width, rect.width);
                            //int h = Math.max(event.height, rect.height);
                            //int xoffset = Math.max(0, (rect.width - w) / 2);
                            int yoffset = Math.max(0, (rect.height - event.height) / 2);
                            Image card = (Image) event.item.getData(PROP_CARD);
                            if (card != null) {
                                event.gc.drawImage(card, event.x + (playersTable.getColumn(13).getWidth() - rect.width) / 2, event.y + yoffset);
                                //event.gc.drawImage(card, xoffset, yoffset, w, Math.min(rect.height, event.height),
                                //        event.x, event.y, w, event.height);
                            }
                        }
                        break;
                    }
                }
            }
        };

        playersTable.addListener(SWT.PaintItem, paintListener);
        playersTable.addListener(SWT.MeasureItem, paintListener);
        playersTable.addListener(SWT.MouseHover, new Listener() {
            @Override
            public void handleEvent(Event event) {
                onPlayersTableMouseHover(event);
            }
        });

        playersTable.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(playersTable);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("Font"));
                item.addSelectionListener(new SelectionAdapter() {

                    public void widgetSelected(SelectionEvent e) {
                        //change font
                        FontDialog dlg = new FontDialog(parentInstance.getShell());
                        dlg.setFontList(fonts[0].getFontData());
                        FontData fontData = dlg.open();
                        if (fontData != null) {
                            createFonts(fontData);
                        }
                        redraw();

                    }
                });

                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        playersTable.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseDoubleClick(MouseEvent e) {
                onTableDoubleClick(playersTable, playersTable.getSelection());
            }
        });
        matchPlayersTable.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                onTableDoubleClick(matchPlayersTable, matchPlayersTable.getSelection());
            }
        });
        matchPlayersTable2.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                onTableDoubleClick(matchPlayersTable2, matchPlayersTable2.getSelection());
            }
        });

        matchPlayersTableEditor = new TableEditor(matchPlayersTable);
        matchPlayersTableEditor.horizontalAlignment = SWT.LEFT;
        matchPlayersTableEditor.grabHorizontal = true;
        matchPlayersTableEditor.minimumWidth = 35;
        //matchPlayersTable.addListener(SWT.MouseDown, new MatchPlayersTableEditListener());
        matchPlayersTable.addMouseListener(maMatchPlayers);

        specialRoleEditor = new SpecialRoleEditor(matchPlayersTable.getDisplay(), matchPlayersTable.getShell(), null, null, true);

        updateAll();
        redrawForm();
    }

    private void createFonts(FontData baseFontData) {
        if (fonts != null) {
            for (Font font : fonts) {
                if (font != null) {
                    font.dispose();
                }
            }
        }
        fonts = new Font[11];
        fonts[0] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[1] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[2] = new Font(parentInstance.getDisplay(), baseFontData);
        baseFontData.setStyle(SWT.BOLD);
        fonts[3] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[4] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[5] = new Font(parentInstance.getDisplay(), baseFontData);

        baseFontData.setStyle(SWT.BOLD | SWT.ITALIC);
        fonts[6] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[7] = new Font(parentInstance.getDisplay(), baseFontData);
        fonts[8] = new Font(parentInstance.getDisplay(), baseFontData);

        baseFontData.setStyle(SWT.ITALIC);
        fonts[9] = new Font(parentInstance.getDisplay(), baseFontData);

        //fontData.setHeight(9);
        baseFontData.setStyle(SWT.BOLD | SWT.ITALIC);
        fonts[10] = new Font(parentInstance.getDisplay(), baseFontData);
    }

    private void onPlayersTableMouseHover(Event event) {
        Point point = new Point(event.x, event.y);
        TableItem item = playersTable.getItem(point);
        if (item == null) {
            return;
        }
        int column = -1;
        for (int i = 0; i < playersTable.getColumnCount(); i++) {
            if (item.getBounds(i) != null && item.getBounds(i).contains(point)) {
                column = i;
                break;
            }
        }
        if (column == 3) {
            String countryCode = (String) item.getData(PROP_COUNTRY_CODE);
            String countryName = countryCode == null ? "" : MainWindow.getMessage("country." + countryCode);
            playersTable.setToolTipText(countryName);
        } else {
            playersTable.setToolTipText("");
        }
    }

    public void redrawAveragePlayersStrength() {
        if (MainWindow.getAllInstance() == null) {
            return;
        }
        Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
        if (curTeam == null) {
            return;
        }
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();

        double averageStrength = 0;
        double averageBaseStrength = 0;
        double avgTeamWork = 0;
        int playersCount = 0;
        boolean gkInSquard = false;
        for (int i = 0; i < playerPositions.length; i++) {
            if (playerPositions[i] != null) {
                Player player = null;
                if (curTeam != null) {
                    player = curTeam.getPlayerByNumber(playerPositions[i].getNumber());
                    if (player != null) {
                        if (playerPositions[i].getAmplua() != PlayerAmplua.GK) {
                            averageStrength += (1 + player.getTeamwork() * TEAMWORK_STRENGTH_MAX_PERCENT / 10000.0) * player.getStrength()
                                    * player.getFitness(daysOfRest) * player.getMorale();
                            averageBaseStrength += player.getStrength();
                        } else {
                            gkInSquard = true;
                        }
                        avgTeamWork += player.getTeamwork();
                        playersCount++;
                    }
                }
            }
        }
        if (playersCount == 0) {
            averageStrength = 0.0;
            averageBaseStrength = 0.0;
            avgTeamWork = 0.0;
        } else {
            int sPlayersCount = gkInSquard ? playersCount - 1 : playersCount;
            if (sPlayersCount == 0) {
                averageStrength = 0.0;
                averageBaseStrength = 0.0;
            } else {
                averageStrength /= sPlayersCount;
                //from fitness + morale = normalization
                averageStrength /= 10000.0;
                averageBaseStrength /= sPlayersCount;
            }
            avgTeamWork /= playersCount;
        }

        if (homeBonus) {
            //averageStrength *= 1.1;
            averageStrength *= 1.0 + (0.04 + (GameForm.getInstance().getPrice() - 4.0) / 100.0);
        }

        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(1);
        try {
            squardRealStrengthValue.setText(nf.format(averageBaseStrength) + " / " + nf.format(averageStrength));
            lAvgTeamWorkValue.setText(nf.format(avgTeamWork));
        } catch (Exception e) {
            //skip error
        }
        squardRealStrengthValue.pack(true);
        lAvgTeamWorkValue.pack(true);
    }

    public void updateAll() {
        redraw();
    }

    public void updateDaysOfRest(int newVal) {
        this.daysOfRest = newVal;
        redrawRest();
    }

    public int getPlayerTypeGameForm(int playerNumber) {
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        for (PlayerPosition playerPosition : playerPositions) {
            if (playerPosition.getNumber() == playerNumber) {
                Player player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(playerNumber);
                if (player.getFitness(daysOfRest) < 70) {
                    return 5;
                }
                if (player.getFitness(daysOfRest) < 90) {
                    return 4;
                }
                return 3;
            }
        }
        int playerSubstitutions[] = GameForm.getInstance().getBench();
        for (int i = 0; i < playerSubstitutions.length; i++) {
            if (playerSubstitutions[i] == playerNumber) {
                Player player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(playerNumber);
                if (player.getFitness(daysOfRest) < 70) {
                    return 8;
                }
                if (player.getFitness(daysOfRest) < 90) {
                    return 7;
                }
                return 6;
            }
        }
        Player player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(playerNumber);

        if (player.getDisqualification() > 0) {
            return 9;
        }
        if (player.getFitness(daysOfRest) < 70) {
            return 2;
        }
        if (player.getFitness(daysOfRest) < 90) {
            return 1;
        }
        return 0;
    }

    public void redraw() {
        if (MainWindow.getAllInstance() != null) {
            playersTable.setRedraw(false);
            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            List<Player> curPlayers = curTeam.getPlayers();
            playersTable.removeAll();
            playersTable.setItemCount(curPlayers.size());
            int i = 0;

            for (Player player : curPlayers) {
                TableItem item = playersTable.getItem(i);
                int index = 0;
                int type = getPlayerTypeGameForm(player.getNumber());
                item.setForeground(colors[type]);
                item.setFont(fonts[type]);
                item.setText(index++, String.valueOf(player.getNumber()));
                item.setText(index++, player.getPosition().toString());
                item.setText(index++, player.getName());
                item.setText(index++, "");
                item.setData(PROP_COUNTRY_CODE, player.getNationalityCode());
                item.setData(PROP_FLAG, flags.get(player.getNationalityCode()));
                item.setText(index++, String.valueOf(player.getAge()));
                item.setText(index++, String.valueOf(player.getTalent()));
                item.setText(index++, String.valueOf(player.getExperience()));
                item.setBackground(index, MainWindow.getSharedColorResource(ColorResourceType.LIGHT_YELLOW));
                item.setText(index++, String.valueOf(player.getFitness(daysOfRest)));
                item.setText(index++, String.valueOf(player.getMorale()));
                item.setText(index++, String.valueOf(player.getStrength()));
                double realStrength = player.getRealStrength(homeBonus, GameForm.getInstance().getPrice(), daysOfRest);
                if (player.getDisqualification() == 0) {
                    item.setText(index++, String.format("%.1f", realStrength));
                } else {
                    item.setText(index++, MainWindow.getMessage("PlayerDisqualified"));
                }
                item.setText(index++, String.valueOf(player.getHealth()));
                String rest = "";
                int restDays = player.getRest() + daysOfRest;
                rest = String.valueOf(restDays);
                item.setText(index++, rest);
                if (player.getDisqualification() > 0) {
                    //hotfix
                    int imgIndex = player.getDisqualification();
                    if (imgIndex > 4) {
                        imgIndex = 0;
                    }
                    item.setData(PROP_CARD, redCards[imgIndex]);
                } else if (player.getYellowCards() % 3 != 0) {
                    int imgIndex = player.getYellowCards() % 3;
                    if (imgIndex > 2) {
                        imgIndex = 0;
                    }
                    item.setData(PROP_CARD, yellowCards[imgIndex]);
                }
                index++;
                item.setText(index++, GameUtils.getPlayerAbilitiesAsString(player));
                item.setText(index++, String.valueOf(player.getTeamwork()));
                colorizeSquardPlayer(item, player, index - 1);
                i++;
            }

            for (i = 0; i < titles.length; i++) {
                TableColumn column = playersTable.getColumn(i);
                column.setText(titles[i]);
                column.pack();

                column.setResizable(true);
            }
            playersTable.getParent().layout();
            playersTable.setRedraw(true);
        } else {
            playersTable.setItemCount(0);
        }
        redrawForm();
    }

    private void colorizeSquardPlayer(TableItem item, Player player, int columnsToFill) {
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();

        if (playerPositions != null && playerPositions.length > 0) {
            for (PlayerPosition pp : playerPositions) {
                if (pp.getNumber() == player.getNumber()) {
                    //for (int j=0; j<columnsToFill;j++) {
                    if (player.getDisqualification() > 0 || player.getFitness(daysOfRest) < 65) {
                        item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.LIGHT_RED));
                    } else {
                        item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.SQUARD));
                    }
                    break;
                    //}
                }
            }
        }

        int[] bench = GameForm.getInstance().getBench();

        if (bench != null && bench.length > 0) {
            for (int number : bench) {
                if (number == player.getNumber()) {
                    if (player.getDisqualification() > 0 || player.getFitness(daysOfRest) < 65) {
                        item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.LIGHT_RED));
                    } else {
                        item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.BENCH));
                    }
                    break;
                }
            }
        }

    }

    /*
        private String getAbility(Player player) {
            StringBuilder result = new StringBuilder();
            if (player.getShooting() > 20) {
                result.append(MainWindow.getMessage("PTshooting").toLowerCase()).append(player.getShooting());
            }
            if (player.getPassing() > 20) {
                result.append(MainWindow.getMessage("PTpassing").toLowerCase()).append(player.getPassing());
            }
            if (player.getCross() > 20) {
                result.append(MainWindow.getMessage("PTcross").toLowerCase()).append(player.getCross());
            }
            if (player.getDribbling() > 20) {
                result.append(MainWindow.getMessage("PTdribbling").toLowerCase()).append(player.getDribbling());
            }
            if (player.getTackling() > 20) {
                result.append(MainWindow.getMessage("PTtackling").toLowerCase()).append(player.getTackling());
            }
            if (player.getSpeed() > 20) {
                result.append(MainWindow.getMessage("PTspeed").toLowerCase()).append(player.getSpeed());
            }
            if (player.getStamina() > 20) {
                result.append(MainWindow.getMessage("PTstamina").toLowerCase()).append(player.getStamina());
            }
            if (player.getHeading() > 20) {
                result.append(MainWindow.getMessage("PTheading").toLowerCase()).append(player.getHeading());
            }
            if (player.getReflexes() > 20) {
                result.append(MainWindow.getMessage("PTreflexes").toLowerCase()).append(player.getReflexes());
            }
            if (player.getHandling() > 20) {
                result.append(MainWindow.getMessage("PThandling").toLowerCase()).append(player.getHandling());
            }
    
            return result.toString();
        }
    */
    public void redrawRest() {
        if (MainWindow.getAllInstance() != null) {
            playersTable.setRedraw(false);
            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            List<Player> curPlayers = curTeam.getPlayers();
            int i = 0;

            for (Player player : curPlayers) {
                TableItem item = playersTable.getItem(i);
                int index = 0;

                int type = getPlayerTypeGameForm(player.getNumber());
                item.setForeground(colors[type]);
                item.setFont(fonts[type]);

                index += 7;
                item.setText(index++, String.valueOf(player.getFitness(daysOfRest)));
                index += 2;
                //double realStrength = player.getRealStrength(homeBonus, daysOfRest);
                double realStrength = player.getRealStrength(homeBonus, GameForm.getInstance().getPrice(), daysOfRest);
                if (player.getDisqualification() == 0) {
                    item.setText(index++, String.format("%.1f", realStrength));
                } else {
                    item.setText(index++, MainWindow.getMessage("PlayerDisqualified"));
                }
                i++;
                index += 1;
                String rest = "";
                int restDays = player.getRest() + daysOfRest;
                rest = String.valueOf(restDays);
                item.setText(index++, rest);
            }

            int index = 0;
            index += 7;
            playersTable.getColumn(index++).pack();
            index += 2;
            playersTable.getColumn(index++).pack();
            index += 1;
            playersTable.getColumn(index++).pack();

            playersTable.setRedraw(true);
            redrawAveragePlayersStrength();
        }

    }

    public void setDragDropSource(final Table table) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        final DragSource source = new DragSource(table, operations);
        source.setTransfer(types);
        source.addDragListener(new DragSourceListener() {
            public void dragStart(DragSourceEvent event) {
                TableItem items[] = table.getSelection();

                if (items.length <= 0) {
                    event.doit = false;
                    return;
                }
                if (items[0].getText().length() == 0) {
                    event.doit = false;
                    return;
                }
                int number = Integer.valueOf(items[0].getText());
                if (MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(number).getDisqualification() > 0) {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {
                TableItem items[] = table.getSelection();
                StringBuilder sb = new StringBuilder();
                for (TableItem item : items) {
                    for (int j = 0; j < table.getColumnCount(); j++) {
                        sb.append(item.getText(j));
                        sb.append("/");
                    }
                    sb.append("\n");
                }
                event.data = sb.toString();
            }

            public void dragFinished(DragSourceEvent event) {

            }
        });
    }

    public void setDragDropTarget(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        Transfer types[] = new Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    TableItem item = null;
                    if (event.item instanceof TableItem) {
                        String text = (String) event.data;
                        item = (TableItem) event.item;
                        if ((Integer) item.getData("type") == 0) {
                            setPlayer(item, text);
                        }
                        if ((Integer) item.getData("type") == 1) {
                            setPlayerSubstitution(item, text);
                        }
                    }
                }
            }
        });

    }

    public void setPlayer(TableItem item, String playerData) {
        if (playerData == null) {
            return;
        }
        String playerItems[] = playerData.split("/");
        Integer number = (Integer) item.getData("number");
        int playerNumber;
        try {
            playerNumber = Integer.valueOf(playerItems[0]);
        } catch (NumberFormatException ex) {
            return;
        }
        
        clearPlayerFromStartupLine(playerNumber);
        clearPlayerFromSubstitutions(playerNumber);
        
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
//        for (PlayerPosition playerPosition : playerPositions) {
//            if (playerPosition.getNumber() == playerNumber) {
//                playerPosition.reset();
//            }
//        }
//        int playerSubstitutions[] = GameForm.getInstance().getBench();
//        for (int i = 0; i < playerSubstitutions.length; i++) {
//            if (playerSubstitutions[i] == playerNumber) {
//                playerSubstitutions[i] = 0;
//            }
//        }
        if (number < playerPositions.length && number >= 0) {
            playerPositions[number].setNumber(playerNumber);
        }
        redraw();
    }

    public void setPlayerSubstitution(TableItem item, String playerData) {
        if (playerData == null) {
            return;
        }
        String playerItems[] = playerData.split("/");
        Integer number = (Integer) item.getData("number");
        int playerNumber;
        try {
            playerNumber = Integer.valueOf(playerItems[0]);
        } catch (NumberFormatException ex) {
            return;
        }
        clearPlayerFromStartupLine(playerNumber);
        clearPlayerFromSubstitutions(playerNumber);
        
//        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
//        for (int i = 0; i < playerPositions.length; i++) {
//            if (playerPositions[i].getNumber() == playerNumber) {
//                playerPositions[i].reset();
//            }
//        }
        int playerSubstitutions[] = GameForm.getInstance().getBench();
//        for (int i = 0; i < playerSubstitutions.length; i++) {
//            if (playerSubstitutions[i] == playerNumber) {
//                playerSubstitutions[i] = 0;
//            }
//        }
        if (number < playerSubstitutions.length && number >= 0) {
            playerSubstitutions[number] = playerNumber;
        }
        redraw();
    }

    public void redrawForm() {
        if (matchPlayersTable == null || matchPlayersTable2 == null) {
            return;
        }
        matchPlayersTable.setRedraw(false);
        matchPlayersTable2.setRedraw(false);
        Team curTeam = null;
        if (MainWindow.getAllInstance() != null) {
            curTeam = MainWindow.getAllInstance().getCurrentTeam();
        }
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        int playerSubstitution[] = GameForm.getInstance().getBench();
        //TODO check is this right?
        //TODO remove magic numbers!!!
        int itemsCount = Math.max(SQUARD_CAPACITY, playerPositions.length);
        matchPlayersTable.setItemCount(itemsCount);
        matchPlayersTable2.setItemCount(SUBSTITUTION_SQUARD_CAPACITY);
        for (int i = 0; i < itemsCount; i++) {
            TableItem item = matchPlayersTable.getItem(i);
            item.setData("number", new Integer(i));
            item.setData("type", new Integer(0));
        }
        //substitutions
        for (int i = 0; i < matchPlayersTable2.getItemCount(); i++) {
            TableItem item = matchPlayersTable2.getItem(i);
            item.setData("number", new Integer(i));
            item.setData("type", new Integer(1));
        }

        for (int i = 0; i < matchPlayersTable.getItemCount(); i++) {
            TableItem item = matchPlayersTable.getItem(i);
            for (int index = 0; index < matchPlayersTable.getColumnCount(); index++) {
                item.setText(index, "");
            }
        }
        for (int i = 0; i < matchPlayersTable2.getItemCount(); i++) {
            TableItem item = matchPlayersTable2.getItem(i);
            for (int index = 0; index < matchPlayersTable2.getColumnCount(); index++) {
                item.setText(index, "");
            }
        }

        for (int i = 0; i < playerPositions.length; i++) {
            TableItem item = matchPlayersTable.getItem(i);
            int index = 0;
            if (playerPositions[i] != null) {
                Player player = null;
                if (curTeam != null) {
                    player = curTeam.getPlayerByNumber(playerPositions[i].getNumber());
                }
                if (player != null) {
                    item.setText(index++, String.valueOf(player.getNumber()));
                } else {
                    index++;
                }
                item.setText(index++, playerPositions[i].getAmplua().toString());
                if (player != null) {
                    item.setText(index++, player.getName());
                } else {
                    index++;
                }
                item.setText(index++, printSRflags(playerPositions[i]));
            }
        }
        for (int i = 0; i < playerSubstitution.length; i++) {
            TableItem item = matchPlayersTable2.getItem(i);
            int index = 0;
            Player player = null;
            if (curTeam != null) {
                player = curTeam.getPlayerByNumber(playerSubstitution[i]);
            }
            if (player != null) {
                item.setText(index++, String.valueOf(player.getNumber()));
                item.setText(index++, player.getPosition().toString());
                item.setText(index++, player.getName());
            }
        }

        int width = 0;
        for (int i = 0; i < matchPlayersTable.getColumnCount(); i++) {
            int widthColum = 0;
            matchPlayersTable.getColumn(i).pack();
            int dWidth = matchPlayersTable.getColumn(i).getWidth();
            widthColum = dWidth;
            matchPlayersTable2.getColumn(i).pack();
            dWidth = matchPlayersTable2.getColumn(i).getWidth();
            widthColum = Math.max(widthColum, dWidth);
            width += widthColum;
            matchPlayersTable.getColumn(i).setWidth(widthColum);
            matchPlayersTable2.getColumn(i).setWidth(widthColum);
        }

        // TODO : optimize memory
        int heightAll = 0;
        int widthAll = 0;
        FormData data = (FormData) matchPlayersTable.getLayoutData();
        width += matchPlayersTable.getBorderWidth() * 2;
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.right = new FormAttachment(matchPlayersTable, width);
        int height = matchPlayersTable.getItemCount() * matchPlayersTable.getItemHeight();
        height += matchPlayersTable.getHeaderHeight();
        height += matchPlayersTable.getBorderWidth() * 3;
        data.bottom = new FormAttachment(matchPlayersTable, height);
        heightAll += height;
        heightAll += 10;
        heightAll += daysOfRestComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        heightAll += 10;
        heightAll += Math.max(homeBonusButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y, homeBonusLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        heightAll += 10;
        heightAll += Math.max(squardRealStrengthLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y,
                squardRealStrengthValue.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        heightAll += 5;
        heightAll += Math.max(lAvgTeamWork.computeSize(SWT.DEFAULT, SWT.DEFAULT).y, lAvgTeamWorkValue.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        heightAll += 5;
        widthAll = width;

        data = (FormData) matchPlayersTable2.getLayoutData();
        width -= matchPlayersTable.getBorderWidth() * 2;
        width += matchPlayersTable2.getBorderWidth() * 2;
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.right = new FormAttachment(matchPlayersTable2, width);
        height = matchPlayersTable2.getItemCount() * matchPlayersTable.getItemHeight();
        height += matchPlayersTable2.getHeaderHeight();
        height += matchPlayersTable2.getBorderWidth() * 3;
        data.bottom = new FormAttachment(matchPlayersTable2, height);
        heightAll += height;
        heightAll += schemeControlPanel.getBorderWidth() * 2;
        heightAll += schemeControlPanel.getSize().y;
        widthAll = Math.max(widthAll, width);
        matchPlayersTable.getParent().layout();
        //layoutSchemeControls();

        daysOfRestComposite.layout();
        daysOfRestComposite.getParent().layout();

        rightPanel.setSize(widthAll, heightAll);
        rightPanelScrl.setExpandHorizontal(true);
        rightPanelScrl.setMinWidth(widthAll);
        rightPanelScrl.setMinHeight(heightAll);

        rightPanelScrl.layout();

        data = (FormData) rightPanelScrl.getLayoutData();

        int scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        data.left = new FormAttachment(rightPanelScrl, -scrollWidth);

        rightPanelScrl.layout();
        rightPanelScrl.getParent().layout();

        matchPlayersTable.setRedraw(true);
        matchPlayersTable2.setRedraw(true);

        redrawAveragePlayersStrength();
    }

    public void onGameFormUpdate() {

        isSchemeUpdate = false;
        schemeControlPanel.getSchemeCombo().select(GameForm.getInstance().getSelectedSchemeIndex());
        isSchemeUpdate = true;
        updateAll();
    }

    public void updatePassword(String password) {

    }

    private MouseAdapter maMatchPlayers = new MouseAdapter() {
        @Override
        public void mouseDoubleClick(MouseEvent e) {
            //System.out.println("mouseDoubleClick");
            //if (здф)
        }

        @Override
        public void mouseDown(MouseEvent e) {
            //System.out.println("mouseDown");
            Rectangle clientArea = matchPlayersTable.getClientArea();
            Point pt = new Point(e.x, e.y);
            if (e.button == 1) {
                int index = matchPlayersTable.getTopIndex();
                while (index < matchPlayersTable.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = matchPlayersTable.getItem(index);
                    for (int i = 0; i < matchPlayersTable.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            final int column = i;
                            final int row = index;
                            final PlayerPosition playerPos = GameForm.getInstance().getFirstTeam()[row];
                            if (column == 3) {
                                Rectangle rect1 = matchPlayersTable.getDisplay().map(item.getParent(), null, rect);
                                specialRoleEditor.updatePlayerPosition(playerPos);
                                specialRoleEditor.show(new Point(rect1.x + rect1.width, rect1.y));
                                redraw();
                            }

                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }

        }
    };

    /*
        private class MatchPlayersTableEditListener implements Listener {
    
            public void handleEvent(Event event) {
                Rectangle clientArea = matchPlayersTable.getClientArea();
                Point pt = new Point(event.x, event.y);
                if (event.button == 1) {
                    System.out.println("MatchPlayersTableEditListener.mouseClick");
                    int index = matchPlayersTable.getTopIndex();
                    while (index < matchPlayersTable.getItemCount()) {
                        boolean visible = false;
                        final TableItem item = matchPlayersTable.getItem(index);
                        for (int i = 0; i < matchPlayersTable.getColumnCount(); i++) {
                            Rectangle rect = item.getBounds(i);
                            if (rect.contains(pt)) {
                                final int column = i;
                                final int row = index;
                                final PlayerPosition playerPos = GameForm.getInstance().getFirstTeam()[row];
                                if (column == 3) {
                                    Rectangle rect1 = matchPlayersTable.getDisplay().map(item.getParent(), null, rect);
                                    specialRoleEditor.updatePlayerPosition(playerPos);
                                    specialRoleEditor.show(new Point(rect1.x + rect1.width, rect1.y));
                                    redraw();
                                }
    
                                return;
                            }
                            if (!visible && rect.intersects(clientArea)) {
                                visible = true;
                            }
                        }
                        if (!visible) {
                            return;
                        }
                        index++;
                    }
                }
            }
    
        }
    */
    private String printSRflags(PlayerPosition subst) {
        StringBuilder st = new StringBuilder();
        if (subst.isCaptain()) {
            st.append(MainWindow.getMessage("SRCaptainShort"));
        }
        if (subst.isCaptainAssistant()) {
            st.append(MainWindow.getMessage("SRCaptainShort").toLowerCase());
        }
        if (subst.isDirectFreekick()) {
            st.append(MainWindow.getMessage("SRDirectFreekickShort"));
        }
        if (subst.isDirectFreekickAssistant()) {
            st.append(MainWindow.getMessage("SRDirectFreekickShort").toLowerCase());
        }
        if (subst.isIndirectFreekick()) {
            st.append(MainWindow.getMessage("SRIndirectFreekickShort"));
        }
        if (subst.isIndirectFreekickAssistant()) {
            st.append(MainWindow.getMessage("SRIndirectFreekickShort").toLowerCase());
        }
        if (subst.isPenalty()) {
            st.append(MainWindow.getMessage("SRPenaltyShort"));
        }
        if (subst.isPenaltyAssistant()) {
            st.append(MainWindow.getMessage("SRPenaltyShort").toLowerCase());
        }
        if (subst.isLeftCorner()) {
            st.append(MainWindow.getMessage("SRLeftCornerShort"));
        }
        if (subst.isLeftCornerAssistant()) {
            st.append(MainWindow.getMessage("SRLeftCornerShort").toLowerCase());
        }
        if (subst.isRightCorner()) {
            st.append(MainWindow.getMessage("SRRightCornerShort"));
        }
        if (subst.isRightCornerAssistant()) {
            st.append(MainWindow.getMessage("SRRightCornerShort").toLowerCase());
        }
        if (subst.getPenaltyOrder() > 0) {
            st.append(" ").append(subst.getPenaltyOrder());
        }
        return st.toString();
    }

    /*private void clearAllSRAflags(PlayerPosition subst) {
        subst.setCaptainAssistant(false);
        subst.setDirectFreekickAssistant(false);
        subst.setIndirectFreekickAssistant(false);
        subst.setPenaltyAssistant(false);
        subst.setLeftCornerAssistant(false);
        subst.setRightCornerAssistant(false);
    }*/

    public void updateMessages() {

        String[] defaultTitles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerAge"),
                MainWindow.getMessage("PlayerTalant"), MainWindow.getMessage("PlayerExperience"), MainWindow.getMessage("PlayerFitness"),
                MainWindow.getMessage("PlayerMorale"), MainWindow.getMessage("PlayerStrength"), MainWindow.getMessage("PlayerRealStrength"),
                MainWindow.getMessage("PlayerHealth"), MainWindow.getMessage("PlayerRest"), MainWindow.getMessage("PlayerCards"),
                MainWindow.getMessage("PlayerAbilities"), MainWindow.getMessage("PlayerTeamwork") };

        String[] defaultTooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("tooltip.Age"),
                MainWindow.getMessage("tooltip.Talent"), MainWindow.getMessage("tooltip.Experience"), MainWindow.getMessage("tooltip.Physics"),
                MainWindow.getMessage("tooltip.Morale"), MainWindow.getMessage("tooltip.Strength"), MainWindow.getMessage("tooltip.RealStrength"),
                MainWindow.getMessage("tooltip.Health"), MainWindow.getMessage("tooltip.RestDays"), MainWindow.getMessage("PlayerCards"),
                MainWindow.getMessage("tooltip.Skills"), MainWindow.getMessage("tooltip.Teamwork"), };

        titles = defaultTitles;
        for (int i = 0; i < defaultTitles.length; i++) {
            playersTable.getColumn(i).setText(defaultTitles[i]);
            playersTable.getColumn(i).setToolTipText(defaultTooltips[i]);
        }
        schemeControlPanel.updateMessages();
        //deleteSchemeButton.setToolTipText(MainWindow.getMessage("DeleteSchemeButton"));
        //saveSchemeButton.setToolTipText(MainWindow.getMessage("global.save"));
        //resetSchemeButton.setToolTipText(MainWindow.getMessage("resetSchemesToDefault"));

        String[] titlesMatch = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("Standards") };

        String[] titlesBench = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                "   " };

        for (int i = 0; i < titlesMatch.length; i++) {
            matchPlayersTable.getColumn(i).setText(titlesMatch[i]);
            matchPlayersTable2.getColumn(i).setText(titlesBench[i]);
        }

        specialRoleEditor.updateMessages();
        homeBonusLabel.setText(MainWindow.getMessage("homeBonus"));

        daysOfRestComposite.updateMessages();

        squardRealStrengthLabel.setText(MainWindow.getMessage("strength.average"));
        lAvgTeamWork.setText(MainWindow.getMessage("teamwork.average"));

        redraw();
    }

    private boolean needUpdate = true;
    private String squardFontData;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
    }

    @Override
    public TabItem asTabItem() {
        return null;
    }

    @Override
    public void dispose() {
        if (fonts != null) {
            for (Font font : fonts) {
                if (font != null) {
                    font.dispose();
                }
            }
        }
        if (colors != null) {
            for (Color color : colors) {
                if (color != null) {
                    color.dispose();
                }
            }
        }

        if (flags != null) {
            for (Map.Entry<String, Image> entry : flags.entrySet()) {
                if (entry != null && entry.getValue() != null && !entry.getValue().isDisposed()) {
                    entry.getValue().dispose();
                }
            }
        }

    }

    @Override
    public void store(Properties props) {

        props.put(MainWindow.PROP_SQUARD_FONT_DATA, squardFontData);
    }

    public void onHomeBonusChanged(int homeBonusValue) {
        redraw();
    }

    private void onTableDoubleClick(Table source, TableItem[] ti) {
        //System.out.println("table double click..." + (ti == null ? "empty" : ti[0].getText()));
        if (source == playersTable) {
            if (ti == null || ti.length == 0 || ti[0] == null) {
                return;
            }
            try {
                int playerNumber = Integer.valueOf(ti[0].getText(0));
                clearPlayerFromStartupLine(playerNumber);
                clearPlayerFromSubstitutions(playerNumber);
            } catch (NumberFormatException ex) {
            }
        } else if (source == matchPlayersTable) {
            setPlayer(ti[0], prepareTableDragData(playersTable, playersTable.getSelection()));
        } else if (source == matchPlayersTable2) {
            setPlayerSubstitution(ti[0], prepareTableDragData(playersTable, playersTable.getSelection()));
        } 
    }

    private String prepareTableDragData(Table table, TableItem[] items) {
        StringBuilder sb = new StringBuilder();
        for (TableItem item : items) {
            for (int j = 0; j < table.getColumnCount(); j++) {
                sb.append(item.getText(j));
                sb.append("/");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    private void clearPlayerFromStartupLine(int playerNumber) {
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        for (int i = 0; i < playerPositions.length; i++) {
            if (playerPositions[i].getNumber() == playerNumber) {
                playerPositions[i].reset();
            }
        }
    }
    
    private void clearPlayerFromSubstitutions(int playerNumber) {
        int playerSubstitutions[] = GameForm.getInstance().getBench();
        for (int i = 0; i < playerSubstitutions.length; i++) {
            if (playerSubstitutions[i] == playerNumber) {
                playerSubstitutions[i] = 0;
            }
        }
    }
    
}