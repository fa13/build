package com.fa13.build.view;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.controller.io.TrainingReader;
import com.fa13.build.controller.io.TrainingWriter;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.model.training.PlayerTraining;
import com.fa13.build.model.training.TrainingForm;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.utils.HttpUtils;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;

public class TrainingView implements UIItem, TeamUIItem, IDisposable, IStorable {

    private TrainingWriter writer = new TrainingWriter();

    String password;
    TabItem mainItem;
    Table playersTable;
    TrainingForm trainInstance;
    Button openButton;
    Button saveButton;
    Button resetTraningsButton;
    Composite topComposite;
    TabFolder parentInstance;
    Composite buttonsComposite;
    Composite infoComposite;
    DaysOfRestComposite daysOfRestComposite;
    Label mainCoachLabelName;
    Label defenderLabelName;
    Label midfielderLabelName;
    Label forwardLabelName;
    Label goalkeeperLabelName;
    Label mainCoachLabelVal;
    Label defenderLabelVal;
    Label midfielderLabelVal;
    Label forwardLabelVal;
    Label goalkeeperLabelVal;
    Label clubResLabelVal;
    Label scoutRestLabel;
    Label scoutTalent;
    Button scoutRestButton;
    Spinner scoutTalentSpinner;
    TableEditor editor;
    Group pointsGroup;
    Group scoutGroup;
    Group clubResGroup;
    Label restLabel;
    int mainCoachPoints;
    int goalkeeperPoints;
    int defenderPoints;
    int midfielderPoints;
    int forwardPoints;
    int clubResource;

    static final int FIRST_VALUE_INTERVAL = 20;
    static final int SECOND_VALUE_INTERVAL = 40;
    static final int THIRD_VALUE_INTERVAL = 60;
    static final int FOURTH_VALUE_INTERVAL = 80;
    static final int FIFTH_VALUE_INTERVAL = 100;
    static final int REST_SLIDER_MAX = 30;// 10=some specific
    static final int SCOUT_TALENT_MAX = 60;
    static final int SCOUT_TALENT_MIN = 40;

    final Color black;
    final Color blue;
    final Color green;
    final Color red;
    final Color some1;
    final Color some2;

    private Font playersTableFont;

    private Image imgTraining;
    private String playersFontData;
    private Properties props;

    //private Point stringExtentForColumn = new Point(55, 20);

    public TrainingView(TabFolder parent, Properties props) {
        this.props = props;
        parentInstance = parent;
        parent.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                // store settings before widgets disposed
                //name;height;style
                FontData fd = playersTableFont.getFontData()[0];
                playersFontData = fd.getName();
                playersFontData += ";" + fd.getHeight();
                playersFontData += ";" + fd.getStyle();

            }
        });
        Display display = parentInstance.getDisplay();

        loadImages();

        black = new Color(display, 0, 0, 0);
        blue = new Color(display, 0, 0, 255);
        green = new Color(display, 0, 188, 0);
        red = new Color(display, 255, 0, 0);
        some1 = new Color(display, 0, 128, 128);
        some2 = new Color(display, 255, 106, 0);

        if (MainWindow.getAllInstance() != null) {
            updateAll();
        }
        mainItem = new TabItem(parentInstance, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("trainingTabName"));
        mainItem.setImage(imgTraining);
        FontData[] fontData = parent.getFont().getFontData();
        for (int i = 0; i < fontData.length; i++) {
            fontData[i].setHeight(fontData[i].getHeight() - 2);
        }

        topComposite = new Composite(parentInstance, SWT.NONE);
        FormLayout topLayout = new FormLayout();
        topComposite.setLayout(topLayout);
        mainItem.setControl(topComposite);

        createButtonsPanel();

        infoComposite = new Composite(topComposite, SWT.BORDER);
        FormData formData = new FormData();
        formData.right = new FormAttachment(100);
        formData.top = new FormAttachment(0);
        formData.bottom = new FormAttachment(buttonsComposite);
        infoComposite.setLayoutData(formData);
        infoComposite.setLayout(new GridLayout(1, true));

        createInfoPanel();

        refreshLabel();

        playersTable = new Table(topComposite, SWT.BORDER | SWT.FULL_SELECTION);
        playersTable.setLinesVisible(true);
        playersTable.setHeaderVisible(true);

        formData = new FormData();
        formData.top = new FormAttachment(0);
        formData.left = new FormAttachment(0);
        formData.right = new FormAttachment(infoComposite);
        formData.bottom = new FormAttachment(buttonsComposite);
        playersTable.setLayoutData(formData);
        String[] titles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("PTshooting"), "", MainWindow.getMessage("PTpassing"), "", MainWindow.getMessage("PTcross"), "",
                MainWindow.getMessage("PTdribbling"), "", MainWindow.getMessage("PTtackling"), "", MainWindow.getMessage("PTspeed"), "",
                MainWindow.getMessage("PTstamina"), "", MainWindow.getMessage("PTheading"), "", MainWindow.getMessage("PTreflexes"), "",
                MainWindow.getMessage("PThandling"), "", MainWindow.getMessage("PlayerFitness"), "", MainWindow.getMessage("PTmorale"),
                MainWindow.getMessage("PTmoraleFinance"), MainWindow.getMessage("PTfitnessFinance") };

        String[] tooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("tooltip.Shooting"), "", MainWindow.getMessage("tooltip.Passing"), "",
                MainWindow.getMessage("tooltip.Crossing"), "", MainWindow.getMessage("tooltip.Dribbling"), "",
                MainWindow.getMessage("tooltip.Tackling"), "", MainWindow.getMessage("tooltip.Speed"), "", MainWindow.getMessage("tooltip.Stamina"),
                "", MainWindow.getMessage("tooltip.Heading"), "", MainWindow.getMessage("tooltip.Reflexes"), "",
                MainWindow.getMessage("tooltip.TechnicsGK"), "", MainWindow.getMessage("tooltip.Physics"), "",
                MainWindow.getMessage("tooltip.Morale"), MainWindow.getMessage("tooltip.MoraleAward"),
                MainWindow.getMessage("tooltip.FitnessAward") };

        for (int i = 0; i < titles.length; i++) {

            TableColumn column = new TableColumn(playersTable, i == 2 ? SWT.LEFT : SWT.CENTER);
            column.setText(titles[i]);
            column.setToolTipText(tooltips[i]);
            column.setResizable(true);
            GuiUtils.autoAdjustTableColumnWidth(playersTable, i);

        }
        //fake column to avoid last column to take all available space
        TableColumn column = new TableColumn(playersTable, SWT.CENTER);

        redraw();

        editor = new TableEditor(playersTable);
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = 35;
        playersTable.addListener(SWT.MouseDown, new PlayerTableEditListener());

        String storedFontData = props.getProperty(MainWindow.PROP_TRAININGS_FONT_DATA);
        FontData fd;
        if (storedFontData == null || storedFontData.isEmpty()) {
            fd = playersTable.getFont().getFontData()[0];
        } else {
            try {
                String ss[] = storedFontData.split(";");
                //name;height;style
                fd = new FontData(ss[0], Integer.parseInt(ss[1]), Integer.parseInt(ss[2]));
            } catch (Exception e) {
                fd = playersTable.getFont().getFontData()[0];
            }
        }
        playersTableFont = new Font(parentInstance.getShell().getDisplay(), fd);
        //stringExtentForColumn = GuiUtils.getTextExtent("100.", playersTableFont);
        playersTable.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(playersTable);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("Font"));
                item.addSelectionListener(new SelectionAdapter() {

                    public void widgetSelected(SelectionEvent e) {
                        //change font
                        FontDialog dlg = new FontDialog(parentInstance.getShell());
                        dlg.setFontList(playersTable.getFont().getFontData());
                        FontData fontData = dlg.open();
                        if (fontData != null) {
                            playersTableFont = new Font(parentInstance.getShell().getDisplay(), fontData);
                            //stringExtentForColumn = GuiUtils.getTextExtent("100.", playersTableFont);
                        }
                        redraw();
                    }

                });

                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

    }

    private void createInfoPanel() {
        pointsGroup = new Group(infoComposite, SWT.SHADOW_NONE);
        pointsGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        GridData gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false, 1, 1);
        pointsGroup.setText(MainWindow.getMessage("PTPointsGroup"));
        pointsGroup.setLayoutData(gridData);
        pointsGroup.setLayout(new GridLayout(2, false));

        mainCoachLabelName = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        mainCoachLabelName.setText(MainWindow.getMessage("PTMainCoachPoints") + ":");
        mainCoachLabelName.setLayoutData(gridData);
        mainCoachLabelName.pack();

        mainCoachLabelVal = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        mainCoachLabelVal.setLayoutData(gridData);

        goalkeeperLabelName = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        goalkeeperLabelName.setText(MainWindow.getMessage("PTGoalkeeperPoints") + ":");
        goalkeeperLabelName.setLayoutData(gridData);
        goalkeeperLabelName.pack();

        goalkeeperLabelVal = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        goalkeeperLabelVal.setLayoutData(gridData);

        defenderLabelName = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        defenderLabelName.setText(MainWindow.getMessage("PTDefenderPoints") + ":");
        defenderLabelName.setLayoutData(gridData);
        defenderLabelName.pack();

        defenderLabelVal = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        defenderLabelVal.setLayoutData(gridData);

        midfielderLabelName = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        midfielderLabelName.setText(MainWindow.getMessage("PTMidfielderPoints") + ":");
        midfielderLabelName.setLayoutData(gridData);
        midfielderLabelName.pack();

        midfielderLabelVal = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        midfielderLabelVal.setLayoutData(gridData);

        forwardLabelName = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        forwardLabelName.setText(MainWindow.getMessage("PTForwardPoints") + ":");
        forwardLabelName.setLayoutData(gridData);
        forwardLabelName.pack();

        forwardLabelVal = new Label(pointsGroup, SWT.NONE);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        forwardLabelVal.setLayoutData(gridData);

        pointsGroup.layout();

        scoutGroup = new Group(infoComposite, SWT.SHADOW_NONE);
        scoutGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false);
        scoutGroup.setText(MainWindow.getMessage("PTScoutGroup"));
        scoutGroup.setLayoutData(gridData);
        scoutGroup.setLayout(new GridLayout(2, false));

        scoutTalent = new Label(scoutGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        scoutTalent.setText(MainWindow.getMessage("PTScoutTalent"));
        scoutTalent.pack();

        scoutTalentSpinner = new Spinner(scoutGroup, SWT.BORDER);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        scoutTalentSpinner.setLayoutData(gridData);
        scoutTalentSpinner.setMinimum(SCOUT_TALENT_MIN);
        scoutTalentSpinner.setMaximum(SCOUT_TALENT_MAX);
        scoutTalentSpinner.setIncrement(5);
        scoutTalentSpinner.pack();
        scoutTalentSpinner.addSelectionListener(new ScoutTalentListener());

        scoutRestLabel = new Label(scoutGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, true);
        scoutRestLabel.setLayoutData(gridData);
        scoutRestLabel.setText(MainWindow.getMessage("PTScoutVacation"));
        scoutRestLabel.pack();

        scoutRestButton = new Button(scoutGroup, SWT.CHECK);
        gridData = new GridData(GridData.END, GridData.END, true, true);
        scoutRestButton.setLayoutData(gridData);
        scoutRestButton.addSelectionListener(new ScoutRestListener());

        daysOfRestComposite = new DaysOfRestComposite(infoComposite, SWT.NONE, this);
        gridData = new GridData(GridData.FILL, GridData.BEGINNING, false, false);
        daysOfRestComposite.setLayoutData(gridData);

        clubResGroup = new Group(infoComposite, SWT.SHADOW_NONE);
        clubResGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        gridData = new GridData(GridData.BEGINNING, GridData.END, true, false);
        clubResGroup.setText(MainWindow.getMessage("PTClubResGroup"));
        clubResGroup.setLayoutData(gridData);
        clubResGroup.setLayout(new GridLayout(1, false));
        clubResGroup.pack(true);

        clubResLabelVal = new Label(clubResGroup, SWT.NONE);
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, true, true);
        clubResLabelVal.setLayoutData(gridData);
        clubResLabelVal.setText("0" + " " + MainWindow.getMessage("Thousands"));
        clubResLabelVal.pack(true);
        clubResGroup.pack(true);

        infoComposite.layout();
    }

    private void createButtonsPanel() {

        buttonsComposite = new Composite(topComposite, SWT.BORDER);
        FormData fd = new FormData();
        fd.bottom = new FormAttachment(100);
        fd.left = new FormAttachment(0);
        fd.right = new FormAttachment(100);
        buttonsComposite.setLayoutData(fd);
        GridLayout gridLayout = new GridLayout(4, false);
        buttonsComposite.setLayout(gridLayout);

        openButton = new Button(buttonsComposite, SWT.PUSH);
        openButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openButton.setImage(MainWindow.getSharedImage(ImageType.OPEN));
        GridData gridData = new GridData(GridData.END, GridData.CENTER, false, false);
        openButton.setLayoutData(gridData);
        openButton.addSelectionListener(new OpenButtonListner());

        saveButton = new Button(buttonsComposite, SWT.PUSH);
        saveButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saveButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        gridData = new GridData(GridData.END, GridData.CENTER, false, false);
        saveButton.setLayoutData(gridData);
        saveButton.addSelectionListener(new SaveButtonListner());

        gridData = new GridData(GridData.END, GridData.CENTER, false, false);
        resetTraningsButton = new Button(buttonsComposite, SWT.PUSH);
        resetTraningsButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        resetTraningsButton.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
        resetTraningsButton.setLayoutData(gridData);
        resetTraningsButton.addSelectionListener(new ResetTrainingsButtonListner());

    }

    private void loadImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/training_16x16.png"));
        imgTraining = new Image(parentInstance.getDisplay(), imgData);
    }

    public void updateAll() {

        if (!needUpdate)
            return;

        if (MainWindow.getAllInstance() == null) {
            return;
        }
        ArrayList<PlayerTraining> trainPlayers = new ArrayList<PlayerTraining>();
        int money = 0;
        int mainCoachPoints = 0;
        int defenderPoints = 0;
        int midfielderPoints = 0;
        int forwardPoints = 0;
        int goalkeeperPoints = 0;
        Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
        if (curTeam != null) {
            int val = curTeam.getCoach();
            mainCoachPoints = 20 + val / 10;
            val = curTeam.getDefendersCoach();
            defenderPoints = 3 * val + ((val != 0) ? 2 : 0);
            val = curTeam.getMidfieldersCoach();
            midfielderPoints = 3 * val + ((val != 0) ? 2 : 0);
            val = curTeam.getForwardsCoach();
            forwardPoints = 3 * val + ((val != 0) ? 2 : 0);
            val = curTeam.getGoalkeepersCoach();
            goalkeeperPoints = 3 * val + ((val != 0) ? 2 : 0);
            money = curTeam.getManagerFinance() / 1000;

            List<Player> players = curTeam.getPlayers();
            for (Iterator<Player> iterator = players.iterator(); iterator.hasNext();) {
                Player player = iterator.next();
                trainPlayers.add(new PlayerTraining(player.getNumber(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
            }
        }

        this.mainCoachPoints = mainCoachPoints;
        this.defenderPoints = defenderPoints;
        this.midfielderPoints = midfielderPoints;
        this.forwardPoints = forwardPoints;
        this.goalkeeperPoints = goalkeeperPoints;
        this.trainInstance = new TrainingForm("", 40, true, trainPlayers);
        Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
        if (currentTeam != null) {
            trainInstance.setTeamID(currentTeam.getId());
        }
        this.clubResource = money;
        redraw();
        needUpdate = false;
    }

    public void redraw() {

        playersTable.setRedraw(false);
        playersTable.setFont(playersTableFont);
        if (trainInstance != null) {
            scoutRestButton.setSelection(!trainInstance.getScouting());
            scoutTalentSpinner.setSelection(trainInstance.getScouting() ? trainInstance.getMinTalent() : 0);
            scoutTalentSpinner.setVisible(!scoutRestButton.getSelection());
            scoutTalent.setVisible(!scoutRestButton.getSelection());
        }
        if (MainWindow.getAllInstance() != null) {

            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            List<Player> curPlayers = curTeam.getPlayers();
            playersTable.setItemCount(curPlayers.size());
            int i = 0;
            for (Player player : curPlayers) {
                PlayerTraining trainPlayer = findPlayerbyNumber(player.getNumber());
                TableItem item = playersTable.getItem(i);
                if (player != null && player.getFitness() < 70) {
                    item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.LIGHT_RED));
                } else {
                    item.setBackground(playersTable.getBackground());
                }
                item.setData(player);
                item.setText(0, String.valueOf(player.getNumber()));
                item.setText(1, player.getPosition().toString());
                item.setText(2, player.getName());
                item.setText(3, getStringValue(player.getShooting()));
                item.setForeground(3, getColourForValue(player.getShooting()));
                item.setText(5, getStringValue(player.getPassing()));
                item.setText(7, getStringValue(player.getCross()));
                item.setText(9, getStringValue(player.getDribbling()));
                item.setText(11, getStringValue(player.getTackling()));
                item.setText(13, getStringValue(player.getSpeed()));
                item.setText(15, getStringValue(player.getStamina()));
                item.setText(17, getStringValue(player.getHeading()));
                item.setText(19, getStringValue(player.getReflexes()));
                item.setText(21, getStringValue(player.getHandling()));
                item.setBackground(23, MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
                item.setText(23, getStringValue(player.getFitness(daysOfRestComposite.getDaysOfRest())));
                item.setText(25, getStringValue(player.getMorale()));
                if (trainPlayer != null) {
                    item.setText(4, getStringValue(trainPlayer.getShootingPoints()));
                    item.setText(6, getStringValue(trainPlayer.getPassingPoints()));
                    item.setText(8, getStringValue(trainPlayer.getCrossPoints()));
                    item.setText(10, getStringValue(trainPlayer.getDribblingPoints()));
                    item.setText(12, getStringValue(trainPlayer.getTacklingPoints()));
                    item.setText(14, getStringValue(trainPlayer.getSpeedPoints()));
                    item.setText(16, getStringValue(trainPlayer.getStaminaPoints()));
                    item.setText(18, getStringValue(trainPlayer.getHeadingPoints()));
                    item.setText(20, getStringValue(trainPlayer.getReflexesPoints()));
                    item.setText(22, getStringValue(trainPlayer.getHandlingPoints()));
                    item.setText(24, getStringValue(trainPlayer.getFitnessPoints()));
                    item.setText(26, getStringValue(trainPlayer.getMoraleFinance()));
                    item.setText(27, getStringValue(trainPlayer.getFitnessFinance()));
                } else {
                    item.setText(4, "");
                    item.setText(6, "");
                    item.setText(8, "");
                    item.setText(10, "");
                    item.setText(12, "");
                    item.setText(14, "");
                    item.setText(16, "");
                    item.setText(18, "");
                    item.setText(20, "");
                    item.setText(22, "");
                    item.setText(24, "");
                    item.setText(26, "");
                    item.setText(27, "");
                }
                for (int j = 3; j < playersTable.getColumnCount(); j++) {
                    int val = getIntegerValue(item.getText(j));
                    Color color = getColourForValue(val);
                    if (j < playersTable.getColumnCount() - 5) {
                        item.setForeground(j, color);
                    }
                }
                i++;
            }
        }
        GuiUtils.autoAdjustTableColumnsWidth(playersTable);
        playersTable.setRedraw(true);
        topComposite.layout();
        infoComposite.layout();
        refreshLabel();
    }

    public class OpenButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            openTrainingDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            openTrainingDlg();
        }
    }

    public class SaveButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            saveTrainingDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            saveTrainingDlg();
        }
    }

    public class ResetTrainingsButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            widgetSelected(arg0);
        }

        public void widgetSelected(SelectionEvent arg0) {
            resetTrainings();
        }
    }

    public void openTraining(String fname) {
        if (fname == null) {
            return;
        }
        try {
            trainInstance = TrainingReader.readTrainingFile(fname);
            bidFileName = fname;
        } catch (ReaderException e) {
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.open_bid_error"));
        }
        //redraw(); no need here --> done in openTrainingDlg
    }

    public void saveTraining(String fname) {
        if (fname == null) {
            return;
        }
        try {
            trainInstance.setPassword(password);
            trainInstance.setScouting(!scoutRestButton.getSelection());
            int minTalent = scoutRestButton.getSelection() ? 0 : Integer.valueOf(scoutTalentSpinner.getText());
            trainInstance.setMinTalent(minTalent);
            writer.write(fname, trainInstance);
            bidFileName = fname;
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx.setMessage(MainWindow.getMessage("global.bidSaveSuccess"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx.setMessage(MainWindow.getMessage("global.bidSaveError"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        }
        redraw();
    }

    private void resetTrainings() {
        if (MainWindow.getAllInstance() != null) {

            if (trainInstance.getPlayers() != null) {
                for (PlayerTraining trainPlayer : trainInstance.getPlayers()) {
                    trainPlayer.setCrossPoints(0);
                    trainPlayer.setDribblingPoints(0);
                    trainPlayer.setFitnessFinance(0);
                    trainPlayer.setFitnessPoints(0);
                    trainPlayer.setHandlingPoints(0);
                    trainPlayer.setHeadingPoints(0);
                    trainPlayer.setMoraleFinance(0);
                    trainPlayer.setPassingPoints(0);
                    trainPlayer.setReflexesPoints(0);
                    trainPlayer.setShootingPoints(0);
                    trainPlayer.setSpeedPoints(0);
                    trainPlayer.setStaminaPoints(0);
                    trainPlayer.setTacklingPoints(0);
                }
                redraw();
                redistributePlayerPointsAndUpdate();
            }
        }
    }

    private void redistributePlayerPointsAndUpdate() {
        String strVal = playersTable.getItem(0).getText(4);
        int currPoints = strVal.isEmpty() ? 0 : Integer.valueOf(strVal);
        distributePlayerPoints(MainWindow.getAllInstance().getCurrentTeam().getPlayers().get(0), 4, currPoints);
        refreshLabel();
    }

    //check training with current team
    //delete trainings for players that are not in current team roster
    //add absent trainings for players from team roster
    public void checkAndFixTraining() {
        if (MainWindow.getAllInstance() == null) {
            return;
        }
        //add
        Team currTeam = MainWindow.getAllInstance().getCurrentTeam();
        if (currTeam == null) {
            return;
        }

        for (Player player : currTeam.getPlayers()) {

            PlayerTraining pt = findPlayerbyNumber(player.getNumber(), false);
            if (pt == null) {
                //adding absent player training
                trainInstance.getPlayers().add(new PlayerTraining(player.getNumber(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
                //System.out.println("added pt:"+player.getNumber());
            }
        }
        //delete
        ArrayList<PlayerTraining> toDel = new ArrayList<PlayerTraining>();
        for (PlayerTraining playerTraining : trainInstance.getPlayers()) {
            Player p = currTeam.getPlayerByNumber(playerTraining.getNumber());
            if (p == null) {
                toDel.add(playerTraining);
                //System.out.println("deleted pt:"+playerTraining.getNumber());
            }
        }
        trainInstance.getPlayers().removeAll(toDel);

    }

    public void openTrainingDlg() {

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(bidFileName == null ? getBidDefaultFileName() : bidFileName);
        String result = dlg.open();
        if (result != null) {
            openTraining(result);
            checkAndFixTraining();
            redraw();
            redistributePlayerPointsAndUpdate();

        }
    }

    public void saveTrainingDlg() {
        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.SAVE);

        if (mainCoachPoints < 0) {
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("global.save_bid_error"),
                    MainWindow.getMessage("training.error.coachPointsLimitExceeded"));
            return;
        }

        int sum = 0;
        for (PlayerTraining trainPlayer : trainInstance.getPlayers()) {
            sum += trainPlayer.getFitnessFinance() + trainPlayer.getMoraleFinance();
        }

        if (sum > 0 && sum > MainWindow.getAllInstance().getCurrentTeam().getManagerFinance() / 1000) {
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK | SWT.CANCEL);
            mbx.setMessage(MainWindow.getMessage("PTSaveError"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        }

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(getBidDefaultFileName());
        String result = dlg.open();
        if (result != null) {
            this.saveTraining(result);
            //sendBidToServer(result);
            redraw();
        }
    }

    private static final String PROP_SEND_URI = "http://fa13.info/my/training/send";
    private static final String PROP_SEND_PARAM_TEAM = "team_sok";
    private static final String PROP_SEND_PARAM_PASS = "pass";
    private static final String PROP_SEND_PARAM_CLIENT = "client";
    private static final String PROP_SEND_PARAM_TRAINING_FILE = "trainingFile";

    private static final String PROP_SEND_PARAM_CLIENT_VALUE = "java_build";

    private void sendBidToServer(String fileName) {
        MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK | SWT.CANCEL);
        mbx.setMessage(MainWindow.getMessage("ask.SendBidToServer"));
        if (mbx.open() == SWT.OK) {
            Map<String, Object> params = new HashMap<>();
            params.put(PROP_SEND_PARAM_CLIENT, PROP_SEND_PARAM_CLIENT_VALUE);
            params.put(PROP_SEND_PARAM_TEAM, trainInstance.getTeamID());
            params.put(PROP_SEND_PARAM_PASS, trainInstance.getPassword());
            params.put(PROP_SEND_PARAM_TRAINING_FILE, new File(fileName));
            String res = HttpUtils.executeMultiPartRequest(PROP_SEND_URI, params);

            MessageBox mbx2 = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx2.setMessage(res == null ? MainWindow.getMessage("bidSendFailed") : MainWindow.getMessage("bidSendOk"));
            mbx2.open();

        }

    }

    private String bidFileName = null;

    private String getBidDefaultFileName() {
        String bidFileName = "";

        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                bidFileName = "Tren" + currentTeam.getId() + ".f13";
            }
        }

        return bidFileName;
    }

    private PlayerTraining findPlayerbyNumber(int number) {
        return findPlayerbyNumber(number, true);
    }

    private PlayerTraining findPlayerbyNumber(int number, boolean createIfNotFound) {
        if (trainInstance != null) {
            List<PlayerTraining> curPlayers = trainInstance.getPlayers();
            for (PlayerTraining player : curPlayers) {
                if (player.getNumber() == number) {
                    return player;
                }
            }
            if (!createIfNotFound) {
                return null;
            }
            //in the case of old training loaded new players appeared in new All
            //we create default to avoid crash
            PlayerTraining defaultPlayerTraining = new PlayerTraining(number, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            curPlayers.add(defaultPlayerTraining);
            return defaultPlayerTraining;
        }

        return null;
    }

    private String getStringValue(int i) throws NumberFormatException {
        if (i == 0) {
            return "";
        }
        return String.valueOf(i);
    }

    private int getIntegerValue(String st) {
        if (st == null || st.isEmpty()) {
            return 0;
        }
        return Integer.valueOf(st);
    }

    private Color getColourForValue(int value) {
        if (value <= FIRST_VALUE_INTERVAL) {
            return black;
        } else if (value < SECOND_VALUE_INTERVAL) {
            return blue;
        } else if (value < THIRD_VALUE_INTERVAL) {
            return green;
        } else if (value < FOURTH_VALUE_INTERVAL) {
            return red;
        } else if (value < FIFTH_VALUE_INTERVAL) {
            return some1;
        } else {
            return some2;
        }
    }

    private int distributePlayerPoints(Player player, int column, int newPoints) {
        if (column == 27) {
            column++;
        }
        int skill = (column - 3) / 2;

        Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
        List<Player> players = curTeam.getPlayers();
        int val = curTeam.getCoach();
        int mainCoachPointsMax = 20 + val / 10;
        val = curTeam.getDefendersCoach();
        int defenderPointsMax = 3 * val + ((val != 0) ? 2 : 0);
        val = curTeam.getMidfieldersCoach();
        int midfielderPointsMax = 3 * val + ((val != 0) ? 2 : 0);
        val = curTeam.getForwardsCoach();
        int forwardPointsMax = 3 * val + ((val != 0) ? 2 : 0);
        val = curTeam.getGoalkeepersCoach();
        int goalkeeperPointsMax = 3 * val + ((val != 0) ? 2 : 0);
        int playerPointsTotal = 0;
        int rest = 0;
        PlayerTraining trainPlayer;

        // System.out.printf("1: %d\n", mainCoachPointsMax);

        for (Player curr : players) {
            trainPlayer = findPlayerbyNumber(curr.getNumber());
            playerPointsTotal = trainPlayer.getShootingPoints() + trainPlayer.getPassingPoints() + trainPlayer.getCrossPoints()
                    + trainPlayer.getDribblingPoints() + trainPlayer.getTacklingPoints() + trainPlayer.getSpeedPoints()
                    + trainPlayer.getStaminaPoints() + trainPlayer.getHeadingPoints() + trainPlayer.getReflexesPoints()
                    + trainPlayer.getHandlingPoints() + trainPlayer.getFitnessPoints();
            if (trainPlayer.getNumber() == player.getNumber()) {
                switch (skill) {
                    case 0:
                        playerPointsTotal -= trainPlayer.getShootingPoints();
                        break;
                    case 1:
                        playerPointsTotal -= trainPlayer.getPassingPoints();
                        break;
                    case 2:
                        playerPointsTotal -= trainPlayer.getCrossPoints();
                        break;
                    case 3:
                        playerPointsTotal -= trainPlayer.getDribblingPoints();
                        break;
                    case 4:
                        playerPointsTotal -= trainPlayer.getTacklingPoints();
                        break;
                    case 5:
                        playerPointsTotal -= trainPlayer.getSpeedPoints();
                        break;
                    case 6:
                        playerPointsTotal -= trainPlayer.getStaminaPoints();
                        break;
                    case 7:
                        playerPointsTotal -= trainPlayer.getHeadingPoints();
                        break;
                    case 8:
                        playerPointsTotal -= trainPlayer.getReflexesPoints();
                        break;
                    case 9:
                        playerPointsTotal -= trainPlayer.getHandlingPoints();
                        break;
                    case 10:
                        playerPointsTotal -= trainPlayer.getFitnessPoints();
                        break;
                    default:
                        break;
                }
            }
            switch (curr.getPosition()) {
                case LF:
                case CF:
                case RF:
                    rest = forwardPointsMax - playerPointsTotal;
                    if (rest > 0) {
                        forwardPointsMax = rest;
                        rest = 0;
                    } else {
                        forwardPointsMax = 0;
                    }
                    break;
                case LM:
                case CM:
                case RM:
                    rest = midfielderPointsMax - playerPointsTotal;
                    if (rest > 0) {
                        midfielderPointsMax = rest;
                        rest = 0;
                    } else {
                        midfielderPointsMax = 0;
                    }
                    break;
                case LD:
                case CD:
                case RD:
                    rest = defenderPointsMax - playerPointsTotal;
                    if (rest > 0) {
                        defenderPointsMax = rest;
                        rest = 0;
                    } else {
                        defenderPointsMax = 0;
                    }
                    break;
                case GK:
                    rest = goalkeeperPointsMax - playerPointsTotal;
                    if (rest > 0) {
                        goalkeeperPointsMax = rest;
                        rest = 0;
                    } else {
                        goalkeeperPointsMax = 0;
                    }
                    break;
            }
            mainCoachPointsMax += rest;
        }

        switch (player.getPosition()) {
            case LF:
            case CF:
            case RF:
                newPoints = (newPoints < (mainCoachPointsMax + forwardPointsMax)) ? newPoints : (mainCoachPointsMax + forwardPointsMax);
                if (newPoints < 0) {
                    newPoints = 0;
                }
                rest = forwardPointsMax - newPoints;
                if (rest > 0) {
                    forwardPointsMax = rest;
                    rest = 0;
                } else {
                    forwardPointsMax = 0;
                    mainCoachPointsMax += rest;
                }
                break;
            case LM:
            case CM:
            case RM:
                newPoints = (newPoints < (mainCoachPointsMax + midfielderPointsMax)) ? newPoints : (mainCoachPointsMax + midfielderPointsMax);
                if (newPoints < 0) {
                    newPoints = 0;
                }
                rest = midfielderPointsMax - newPoints;
                if (rest > 0) {
                    midfielderPointsMax = rest;
                    rest = 0;
                } else {
                    midfielderPointsMax = 0;
                    mainCoachPointsMax += rest;
                }
                break;
            case LD:
            case CD:
            case RD:
                newPoints = (newPoints < (mainCoachPointsMax + defenderPointsMax)) ? newPoints : (mainCoachPointsMax + defenderPointsMax);
                if (newPoints < 0) {
                    newPoints = 0;
                }
                rest = defenderPointsMax - newPoints;
                if (rest > 0) {
                    defenderPointsMax = rest;
                    rest = 0;
                } else {
                    defenderPointsMax = 0;
                    mainCoachPointsMax += rest;
                }
                break;
            case GK:
                newPoints = (newPoints < (mainCoachPointsMax + goalkeeperPointsMax)) ? newPoints : (mainCoachPointsMax + goalkeeperPointsMax);
                if (newPoints < 0) {
                    newPoints = 0;
                }
                rest = goalkeeperPointsMax - newPoints;
                if (rest > 0) {
                    goalkeeperPointsMax = rest;
                    rest = 0;
                } else {
                    goalkeeperPointsMax = 0;
                    mainCoachPointsMax += rest;
                }
                break;
        }
        trainPlayer = findPlayerbyNumber(player.getNumber());
        switch (skill) {
            case 0:
                trainPlayer.setShootingPoints(newPoints);
                break;
            case 1:
                trainPlayer.setPassingPoints(newPoints);
                break;
            case 2:
                trainPlayer.setCrossPoints(newPoints);
                break;
            case 3:
                trainPlayer.setDribblingPoints(newPoints);
                break;
            case 4:
                trainPlayer.setTacklingPoints(newPoints);
                break;
            case 5:
                trainPlayer.setSpeedPoints(newPoints);
                break;
            case 6:
                trainPlayer.setStaminaPoints(newPoints);
                break;
            case 7:
                trainPlayer.setHeadingPoints(newPoints);
                break;
            case 8:
                trainPlayer.setReflexesPoints(newPoints);
                break;
            case 9:
                trainPlayer.setHandlingPoints(newPoints);
                break;
            case 10:
                trainPlayer.setFitnessPoints(newPoints);
                break;
            case 11:
                trainPlayer.setMoraleFinance(newPoints);
                break;
            case 12:
                trainPlayer.setFitnessFinance(newPoints);
                break;
        }

        this.mainCoachPoints = mainCoachPointsMax;
        this.defenderPoints = defenderPointsMax;
        this.midfielderPoints = midfielderPointsMax;
        this.forwardPoints = forwardPointsMax;
        this.goalkeeperPoints = goalkeeperPointsMax;
        // System.out.printf("4: %d\n", mainCoachPointsMax);

        return newPoints;
    }

    private void refreshLabel() {
        infoComposite.setRedraw(false);

        mainCoachLabelVal.setText(String.valueOf(mainCoachPoints));
        goalkeeperLabelVal.setText(String.valueOf(goalkeeperPoints));
        defenderLabelVal.setText(String.valueOf(defenderPoints));
        forwardLabelVal.setText(String.valueOf(forwardPoints));
        midfielderLabelVal.setText(String.valueOf(midfielderPoints));
        clubResLabelVal.setText(String.valueOf(clubResource) + " " + MainWindow.getMessage("Thousands"));

        mainCoachLabelVal.pack();
        goalkeeperLabelVal.pack();
        defenderLabelVal.pack();
        forwardLabelVal.pack();
        midfielderLabelVal.pack();
        pointsGroup.layout();
        clubResLabelVal.pack();
        clubResGroup.pack();
        infoComposite.layout();
        infoComposite.setRedraw(true);
    }

    private class PlayerTableEditListener implements Listener {

        public void handleEvent(Event event) {
            Rectangle clientArea = playersTable.getClientArea();
            Point pt = new Point(event.x, event.y);
            int index = playersTable.getTopIndex();
            while (index < playersTable.getItemCount()) {
                boolean visible = false;
                final TableItem item = playersTable.getItem(index);
                for (int i = 0; i < playersTable.getColumnCount() - 1; i++) {
                    Rectangle rect = item.getBounds(i);
                    if (rect.contains(pt)
                            && (((i > 3) && (i % 2 == 0)) || (i == playersTable.getColumnCount() - 3) || (i == playersTable.getColumnCount() - 2))) {
                        final int column = i;
                        final Spinner spin = new Spinner(playersTable, SWT.NONE);
                        final Player player = (Player) item.getData();
                        final PlayerTraining trainPlayer = findPlayerbyNumber(player.getNumber());
                        spin.setValues(getPlayerPoints(column, trainPlayer), 0, 99, 0, 1, 1);
                        Listener textListener = new Listener() {
                            public void handleEvent(final Event e) {
                                int val;
                                try {
                                    val = getIntegerValue(spin.getText());
                                } catch (NumberFormatException nfe) {
                                    val = -1;
                                }
                                switch (e.type) {
                                    case SWT.Modify:
                                        if (val == -1) {
                                            val = 0;
                                        }
                                        if (column != 26 && column != 27) {
                                            val = distributePlayerPoints(player, column, val);
                                            refreshLabel();
                                        } else {
                                            if (column == 26) {
                                                trainPlayer.setMoraleFinance(val);
                                            }
                                            if (column == 27) {
                                                trainPlayer.setFitnessFinance(val);
                                            }
                                        }
                                        item.setText(column, getStringValue(val));

                                        if (val != spin.getSelection()) {
                                            spin.setSelection(val);
                                        }

                                        break;
                                    case SWT.FocusOut:
                                        if (val == -1) {
                                            val = 0;
                                        }
                                        if (column != 26 && column != 27) {
                                            val = distributePlayerPoints(player, column, val);
                                            refreshLabel();
                                        } else {
                                            if (column == 26) {
                                                trainPlayer.setMoraleFinance(val);
                                            }
                                            if (column == 27) {
                                                trainPlayer.setFitnessFinance(val);
                                            }
                                        }
                                        item.setText(column, getStringValue(val));
                                        playersTable.setRedraw(false);
                                        GuiUtils.autoAdjustTableColumnWidth(playersTable, column);
                                        playersTable.setRedraw(true);
                                        if (val != spin.getSelection()) {
                                            spin.setSelection(val);
                                        }

                                        spin.dispose();
                                        break;
                                    case SWT.Verify:
                                        String string = e.text;
                                        char[] chars = new char[string.length()];
                                        string.getChars(0, chars.length, chars, 0);
                                        for (char c : chars) {
                                            if (!('0' <= c && c <= '9')) {
                                                e.doit = false;
                                                return;
                                            }
                                        }
                                }
                            }
                        };
                        //spin.getClientArea().x = 50;
                        spin.setFont(playersTable.getFont());
                        spin.addListener(SWT.FocusOut, textListener);
                        spin.addListener(SWT.Modify, textListener);
                        spin.addListener(SWT.Verify, textListener);
                        spin.setTextLimit(2);
                        spin.setSelection(getIntegerValue(item.getText(i)));
                        spin.setSize(spin.computeSize(SWT.DEFAULT, SWT.DEFAULT));
                        editor.minimumWidth = spin.getSize().x;
                        editor.setEditor(spin, item, i);
                        spin.setFocus();
                        return;
                    }
                    if (!visible && rect.intersects(clientArea)) {
                        visible = true;
                    }
                }
                if (!visible) {
                    return;
                }
                index++;
            }
        }

    }

    private class ScoutRestListener implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }

        public void widgetSelected(SelectionEvent e) {
            if (trainInstance != null) {
                trainInstance.setScouting(!scoutRestButton.getSelection());
                scoutTalentSpinner.setVisible(!scoutRestButton.getSelection());
                scoutTalent.setVisible(!scoutRestButton.getSelection());
            }
        }

    }

    private class ScoutTalentListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            if (trainInstance != null) {
                trainInstance.setMinTalent(Integer.valueOf(scoutTalentSpinner.getText()));
            }
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }
    }

    public void updatePassword(String password) {
        this.password = password;
    }

    public void updateDaysOfRest(int daysOfRest) {
        for (int i = 0; i < playersTable.getItemCount(); i++) {
            TableItem item = playersTable.getItem(i);
            Player player = (Player) item.getData();
            item.setText(23, String.valueOf(player.getFitness(daysOfRest)));
        }
        playersTable.setRedraw(false);
        GuiUtils.autoAdjustTableColumnWidth(playersTable, 23);
        playersTable.setRedraw(true);
    }

    private int getPlayerPoints(int column, PlayerTraining trainPlayer) {
        int ret = 0;
        int skill = (column - 3) / 2;

        switch (skill) {
            case 0:
                ret = trainPlayer.getShootingPoints();
                break;
            case 1:
                ret = trainPlayer.getPassingPoints();
                break;
            case 2:
                ret = trainPlayer.getCrossPoints();
                break;
            case 3:
                ret = trainPlayer.getDribblingPoints();
                break;
            case 4:
                ret = trainPlayer.getTacklingPoints();
                break;
            case 5:
                ret = trainPlayer.getSpeedPoints();
                break;
            case 6:
                ret = trainPlayer.getStaminaPoints();
                break;
            case 7:
                ret = trainPlayer.getHeadingPoints();
                break;
            case 8:
                ret = trainPlayer.getReflexesPoints();
                break;
            case 9:
                ret = trainPlayer.getHandlingPoints();
                break;
            case 10:
                ret = trainPlayer.getFitnessPoints();
                break;
            case 11:
                ret = trainPlayer.getMoraleFinance();
                break;
        }
        return ret;
    }

    public void updateMessages() {

        mainItem.setText(MainWindow.getMessage("trainingTabName"));

        String[] titles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("PTshooting"), "", MainWindow.getMessage("PTpassing"), "", MainWindow.getMessage("PTcross"), "",
                MainWindow.getMessage("PTdribbling"), "", MainWindow.getMessage("PTtackling"), "", MainWindow.getMessage("PTspeed"), "",
                MainWindow.getMessage("PTstamina"), "", MainWindow.getMessage("PTheading"), "", MainWindow.getMessage("PTreflexes"), "",
                MainWindow.getMessage("PThandling"), "", MainWindow.getMessage("PlayerFitness"), "", MainWindow.getMessage("PTmorale"),
                MainWindow.getMessage("PTmoraleFinance"), MainWindow.getMessage("PTfitnessFinance") };

        String[] tooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("tooltip.Shooting"), "", MainWindow.getMessage("tooltip.Passing"), "",
                MainWindow.getMessage("tooltip.Crossing"), "", MainWindow.getMessage("tooltip.Dribbling"), "",
                MainWindow.getMessage("tooltip.Tackling"), "", MainWindow.getMessage("tooltip.Speed"), "", MainWindow.getMessage("tooltip.Stamina"),
                "", MainWindow.getMessage("tooltip.Heading"), "", MainWindow.getMessage("tooltip.Reflexes"), "",
                MainWindow.getMessage("tooltip.TechnicsGK"), "", MainWindow.getMessage("tooltip.Physics"), "",
                MainWindow.getMessage("tooltip.Morale"), MainWindow.getMessage("tooltip.MoraleAward"),
                MainWindow.getMessage("tooltip.FitnessAward") };
        playersTable.setRedraw(false);
        for (int i = 0; i < titles.length; i++) {
            TableColumn column = playersTable.getColumn(i);
            column.setText(titles[i]);
            column.setToolTipText(tooltips[i]);
            GuiUtils.autoAdjustTableColumnWidth(playersTable, i);
        }
        playersTable.setRedraw(true);
        pointsGroup.setText(MainWindow.getMessage("PTPointsGroup"));
        pointsGroup.getParent().layout();
        mainCoachLabelName.setText(MainWindow.getMessage("PTMainCoachPoints") + ":");
        mainCoachLabelName.getParent().layout();
        goalkeeperLabelName.setText(MainWindow.getMessage("PTGoalkeeperPoints") + ":");
        goalkeeperLabelName.getParent().layout();
        defenderLabelName.setText(MainWindow.getMessage("PTDefenderPoints") + ":");
        defenderLabelName.getParent().layout();
        midfielderLabelName.setText(MainWindow.getMessage("PTMidfielderPoints") + ":");
        midfielderLabelName.getParent().layout();
        forwardLabelName.setText(MainWindow.getMessage("PTForwardPoints") + ":");
        forwardLabelName.getParent().layout();

        scoutGroup.setText(MainWindow.getMessage("PTScoutGroup"));
        scoutGroup.getParent().layout();
        scoutRestLabel.setText(MainWindow.getMessage("PTScoutVacation"));
        scoutRestLabel.getParent().layout();
        clubResGroup.setText(MainWindow.getMessage("PTClubResGroup"));
        clubResGroup.getParent().layout();
        scoutTalent.setText(MainWindow.getMessage("PTScoutTalent"));
        scoutTalent.getParent().layout();

        clubResLabelVal.setText(String.valueOf(clubResource) + " " + MainWindow.getMessage("Thousands"));

        openButton.setText(MainWindow.getMessage("global.open"));
        saveButton.setText(MainWindow.getMessage("global.save"));
        resetTraningsButton.setText(MainWindow.getMessage("training.resetTrainings"));

        openButton.getParent().layout();

        daysOfRestComposite.updateMessages();
        daysOfRestComposite.getParent().layout();
        redraw();
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
        if (!lazyUpdate) {
            updateAll();
        }
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    @Override
    public void dispose() {
        playersTableFont.dispose();
        imgTraining.dispose();
        red.dispose();
        green.dispose();
        black.dispose();
        blue.dispose();
        some1.dispose();
        some2.dispose();
    }

    @Override
    public void store(Properties props) {
        props.put(MainWindow.PROP_TRAININGS_FONT_DATA, playersFontData);
    }

}
