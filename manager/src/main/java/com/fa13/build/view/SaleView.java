package com.fa13.build.view;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.PlayerActionsReader;
import com.fa13.build.controller.io.PlayerActionsWriter;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.model.ExchangeBid;
import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerActions;
import com.fa13.build.model.PlayerBid;
import com.fa13.build.model.RentBid;
import com.fa13.build.model.RentBid.RentType;
import com.fa13.build.model.SellBid;
import com.fa13.build.model.SellBid.SellType;
import com.fa13.build.model.Team;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;

public class SaleView implements UIItem, IDisposable, IStorable {

    String password;
    TabFolder parentInstance;
    TabItem mainItem;
    Composite teamSquardPanel;
    Composite salePanel;
    Composite exchangePanel;
    Composite topComposite;
    Composite gridComposite;
    Composite saveComposite;
    Composite toggleComposite;
    Composite someComposite;
    Composite rentPanel;
    ScrolledComposite rightPanelScrl;
    Table teamTable;
    Table rentTableIn;
    Table rentTableOut;
    Table saleTablePrice;
    Table exchangeTable;
    Button saleButton;
    Button rentButton;
    Button exchangeButton;
    Button saveButton;
    Button openButton;

    Button[] toggleButtons;

    PlayerActions bid;
    List<RentBid> rentBidsIn;
    List<RentBid> rentBidsOut;
    List<SellBid> saleBids;
    List<ExchangeBid> exchangeBids;
    TableEditor saleEditor;
    TableEditor rentInEditor;
    TableEditor rentOutEditor;
    TableEditor exchangeEditor;

    static Color tableColors[] = null;
    static Font tableFonts[] = null;

    static Map<String, Image> flags;

    static Listener paintListener;
    static String[] titles;
    ViewMode viewMode;
    private Label rentOutLabel;
    private Label rentInLabel;
    private Font teamTableFont;
    private Image imageSale;
    private Image imageRent;
    private Image imageExchange;
    private Image imgPlayers;

    static final int BID_SIZE = 5;

    private static enum ViewMode {
        SALES, RENT, EXCHANGE
    }

    public SaleView(TabFolder parent) {

        parentInstance = parent;
        loadImages();
        rentBidsIn = new ArrayList<RentBid>(BID_SIZE);
        rentBidsOut = new ArrayList<RentBid>(BID_SIZE);
        saleBids = new ArrayList<SellBid>(BID_SIZE);
        saleBids = new ArrayList<SellBid>(BID_SIZE);
        exchangeBids = new ArrayList<ExchangeBid>(BID_SIZE);
        for (int i = 0; i < BID_SIZE; i++) {
            RentBid rb = new RentBid(RentType.RT_OFFER, "", 0);
            rb.setEmpty(true);
            rentBidsOut.add(rb);
            rb = new RentBid(RentType.RT_TAKE, "", 0);
            rb.setEmpty(true);
            rentBidsIn.add(rb);

            SellBid sb = new SellBid(SellType.SL_VALUE, 0, 0);
            sb.setEmpty(true);
            saleBids.add(sb);

            ExchangeBid eb = new ExchangeBid(0, 0, 0, "");
            exchangeBids.add(eb);
        }

        mainItem = new TabItem(parent, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("saleEditorTabName"));
        mainItem.setImage(imgPlayers);
        topComposite = new Composite(parent, SWT.NONE);
        mainItem.setControl(topComposite);
        topComposite.setLayout(new FormLayout());

        FormData data = new FormData();

        gridComposite = new Composite(topComposite, SWT.NONE);
        saveComposite = new Composite(topComposite, SWT.BORDER);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.bottom = new FormAttachment(saveComposite, -3);
        data.top = new FormAttachment(0, 0);
        gridComposite.setLayoutData(data);
        gridComposite.setLayout(new FormLayout());

        data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        saveComposite.setLayoutData(data);
        saveComposite.setLayout(new FormLayout());

        saveButton = new Button(saveComposite, SWT.PUSH);
        saveButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saveButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        data = new FormData();
        data.bottom = new FormAttachment(100, -5);
        data.top = new FormAttachment(0, 5);
        data.right = new FormAttachment(100, -5);
        saveButton.setLayoutData(data);
        saveButton.addSelectionListener(new SaveButtonListner());

        openButton = new Button(saveComposite, SWT.PUSH);
        openButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openButton.setImage(MainWindow.getSharedImage(ImageType.OPEN));
        data = new FormData();
        data.bottom = new FormAttachment(100, -5);
        data.top = new FormAttachment(0, 5);
        data.right = new FormAttachment(saveButton, -5);
        openButton.setLayoutData(data);
        openButton.addSelectionListener(new OpenButtonListner());

        saveComposite.layout();

        teamSquardPanel = new Composite(gridComposite, SWT.NONE);
        someComposite = new Composite(gridComposite, SWT.NONE);
        someComposite.setLayout(new FormLayout());
        teamSquardPanel.setLayout(new FillLayout());

        teamTable = new Table(teamSquardPanel, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
        teamTable.setHeaderVisible(true);
        teamTable.setLinesVisible(true);
        teamTable.setHeaderVisible(true);
        teamTableFont = teamTable.getFont();
        teamTable.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(teamTable);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("Font"));
                item.addSelectionListener(new SelectionAdapter() {

                    public void widgetSelected(SelectionEvent e) {
                        //change font
                        FontDialog dlg = new FontDialog(parentInstance.getShell());
                        dlg.setFontList(teamTableFont.getFontData());
                        FontData fontData = dlg.open();
                        if (fontData != null) {
                            teamTableFont = new Font(parentInstance.getShell().getDisplay(), fontData);
                        }
                        redrawTeamSquard();

                    }
                });

                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        String[] titles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("PlayerSalary"), MainWindow.getMessage("PlayerGames"),
                MainWindow.getMessage("PlayerGoalsTotal"), MainWindow.getMessage("PlayerChampGoals"), MainWindow.getMessage("PlayerGoalsMissed"),
                MainWindow.getMessage("PlayerAssists"), MainWindow.getMessage("PlayerProfit"), MainWindow.getMessage("PlayerMark"),
                MainWindow.getMessage("PlayerGamesCareer"), MainWindow.getMessage("PlayerGoalsCareer"), MainWindow.getMessage("PlayerState"),
                MainWindow.getMessage("PlayerYellowCards"), MainWindow.getMessage("PlayerRedCards"), MainWindow.getMessage("PlayerHomeClub"),
                MainWindow.getMessage("PlayerBirthTour"), MainWindow.getMessage("PlayerBirthDate") };

        String[] tooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("tooltip.Salary"),
                MainWindow.getMessage("tooltip.GamePlayed"), MainWindow.getMessage("tooltip.TotalGoals"),
                MainWindow.getMessage("tooltip.ChampionshipGoals"), MainWindow.getMessage("tooltip.MissedGoals"),
                MainWindow.getMessage("tooltip.GoalAssists"), MainWindow.getMessage("tooltip.PlayerProfit"), MainWindow.getMessage("PlayerMark"),
                MainWindow.getMessage("tooltip.CareerMatches"), MainWindow.getMessage("tooltip.CareerGoals"),
                MainWindow.getMessage("tooltip.PlayerState"), MainWindow.getMessage("tooltip.YellowCards"),
                MainWindow.getMessage("tooltip.RedCards"), MainWindow.getMessage("PlayerHomeClub"), MainWindow.getMessage("PlayerBirthTour"),
                MainWindow.getMessage("PlayerBirthDate") };

        GuiUtils.initTable(teamTable, titles, tooltips, SWT.CENTER, true);
        teamTable.getColumn(2).setAlignment(SWT.LEFT);
        
        toggleComposite = new Composite(someComposite, SWT.NONE);
        toggleComposite.setLayout(new FormLayout());
        rightPanelScrl = new ScrolledComposite(someComposite, SWT.NONE | SWT.H_SCROLL | SWT.V_SCROLL);
        rightPanelScrl.setExpandHorizontal(false);
        rightPanelScrl.setExpandVertical(true);
        rentPanel = new Composite(rightPanelScrl, SWT.BORDER);
        salePanel = new Composite(rightPanelScrl, SWT.BORDER);
        exchangePanel = new Composite(rightPanelScrl, SWT.BORDER);

        rentPanel.setLayout(new GridLayout(1, false));

        saleButton = new Button(toggleComposite, SWT.TOGGLE);
        saleButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saleButton.setImage(imageSale);
        rentButton = new Button(toggleComposite, SWT.TOGGLE);
        rentButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        rentButton.setImage(imageRent);
        exchangeButton = new Button(toggleComposite, SWT.TOGGLE);
        exchangeButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        exchangeButton.setImage(imageExchange);

        toggleButtons = new Button[3];
        toggleButtons[0] = saleButton;
        toggleButtons[1] = rentButton;
        toggleButtons[2] = exchangeButton;

        ToggleViewModeBtnListener toggleListener = new ToggleViewModeBtnListener();

        int scrollWidth = 0;// = rightPanelScrl.getVerticalBar().getSize().x;
        saleButton.setText(MainWindow.getMessage("SaleToggleSale"));
        data = new FormData();
        data.left = new FormAttachment(0, scrollWidth);
        //data.right = new FormAttachment(saleButton, saleButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(0, 0);
        saleButton.setLayoutData(data);
        saleButton.addSelectionListener(toggleListener);

        rentButton.setText(MainWindow.getMessage("SaleToggleRent"));
        data = new FormData();
        data.left = new FormAttachment(saleButton, 0);
        //data.right = new FormAttachment(rentButton, rentButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(0, 0);
        rentButton.setLayoutData(data);
        rentButton.addSelectionListener(toggleListener);

        exchangeButton.setText(MainWindow.getMessage("SaleToggleExchange"));
        data = new FormData();
        data.left = new FormAttachment(rentButton, 0);
        data.top = new FormAttachment(0, 0);
        exchangeButton.setLayoutData(data);
        exchangeButton.addSelectionListener(toggleListener);

        Point toggleCompositeSize = toggleComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(toggleComposite, 10);
        data.bottom = new FormAttachment(toggleComposite, toggleCompositeSize.y);
        toggleComposite.setLayoutData(data);

        rentInLabel = new Label(rentPanel, SWT.CENTER);
        rentInLabel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        rentInLabel.setText(MainWindow.getMessage("rentIn_players"));
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(toggleComposite, 10);
        rentInLabel.setLayoutData(data);

        rentTableIn = new Table(rentPanel, SWT.BORDER | SWT.FULL_SELECTION);
        rentTableIn.setHeaderVisible(true);
        rentTableIn.setLinesVisible(true);
        rentTableIn.setHeaderVisible(true);
        rentTableIn.setToolTipText(MainWindow.getMessage("RentIn.tooltip"));

        String[] titlesRentIn = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("OldClub") };
        String[] tooltipsRentIn = { MainWindow.getMessage("playerNumberHint"), MainWindow.getMessage("OldClub") };
        GuiUtils.initTable(rentTableIn, titlesRentIn, tooltipsRentIn, SWT.NONE, true);
        
        rentTableIn.setItemCount(BID_SIZE);
        for (int i = 0; i < BID_SIZE; i++) {
            rentTableIn.getItem(i).setData(rentBidsIn.get(i));
        }
        GuiUtils.autoAdjustTableColumnsWidth(rentTableIn);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        Point size = rentTableIn.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.right = new FormAttachment(rentTableIn, size.x);
        data.top = new FormAttachment(rentInLabel, 0);
        rentTableIn.setLayoutData(data);

        saleTablePrice = new Table(salePanel, SWT.BORDER | SWT.FULL_SELECTION);
        saleTablePrice.setHeaderVisible(true);
        saleTablePrice.setLinesVisible(true);
        saleTablePrice.setHeaderVisible(true);

        String[] titlesPrice = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("SalePrice"),
                MainWindow.getMessage("SaleType") };

        GuiUtils.initTable(saleTablePrice, titlesPrice, null, SWT.NONE, true);
        saleTablePrice.setItemCount(BID_SIZE);
        for (int i = 0; i < BID_SIZE; i++) {
            saleTablePrice.getItem(i).setData(saleBids.get(i));
        }
        GuiUtils.autoAdjustTableColumnsWidth(saleTablePrice);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        size = saleTablePrice.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.right = new FormAttachment(saleTablePrice, size.x);
        data.top = new FormAttachment(toggleComposite, 4);
        saleTablePrice.setLayoutData(data);

        //exchanges table
        exchangeTable = new Table(exchangePanel, SWT.BORDER | SWT.FULL_SELECTION);
        exchangeTable.setHeaderVisible(true);
        exchangeTable.setLinesVisible(true);
        exchangeTable.setHeaderVisible(true);

        exchangeTable.setToolTipText(MainWindow.getMessage("Exchanges.tooltip"));
        String[] titlesExchanges = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("Club"), "##",
                MainWindow.getMessage("Surcharge") };

        String[] tooltipsExchanges = { MainWindow.getMessage("OwnPlayerNumberHint"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("OtherClub"), MainWindow.getMessage("OtherTeamPlayerNumberHint"), MainWindow.getMessage("Surcharge") };

        GuiUtils.initTable(exchangeTable, titlesExchanges, tooltipsExchanges, SWT.NONE, true);
        exchangeTable.setItemCount(BID_SIZE);
        for (int i = 0; i < BID_SIZE; i++) {
            exchangeTable.getItem(i).setData(exchangeBids.get(i));
        }
        GuiUtils.autoAdjustTableColumnsWidth(exchangeTable);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        size = exchangeTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.right = new FormAttachment(exchangeTable, size.x);
        data.top = new FormAttachment(toggleComposite, 4);
        exchangeTable.setLayoutData(data);
        //end exchanges table

        rentOutLabel = new Label(rentPanel, SWT.CENTER);
        rentOutLabel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        rentOutLabel.setText(MainWindow.getMessage("rentOut_players"));
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(rentTableIn, 10);
        rentOutLabel.setLayoutData(data);

        rentTableOut = new Table(rentPanel, SWT.BORDER | SWT.FULL_SELECTION);
        rentTableOut.setHeaderVisible(true);
        rentTableOut.setLinesVisible(true);
        rentTableOut.setHeaderVisible(true);
        rentTableOut.setToolTipText(MainWindow.getMessage("RentOut.tooltip"));

        String[] titlesRentOut = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("NewClub") };
        String[] tooltipsRentOut = { MainWindow.getMessage("playerNumberHint"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("NewClub") };

        GuiUtils.initTable(rentTableOut, titlesRentOut, tooltipsRentOut, SWT.NONE, true);
        rentTableOut.setItemCount(BID_SIZE);
        for (int i = 0; i < BID_SIZE; i++) {
            rentTableOut.getItem(i).setData(rentBidsOut.get(i));
        }
        GuiUtils.autoAdjustTableColumnsWidth(rentTableOut);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(rentOutLabel, 0);
        rentTableOut.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(toggleComposite, 0);
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(100, 0);
        rightPanelScrl.setLayoutData(data);

        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        rentPanel.setLayoutData(data);
        rentPanel.setLayout(new FormLayout());

        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        salePanel.setLayoutData(data);
        salePanel.setLayout(new FormLayout());

        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        exchangePanel.setLayoutData(data);
        exchangePanel.setLayout(new FormLayout());

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(someComposite, -5);
        teamSquardPanel.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(100, -200);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(100, 0);
        someComposite.setLayoutData(data);

        //pricePanel.setVisible(true);
        //viewMode = ViewMode.SALES;
        saleButton.setSelection(true);
        updateViewMode();
        /*
         * size = rightPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT);
         * rightPanelScrl.setMinWidth(size.x);
         * rightPanelScrl.setMinHeight(size.y);
         */

        Map<String, Image> tmp = new HashMap<String, Image>();
        Set<Map.Entry<String, String>> nations = Player.NATIONALITIES.entrySet();

        for (Iterator<Map.Entry<String, String>> iterator = nations.iterator(); iterator.hasNext();) {
            Map.Entry<String, String> currEntry = iterator.next();
            String curr = currEntry.getValue();
            Image img = new Image(gridComposite.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/flags/" + curr + ".png"));
            tmp.put(curr, img);
        }
        flags = Collections.unmodifiableMap(tmp);
        final Image img = new Image(gridComposite.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/flags/not.png"));

        paintListener = new Listener() {
            public void handleEvent(Event event) {
                switch (event.type) {
                    case SWT.MeasureItem: {
                        if (event.index == 3) {
                            Rectangle rect = img.getBounds();
                            event.width = rect.width;
                            event.height = Math.max(event.height, rect.height + 2);
                        }
                        break;
                    }
                    case SWT.PaintItem: {
                        if (event.index == 3) {
                            int x = event.x;
                            Rectangle rect = img.getBounds();
                            int yoffset = Math.max(0, (event.height - rect.height) / 2);
                            event.gc.drawImage((Image) event.item.getData(), x + (teamTable.getColumn(3).getWidth() - rect.width) / 2, event.y
                                    + yoffset);
                        }
                        break;
                    }
                }
            }
        };

        setDragDropSource(teamTable);
        setDragDropTarget(rentTableOut);
        setDragDropTarget(saleTablePrice);
        setDragDropTarget(exchangeTable);

        rentTableIn.addMenuDetectListener(new BidTableMenuListener(rentTableIn));
        rentTableOut.addMenuDetectListener(new BidTableMenuListener(rentTableOut));
        saleTablePrice.addMenuDetectListener(new BidTableMenuListener(saleTablePrice));
        exchangeTable.addMenuDetectListener(new BidTableMenuListener(exchangeTable));

        saleEditor = new TableEditor(saleTablePrice);
        saleEditor.horizontalAlignment = SWT.LEFT;
        saleEditor.grabHorizontal = true;
        saleEditor.minimumWidth = 35;
        saleTablePrice.addListener(SWT.MouseDown, new SaleEditListener());

        rentInEditor = new TableEditor(rentTableIn);
        rentInEditor.horizontalAlignment = SWT.LEFT;
        rentInEditor.grabHorizontal = true;
        rentInEditor.minimumWidth = 35;
        rentTableIn.addListener(SWT.MouseDown, new RentInEditListener());

        rentOutEditor = new TableEditor(rentTableOut);
        rentOutEditor.horizontalAlignment = SWT.LEFT;
        rentOutEditor.grabHorizontal = true;
        rentOutEditor.minimumWidth = 35;
        rentTableOut.addListener(SWT.MouseDown, new RentOutEditListener());

        exchangeEditor = new TableEditor(exchangeTable);
        exchangeEditor.horizontalAlignment = SWT.LEFT;
        exchangeEditor.grabHorizontal = true;
        exchangeEditor.minimumWidth = 35;
        exchangeTable.addListener(SWT.MouseDown, new ExchangeEditListener());

        updateAll();

    }

    private void loadImages() {

        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/sale_16x16.png"));
        imageSale = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/rent_16x16.png"));
        imageRent = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/exchange_16x16.png"));
        imageExchange = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/players_16x16.png"));
        imgPlayers = new Image(parentInstance.getDisplay(), imgData);
    }

    public void updateAll() {
        if (!needUpdate)
            return;
        for (int i = 0; i < BID_SIZE; ++i) {
            rentBidsIn.get(i).setEmpty(true);
            rentBidsOut.get(i).setEmpty(true);
            saleBids.get(i).setEmpty(true);
            exchangeBids.get(i).setEmpty(true);
        }
        redrawTeamSquard();
        redraw();
        needUpdate = false;
    }

    /**
     * redraw squard players table & all bids tables
     */
    public void redraw() {

        if (MainWindow.getAllInstance() == null || MainWindow.getAllInstance().getCurrentTeam() == null) {
            return;
        }

        Team curTeam = MainWindow.getAllInstance().getCurrentTeam();

        rentTableIn.setRedraw(false);
        rentTableOut.setRedraw(false);
        saleTablePrice.setRedraw(false);
        exchangeTable.setRedraw(false);

        for (int i = 0; i < BID_SIZE; i++) {
            
            TableItem item = saleTablePrice.getItem(i);
            SellBid sb = saleBids.get(i);
            RentBid rbIn = rentBidsIn.get(i);
            RentBid rbOut = rentBidsOut.get(i);
            ExchangeBid eb = exchangeBids.get(i);
            
            if (!sb.isEmpty()) {
                Player player = curTeam.getPlayerByNumber(sb.getNumber());
                item.setText(0, String.valueOf(sb.getNumber()));
                item.setText(1, (player == null) ? "" : player.getName());
                item.setText(2, String.valueOf(sb.getValue()));
                item.setText(3, printSaleType(sb.getType()));
            } else {
                item.setText(0, "");
                item.setText(1, "");
                item.setText(2, "");
            }
            item = rentTableIn.getItem(i);
            if (!rbIn.isEmpty()) {
                item.setText(0, String.valueOf(rbIn.getNumber()));
                item.setText(1, rbIn.getTeam());
            } else {
                item.setText(0, "");
                item.setText(1, "");
            }
            item = rentTableOut.getItem(i);
            if (!rbOut.isEmpty()) {
                Player player = curTeam.getPlayerByNumber(rbOut.getNumber());
                item.setText(0, String.valueOf(rbOut.getNumber()));
                item.setText(1, (player == null) ? "" : player.getName());
                item.setText(2, rbOut.getTeam());
            } else {
                item.setText(0, "");
                item.setText(1, "");
                item.setText(2, "");
            }

            item = exchangeTable.getItem(i);
            if (!eb.isEmpty()) {
                Player player = curTeam.getPlayerByNumber(eb.getNumber());
                item.setText(0, String.valueOf(eb.getNumber()));
                item.setText(1, (player == null) ? "" : player.getName());
                item.setText(2, String.valueOf(eb.getOtherTeam()));
                item.setText(3, String.valueOf(eb.getOtherPlayerNumber()));
                item.setText(4, String.valueOf(eb.getSurcharge()));
            } else {
                item.setText(0, "");
                item.setText(1, "");
                item.setText(2, "");
                item.setText(3, "");
                item.setText(4, "");
            }

        }

        saleTablePrice.setRedraw(true);
        exchangeTable.setRedraw(true);
        rentTableIn.setRedraw(true);
        rentTableOut.setRedraw(true);

        topComposite.layout();

        autoLayoutBidsPanel();
    }

    public void redrawTeamSquard() {

        teamTable.setRedraw(false);
        teamTable.setFont(teamTableFont);
        teamTable.removeAll();
        if (MainWindow.getAllInstance() != null) {
            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (curTeam != null) {
                int i = 0;
                List<Player> players = curTeam.getPlayers();
                teamTable.setItemCount(players.size());
                for (Player player : players) {
                    int index = 0;
                    TableItem item = teamTable.getItem(i);
                    item.setData(player);
                    item.setText(index++, String.valueOf(player.getNumber()));
                    item.setText(index++, String.valueOf(player.getPosition()));
                    item.setText(index++, player.getName());
                    item.setText(index++, String.valueOf(player.getPrice()));
                    item.setText(index++, String.valueOf(player.getSalary()));
                    item.setText(index++, String.valueOf(player.getGames()));
                    item.setText(index++, String.valueOf(player.getGoalsTotal()));
                    item.setText(index++, String.valueOf(player.getGoalsChamp()));
                    item.setText(index++, String.valueOf(player.getGoalsMissed()));
                    item.setText(index++, String.valueOf(player.getAssists()));
                    item.setText(index++, String.valueOf(player.getProfit()));
                    item.setText(index++, String.valueOf(player.getMark()));
                    item.setText(index++, String.valueOf(player.getGamesCareer()));
                    item.setText(index++, String.valueOf(player.getGoalsCareer()));
                    String tmpString = "";
                    if (player.isTransfer()) {
                        tmpString = "T";
                    } else if (player.isLease()) {
                        tmpString = "A";
                    } else {
                        tmpString = "K";
                    }
                    item.setText(index++, tmpString);
                    item.setText(index++, String.valueOf(player.getYellowCards()));
                    item.setText(index++, String.valueOf(player.getRedCards()));
                    item.setText(index++, player.getBirthplace());
                    item.setText(index++, String.valueOf(player.getBirthtour()));
                    if (player.getBirthdate() == null) {
                        tmpString = "";
                    } else {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        tmpString = dateFormat.format(player.getBirthdate());
                    }
                    item.setText(index++, tmpString);
                    i++;
                }
                
                GuiUtils.autoAdjustTableColumnsWidth(teamTable);
            }
        }
        teamTable.setRedraw(true);

    }

    public void setDragDropSource(final Table table) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        final DragSource source = new DragSource(table, operations);
        source.setTransfer(types);
        source.addDragListener(new DragSourceListener() {
            public void dragStart(DragSourceEvent event) {

            }

            public void dragSetData(DragSourceEvent event) {
                TableItem items[] = table.getSelection();
                Player player = (Player) items[0].getData();
                try {
                    event.data = String.valueOf(player.getNumber());
                } catch (SWTException e) {
                    event.doit = false;
                }
            }

            public void dragFinished(DragSourceEvent event) {

            }
        });
    }

    private class RentInEditListener implements Listener {

        public void handleEvent(Event event) {
            Rectangle clientArea = rentTableIn.getClientArea();
            Point pt = new Point(event.x, event.y);
            if (event.button == 1) {
                int index = rentTableIn.getTopIndex();
                while (index < rentTableIn.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = rentTableIn.getItem(index);
                    for (int i = 0; i < rentTableIn.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt) && i != 2) {
                            final int column = i;
                            final int row = index;
                            final Text text = new Text(rentTableIn, SWT.NONE);
                            Listener textListener = new Listener() {
                                public void handleEvent(final Event e) {
                                    RentBid rb = (RentBid) item.getData();
                                    switch (e.type) {
                                        case SWT.FocusOut:
                                            switch (column) {
                                                case 0:
                                                    int val = 1;
                                                    try {
                                                        val = Integer.valueOf(text.getText());
                                                    } catch (NumberFormatException nbe) {
                                                        val = -1;
                                                    } finally {
                                                        if ((val < 1) || (val > 25)) {
                                                            val = 0;
                                                        }
                                                    }
                                                    rb.setNumber(val);
                                                    break;
                                                case 1:
                                                    rb.setTeam(text.getText());
                                            }
                                            rb.setEmpty(false);
                                            redraw();
                                            text.dispose();
                                            break;
                                        case SWT.Traverse:
                                            switch (e.detail) {
                                                case SWT.TRAVERSE_RETURN:
                                                    switch (column) {
                                                        case 0:
                                                            int val = 1;
                                                            try {
                                                                val = Integer.valueOf(text.getText());
                                                            } catch (NumberFormatException nbe) {
                                                                val = -1;
                                                            } finally {
                                                                if ((val < 1) || (val > 25)) {
                                                                    val = 0;
                                                                }
                                                            }
                                                            rb.setNumber(val);
                                                            break;
                                                        case 1:
                                                            rb.setTeam(text.getText());
                                                    }
                                                    rb.setEmpty(false);
                                                    redraw();
                                                    //FALL THROUGH
                                                case SWT.TRAVERSE_ESCAPE:
                                                    text.dispose();
                                                    e.doit = false;
                                            }
                                            break;
                                        case SWT.Verify:
                                            if (column == 0) {
                                                String string = e.text;
                                                char[] chars = new char[string.length()];
                                                string.getChars(0, chars.length, chars, 0);
                                                for (int i = 0; i < chars.length; i++) {
                                                    if (!('0' <= chars[i] && chars[i] <= '9')) {
                                                        e.doit = false;
                                                        return;
                                                    }
                                                }
                                            } else {
                                                e.doit = true;
                                            }
                                    }
                                }
                            };
                            text.addListener(SWT.FocusOut, textListener);
                            text.addListener(SWT.Traverse, textListener);
                            text.addListener(SWT.Verify, textListener);
                            rentInEditor.setEditor(text, item, i);
                            text.setText(item.getText(i));
                            text.selectAll();
                            text.setFocus();
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        }

    }

    public void setDragDropTarget(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        Transfer types[] = new Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    if (event.item instanceof TableItem) {
                        if (event.data != null) {
                            String st = (String) event.data;
                            int playerNumber = Integer.valueOf(st);
                            for (int i = 0; i < table.getItemCount(); i++) {
                                TableItem targetItem = table.getItem(i);
                                PlayerBid pb = (PlayerBid) targetItem.getData();

                                if (pb.getNumber() == playerNumber) {
                                    return;
                                }

                                for (int j = 0; j < table.getColumnCount(); j++) {

                                    Rectangle rect = targetItem.getDisplay().map(targetItem.getParent(), null, targetItem.getBounds(j));
                                    //Rectangle rect = targetItem.getBounds();
                                    if (rect.contains(event.x, event.y)) {
                                        pb.setNumber(playerNumber);
                                        pb.setEmpty(false);

                                        if (table == saleTablePrice) {
                                            SellBid sb = (SellBid) targetItem.getData();
                                            sb.setType(SellType.SL_PERCENT);
                                            sb.setValue(90);
                                        }

                                        redraw();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    public void autoLayoutBidsPanel() {

        if (MainWindow.getAllInstance() != null) {

            Point togglePanelSize = toggleComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);

            if (viewMode == ViewMode.RENT) {
                rentTableIn.setRedraw(false);
                rentTableOut.setRedraw(false);
                Point size = rentTableIn.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
                Point size2 = rentTableOut.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
                int width = 0;

                for (int i = 0; i < rentTableIn.getColumnCount(); i++) {
                    GuiUtils.autoAdjustTableColumnWidth(rentTableIn, i);
                }

                for (int i = 0; i < rentTableOut.getColumnCount(); i++) {
                    GuiUtils.autoAdjustTableColumnWidth(rentTableOut, i);
                    int dWidth = rentTableOut.getColumn(i).getWidth();
                    width += dWidth;
                }
                
                //rentTableIn.getColumn(0).setWidth(rentTableOut.getColumn(0).getWidth());
                int sumW = rentTableOut.getColumn(1).getWidth() + rentTableOut.getColumn(2).getWidth();
                //rentTableIn.getColumn(1).setWidth(sumW);

                int paddingX = rightPanelScrl.getVerticalBar().getSize().x; 
                int heightAll = 0;
                int widthAll = 0;
                FormData data = (FormData) rentTableIn.getLayoutData();
                width += rentTableIn.getBorderWidth() * 2;
                data.left = new FormAttachment(0, paddingX);
                //data.right = new FormAttachment(rentTableIn, width);
                data.right = new FormAttachment(rentTableIn, size.x+paddingX);
                int height = rentTableIn.getItemCount() * rentTableIn.getItemHeight();
                height += rentTableIn.getHeaderHeight();
                height += rentTableIn.getBorderWidth() * 3;
                data.bottom = new FormAttachment(rentTableIn, size.y);
                //data.bottom = new FormAttachment(rentTableIn, height);
                //heightAll += height;
                //widthAll = width;
                heightAll = size.y;
                widthAll = size.x;
                
                data = (FormData) rentTableOut.getLayoutData();
                width -= rentTableIn.getBorderWidth() * 2;
                width += rentTableOut.getBorderWidth() * 2;
                data.left = new FormAttachment(0, paddingX);
                //data.right = new FormAttachment(rentTableOut, width);
                data.right = new FormAttachment(rentTableOut, size2.x+paddingX);
                height = rentTableOut.getItemCount() * rentTableIn.getItemHeight();
                height += rentTableOut.getHeaderHeight();
                height += rentTableOut.getBorderWidth() * 3;
                data.bottom = new FormAttachment(rentTableOut, size2.y);
                //data.bottom = new FormAttachment(rentTableOut, height);
//                heightAll += height;
//                widthAll = Math.max(widthAll, width);
                heightAll += size2.y;
                widthAll = Math.max(widthAll, size2.x);

                rentTableIn.getParent().layout();
                heightAll += rentInLabel.getSize().y + rentOutLabel.getSize().y + 20; //2*10 paddings
                rightPanelScrl.setMinWidth(widthAll);
                rightPanelScrl.setExpandHorizontal(true);
                rightPanelScrl.setMinHeight(heightAll);
                rightPanelScrl.layout();

                data = (FormData) rightPanelScrl.getLayoutData();
                int scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                scrollWidth = Math.max(scrollWidth, togglePanelSize.x);
                //data.left = new FormAttachment(rightPanelScrl, -scrollWidth);

                data = (FormData) toggleComposite.getLayoutData();
                data.right.offset = scrollWidth;
                data = (FormData) someComposite.getLayoutData();
                data.left.offset = -scrollWidth;
                rightPanelScrl.getParent().layout();
                rightPanelScrl.layout();

                rentTableIn.setRedraw(true);
                rentTableOut.setRedraw(true);

            } else if (viewMode == ViewMode.SALES) {
                saleTablePrice.setRedraw(false);
                int paddingX = rightPanelScrl.getVerticalBar().getSize().x; 
                Point size = saleTablePrice.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
                int width = 0;
                for (int i = 0; i < saleTablePrice.getColumnCount(); i++) {
                    int widthColum = 0;
                    GuiUtils.autoAdjustTableColumnWidth(saleTablePrice, i);
                    //saleTablePrice.getColumn(i).pack();
                    int dWidth = saleTablePrice.getColumn(i).getWidth();
                    widthColum = dWidth;
                    dWidth = 0;//saleTablePercent.getColumn(i).getWidth();
                    widthColum = Math.max(widthColum, dWidth);
                    width += widthColum;
                    //saleTablePrice.getColumn(i).setWidth(widthColum);
                }

                int heightAll = 0;
                int widthAll = 0;
                FormData data = (FormData) saleTablePrice.getLayoutData();
                width += saleTablePrice.getBorderWidth() * 2;
                data.left = new FormAttachment(0, paddingX);
                //data.right = new FormAttachment(saleTablePrice, width);
                data.right = new FormAttachment(saleTablePrice, size.x+paddingX);
                int height = saleTablePrice.getItemCount() * saleTablePrice.getItemHeight();
                height += saleTablePrice.getHeaderHeight();
                height += saleTablePrice.getBorderWidth() * 3;
                data.bottom = new FormAttachment(saleTablePrice, size.y);
                //data.bottom = new FormAttachment(saleTablePrice, height);
                heightAll += height;
                widthAll = width;

                heightAll += height;
                widthAll = Math.max(widthAll, width);
                heightAll = size.y;
                widthAll = size.x;
                saleTablePrice.getParent().layout();

                salePanel.setSize(widthAll, heightAll);
                rightPanelScrl.setMinWidth(widthAll);
                rightPanelScrl.setExpandHorizontal(true);
                rightPanelScrl.setMinHeight(heightAll);
                rightPanelScrl.layout();

                data = (FormData) rightPanelScrl.getLayoutData();
                int scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                //data.left = new FormAttachment(rightPanelScrl, -scrollWidth);

                scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                scrollWidth = Math.max(scrollWidth, togglePanelSize.x);

                data = (FormData) toggleComposite.getLayoutData();
                data.right.offset = scrollWidth;
                data = (FormData) someComposite.getLayoutData();
                data.left.offset = -scrollWidth;
                rightPanelScrl.getParent().layout();

                saleTablePrice.setRedraw(true);

            } else if (viewMode == ViewMode.EXCHANGE) {

                exchangeTable.setRedraw(false);
                int paddingX = exchangeTable.getVerticalBar().getSize().x; 
                Point size = exchangeTable.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
                
                int width = 0;
                for (int i = 0; i < exchangeTable.getColumnCount(); i++) {
                    int widthColum = 0;
                    GuiUtils.autoAdjustTableColumnWidth(exchangeTable, i);
                    //exchangeTable.getColumn(i).pack();
                    int dWidth = exchangeTable.getColumn(i).getWidth();
                    widthColum = dWidth;
                    dWidth = 0;
                    widthColum = Math.max(widthColum, dWidth);
                    width += widthColum;
                    //exchangeTable.getColumn(i).setWidth(widthColum);
                }

                int heightAll = 0;
                int widthAll = 0;
                FormData data = (FormData) exchangeTable.getLayoutData();
                width += exchangeTable.getBorderWidth() * 2;
                data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
                //data.right = new FormAttachment(exchangeTable, width);
                data.right = new FormAttachment(exchangeTable, size.x + paddingX);
                int height = exchangeTable.getItemCount() * exchangeTable.getItemHeight();
                height += exchangeTable.getHeaderHeight();
                height += exchangeTable.getBorderWidth() * 3;
                data.bottom = new FormAttachment(exchangeTable, size.y);
                //data.bottom = new FormAttachment(exchangeTable, height);
                heightAll += height;
                widthAll = width;
                heightAll += height;
                widthAll = Math.max(widthAll, width);
                exchangeTable.getParent().layout();

                exchangePanel.setSize(widthAll, heightAll);
                rightPanelScrl.setMinWidth(widthAll);
                rightPanelScrl.setExpandHorizontal(true);
                rightPanelScrl.setMinHeight(heightAll);
                rightPanelScrl.layout();

                data = (FormData) rightPanelScrl.getLayoutData();
                int scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                scrollWidth = Math.max(scrollWidth, togglePanelSize.x);

                //data.left = new FormAttachment(rightPanelScrl, -scrollWidth);

                scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;

                data = (FormData) toggleComposite.getLayoutData();
                data.right.offset = scrollWidth;
                data = (FormData) someComposite.getLayoutData();
                data.left.offset = -scrollWidth;
                rightPanelScrl.getParent().layout();

                exchangeTable.setRedraw(true);
            }
            someComposite.getParent().layout();
            someComposite.layout();
            rightPanelScrl.layout();
        }
    }

    private void updateViewMode() {

        if (saleButton.getSelection()) {
            viewMode = ViewMode.SALES;
            rentButton.setSelection(false);
            exchangeButton.setSelection(false);
            rightPanelScrl.setRedraw(false);
            rentPanel.setVisible(false);
            exchangePanel.setVisible(false);
            salePanel.setVisible(true);
            rightPanelScrl.setContent(salePanel);
            rightPanelScrl.setRedraw(true);

        }
        if (rentButton.getSelection()) {
            viewMode = ViewMode.RENT;
            saleButton.setSelection(false);
            exchangeButton.setSelection(false);
            rightPanelScrl.setRedraw(false);
            rentPanel.setVisible(true);
            exchangePanel.setVisible(false);
            salePanel.setVisible(false);
            rightPanelScrl.setContent(rentPanel);
            rightPanelScrl.setRedraw(true);

        } else if (exchangeButton.getSelection()) {
            viewMode = ViewMode.EXCHANGE;
            saleButton.setSelection(false);
            rentButton.setSelection(false);
            rightPanelScrl.setRedraw(false);
            rentPanel.setVisible(false);
            exchangePanel.setVisible(true);
            salePanel.setVisible(false);
            rightPanelScrl.setContent(exchangePanel);
            rightPanelScrl.setRedraw(true);
        }

    }

    private boolean toggleButtonSelectProcessing = false;

    public class ToggleViewModeBtnListener implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent e) {

            if (toggleButtonSelectProcessing)
                return;
            toggleButtonSelectProcessing = true;

            Button btn = (Button) e.widget;
            for (int i = 0; i < toggleButtons.length; i++)
                toggleButtons[i].setSelection(btn == toggleButtons[i]);

            if (!btn.getSelection())
                btn.setSelection(true);
            updateViewMode();
            autoLayoutBidsPanel();
            toggleButtonSelectProcessing = false;
        }

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

    }

    private class SaleEditListener implements Listener {

        public void handleEvent(Event event) {
            Rectangle clientArea = saleTablePrice.getClientArea();
            Point pt = new Point(event.x, event.y);
            if (event.button == 1) {
                int index = saleTablePrice.getTopIndex();
                while (index < saleTablePrice.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = saleTablePrice.getItem(index);

                    for (int i = 0; i < saleTablePrice.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {

                            PlayerBid pb = (PlayerBid) item.getData();
                            if (pb == null || pb.getNumber() == 0 || pb.isEmpty()) {
                                return;//actually empty bid - don't allow edit
                            }

                            final int column = i;
                            final int row = index;
                            if (i == 2) {
                                final Text text = new Text(saleTablePrice, SWT.NONE);
                                Listener textListener = new Listener() {
                                    public void handleEvent(final Event e) {
                                        int val = -1;
                                        try {
                                            val = Integer.valueOf(text.getText());
                                        } catch (NumberFormatException nfe) {
                                            val = -1;
                                        }
                                        SellBid sb = (SellBid) item.getData();
                                        switch (e.type) {
                                            case SWT.FocusOut:
                                                if (val > 0) {
                                                    sb.setValue(val);
                                                    sb.setEmpty(false);
                                                    redraw();
                                                }
                                                text.dispose();
                                                break;
                                            case SWT.Traverse:
                                                switch (e.detail) {
                                                    case SWT.TRAVERSE_RETURN:
                                                        if (val > 0) {
                                                            sb.setValue(val);
                                                            sb.setEmpty(false);
                                                            redraw();
                                                        }
                                                        //FALL THROUGH
                                                    case SWT.TRAVERSE_ESCAPE:
                                                        text.dispose();
                                                        e.doit = false;
                                                }
                                                break;
                                            case SWT.Verify:
                                                String string = e.text;
                                                char[] chars = new char[string.length()];
                                                string.getChars(0, chars.length, chars, 0);
                                                for (int i = 0; i < chars.length; i++) {
                                                    if (!('0' <= chars[i] && chars[i] <= '9')) {
                                                        e.doit = false;
                                                        return;
                                                    }
                                                }
                                        }
                                    }
                                };
                                text.addListener(SWT.FocusOut, textListener);
                                text.addListener(SWT.Traverse, textListener);
                                text.addListener(SWT.Verify, textListener);
                                text.setTextLimit(5);
                                saleEditor.setEditor(text, item, i);
                                text.setText(item.getText(i));
                                text.selectAll();
                                text.setFocus();
                            } else {
                                if (i == 3) {
                                    final Combo combo = new Combo(saleTablePrice, SWT.READ_ONLY);
                                    combo.add(MainWindow.getMessage("SalePercent"));
                                    combo.add(MainWindow.getMessage("SalePrice"));
                                    combo.select(0);
                                    Listener comboListener = new Listener() {
                                        public void handleEvent(final Event e) {
                                            switch (e.type) {
                                                case SWT.FocusOut:
                                                    if (combo.getSelectionIndex() != -1) {
                                                        SellBid sb = (SellBid) item.getData();
                                                        sb.setEmpty(false);
                                                        SellType st = (combo.getSelectionIndex() == 0) ? SellType.SL_PERCENT : SellType.SL_VALUE;
                                                        sb.setType(st);
                                                    }
                                                    redraw();
                                                    combo.dispose();
                                                    break;
                                                case SWT.Traverse:
                                                    switch (e.detail) {
                                                        case SWT.TRAVERSE_RETURN:
                                                        case SWT.FocusOut:
                                                            if (combo.getSelectionIndex() != -1) {
                                                                if (combo.getSelectionIndex() != -1) {
                                                                    SellBid sb = (SellBid) item.getData();
                                                                    sb.setEmpty(false);
                                                                    SellType st = (combo.getSelectionIndex() == 0) ? SellType.SL_PERCENT
                                                                            : SellType.SL_VALUE;
                                                                    sb.setType(st);
                                                                }
                                                                redraw();
                                                            }
                                                            redraw();
                                                            //FALL THROUGH
                                                        case SWT.TRAVERSE_ESCAPE:
                                                            combo.dispose();
                                                            e.doit = false;
                                                    }
                                                    break;
                                            }
                                        }
                                    };
                                    combo.addListener(SWT.FocusOut, comboListener);
                                    combo.addListener(SWT.Traverse, comboListener);
                                    saleEditor.setEditor(combo, item, i);
                                    combo.setText(item.getText(i));
                                    combo.setFocus();
                                }
                            }

                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        }

    }

    private class ExchangeEditListener implements Listener {

        public void handleEvent(Event event) {

            Rectangle clientArea = exchangeTable.getClientArea();
            Point pt = new Point(event.x, event.y);

            if (event.button == 1) {

                int index = exchangeTable.getTopIndex();

                while (index < exchangeTable.getItemCount()) {

                    boolean visible = false;
                    final TableItem item = exchangeTable.getItem(index);

                    for (int i = 0; i < exchangeTable.getColumnCount(); i++) {

                        Rectangle rect = item.getBounds(i);

                        if (rect.contains(pt)) {

                            PlayerBid pb = (PlayerBid) item.getData();
                            if (pb == null || pb.getNumber() == 0 || pb.isEmpty()) {
                                return;//actually empty bid - don't allow edit
                            }

                            final int column = i;
                            final int row = index;

                            if (column > 1) {

                                final Text text = new Text(exchangeTable, SWT.NONE);

                                Listener textListener = new Listener() {

                                    public void handleEvent(final Event e) {

                                        ExchangeBid eb = (ExchangeBid) item.getData();
                                        switch (e.type) {
                                            case SWT.FocusOut:

                                                if (column == 2)
                                                    eb.setOtherTeam(text.getText());
                                                else if (column == 3) {
                                                    int val = 0;
                                                    try {
                                                        val = Integer.valueOf(text.getText());
                                                    } catch (NumberFormatException ex) {
                                                        val = 0;
                                                    }
                                                    eb.setOtherPlayerNumber(val);
                                                } else if (column == 4) {
                                                    int val = 0;
                                                    try {
                                                        val = Integer.valueOf(text.getText());
                                                    } catch (NumberFormatException ex) {
                                                        val = 0;
                                                    }
                                                    eb.setSurcharge(val);
                                                }
                                                eb.setEmpty(false);

                                                redraw();
                                                text.dispose();
                                                break;
                                            case SWT.Traverse:
                                                switch (e.detail) {
                                                    case SWT.TRAVERSE_RETURN:
                                                        if (column == 2)
                                                            eb.setOtherTeam(text.getText());
                                                        else if (column == 3) {
                                                            int val = 0;
                                                            try {
                                                                val = Integer.valueOf(text.getText());
                                                            } catch (NumberFormatException ex) {
                                                                val = 0;
                                                            }
                                                            eb.setOtherPlayerNumber(val);
                                                        } else if (column == 4) {
                                                            int val = 0;
                                                            try {
                                                                val = Integer.valueOf(text.getText());
                                                            } catch (NumberFormatException ex) {
                                                                val = 0;
                                                            }
                                                            eb.setSurcharge(val);
                                                        }
                                                        eb.setEmpty(false);
                                                        redraw();
                                                        //FALL THROUGH
                                                    case SWT.TRAVERSE_ESCAPE:
                                                        text.dispose();
                                                        e.doit = false;
                                                }
                                                break;
                                            case SWT.Verify:
                                                if (column < 3)
                                                    return;
                                                String string = e.text;
                                                char[] chars = new char[string.length()];
                                                string.getChars(0, chars.length, chars, 0);
                                                for (int i = 0; i < chars.length; i++) {
                                                    if (column == 4) { //hotfix to allow negative
                                                        if (!(chars[i] == '-' || ('0' <= chars[i] && chars[i] <= '9'))) {
                                                            e.doit = false;
                                                            return;
                                                        }
                                                    } else if (!('0' <= chars[i] && chars[i] <= '9')) {
                                                        e.doit = false;
                                                        return;
                                                    }
                                                }

                                        }
                                    }
                                };
                                text.addListener(SWT.FocusOut, textListener);
                                text.addListener(SWT.Traverse, textListener);
                                text.addListener(SWT.Verify, textListener);
                                exchangeEditor.setEditor(text, item, i);
                                text.setText(item.getText(i));
                                text.selectAll();
                                text.setFocus();

                            } else if (column == 3 || column == 4) {
                                final Spinner spin = new Spinner(exchangeTable, SWT.NONE);
                            }
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }

        }

    }

    private String printSaleType(SellType st) {
        switch (st) {
            case SL_PERCENT:
                return MainWindow.getMessage("SalePercent");
            case SL_VALUE:
                return MainWindow.getMessage("SalePrice");
            default:
                return "";
        }
    }

    public class BidTableMenuListener implements MenuDetectListener {

        Table listenTable;

        public BidTableMenuListener(Table tbl) {
            listenTable = tbl;
        }

        public void menuDetected(MenuDetectEvent e) {
            Menu menu = new Menu(listenTable);
            MenuItem item = new MenuItem(menu, SWT.NONE);
            item.setText(MainWindow.getMessage("Clear"));
            item.addSelectionListener(new SelectionListener() {

                public void widgetSelected(SelectionEvent e) {
                    widgetDefaultSelected(e);

                }

                public void widgetDefaultSelected(SelectionEvent e) {
                    int row = listenTable.getSelectionIndex();
                    PlayerBid pb = ((PlayerBid) listenTable.getItem(row).getData());
                    pb.setEmpty(true);
                    pb.setNumber(0);

                    for (int col = 0; col < listenTable.getColumnCount(); col++)
                        listenTable.getItem(row).setText(col, "");

                    redraw();

                }
            });

            menu.setLocation(e.x, e.y);
            menu.setVisible(true);
        }

    }

    private class RentOutEditListener implements Listener {

        public void handleEvent(Event event) {
            Rectangle clientArea = rentTableOut.getClientArea();
            Point pt = new Point(event.x, event.y);
            if (event.button == 1) {
                int index = rentTableOut.getTopIndex();
                while (index < rentTableOut.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = rentTableOut.getItem(index);
                    for (int i = 0; i < rentTableOut.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {

                            PlayerBid pb = (PlayerBid) item.getData();
                            if (pb == null || pb.getNumber() == 0 || pb.isEmpty()) {
                                return;//actually empty bid - don't allow edit
                            }

                            final int column = i;
                            final int row = index;
                            if (column == 2) {
                                final Text text = new Text(rentTableOut, SWT.NONE);
                                Listener textListener = new Listener() {
                                    public void handleEvent(final Event e) {
                                        RentBid rb = (RentBid) item.getData();
                                        switch (e.type) {
                                            case SWT.FocusOut:
                                                rb.setTeam(text.getText());
                                                rb.setEmpty(false);
                                                redraw();
                                                text.dispose();
                                                break;
                                            case SWT.Traverse:
                                                switch (e.detail) {
                                                    case SWT.TRAVERSE_RETURN:
                                                        rb.setTeam(text.getText());
                                                        rb.setEmpty(false);
                                                        redraw();
                                                        //FALL THROUGH
                                                    case SWT.TRAVERSE_ESCAPE:
                                                        text.dispose();
                                                        e.doit = false;
                                                }
                                                break;
                                        }
                                    }
                                };
                                text.addListener(SWT.FocusOut, textListener);
                                text.addListener(SWT.Traverse, textListener);
                                text.addListener(SWT.Verify, textListener);
                                //text.setTextLimit(5); no need
                                rentOutEditor.setEditor(text, item, i);
                                text.setText(item.getText(i));
                                text.selectAll();
                                text.setFocus();
                            }
                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        }

    }

    public class OpenButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            openSaleDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            openSaleDlg();
        }
    }

    public class SaveButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            saveSaleDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            saveSaleDlg();
        }
    }

    public void openSale(String fname) {
        if (fname == null) {
            return;
        }
        try {
            PlayerActions actions = PlayerActionsReader.readPlayerActionsFile(fname);
            bidFileName = fname;
            List<PlayerBid> bids = actions.getBids();
            PlayerBid pb;
            rentBidsIn = new ArrayList<RentBid>(BID_SIZE);
            rentBidsOut = new ArrayList<RentBid>(BID_SIZE);
            saleBids = new ArrayList<SellBid>(BID_SIZE);
            exchangeBids = new ArrayList<ExchangeBid>(BID_SIZE);
            int m = 0;
            int j = 0;
            int k = 0;
            int e = 0;
            for (int i = 0; i < bids.size(); i++) {
                pb = bids.get(i);
                if (pb.getClass() == SellBid.class && m < 5) {
                    saleBids.add((SellBid) pb);
                    m++;
                } else if (pb.getClass() == RentBid.class) {
                    if (((RentBid) pb).getType() == RentType.RT_TAKE && j < 5) {
                        rentBidsIn.add((RentBid) pb);
                        j++;
                    } else if (k < 5) {
                        rentBidsOut.add((RentBid) pb);
                        k++;
                    }
                } else if (pb.getClass() == ExchangeBid.class && e < 5) {
                    exchangeBids.add((ExchangeBid) pb);
                    e++;
                }
            }
            while (m < 5) {
                SellBid sb = new SellBid(SellType.SL_VALUE, 0, 0);
                sb.setEmpty(true);
                saleBids.add(sb);
                m++;
            }
            while (j < 5) {
                RentBid rb = new RentBid(RentType.RT_TAKE, "", 0);
                rb.setEmpty(true);
                rentBidsIn.add(rb);
                j++;
            }
            while (k < 5) {
                RentBid rb = new RentBid(RentType.RT_OFFER, "", 0);
                rb.setEmpty(true);
                rentBidsOut.add(rb);
                k++;
            }
            while (e < 5) {
                ExchangeBid eb = new ExchangeBid(0, 0, 0, "");
                eb.setEmpty(true);
                exchangeBids.add(eb);
                e++;
            }
            for (int i = 0; i < BID_SIZE; i++) {
                rentTableIn.getItem(i).setData(rentBidsIn.get(i));
                rentTableOut.getItem(i).setData(rentBidsOut.get(i));
                saleTablePrice.getItem(i).setData(saleBids.get(i));
                exchangeTable.getItem(i).setData(exchangeBids.get(i));
            }

        } catch (ReaderException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.open_bid_error"));
        }
        redraw();
    }

    public void saveSale(String fname) {
        if (fname == null) {
            return;
        }
        try {
            PlayerActions pa;
            List<PlayerBid> concatBids = new ArrayList<PlayerBid>(4 * BID_SIZE);

            for (int i = 0; i < BID_SIZE; i++) {
                if (rentBidsIn.get(i).isValid())
                    concatBids.add(rentBidsIn.get(i));
                if (rentBidsOut.get(i).isValid())
                    concatBids.add(rentBidsOut.get(i));
            }
            for (int i = 0; i < BID_SIZE; i++) {
                if (saleBids.get(i).isValid())
                    concatBids.add(saleBids.get(i));
            }

            for (int i = 0; i < BID_SIZE; i++) {
                if (exchangeBids.get(i).isValid())
                    concatBids.add(exchangeBids.get(i));
            }

            pa = new PlayerActions(concatBids, "");
            pa.setPassword(password);

            if (MainWindow.getAllInstance() != null) {
                Team team = MainWindow.getAllInstance().getCurrentTeam();
                if (team != null)
                    pa.setTeamID(team.getId());
            }

            PlayerActionsWriter.writePlayerActionsFile(fname, pa);
            bidFileName = fname;
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx.setMessage(MainWindow.getMessage("global.bidSaveSuccess"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.save_bid_error"));
        }
        redraw();
    }

    public void openSaleDlg() {

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(bidFileName == null ? getBidDefaultFileName() : bidFileName);
        String result = dlg.open();
        if (result != null) {
            this.openSale(result);
            redraw();
        }
    }

    public void saveSaleDlg() {

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.SAVE);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(getBidDefaultFileName());
        String result = dlg.open();
        if (result != null) {
            this.saveSale(result);
            redraw();
        }
    }

    private String bidFileName = null;

    private String getBidDefaultFileName() {
        String bidFileName = "";

        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                bidFileName = "Player" + currentTeam.getId() + ".f13";
            }
        }

        return bidFileName;
    }

    public void updatePassword(String password) {
        this.password = password;
    }

    public void updateDaysOfRest(int daysOfRest) {
        // TODO Auto-generated method stub

    }

    public void updateMessages() {

        mainItem.setText(MainWindow.getMessage("saleEditorTabName"));

        openButton.setText(MainWindow.getMessage("global.open"));
        saveButton.setText(MainWindow.getMessage("global.save"));

        String[] titles = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("PlayerSalary"), MainWindow.getMessage("PlayerGames"),
                MainWindow.getMessage("PlayerGoalsTotal"), MainWindow.getMessage("PlayerChampGoals"), MainWindow.getMessage("PlayerGoalsMissed"),
                MainWindow.getMessage("PlayerAssists"), MainWindow.getMessage("PlayerProfit"), MainWindow.getMessage("PlayerMark"),
                MainWindow.getMessage("PlayerGamesCareer"), MainWindow.getMessage("PlayerGoalsCareer"), MainWindow.getMessage("PlayerState"),
                MainWindow.getMessage("PlayerYellowCards"), MainWindow.getMessage("PlayerRedCards"), MainWindow.getMessage("PlayerHomeClub"),
                MainWindow.getMessage("PlayerBirthTour"), MainWindow.getMessage("PlayerBirthDate") };

        String[] tooltips = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"),
                MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("tooltip.Salary"),
                MainWindow.getMessage("tooltip.GamePlayed"), MainWindow.getMessage("tooltip.TotalGoals"),
                MainWindow.getMessage("tooltip.ChampionshipGoals"), MainWindow.getMessage("tooltip.MissedGoals"),
                MainWindow.getMessage("tooltip.GoalAssists"), MainWindow.getMessage("tooltip.PlayerProfit"), MainWindow.getMessage("PlayerMark"),
                MainWindow.getMessage("tooltip.CareerMatches"), MainWindow.getMessage("tooltip.CareerGoals"),
                MainWindow.getMessage("tooltip.PlayerState"), MainWindow.getMessage("tooltip.YellowCards"),
                MainWindow.getMessage("tooltip.RedCards"), MainWindow.getMessage("PlayerHomeClub"), MainWindow.getMessage("PlayerBirthTour"),
                MainWindow.getMessage("PlayerBirthDate") };

        GuiUtils.initTable(teamTable, titles, tooltips, SWT.CENTER, true);
        teamTable.getColumn(2).setAlignment(SWT.LEFT);

        saleButton.setText(MainWindow.getMessage("SaleToggleSale"));
        saleButton.getParent().layout();
        rentButton.setText(MainWindow.getMessage("SaleToggleRent"));
        saleButton.getParent().layout();
        exchangeButton.setText(MainWindow.getMessage("SaleToggleExchange"));
        exchangeButton.getParent().layout();
        
        rentInLabel.setText(MainWindow.getMessage("rentIn_players"));
        rentTableIn.setToolTipText(MainWindow.getMessage("RentIn.tooltip"));
        String[] titlesRentIn = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("OldClub") };
        String[] tooltipsRentIn = { MainWindow.getMessage("playerNumberHint"), MainWindow.getMessage("OldClub") };
        GuiUtils.initTable(rentTableIn, titlesRentIn, tooltipsRentIn, SWT.LEFT, true);

        rentOutLabel.setText(MainWindow.getMessage("rentOut_players"));
        rentTableOut.setToolTipText(MainWindow.getMessage("RentOut.tooltip"));
        String[] titlesRentOut = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("NewClub") };
        String[] tooltipsRentOut = { MainWindow.getMessage("playerNumberHint"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("NewClub") };
        GuiUtils.initTable(rentTableOut, titlesRentOut, tooltipsRentOut, SWT.LEFT, true);

        saleTablePrice.setToolTipText(MainWindow.getMessage("Sales.tooltip"));
        String[] titlesPrice = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("SalePrice"),
                MainWindow.getMessage("SaleType") };
        GuiUtils.initTable(saleTablePrice, titlesPrice, null, SWT.LEFT, true);

        exchangeTable.setToolTipText(MainWindow.getMessage("Exchanges.tooltip"));
        String[] titlesExchanges = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("Club"), "##",
                MainWindow.getMessage("Surcharge") };

        String[] tooltipsExchanges = { MainWindow.getMessage("OwnPlayerNumberHint"), MainWindow.getMessage("PlayerName"),
                MainWindow.getMessage("OtherClub"), MainWindow.getMessage("OtherTeamPlayerNumberHint"), MainWindow.getMessage("Surcharge") };
        GuiUtils.initTable(exchangeTable, titlesExchanges, tooltipsExchanges, SWT.LEFT, true);

        topComposite.layout();
        teamSquardPanel.layout();
        salePanel.layout();
        exchangePanel.layout();
        gridComposite.layout();
        saveComposite.layout();
        toggleComposite.layout();
        someComposite.layout();
        rentPanel.layout();
        redrawTeamSquard();
        redraw();
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
        if (!lazyUpdate) {
            updateAll();
        }
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    @Override
    public void dispose() {

        imageExchange.dispose();
        imageRent.dispose();
        imageSale.dispose();
        imgPlayers.dispose();
        if (flags.size() > 0) {
            for (Entry<String, Image> entry : flags.entrySet()) {
                if (entry.getValue() != null) {
                    entry.getValue().dispose();
                }
            }
        }

    }

    @Override
    public void store(Properties props) {
        // TODO Auto-generated method stub

    }

}
