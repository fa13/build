package com.fa13.build.view.calc;

import java.util.Properties;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.ILoadable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.AllReader;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.model.All;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.UIItem;

public class CalcView implements UIItem, IDisposable, IStorable, ILoadable {

    final private TabFolder parentTabFolder;
    private final TabItem mainItem;
    private ScrolledComposite scrollPanel;
    private Composite mainComposite;
    private TabFolder tabFolder;
    private Image imgCalc;
    private Composite topPanel;
    private Composite bottomPanel;
    private Button bUseOtherAll;
    private Button bLoadOtherAll;
    private PlayerExchangesView playerExchangesView;
    private PlayerPriceView playerPriceView;
    private All all;
    private Label lOtherAllInfo;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public static final String PROP_CALC_ALL_FILE = "AllForCalcFile";
    public static final String PROP_USE_OTHER_ALL_FOR_CALC = "UseOtherAllForCalc";
    private String otherAllFile;
    private boolean useOtherAllFlag;
    private boolean allLoaded = false;

    public CalcView(TabFolder parent) {
        this.parentTabFolder = parent;
        loadImages();
        mainItem = new TabItem(parentTabFolder, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("Calculations"));
        mainItem.setImage(imgCalc);

        initComponents();
    }

    private void loadImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/calc_16x16.png"));
        imgCalc = new Image(parentTabFolder.getDisplay(), imgData);

    }

    private void initComponents() {
        scrollPanel = new ScrolledComposite(parentTabFolder, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
        scrollPanel.setExpandHorizontal(true);
        scrollPanel.setExpandVertical(true);
        mainComposite = new Composite(scrollPanel, SWT.NONE);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(new FormLayout());

        scrollPanel.setContent(mainComposite);

        createTopPanel();
        createBottomPanel();
        createTabFolder();
        mainItem.setControl(scrollPanel);

        mainComposite.setRedraw(true);
        tabFolder.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		TabItem tabItem = tabFolder.getSelection()[0];
        		if (playerExchangesView.asTabItem() == tabItem) {
        			playerExchangesView.updateView();
        		} else if (playerPriceView.asTabItem() == tabItem) {
        			playerPriceView.updateView();
        		}
        		scrollPanel.setContent(mainComposite);
        		scrollPanel.update();
        	}
		});
    }

    private void createTabFolder() {
        tabFolder = new TabFolder(bottomPanel, SWT.NONE);
        tabFolder.setFont(MainWindow.getSharedFont(FontType.TITLE_BIG));
        FormData fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(0, 0);
        fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        tabFolder.setLayoutData(fd);
        playerPriceView = new PlayerPriceView(tabFolder, SWT.NONE);
        All all = MainWindow.getAllInstance();
        if (all != null) {
            lOtherAllInfo.setText(all.getShortDescription());
        }
        playerPriceView.setAll(MainWindow.getAllInstance());
        
        playerExchangesView = new PlayerExchangesView(tabFolder, SWT.NONE);
        playerExchangesView.setAll(MainWindow.getAllInstance());
    }

    private void createTopPanel() {
        topPanel = new Composite(mainComposite, SWT.NONE);
        RowLayout rowLayout = new RowLayout();
        rowLayout.pack = true;
        rowLayout.center = true;
        rowLayout.spacing = 10;
        rowLayout.marginLeft = 10;
        rowLayout.marginTop = 10;
        rowLayout.marginBottom = 10;
        rowLayout.marginRight = 10;
        topPanel.setLayout(rowLayout);
        FormData fd = new FormData();
        fd.top = new FormAttachment(0, 0);
        fd.left = new FormAttachment(0, 0);
        fd.right = new FormAttachment(100, 0);
        bUseOtherAll = new Button(topPanel, SWT.CHECK);
        bUseOtherAll.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        bUseOtherAll.setText(MainWindow.getMessage("UseOtherAllForCalc"));
        //TODO load state from props
        bUseOtherAll.setSelection(false);
        bUseOtherAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                bLoadOtherAll.setVisible(bUseOtherAll.getSelection());
                lOtherAllInfo.setVisible(bUseOtherAll.getSelection());
            }
        });
        bUseOtherAll.addDisposeListener(new DisposeListener() {

            @Override
            public void widgetDisposed(DisposeEvent e) {
                //store state to save it later
                useOtherAllFlag = bUseOtherAll.getSelection();

            }
        });
        bLoadOtherAll = new Button(topPanel, SWT.PUSH);
        bLoadOtherAll.setFont(MainWindow.getSharedFont(FontType.BUTTON_IMPORTANT));
        bLoadOtherAll.setImage(MainWindow.getSharedImage(ImageType.ALL_FILE));
        bLoadOtherAll.setText(MainWindow.getMessage("openAllForCalc"));
        bLoadOtherAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                onLoadOtherAll();
            }
        });
        bLoadOtherAll.setVisible(false);

        lOtherAllInfo = new Label(topPanel, SWT.NONE);
        lOtherAllInfo.setFont(MainWindow.getSharedFont(FontType.TITLE_BIG));
        lOtherAllInfo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        lOtherAllInfo.setToolTipText(MainWindow.getMessage("AllFileDate"));
        //lOtherAllInfo.setText("all description here here here here here");
        lOtherAllInfo.setVisible(false);
    }

    private void onLoadOtherAll() {
        FileDialog dlg = new FileDialog(parentTabFolder.getShell(), SWT.OPEN);

        String ext[] = new String[2];
        ext[0] = MainWindow.getMessage("FilterExtensionALLZIP");
        ext[1] = MainWindow.getMessage("FilterExtensionALL");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[2];
        extNames[0] = MainWindow.getMessage("FilterExtensionALLZIPName");
        extNames[1] = MainWindow.getMessage("FilterExtensionALLName");
        dlg.setFilterNames(extNames);
        if (otherAllFile != null) {
            dlg.setFileName(otherAllFile);
        }
        final String fname = dlg.open();

        loadAll(fname);

    }

    private void loadAll(String fname) {
        if (fname != null && !fname.isEmpty()) {

            try {
                all = AllReader.readAllFile(fname);
                playerPriceView.setAll(all);
                playerExchangesView.setAll(all);
                otherAllFile = fname;
                lOtherAllInfo.setVisible(true);
                lOtherAllInfo.setText(all.getShortDescription());
                RowData rd = new RowData(lOtherAllInfo.computeSize(SWT.DEFAULT, SWT.DEFAULT));
                lOtherAllInfo.setLayoutData(rd);
                lOtherAllInfo.getParent().pack();
                //lOtherAllInfo.setSize(lOtherAllInfo.computeSize(SWT.DEFAULT, SWT.DEFAULT));
                //lOtherAllInfo.getParent().layout(true);
                bLoadOtherAll.setToolTipText(fname);
                allLoaded = true;
            } catch (ReaderException e) {
                GuiUtils.showErrorMessage(parentTabFolder.getShell(), MainWindow.getMessage("error"), e.getLocalizedMessage());
            }

        } else {
            GuiUtils.showErrorMessage(parentTabFolder.getShell(), MainWindow.getMessage("Warning"), MainWindow.getMessage("FileIsNotChoosen"));
        }
    }

    private void createBottomPanel() {
        bottomPanel = new Composite(mainComposite, SWT.NONE);
        bottomPanel.setLayout(new FormLayout());
        FormData fd = new FormData();
        fd.top = new FormAttachment(topPanel, 0);
        fd.left = new FormAttachment(0, 0);
        fd.right = new FormAttachment(100, 0);
        fd.bottom = new FormAttachment(100, 0);
        bottomPanel.setLayoutData(fd);
    }

    @Override
    public void load(Properties props) {

        String data = props.getProperty(PROP_USE_OTHER_ALL_FOR_CALC, "");
        if (!data.isEmpty()) {
            boolean flag = (data.equalsIgnoreCase("1"));
            bUseOtherAll.setSelection(flag);
            bLoadOtherAll.setVisible(flag);
            lOtherAllInfo.setVisible(flag);
        }

        data = props.getProperty(PROP_CALC_ALL_FILE, "");
        if (!data.isEmpty()) {
            otherAllFile = data;
        }

    }

    @Override
    public void store(Properties props) {
        if (otherAllFile != null) {
            props.setProperty(PROP_CALC_ALL_FILE, otherAllFile);
        }

        int flag = useOtherAllFlag ? 1 : 0;
        props.setProperty(PROP_USE_OTHER_ALL_FOR_CALC, String.valueOf(flag));
    }

    @Override
    public void dispose() {
        imgCalc.dispose();
    }

    @Override
    public void updateAll() {
        //System.out.println("CalcView.updateAll...");
        //long time = System.currentTimeMillis();
        if (bUseOtherAll.getSelection() && !allLoaded) {
            loadAll(otherAllFile);
        }

        if (!bUseOtherAll.getSelection()) {
            playerPriceView.setAll(MainWindow.getAllInstance());
            playerExchangesView.setAll(MainWindow.getAllInstance());
        }
        playerPriceView.updateView();
        playerExchangesView.updateView();
        
    	scrollPanel.setContent(mainComposite);
    	scrollPanel.update();
    	//System.out.println("CalcView.updateAll finished in " + (System.currentTimeMillis() - time));
    }

    @Override
    public void updateAll(boolean lazyUpdate) {
        if (lazyUpdate) {
            return;
        }
        updateAll();
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    @Override
    public void updatePassword(String password) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateMessages() {
        // TODO Auto-generated method stub

    }

    @Override
    public void redraw() {
        // TODO Auto-generated method stub

    }

}