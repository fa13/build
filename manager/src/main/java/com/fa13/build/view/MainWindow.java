package com.fa13.build.view;

import static org.eclipse.swt.SWT.ICON_INFORMATION;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

import org.apache.commons.lang.SystemUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.ILoadable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.AllReader;
import com.fa13.build.controller.io.AllReader.AllProgressNotify;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.gui.cocoa.CocoaUIEnhancer;
import com.fa13.build.model.All;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.utils.GuiLongTask;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.utils.HttpUtils;
import com.fa13.build.view.calc.CalcView;
import com.fa13.build.view.game.GameEditView;
import com.fa13.build.view.transfer.TransferView;

public class MainWindow implements IDisposable, IStorable {

    private static final String SETTINGS_FILENAME = "settings.xml";
    private static final String DEFAULT_BUNDLE = "/com/fa13/build/resources/properties/MessagesBundle_ru_RU.properties";

    // private Image ICON_OPEN;
    private static MainWindow INSTANCE;
    String password;
    Properties settings;
    Properties defaultSettings;

    ArrayList<UIItem> uiItems;
    TrainingView trainingView;
    GameEditView gameEditView;
    //MatchVideoView matchVideoView;
    ClubInfoView clubInfoView;
    SaleView saleView;
    TransferView transferView;
    StatisticsView statisticsView;
    CalcView calcView;

    List<IDisposable> listToDispose;
    List<IStorable> listToStore;
    List<ILoadable> listToLoad;

    Display display;

    Shell shell;

    TabFolder tabFolder;

    private Combo teamCombo;

    Button openButton;
    Button settingsButton;

    CoolBar coolBar;
    CoolItem coolItem;

    private static ResourceBundle messageInstance;
    private static All allInstance;

    Label teamLabel;
    Label pwdLabel;
    Text pwdEditor;
    Label dateLabel;
    SimpleDateFormat dateFormat;

    SettingsView settingsDialog;

    Composite topComposite;

    private int selectedTeamIndex = -1;
    private boolean allChanged = false;
    private ToolItem toolItemSupport;
    protected MenuItem miSupportForum;
    protected MenuItem miLastDownloads;
    protected MenuItem miReportIssues;
    protected MenuItem miCheckUpdates;
    private MenuItem miGameRules;

    private Image imageSupport;
    private Image imageBug;
    private Image imageDownload;
    private Image imageSupportForum;
    private Image imageUpdate;
    //private MatchVideoOnlineView matchVideoOnlineView;
    private Image imgAppIcon;
    private Button updateAllButton;

    public static Map<ImageType, Image> sharedImagesMap = new HashMap<ImageType, Image>();

    public enum ImageType {
        OPEN, SAVE, BALL, SETTINGS, CLEAR, ERROR, CANCEL, OK, ALL_FILE, SALE, EXCHANGE
    }

    public static Map<FontType, Font> sharedFontsMap = new HashMap<FontType, Font>();

    public enum FontType {
        TITLE_BIG, VIEW, EDIT, VIEW_IMPORTANT, EDIT_IMPORTANT, BUTTON, BUTTON_IMPORTANT, GROUP_TITLE, TABLE_TITLE
    }

    public static Map<ColorResourceType, Color> sharedColorResourceMap = new HashMap<ColorResourceType, Color>();

    public enum ColorResourceType {
        YELLOW, GRAY, SQUARD, BENCH, LIGHT_YELLOW, LIGHT_RED
    }

    public static final String APP_TITLE = "FA13 Jogador";
    public static final String PROP_ALL_PATH = "allPath";
    public static final String PROP_LANG = "lang";
    public static final String PROP_CURRENT_TEAM = "currentTeam";
    //public static final String PROP_UPDATE_CHECK_GODMESDAY = "updateCheckGodMesDay";
    public static final String PROP_UPDATE_STATUS = "updateStatus";
    public static final String PROP_CHECK_UPDATES_STARTUP = "checkUpdatesOnStartup";
    public static final String PROP_SERVER_BUILD_GODMES = "serverBuildGodmes";
    public static final String PROP_SERVER_BUILD_REVISION = "serverBuildRevision";
    public static final String PROP_PASSWORD = "password";
    public static final String PROP_CREATED_AT = "Created-At";
    public static final String PROP_IMPLEMENTATION_VERSION = "Implementation-Version";
    public static final String PROP_CHECK_CURRENT_ALL_STARTUP = "checkCurrentAllOnStartup";
    public static final String PROP_ALL_LAST_MODIFIED = "allLastModified";
    public static final String PROP_ALL_BASENAME = "AllBaseName";
    public static final String PROP_PLAYER_IMAGE_SIZE = "playerImageSize";
    public static final String PROP_ALL_ATTACK_DEFENCE = "allAttackDefense";
    public static final String PROP_TICKETS_PRICE = "TicketsPrice";
    public static final String PROP_TACTICS_COACH_SETTINGS = "Tactics_CoachSettings";
    public static final String PROP_TACTICS_TACTICS = "Tactics_Tactics";
    public static final String PROP_TACTICS_AGRESSION = "Tactics_Agression";
    public static final String PROP_TACTICS_STYLE = "Tactics_Style";
    public static final String PROP_MATCH_TYPE = "MatchDefaultType";
    public static final String PROP_MATCH_ROUND = "MatchDefaultRound";
    public static final String PROP_USE_CLUB_COLORS_IN_SCHEME = "useClubColorsInScheme";

    public static final String PROP_SQUARD_FONT_DATA = "SquardFontData";
    public static final String PROP_TRAININGS_FONT_DATA = "TrainingsFontData";

    public static final String PROP_USE_NEW_TICKETS = "useNewTickets";

    public static MainWindow getDefault() {
        return INSTANCE;
    }

    public MainWindow() throws IOException {
        INSTANCE = this;
        defaultSettings = new Properties();
        defaultSettings.setProperty(PROP_LANG, DEFAULT_BUNDLE);

        settings = new Properties(defaultSettings);
        try {
            settings.loadFromXML(new FileInputStream(SETTINGS_FILENAME));
        } catch (Exception e) {
            settings = defaultSettings;
        }
        boolean pendingSettingStoreError = false;
        try {
            settings.storeToXML(new FileOutputStream(SETTINGS_FILENAME), "Fa13Manager Settings");
        } catch (Exception e) {
            pendingSettingStoreError = true;
            GuiUtils.showErrorMessage(shell, getMessage("error"), getMessage("cantSaveSettingsError"));
        }
        String langPropName = settings.getProperty(PROP_LANG, DEFAULT_BUNDLE);
        InputStream stream = this.getClass().getResourceAsStream(langPropName);
        InputStreamReader readerIs = new InputStreamReader(stream, "UTF-8");

        messageInstance = new PropertyResourceBundle(readerIs);

        Display.setAppName(APP_TITLE);
        display = new Display();

        loadImages();
        initSharedImages();
        loadSharedColorResources();

        shell = new Shell(display);
        shell.setImage(imgAppIcon);
        shell.setMaximized(true);

        if (pendingSettingStoreError) {
            GuiUtils.showErrorMessage(shell, getMessage("error"), getMessage("cantSaveSettingsError"));
        }
        initSharedFonts();

        GridLayout gridLayout = new GridLayout(1, true);
        GridData gridData;

        shell.setLayout(gridLayout);

        topComposite = new Composite(shell, SWT.BORDER);
        topComposite.setLayout(new GridLayout(9, false));

        openButton = new Button(topComposite, SWT.PUSH);
        openButton.setImage(getSharedImage(ImageType.ALL_FILE));
        openButton.setFont(getSharedFont(FontType.BUTTON));
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);

        openButton.setLayoutData(gridData);
        openButton.addSelectionListener(new OpenButtonListner());

        updateAllButton = new Button(topComposite, SWT.PUSH);
        updateAllButton.setImage(imageUpdate);
        updateAllButton.setToolTipText(getMessage("CheckAndUpdateAll"));
        updateAllButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                String allPath = settings.getProperty(PROP_ALL_PATH);
                if (allPath != null && !allPath.isEmpty()) {
                    checkForAllUpdates(allPath);
                    //add allLoader.AllLoadListener to react when load is done
                    allLoader.doLoadAll(allPath);
                }
            }
        });

        teamLabel = new Label(topComposite, SWT.NONE);
        teamLabel.setFont(getSharedFont(FontType.TITLE_BIG));
        teamLabel.setText(messageInstance.getString("global.team"));
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
        gridData.horizontalIndent = 10;
        teamLabel.setLayoutData(gridData);

        teamCombo = new Combo(topComposite, SWT.DROP_DOWN);// | SWT.READ_ONLY);
        if (!SystemUtils.IS_OS_LINUX) {
            teamCombo.setBackground(getSharedColorResource(ColorResourceType.YELLOW));
        }
        teamCombo.setFont(getSharedFont(FontType.EDIT_IMPORTANT));
        teamCombo.add(messageInstance.getString("global.team"));
        teamCombo.select(0);
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
        gridData.widthHint = 150;
        teamCombo.setLayoutData(gridData);
        teamCombo.addSelectionListener(new TeamComboListener());
        teamCombo.addModifyListener((ModifyListener) new TeamComboListener());
        GuiUtils.addAutoCompleteFeature(teamCombo);
        //teamCombo.addKeyListener(new TeamComboListener());
        pwdLabel = new Label(topComposite, SWT.NONE);
        pwdLabel.setFont(getSharedFont(FontType.TITLE_BIG));
        pwdLabel.setText(messageInstance.getString("global.password"));
        pwdLabel.setLayoutData(new GridData(GridData.BEGINNING, GridData.CENTER, false, false));
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
        gridData.horizontalIndent = 15;
        pwdLabel.setLayoutData(gridData);

        pwdEditor = new Text(topComposite, SWT.PASSWORD | SWT.BORDER);
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
        gridData.widthHint = 150;
        pwdEditor.setLayoutData(gridData);

        Object key = settings.get(PROP_PASSWORD);
        if (key != null) {
            pwdEditor.setText(key.toString());
            password = pwdEditor.getText();
        }

        pwdEditor.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                password = pwdEditor.getText();
                updateTabsPassword();
            }
        });

        dateLabel = new Label(topComposite, SWT.NONE);
        dateLabel.setFont(getSharedFont(FontType.TITLE_BIG));
        dateLabel.setBackground(getSharedColorResource(ColorResourceType.YELLOW));
        dateLabel.setToolTipText(getMessage("AllFileDate"));
        Date date = new Date();

        dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateLabel.setText(dateFormat.format(date));
        dateLabel.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER, true, false));

        settingsDialog = new SettingsView(shell);
        settingsDialog.setText(getMessage("settingsButton"));
        settingsButton = new Button(topComposite, SWT.PUSH);
        settingsButton.setFont(getSharedFont(FontType.BUTTON));
        settingsButton.setImage(getSharedImage(ImageType.SETTINGS));
        gridData = new GridData(GridData.BEGINNING, GridData.CENTER, false, false);
        settingsButton.setText(messageInstance.getString("settingsButton"));
        settingsButton.pack();
        settingsButton.setLayoutData(gridData);
        settingsButton.setVisible(true);

        settingsButton.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            public void widgetDefaultSelected(SelectionEvent e) {
                int x = shell.getLocation().x + settingsButton.getLocation().x - 100;
                int y = shell.getLocation().y + settingsButton.getLocation().y + 20;
                settingsDialog.open(messageInstance, settings, x, y);
                setSettings(settings);
            }
        });

        createSupportToolBar();

        topComposite.pack();

        gridData = new GridData(GridData.FILL, GridData.FILL, true, false);
        topComposite.setLayoutData(gridData);

        uiItems = new ArrayList<UIItem>();
        tabFolder = new TabFolder(shell, SWT.NONE);
        tabFolder.setFont(getSharedFont(FontType.TITLE_BIG));
        /*
                FontData[] fontData = tabFolder.getFont().getFontData();
                for (FontData fd: fontData) {
                    fd.setHeight(12);
                    fd.setStyle(SWT.BOLD);
                }
                final Font font = new Font(shell.getDisplay(), fontData);
                tabFolder.setFont(font);
                tabFolder.addDisposeListener(new DisposeListener() {
                    
                    @Override
                    public void widgetDisposed(DisposeEvent e) {
                        font.dispose();
                    }
                });
        */
        tabFolder.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) {
                // System.out.println(tabFolder.getSelection()[0].getText()
                // + " selected");
                TabItem tabItem = tabFolder.getSelection()[0];
                for (UIItem item : uiItems) {
                    if (item.asTabItem() == tabItem && item.asTabItem() != null) {
                        final UIItem item2 = item;
                        GuiLongTask task = new GuiLongTask() {

                            @Override
                            protected void execute() {
                                item2.updateAll();
                            }
                        };
                        GuiUtils.showModalProgressWindow(shell, task, MainWindow.getMessage("global.pleaseWait"), false);

                    }
                }
            }
        });

        gameEditView = new GameEditView(tabFolder, this);
        uiItems.add(gameEditView);

        clubInfoView = new ClubInfoView(tabFolder);
        uiItems.add(clubInfoView);

        trainingView = new TrainingView(tabFolder, settings);
        uiItems.add(trainingView);

        saleView = new SaleView(tabFolder);
        uiItems.add(saleView);

        transferView = new TransferView(tabFolder);
        uiItems.add(transferView);

        calcView = new CalcView(tabFolder);
        uiItems.add(calcView);

        statisticsView = new StatisticsView(tabFolder);
        uiItems.add(statisticsView);
        //matchVideoOnlineView = new MatchVideoOnlineView(tabFolder, display);
        //uiItems.add(matchVideoOnlineView);
        gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
        tabFolder.setLayoutData(gridData);

        updateMessages();

        shell.setMinimumSize(800, 450);
        shell.setText(APP_TITLE);
        shell.open();
        updateTabsPassword();
        updateTabsAll();
        if (settings.getProperty(PROP_ALL_PATH) == null) {
            this.openAllDlg();
        } else {
            //check for all updates
            key = settings.get(PROP_CHECK_CURRENT_ALL_STARTUP);
            boolean flag = (key != null && key.toString().equals("1"));
            String allPath = settings.getProperty(PROP_ALL_PATH);
            if (flag && allPath != null && !allPath.isEmpty()) {
                checkForAllUpdates(allPath);
            }
            //add allLoader.AllLoadListener to react when load is done
            allLoader.doLoadAll(allPath);

        }

        listToDispose = new ArrayList<IDisposable>();
        listToStore = new ArrayList<IStorable>();
        listToLoad = new ArrayList<ILoadable>();

        listToDispose.add(gameEditView);
        listToDispose.add(clubInfoView);
        listToDispose.add(trainingView);
        listToDispose.add(saleView);
        listToDispose.add(transferView);
        listToDispose.add(calcView);
        listToDispose.add(statisticsView);
        //listToDispose.add(matchVideoOnlineView);
        listToDispose.add(this);

        listToStore.add(gameEditView);
        listToStore.add(clubInfoView);
        listToStore.add(trainingView);
        listToStore.add(saleView);
        listToStore.add(transferView);
        listToStore.add(calcView);
        listToStore.add(statisticsView);
        //listToStore.add(matchVideoOnlineView);
        listToStore.add(this);

        //listToLoad.add(matchVideoOnlineView);
        listToLoad.add(gameEditView);
        listToLoad.add(calcView);

        for (ILoadable l : listToLoad) {
            l.load(settings);
        }

        setupAppVersion();

        key = settings.get(PROP_CHECK_UPDATES_STARTUP);
        boolean flag = (key != null && key.toString().equals("1"));
        if (flag) {
            checkForUpdates();
        }

        if (System.getProperty("os.name").equals("Mac OS X")) {
            CocoaUIEnhancer enhancer = new CocoaUIEnhancer(APP_TITLE);
            enhancer.hookApplicationMenu(display, new Runnable() {
                @Override
                public void run() {
                    MessageBox dialog = new MessageBox(shell, ICON_INFORMATION);
                    dialog.setMessage(APP_TITLE + " v." + jarGodMesDay);
                    dialog.open();
                }
            }, new Runnable() {
                @Override
                public void run() {
                    settingsButton.notifyListeners(SWT.Selection, null);
                }
            });
        }

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();

        //save all need to save
        //warning! Here all widgets are disposed
        //so if want to save some widget's value - don't forget to store it in class
        //using widget's addDisposeListener method and using it in store implementations
        for (IStorable d : listToStore) {
            d.store(settings);
        }
        saveSettings();
        //release all created resources
        for (IDisposable d : listToDispose) {
            d.dispose();
        }

    }

    private void checkForAllUpdates(final String allPath) {
        int found = allPath.lastIndexOf(File.separator);
        String allFileNameBase = (found == -1 ? allPath : allPath.substring(found + 1));
        found = allFileNameBase.indexOf(".");
        allFileNameBase = allFileNameBase.substring(0, found);
        final String allFileNameBaseCopy = allFileNameBase;
        //System.out.println("allFileNameBase=" + allFileNameBase);
        final String allUrl = All.getAllUrlByName(allFileNameBase);
        //System.out.println("allUrl=" + allUrl);
        GuiLongTask downloadTask = new GuiLongTask() {

            @Override
            protected void execute() {

                try {
                    //System.out.println("checking all...");

                    int timeout = 3;//secs
                    RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
                            .setSocketTimeout(timeout * 1000).build();

                    CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

                    HttpHead head = new HttpHead(allUrl);
                    //httpClient.setTimeout(3000);
                    CloseableHttpResponse response = httpClient.execute(head);
                    //int status = client.executeMethod(head);
                    int status = response.getStatusLine().getStatusCode();
                    head.releaseConnection();
                    httpClient.close();
                    if (status == HttpStatus.SC_OK) {
                        String lastModified = response.getFirstHeader("last-modified").getValue();
                        //String lastModified = head.getResponseHeader("last-modified").getValue();
                        System.out.println("[" + allUrl + "] ->got lastModified=" + lastModified);
                        Object storedLastModified = settings.get(MainWindow.PROP_ALL_LAST_MODIFIED);
                        String storedAllBaseName = settings.getProperty(PROP_ALL_BASENAME);
                        if ((storedAllBaseName == null) || (!storedAllBaseName.equalsIgnoreCase(allFileNameBaseCopy)) || (lastModified != null
                                && !lastModified.isEmpty() && (storedLastModified == null || !storedLastModified.equals(lastModified)))) {
                            //need update all
                            //System.out.println("downloading... [" + allUrl + "]");
                            GuiUtils.downloadUrlResource(allUrl, allPath);
                            settings.put(MainWindow.PROP_ALL_LAST_MODIFIED, lastModified);
                            settings.put(MainWindow.PROP_ALL_BASENAME, allFileNameBaseCopy);
                        }

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        };
        GuiUtils.showModalProgressWindow(this.shell, downloadTask, getMessage("checkingAndUpdatingAll"), false);

    }

    private void loadSharedColorResources() {
        Color color = display.getSystemColor(SWT.COLOR_YELLOW);
        sharedColorResourceMap.put(ColorResourceType.YELLOW, color);

        color = display.getSystemColor(SWT.COLOR_GRAY);
        sharedColorResourceMap.put(ColorResourceType.GRAY, color);

        color = new Color(display, 173, 255, 173); //175,221,255
        sharedColorResourceMap.put(ColorResourceType.SQUARD, color);

        color = new Color(display, 224, 255, 224);
        sharedColorResourceMap.put(ColorResourceType.BENCH, color);

        color = new Color(display, 255, 255, 153);
        sharedColorResourceMap.put(ColorResourceType.LIGHT_YELLOW, color);

        color = new Color(display, 255, 204, 204);
        sharedColorResourceMap.put(ColorResourceType.LIGHT_RED, color);
    }

    private void loadImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/support_16x16.png"));
        imageSupport = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/bug_16x16.png"));
        imageBug = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/download_16x16.png"));
        imageDownload = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/update_16x16.png"));
        imageUpdate = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/question_16x16.png"));
        imageSupportForum = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/fa13-jogador.ico"));
        imgAppIcon = new Image(display, imgData);
    }

    public static final String URL_GAME_RULES = "http://fa13.info/rules";
    public static final String URL_SUPPORT_FORUM = "http://forum.fa13.info/index.php?showtopic=33781";
    public static final String URL_DOWNLOADS = "http://fa13.info/download";
    public static final String URL_ISSUES = "https://gitlab.com/fa13/build/issues?state=opened";
    public static final String URL_BUILD_TIMESTAMP = "http://repository.fa13.info/distrib/version.txt";

    //public static final String URL_BUILD_TIMESTAMP = "http://old.fa13.info/txt/nightly-build.txt";

    //public static long MILLISECONDS_PER_DAY = 86400000; //60*60*24*1000

    private int getGodMesDay(Calendar c) {
        return c.get(Calendar.YEAR) * 10000 + (c.get(Calendar.MONTH) + 1) * 100 + c.get(Calendar.DAY_OF_MONTH);
    }

    enum UpdateStatus {
        AVAILABLE, NOT_CHECKED, NOT_AVAILABLE, CHECK_ERROR
    }

    private int jarGodMesDay = -1;
    private String clientRevision;

    private void setupAppVersion() {
        File file = null;
        Calendar calendar = Calendar.getInstance();
        try {
            file = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
            JarFile jarFile = new JarFile(file);
            Attributes jarAttributes = jarFile.getManifest().getMainAttributes();
            String value = jarAttributes.getValue(PROP_CREATED_AT);
            if (value != null) {
                Date clientBuildDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").parse(value);
                calendar.setTime(clientBuildDate);
                jarGodMesDay = getGodMesDay(calendar);
            }
            value = jarAttributes.getValue(PROP_IMPLEMENTATION_VERSION);
            if (value != null) {
                int start = value.lastIndexOf("#");
                int end = value.lastIndexOf(")");
                value = value.substring(start + 1, end);
            }
            clientRevision = value;

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jarGodMesDay == -1 && file != null) {
            Date dateJar = new Date(file.lastModified());
            calendar.setTime(dateJar);
            jarGodMesDay = getGodMesDay(calendar);
        }
        shell.setText(APP_TITLE + " [ " + jarGodMesDay + " ]");
    }

    private void checkForUpdates() {

        GuiLongTask task = new GuiLongTask() {

            @Override
            protected void execute() {

                try {

                    setupAppVersion();
                    //download & parse date
                    try {
                        String[] nightlyBuildInfoLines = HttpUtils.getRemoteContent(URL_BUILD_TIMESTAMP).split("[" + System.lineSeparator() + "]+");

                        String nightlyBuildDateTime = nightlyBuildInfoLines[0].trim();
                        Date serverBuildDate = new SimpleDateFormat("dd.MM.yyyy HH:mm z").parse(nightlyBuildDateTime);
                        Calendar c = Calendar.getInstance();
                        c.setTime(serverBuildDate);
                        int serverBuildGodmes = getGodMesDay(c);
                        settings.put(PROP_SERVER_BUILD_GODMES, String.valueOf(serverBuildGodmes));

                        String serverRevision = "";
                        if (nightlyBuildInfoLines.length > 1) {
                            serverRevision = nightlyBuildInfoLines[1].trim();
                            settings.put(PROP_SERVER_BUILD_REVISION, serverRevision);
                            if (serverRevision.length() > 11) {
                                serverRevision = serverRevision.substring(0, 12);
                            }
                        }

                        String checkInfo = getMessage("currentVersionDate") + " = " + jarGodMesDay + "\n[" + clientRevision + "]" + "\n"
                                + getMessage("serverVersionDate") + " = " + serverBuildGodmes + "\n[" + serverRevision + "]";

                        if (!serverRevision.isEmpty() && !clientRevision.equals(serverRevision)) {
                            settings.put(PROP_UPDATE_STATUS, UpdateStatus.AVAILABLE.name());
                            MessageBox dialog = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
                            dialog.setText(MainWindow.getMessage("updatesCheck"));
                            dialog.setMessage(MainWindow.getMessage("updateFoundGoToDownloads") + "\n" + checkInfo);
                            if (dialog.open() == SWT.OK) {
                                GuiUtils.openWebpage(URL_DOWNLOADS);
                            }
                        } else {
                            settings.put(PROP_UPDATE_STATUS, UpdateStatus.NOT_AVAILABLE.name());
                            //GuiUtils.showSuccessMsessage(shell, MainWindow.getMessage("updatesCheck"), checkInfo);
                        }

                    } catch (Exception e) {
                        settings.put(PROP_UPDATE_STATUS, UpdateStatus.CHECK_ERROR.name());
                        //System.out.println(e.toString());
                    }
                    //                    }
                } catch (Exception e1) {
                    settings.put(PROP_UPDATE_STATUS, UpdateStatus.CHECK_ERROR.name());
                    //System.out.println("update check failed");
                    //e1.printStackTrace();
                }

            }
        };
        GuiUtils.showModalProgressWindow(shell, task, MainWindow.getMessage("updatesCheck"), false);

    }

    private void createSupportToolBar() {

        ToolBar toolbar = new ToolBar(topComposite, SWT.FLAT | SWT.RIGHT);
        toolItemSupport = new ToolItem(toolbar, SWT.DROP_DOWN);
        toolItemSupport.setText(getMessage("support"));
        toolItemSupport.setImage(imageSupport);

        toolItemSupport.addSelectionListener(new SelectionAdapter() {

            Menu dropMenu = null;

            @Override
            public void widgetSelected(SelectionEvent e) {
                if (dropMenu == null) {
                    dropMenu = new Menu(shell, SWT.POP_UP);
                    shell.setMenu(dropMenu);

                    miGameRules = new MenuItem(dropMenu, SWT.NONE);
                    miGameRules.setText(getMessage("support.gameRules"));
                    miGameRules.setImage(getSharedImage(ImageType.BALL));
                    miGameRules.addSelectionListener(new SelectionAdapter() {
                        public void widgetSelected(SelectionEvent e) {
                            GuiUtils.openWebpage(URL_GAME_RULES);
                        };
                    });

                    miSupportForum = new MenuItem(dropMenu, SWT.NONE);
                    miSupportForum.setText(getMessage("support.forumPage"));
                    miSupportForum.setImage(imageSupportForum);
                    miSupportForum.addSelectionListener(new SelectionListener() {

                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            GuiUtils.openWebpage(URL_SUPPORT_FORUM);
                        }

                        @Override
                        public void widgetDefaultSelected(SelectionEvent e) {
                            widgetSelected(e);
                        }
                    });
                    miLastDownloads = new MenuItem(dropMenu, SWT.NONE);
                    miLastDownloads.setText(getMessage("support.downloadsPage"));
                    miLastDownloads.setImage(imageDownload);
                    miLastDownloads.addSelectionListener(new SelectionListener() {

                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            GuiUtils.openWebpage(URL_DOWNLOADS);
                        }

                        @Override
                        public void widgetDefaultSelected(SelectionEvent e) {
                            widgetSelected(e);
                        }
                    });
                    miReportIssues = new MenuItem(dropMenu, SWT.NONE);
                    miReportIssues.setText(getMessage("support.reportIssues"));
                    miReportIssues.setImage(imageBug);
                    miReportIssues.addSelectionListener(new SelectionListener() {

                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            GuiUtils.openWebpage(URL_ISSUES);
                        }

                        @Override
                        public void widgetDefaultSelected(SelectionEvent e) {
                            widgetSelected(e);
                        }
                    });

                    miCheckUpdates = new MenuItem(dropMenu, SWT.NONE);
                    miCheckUpdates.setText(getMessage("updatesCheck"));
                    miCheckUpdates.setImage(imageUpdate);
                    miCheckUpdates.addSelectionListener(new SelectionListener() {

                        @Override
                        public void widgetSelected(SelectionEvent e) {
                            checkForUpdates();
                        }

                        @Override
                        public void widgetDefaultSelected(SelectionEvent e) {
                            widgetSelected(e);
                        }
                    });
                }

                //if (e.detail == SWT.ARROW) {
                // Position the menu below and vertically aligned with the the drop down tool button.
                final ToolItem toolItem = (ToolItem) e.widget;
                final ToolBar toolBar = toolItem.getParent();

                Point point = toolBar.toDisplay(new Point(e.x, e.y));
                dropMenu.setLocation(point.x, point.y);
                dropMenu.setVisible(true);
                //} 

            }

        });
    }

    private void initSharedImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/open_16x16.png"));
        Image image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.OPEN, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/save_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.SAVE, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/ball_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.BALL, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/settings_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.SETTINGS, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/clear_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.CLEAR, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/error_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.ERROR, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/cancel_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.CANCEL, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/ok_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.OK, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/archive_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.ALL_FILE, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/sale_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.SALE, image);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/exchange_16x16.png"));
        image = new Image(display, imgData);
        sharedImagesMap.put(ImageType.EXCHANGE, image);

    }

    public static Image getSharedImage(ImageType imageType) {

        return sharedImagesMap.get(imageType);

    }

    private void initSharedFonts() {

        Font font = GuiUtils.createFont(shell.getFont(), 11, SWT.BOLD);
        sharedFontsMap.put(FontType.TITLE_BIG, font);

        font = GuiUtils.createFont(shell.getFont(), 8, SWT.BOLD);
        sharedFontsMap.put(FontType.BUTTON, font);

        font = GuiUtils.createFont(shell.getFont(), 10, SWT.BOLD);
        sharedFontsMap.put(FontType.GROUP_TITLE, font);

        font = GuiUtils.createFont(shell.getFont(), 9, SWT.BOLD);
        sharedFontsMap.put(FontType.TABLE_TITLE, font);

        font = GuiUtils.createFont(shell.getFont(), 9, SWT.BOLD);
        sharedFontsMap.put(FontType.VIEW_IMPORTANT, font);

        font = GuiUtils.createFont(shell.getFont(), 9, SWT.BOLD);
        sharedFontsMap.put(FontType.EDIT_IMPORTANT, font);
    }

    public static Font getSharedFont(FontType fontType) {

        return sharedFontsMap.get(fontType);

    }

    public static Color getSharedColorResource(ColorResourceType colorResourceType) {

        return sharedColorResourceMap.get(colorResourceType);

    }

    public class OpenButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            openAllDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            openAllDlg();
        }
    }

    public void openAllDlg() {
        FileDialog dlg = new FileDialog(shell, SWT.OPEN);

        String ext[] = new String[2];
        ext[0] = messageInstance.getString("FilterExtensionALLZIP");
        ext[1] = messageInstance.getString("FilterExtensionALL");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[2];
        extNames[0] = messageInstance.getString("FilterExtensionALLZIPName");
        extNames[1] = messageInstance.getString("FilterExtensionALLName");
        dlg.setFilterNames(extNames);
        final String fname = dlg.open();
        if (fname != null) {

            allLoader.addAllLoadListener(new AllLoadListener() {

                @Override
                public void loadDone(boolean result) {
                    if (result) {
                        settings.setProperty(PROP_ALL_PATH, fname);
                        saveSettings();
                    }
                    //no need: the listeners are automatically removed then notified
                    //allLoader.removeAllLoadListener(this);
                }

            });
            allLoader.doLoadAll(fname);

        } else {
            updateTabsAll();
        }
    }

    public void saveSettings() {
        try {
            settings.storeToXML(new FileOutputStream(SETTINGS_FILENAME), "Fa13Manager Settings");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(shell, getMessage("error"), getMessage("cantSaveSettingsError"));
        }
    }

    public interface AllLoadListener {
        public void loadDone(boolean result);
    }

    final private AllLoader allLoader = new AllLoader();

    public class AllLoader {

        private List<AllLoadListener> listLoadListeners = new ArrayList<AllLoadListener>();

        public void addAllLoadListener(AllLoadListener listener) {
            if (listLoadListeners.indexOf(listener) == -1) {
                listLoadListeners.add(listener);
            }
        }

        public void removeAllLoadListener(AllLoadListener listener) {
            listLoadListeners.remove(listener);
        }

        protected void notifyAndClearListeners(boolean result) {
            for (AllLoadListener listener : listLoadListeners) {
                listener.loadDone(result);
            }

            listLoadListeners.clear();
        }

        public void doLoadAll(final String fname) {
            if (fname == null) {
                notifyAndClearListeners(false);
            }

            final GuiLongTask task = new GuiLongTask() {

                @Override
                protected void execute() {

                    try {
                        allInstance = AllReader.readAllFile(fname);
                    } catch (ReaderException e) {
                        MessageBox dlg = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
                        dlg.setText("Reader Error");
                        dlg.setMessage("Error while reading all file");
                        dlg.open();

                        notifyAndClearListeners(false);
                    }
                    allChanged = true;
                    updateView();
                    notifyAndClearListeners(true);

                };
            };
            AllReader.setAllProgressNotify(new AllProgressNotify() {

                @Override
                public void progressChanged(String progressText, int progressCount) {

                    task.progress(progressCount, progressText);

                }
            });
            GuiUtils.showModalProgressWindow(shell, task, MainWindow.getMessage("loadingAll"), false);

        }

    }

    public void updateView() {
        // System.out.println("updateView()...");
        // long time = System.currentTimeMillis();
        teamCombo.removeAll();
        // System.out.println("updateView() after teamCombo.removeAll()");
        if (allInstance == null) {
            return;
        }
        String defaultTeam = settings.getProperty(PROP_CURRENT_TEAM);
        int i = 0;
        int index = 0;
        for (Team team : allInstance.getTeams()) {
            teamCombo.add(team.getName());
            //teamCombo.setData(team.getName(), team);
            if (defaultTeam != null && defaultTeam.compareTo(team.getName()) == 0) {
                index = i;
            }
            ++i;
        }
        teamCombo.select(index);

        dateLabel.setText(dateFormat.format(allInstance.getDate()));
        //dateLabel.setText(allInstance.getShortDescription());
        //dateLabel.pack();
        // System.out.println(">updateView() time: "+String.valueOf(System.currentTimeMillis()-time)+" ms");
    }

    public void teamChanged() {
        int index = teamCombo.getSelectionIndex();
        if (index == -1 && teamCombo.getText() != null && !teamCombo.getText().isEmpty()) {
            //user makes quick search -> handling it on keyHandler
            return;
        }
        if (index == selectedTeamIndex) {
            return;
        }
        selectedTeamIndex = index;
        //System.out.println("teamChanged: index="+String.valueOf(index));
        if (index >= 0) {
            if (allInstance != null) {
                allInstance.setCurrentTeam(index);
                Team team = allInstance.getCurrentTeam();
                if (team != null) {
                    settings.setProperty(PROP_CURRENT_TEAM, team.getName());
                    saveSettings();
                }
            }
        }

        // dirty tabs ui to update it via updateAll
        for (UIItem item : uiItems) {
            if (item != statisticsView || (item == statisticsView && allChanged)) {
                item.updateAll(true);
            }
        }

        updateTabsAll();
        // System.out.println(">teamChanged: index="+String.valueOf(index)+" finished");
    }

    public class TeamComboListener implements SelectionListener, ModifyListener, KeyListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            // System.out.println("widgetDefaultSelected...");
            teamChanged();
            // System.out.println(">widgetDefaultSelected");
        }

        public void widgetSelected(SelectionEvent arg0) {
            // System.out.println("widgetSelected...");
            teamChanged();
            // System.out.println(">widgetSelected");
        }

        public void modifyText(ModifyEvent arg0) {
            // System.out.println("modifyText...");
            teamChanged();
            // System.out.println(">modifyText");
        }

        @Override
        public void keyPressed(KeyEvent e) {
            // User pressed Enter.
            if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR) {
                if (allInstance != null) {
                    //search first matching team
                    String text = teamCombo.getText();
                    if (text != null) {
                        text = text.toUpperCase();
                    }
                    for (Team team : allInstance.getTeams()) {
                        if (team != null && team.getName().toUpperCase().startsWith(text)) {
                            teamCombo.setText(team.getName());
                            break;
                        }
                    }
                }
            }

        }

        @Override
        public void keyReleased(KeyEvent e) {
            // TODO Auto-generated method stub

        }
    }

    public void updateTabsAll() {
        //System.out.println("updateTabsAll()...");
        //System.out.println("allChanged="+allChanged);
        // long time=System.currentTimeMillis();
        boolean flag = allInstance == null;
        tabFolder.setEnabled(!flag);
        tabFolder.setVisible(!flag);
        teamCombo.setEnabled(!flag);
        pwdEditor.setEnabled(!flag);
        dateLabel.setVisible(!flag);

        if (!flag) {

            GuiLongTask task = new GuiLongTask() {

                @Override
                protected void execute() {
                    // System.out.println("execute impl started");
                    // int step = 0;
                    // progress(0, "loading...");
                    // int percent = 0;
                    TabItem selectedTab = (TabItem) tabFolder.getSelection()[0];
                    for (UIItem uiItem : uiItems) {

                        if (uiItem != null) {
                            if (uiItem.asTabItem() == selectedTab) {
                                uiItem.updateAll();
                            } else {
                                // update later lazy
                                if (uiItem != statisticsView || (uiItem == statisticsView && allChanged)) {
                                    uiItem.updateAll(true);
                                }
                            }
                        }
                        // percent = (100 * ++step) / uiItems.size();
                        // progress(percent,
                        // "Progress  " + String.valueOf(percent));
                        // System.out.println("execute impl progress = "
                        // + String.valueOf(percent));
                    }
                    allChanged = false;

                }
            };

            GuiUtils.showModalProgressWindow(shell, task, MainWindow.getMessage("global.pleaseWait"), false);
        }
        //allChanged = false;
        // System.out.println(">updateTabsAll() time: "+String.valueOf(System.currentTimeMillis()-time)+" ms");
    }

    public void updateTabsPassword() {
        for (UIItem uiItem : uiItems) {
            if (uiItem != null) {
                uiItem.updatePassword(password);
            }
        }
    }

    public Properties getSettings() {
        return settings;
    }

    public void setSettings(Properties settings) {
        this.settings = settings;
        String langPropName = settings.getProperty(PROP_LANG, DEFAULT_BUNDLE);
        // System.out.println(langPropName);
        InputStream stream = this.getClass().getResourceAsStream(langPropName);

        try {
            InputStreamReader readerIs = new InputStreamReader(stream, "UTF-8");
            messageInstance = new PropertyResourceBundle(readerIs);
        } catch (IOException e1) {
            stream = this.getClass().getResourceAsStream(DEFAULT_BUNDLE);
            try {
                InputStreamReader readerIs = new InputStreamReader(stream, "UTF-8");
                messageInstance = new PropertyResourceBundle(readerIs);
            } catch (IOException e2) {
                e1.printStackTrace();
            }
        }
        updateMessages();
        saveSettings();
    }

    public void updateMessages() {
        openButton.setText(messageInstance.getString("openAll"));
        updateAllButton.setToolTipText(getMessage("CheckAndUpdateAll"));
        teamLabel.setText(messageInstance.getString("global.team"));
        pwdLabel.setText(messageInstance.getString("global.password"));
        settingsButton.setText(messageInstance.getString("settingsButton"));

        toolItemSupport.setText(getMessage("support"));

        if (miGameRules != null) {
            miGameRules.setText(getMessage("support.gameRules"));
        }
        if (miSupportForum != null) {
            miSupportForum.setText(getMessage("support.forumPage"));
        }
        if (miLastDownloads != null) {
            miLastDownloads.setText(getMessage("support.downloadsPage"));
        }
        if (miReportIssues != null) {
            miReportIssues.setText(getMessage("support.reportIssues"));
        }
        if (miCheckUpdates != null) {
            miCheckUpdates.setText(getMessage("updatesCheck"));
        }

        topComposite.layout();

        for (UIItem uiItem : uiItems) {
            if (uiItem != null) {
                uiItem.updateMessages();
            }
        }
    }

    public static All getAllInstance() {
        return allInstance;
    }

    public static ResourceBundle getMessageInstance() {
        return messageInstance;
    }

    public static String getMessage(String key) {
        return messageInstance.getString(key);
    }

    public static Player getCurrentTeamPlayerByNumber(int playerNumber) {
        if (getAllInstance() != null) {
            Team team = getAllInstance().getCurrentTeam();
            if (team != null) {
                return team.getPlayerByNumber(playerNumber);
            }
        }
        return null;
    }

    @Override
    public void dispose() {
        //release shared images
        if (sharedImagesMap.entrySet().size() > 0) {
            for (Entry<ImageType, Image> entry : sharedImagesMap.entrySet()) {
                entry.getValue().dispose();
            }
        }
        //release system colors
        if (sharedColorResourceMap.entrySet().size() > 0) {
            for (Entry<ColorResourceType, Color> entry : sharedColorResourceMap.entrySet()) {
                entry.getValue().dispose();
            }
        }

        //release system fonts
        if (sharedFontsMap.entrySet().size() > 0) {
            for (Entry<FontType, Font> entry : sharedFontsMap.entrySet()) {
                entry.getValue().dispose();
            }
        }

        //release own images
        imageSupport.dispose();
        imageBug.dispose();
        imgAppIcon.dispose();
    }

    @Override
    public void store(Properties props) {
        settings.put(PROP_PASSWORD, password == null ? "" : password);
    }

    //    public Combo getTeamCombo() {
    //        return teamCombo;
    //    }

}