package com.fa13.build.view;

import org.eclipse.swt.widgets.TabItem;


public interface UIItem {


    /**use
     *  {@link #updateAll(boolean)}
     *  instead
     *  for lazy gui update.
     */
    @Deprecated
    public void updateAll();
    public void updateAll(boolean lazyUpdate);
    public TabItem asTabItem();
    public void updatePassword(String password);
    public void updateMessages();
    public void redraw();
    
}
