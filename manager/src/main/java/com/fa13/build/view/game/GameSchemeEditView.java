package com.fa13.build.view.game;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.ILoadable;
import com.fa13.build.api.IStorable;
import com.fa13.build.model.All;
import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.CoachSettings;
import com.fa13.build.model.game.FreekickAction;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.Hardness;
import com.fa13.build.model.game.PassingStyle;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.Style;
import com.fa13.build.model.game.Tactic;
import com.fa13.build.model.game.TeamTactics;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.ClubInfoView;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.UIItem;

public class GameSchemeEditView implements UIItem, IDisposable, ILoadable, IStorable {

    private int maxWidth = 0;
    private int maxHeight = 0;

    private SchemeViewType schemeType = SchemeViewType.ATTACK;
    private Label playerTestLabel;

    private Image playerNormalImage;
    private Image playerHighlightedImage;
    private Image playerCoordSubstImage;

    private Composite parentInstance;
    private Composite rightPanel;
    private ScrolledComposite rightPanelScrl;
    private boolean isSchemeUpdate = true;
    private Button attackButton;
    private Button defenceButton;
    private Button freeKickButton;

    private Button mainPlayersButton;
    private Button substitutionPlayersButton;

    private Label playerNameLabel;
    private Group playerGroup;

    private Label playerXLabel;
    private Label playerYLabel;
    private Label longShotLabel;
    private Label actOnFreekickLabel;
    private Label fantasistaLabel;
    private Label dispatcherLabel;
    private Label personalDefenceLabel;
    private Label pressingLabel;
    private Label keepBallLabel;
    private Label defenderAttackLabel;
    private Label passingStyleLabel;
    private Label hardnessLabel;

    private Button playerXButton;
    private Button playerYButton;
    private Button longShotButton;
    private Button actOnFreekickButton;
    private Button fantasistaButton;
    private Button dispatcherButton;
    private Button personalDefenceButton;
    private Button pressingButton;
    private Button keepBallButton;
    private Button defenderAttackButton;
    private Button passingStyleButton;
    private Button hardnessButton;

    private Spinner personalDefenceSpinner;
    private Spinner playerXSpinner;
    private Spinner playerYSpinner;

    private Combo passingStyleCombo;
    private Combo hardnessCombo;

    private Composite fieldPanel;

    private Combo coachSettingsCombo;
    private Combo teamTacticCombo;
    private Combo teamHardnessCombo;
    private Combo teamStyleCombo;
    private Label coachSettingsLabel;
    private Label teamTacticLabel;
    private Label teamHardnessLabel;
    private Label teamStyleLabel;

    private Spinner attackTimeSpinner;
    private Spinner attackMinSpinner;
    private Spinner attackMaxSpinner;
    private Spinner defenceTimeSpinner;
    private Spinner defenceMinSpinner;
    private Spinner defenceMaxSpinner;
    //private Spinner ticketPriceSpinner;

    private Label timeLabel;
    private Label resultLabel;
    private Label timeLabelD;
    private Label resultLabelD;
    //private Label ticketLabel;

    private Group attackGroup;
    private Group defenceGroup;
    private Group teamTacticsGroup;
    //private Group ticketPriceGroup;

    private Canvas fieldCanvas;
    private Image fieldImage;

    private final Font playerNumberFont;
    private final Font playerNameFont;
    private final Font playerLabelsFont;
    private final Color playerNumberColor;

    private int currentPlayerPositionIndex = -1;

    public SchemeControlPanel schemeControlPanel;
    private SchemesModel schemesModel;
    private Image defenseImage;
    private Image attackImage;
    private Image freeKickImage;
    
    private Image repairImage;
    
    private boolean playerCoordSubstitutionMode;
    private ScrolledComposite coachSettingsScrolledPanel;
    private boolean useManIcon = false;

    private Team currentTeam;

    public GameSchemeEditView(Composite parent, SchemesModel schemesModel, PlayerImageType playerImageType) {
        this.playerImageType = playerImageType;
        useManIcon = playerImageType == PlayerImageType.Man;
        parentInstance = parent;
        loadImages();
        this.schemesModel = schemesModel;
        parent.setLayout(new FormLayout());

        Composite leftPanel = new Composite(parent, SWT.NONE);

        rightPanelScrl = new ScrolledComposite(parent, SWT.V_SCROLL);
        rightPanelScrl.setExpandHorizontal(true);
        rightPanelScrl.setExpandVertical(true);
        rightPanel = new Composite(rightPanelScrl, SWT.NONE);
        rightPanel.setLayout(new FormLayout());
        rightPanelScrl.setContent(rightPanel);
        rightPanelScrl.setLayout(new FillLayout());
        int scrollWidth = rightPanelScrl.getVerticalBar().getSize().x;

        ScrolledComposite fieldScrolledPanel = initFieldPanel(leftPanel);

        coachSettingsScrolledPanel = new ScrolledComposite(leftPanel, SWT.H_SCROLL);
        coachSettingsScrolledPanel.setExpandHorizontal(true);
        coachSettingsScrolledPanel.setExpandVertical(true);
        Composite coachSettingsPanel = new Composite(coachSettingsScrolledPanel, SWT.NONE);
        coachSettingsPanel.setLayout(new FormLayout());
        coachSettingsScrolledPanel.setContent(coachSettingsPanel);
        coachSettingsScrolledPanel.setLayout(new FillLayout());

        leftPanel.setLayout(new FormLayout());

        FormData data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(rightPanelScrl, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        leftPanel.setLayoutData(data);

        schemeControlPanel = new SchemeControlPanel(rightPanel, schemesModel);

        data = new FormData();
        data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        schemeControlPanel.setLayoutData(data);

        schemeControlPanel.getSchemeCombo().addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                Combo combo = (Combo) e.widget;
                if (isSchemeUpdate) {
                    GameEditView.copyScheme(GameForm.getInstance().getFirstTeam(), (PlayerPosition[]) combo.getData(combo.getText()));
                }
                GameForm.getInstance().setSelectedSchemeIndex(combo.getSelectionIndex());
                if (fieldCanvas != null) {
                    fieldCanvas.redraw();
                }
            }
        });

        attackButton = new Button(rightPanel, SWT.TOGGLE);
        defenceButton = new Button(rightPanel, SWT.TOGGLE);
        freeKickButton = new Button(rightPanel, SWT.TOGGLE);

        attackButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        defenceButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        freeKickButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));

        attackButton.setImage(attackImage);
        defenceButton.setImage(defenseImage);
        freeKickButton.setImage(freeKickImage);

        mainPlayersButton = new Button(rightPanel, SWT.TOGGLE);
        substitutionPlayersButton = new Button(rightPanel, SWT.TOGGLE);
        attackButton.setText(MainWindow.getMessage("AttackViewButton"));
        defenceButton.setText(MainWindow.getMessage("DefenceViewButton"));
        freeKickButton.setText(MainWindow.getMessage("FreeKickViewButton"));
        mainPlayersButton.setText(MainWindow.getMessage("MainPlayersPositionsView"));
        substitutionPlayersButton.setText(MainWindow.getMessage("SubstitutionPlayersPositionsView"));

        playerNameLabel = new Label(rightPanel, SWT.NONE);
        playerNameLabel.setText(" ");

        playerNameFont = createFont(parent, 12, SWT.BOLD | SWT.ITALIC);
        playerNameLabel.setFont(playerNameFont);

        playerGroup = new Group(rightPanel, SWT.NONE);
        playerGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        playerGroup.setText(MainWindow.getMessage("PlayerSettingsGroupLabel"));
        playerGroup.setLayout(new FormLayout());

        String labelsNames[] = { MainWindow.getMessage("PositionPlayerX"), MainWindow.getMessage("PositionPlayerY"),
                MainWindow.getMessage("PositionLongShot"), MainWindow.getMessage("PositionActOnFreekick"),
                MainWindow.getMessage("PositionFantasista"), MainWindow.getMessage("PositionDispatcher"),
                MainWindow.getMessage("PositionPersonalDefence"), MainWindow.getMessage("PositionPressing"),
                MainWindow.getMessage("PositionKeepBall"), MainWindow.getMessage("PositionDefenderAttack"),
                MainWindow.getMessage("PositionPassingStyle"), MainWindow.getMessage("PositionHardness") };

        Label labels[] = { playerXLabel, playerYLabel, longShotLabel, actOnFreekickLabel, fantasistaLabel, dispatcherLabel, personalDefenceLabel,
                pressingLabel, keepBallLabel, defenderAttackLabel, passingStyleLabel, hardnessLabel };

        for (int i = 0; i < labelsNames.length; i++) {
            labels[i] = new Label(playerGroup, SWT.NONE);
            labels[i].setText(labelsNames[i]);
        }

        playerXButton = new Button(playerGroup, SWT.CHECK);
        playerXButton.setVisible(false);
        playerYButton = new Button(playerGroup, SWT.CHECK);
        playerYButton.setVisible(false);
        longShotButton = new Button(playerGroup, SWT.CHECK);
        actOnFreekickButton = new Button(playerGroup, SWT.CHECK);
        fantasistaButton = new Button(playerGroup, SWT.CHECK);
        dispatcherButton = new Button(playerGroup, SWT.CHECK);
        personalDefenceButton = new Button(playerGroup, SWT.CHECK);
        pressingButton = new Button(playerGroup, SWT.CHECK);
        keepBallButton = new Button(playerGroup, SWT.CHECK);
        defenderAttackButton = new Button(playerGroup, SWT.CHECK);
        passingStyleButton = new Button(playerGroup, SWT.CHECK);
        hardnessButton = new Button(playerGroup, SWT.CHECK);

        longShotButton.addSelectionListener(new CheckButtonListener(0));
        actOnFreekickButton.addSelectionListener(new CheckButtonListener(1));
        fantasistaButton.addSelectionListener(new CheckButtonListener(2));
        dispatcherButton.addSelectionListener(new CheckButtonListener(3));
        personalDefenceButton.addSelectionListener(new CheckButtonListener(4));
        pressingButton.addSelectionListener(new CheckButtonListener(5));
        keepBallButton.addSelectionListener(new CheckButtonListener(6));
        defenderAttackButton.addSelectionListener(new CheckButtonListener(7));

        passingStyleButton.setVisible(false);
        hardnessButton.setVisible(false);

        passingStyleCombo = new Combo(playerGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
        hardnessCombo = new Combo(playerGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
        
        if (!SystemUtils.IS_OS_LINUX) {
            hardnessCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            passingStyleCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        
        Button buttons[] = { playerXButton, playerYButton, longShotButton, actOnFreekickButton, fantasistaButton, dispatcherButton,
                personalDefenceButton, pressingButton, keepBallButton, defenderAttackButton, passingStyleButton, hardnessButton };

        int index = 0;
        playerXLabel = labels[index++];
        playerYLabel = labels[index++];
        longShotLabel = labels[index++];
        actOnFreekickLabel = labels[index++];
        fantasistaLabel = labels[index++];
        dispatcherLabel = labels[index++];
        personalDefenceLabel = labels[index++];
        pressingLabel = labels[index++];
        keepBallLabel = labels[index++];
        defenderAttackLabel = labels[index++];
        passingStyleLabel = labels[index++];
        hardnessLabel = labels[index++];

        personalDefenceSpinner = new Spinner(playerGroup, SWT.BORDER);
        personalDefenceSpinner.setTextLimit(3);
        personalDefenceSpinner.setMinimum(1);
        personalDefenceSpinner.setMaximum(25);
        personalDefenceSpinner.setIncrement(1);
        Point spinnerSize = personalDefenceSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        personalDefenceSpinner.setDigits(0);
        personalDefenceSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                PlayerPosition pos = getCurrentPlayerPosition();
                if (pos != null) {
                    if (personalDefenceButton.getSelection()) {
                        pos.setPersonalDefence(personalDefenceSpinner.getSelection());
                        redrawCurrentPlayer();
                    }
                }
            }
        });

        playerXSpinner = new Spinner(playerGroup, SWT.BORDER);
        playerXSpinner.setTextLimit(3);
        playerXSpinner.setMinimum(0);
        playerXSpinner.setMaximum(600);
        playerXSpinner.setIncrement(1);
        playerXSpinner.setPageIncrement(10);
        playerXSpinner.setDigits(0);

        playerYSpinner = new Spinner(playerGroup, SWT.BORDER);
        playerYSpinner.setTextLimit(3);
        playerYSpinner.setMinimum(0);
        playerYSpinner.setMaximum(450);
        playerYSpinner.setIncrement(1);
        playerYSpinner.setPageIncrement(10);
        playerYSpinner.setDigits(0);

        spinnerSize.y = Math.max(spinnerSize.y, hardnessCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        spinnerSize.y += 2;

        resetPassingStyleCombo();
        passingStyleCombo.addModifyListener(new PassingStyleComboListener());

        resetHardnessCombo();
        hardnessCombo.addModifyListener(new HardnessComboListener());

        data = new FormData();
        data.right = new FormAttachment(100, -10);
        data.left = new FormAttachment(100, -10 - passingStyleCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(passingStyleButton, 3 - spinnerSize.y);
        passingStyleCombo.setLayoutData(data);

        data = new FormData();
        data.right = new FormAttachment(100, -10);
        data.left = new FormAttachment(100, -10 - hardnessCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(hardnessButton, 3 - spinnerSize.y);
        hardnessCombo.setLayoutData(data);

        data = new FormData();
        data.right = new FormAttachment(100, -10);
        data.left = new FormAttachment(100, -10 - spinnerSize.x);
        int dy = personalDefenceButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y - personalDefenceSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        dy /= 2;
        data.top = new FormAttachment(personalDefenceButton, dy - personalDefenceButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        personalDefenceSpinner.setLayoutData(data);

        data = new FormData();
        data.right = new FormAttachment(100, -10);
        dy = playerXButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y - playerXSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        dy /= 2;
        data.top = new FormAttachment(playerXButton, dy - playerXButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        playerXSpinner.setLayoutData(data);

        data = new FormData();
        data.right = new FormAttachment(100, -10);
        dy = playerYButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y - playerYSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        dy /= 2;
        data.top = new FormAttachment(playerYButton, dy - playerXButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        playerYSpinner.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(0, 6);
        data.right = new FormAttachment(buttons[0], buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(0, spinnerSize.y / 4);
        buttons[0].setLayoutData(data);

        for (int i = 1; i < buttons.length; i++) {
            data = new FormData();
            data.left = new FormAttachment(0, 6);
            data.right = new FormAttachment(buttons[i], buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
            data.top = new FormAttachment(buttons[i - 1], spinnerSize.y - buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
            buttons[i].setLayoutData(data);
        }

        data = new FormData();
        data.left = new FormAttachment(buttons[0], 10);
        data.right = new FormAttachment(labels[0], labels[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(buttons[0], -buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        labels[0].setLayoutData(data);

        for (int i = 1; i < labels.length; i++) {
            data = new FormData();
            data.left = new FormAttachment(buttons[i], 10);
            data.right = new FormAttachment(labels[i], labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
            int dy1 = buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y - labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
            data.top = new FormAttachment(buttons[i], dy1 / 2 - buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
            labels[i].setLayoutData(data);
        }
        /*
                data = new FormData();
                data.left = new FormAttachment(0, scrollWidth);
                data.right = new FormAttachment(deleteScheme, -10);
                data.top = new FormAttachment(0, 1);
                schemeCombo.setLayoutData(data);

                data = new FormData();
                data.left = new FormAttachment(deleteScheme, -deleteScheme.computeSize(
                        SWT.DEFAULT, SWT.DEFAULT).x);
                data.right = new FormAttachment(100, -scrollWidth);
                data.top = new FormAttachment(0, 0);
                deleteScheme.setLayoutData(data);
        */
        data = new FormData();
        data.left = new FormAttachment(0, scrollWidth);
        // data.right = new FormAttachment(defenceButton, defenceButton
        // .computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        //data.top = new FormAttachment(schemeCombo, 10);
        data.top = new FormAttachment(schemeControlPanel, 10);
        defenceButton.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(defenceButton, 0);
        // data.right = new FormAttachment(attackButton,
        // attackButton.computeSize(
        // SWT.DEFAULT, SWT.DEFAULT).x);
        //data.top = new FormAttachment(schemeCombo, 10);
        data.top = new FormAttachment(schemeControlPanel, 10);
        attackButton.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(attackButton, 0);
        // data.right = new FormAttachment(freeKickButton, freeKickButton
        // .computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        //data.top = new FormAttachment(schemeCombo, 10);
        data.top = new FormAttachment(schemeControlPanel, 10);
        freeKickButton.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(0, scrollWidth);
        data.right = new FormAttachment(mainPlayersButton, mainPlayersButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(defenceButton, 3);
        mainPlayersButton.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(mainPlayersButton, 0);
        data.right = new FormAttachment(substitutionPlayersButton, substitutionPlayersButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(defenceButton, 3);
        substitutionPlayersButton.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(0, 10);
        data.right = new FormAttachment(100, -10);
        data.top = new FormAttachment(attackButton/* mainPlayersButton */, 6);
        playerNameLabel.setLayoutData(data);

        int maxLabelsWidth = 0;
        for (int i = 0; i < labels.length - 2; i++) {
            maxLabelsWidth = Math.max(maxLabelsWidth, labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        }
        int groupWidth = 0;
        groupWidth = 10;
        groupWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        groupWidth += 6;
        groupWidth += maxLabelsWidth;
        groupWidth += 6;
        groupWidth += spinnerSize.x;
        groupWidth += 10;
        groupWidth = Math.max(playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).x, groupWidth);

        maxLabelsWidth = 6;
        maxLabelsWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += passingStyleCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += passingStyleLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        groupWidth = Math.max(groupWidth, maxLabelsWidth);

        maxLabelsWidth = 6;
        maxLabelsWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += hardnessCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += hardnessLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        groupWidth = Math.max(groupWidth, maxLabelsWidth);

        data = new FormData();
        data.left = new FormAttachment(0, 10);
        data.right = new FormAttachment(playerGroup, groupWidth);
        data.top = new FormAttachment(playerNameLabel, 6);
        data.bottom = new FormAttachment(playerGroup, playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).y + spinnerSize.y / 2);
        playerGroup.setLayoutData(data);

        int rightPanelWidth = 0;

        rightPanelWidth = 2 * scrollWidth;
        rightPanelWidth += attackButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth += defenceButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth += freeKickButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth = Math.max(rightPanelWidth, playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        rightPanelWidth = Math.max(rightPanelWidth, groupWidth + 20);

        data = new FormData();
        data.left = new FormAttachment(rightPanelScrl, -rightPanelWidth);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        rightPanelScrl.setLayoutData(data);

        int scrollHeight = 0;
        //scrollHeight = schemeCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight = schemeControlPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight += 10;
        scrollHeight += freeKickButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        // scrollHeight += 3;
        // scrollHeight += mainPlayersButton.computeSize(SWT.DEFAULT,
        // SWT.DEFAULT).y;
        scrollHeight += 6;
        scrollHeight += playerNameLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight += 6;
        scrollHeight += playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).y + spinnerSize.y / 2;
        // scrollHeight += 10;

        rightPanelScrl.setMinWidth(rightPanelWidth);
        rightPanelScrl.setMinHeight(scrollHeight);

        coachSettingsPanel.setLayout(new GridLayout(2, false));//set 2 columns to give field more space and see it without scroll

        Composite baseGameSettingsPanel = new Composite(coachSettingsPanel, SWT.NONE);
        baseGameSettingsPanel.setLayout(new GridLayout(2, false));
        Composite forceToSettingsPanel = new Composite(coachSettingsPanel, SWT.NONE);
        forceToSettingsPanel.setLayout(new GridLayout(2, false));

        //        ticketPriceGroup = new Group(baseGameSettingsPanel, SWT.NONE);
        //        ticketPriceGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));

        teamTacticsGroup = new Group(baseGameSettingsPanel, SWT.NONE);
        teamTacticsGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        teamTacticsGroup.setLayout(new GridLayout(4, false));
        teamTacticsGroup.setText(MainWindow.getMessage("teamTacticsGroup"));

        coachSettingsLabel = new Label(teamTacticsGroup, SWT.NONE);
        coachSettingsLabel.setText(MainWindow.getMessage("coachSettingsLabel"));
        teamTacticLabel = new Label(teamTacticsGroup, SWT.NONE);
        teamTacticLabel.setText(MainWindow.getMessage("teamTacticLabel"));
        teamHardnessLabel = new Label(teamTacticsGroup, SWT.NONE);
        teamHardnessLabel.setText(MainWindow.getMessage("teamHardnessLabel"));
        teamStyleLabel = new Label(teamTacticsGroup, SWT.NONE);
        teamStyleLabel.setText(MainWindow.getMessage("teamStyleLabel"));

        coachSettingsCombo = new Combo(teamTacticsGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
        
        teamTacticCombo = new Combo(teamTacticsGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
        
        teamHardnessCombo = new Combo(teamTacticsGroup, SWT.DROP_DOWN | SWT.READ_ONLY);
        
        teamStyleCombo = new Combo(teamTacticsGroup, SWT.DROP_DOWN | SWT.READ_ONLY);

        if (!SystemUtils.IS_OS_LINUX) {
            coachSettingsCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            teamTacticCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            teamHardnessCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            teamStyleCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        
        resetTeamHardnessCombo();
        teamHardnessCombo.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                String text = teamHardnessCombo.getText();
                Hardness value = (Hardness) teamHardnessCombo.getData(text);
                if (GameForm.getInstance().getStartTactics() != null) {
                    GameForm.getInstance().getStartTactics().setTeamHardness(value);
                } else {
                    GameForm.getInstance().setStartTactics(createStartTacticsFromSelectedValues());
                }
            }
        });

        resetTeamStyleCombo();
        teamStyleCombo.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                if (GameForm.getInstance().getStartTactics() != null) {
                    String text = teamStyleCombo.getText();
                    Style value = (Style) teamStyleCombo.getData(text);
                    GameForm.getInstance().getStartTactics().setTeamStyle(value);
                } else {
                    GameForm.getInstance().setStartTactics(createStartTacticsFromSelectedValues());
                }
            }
        });

        resetTeamTacticCombo();
        teamTacticCombo.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                String text = teamTacticCombo.getText();
                Tactic value = (Tactic) teamTacticCombo.getData(text);
                if (GameForm.getInstance().getStartTactics() != null) {
                    GameForm.getInstance().getStartTactics().setTeamTactic(value);
                } else {
                    GameForm.getInstance().setStartTactics(createStartTacticsFromSelectedValues());
                }
            }
        });

        resetCoachSettingsCombo();
        coachSettingsCombo.setVisibleItemCount(CoachSettings.values().length);
        coachSettingsCombo.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                String text = coachSettingsCombo.getText();
                CoachSettings value = (CoachSettings) coachSettingsCombo.getData(text);
                GameForm.getInstance().setTeamCoachSettings(value);
            }
        });

        attackGroup = new Group(forceToSettingsPanel, SWT.NONE);
        attackGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        defenceGroup = new Group(forceToSettingsPanel, SWT.NONE);
        defenceGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        attackGroup.setLayout(new GridLayout(5, false));
        attackGroup.setText(MainWindow.getMessage("attackGroup"));
        defenceGroup.setText(MainWindow.getMessage("defenceGroup"));
        defenceGroup.setLayout(new GridLayout(5, false));
        //        ticketPriceGroup.setText(MainWindow.getMessage("ticketPriceGroup"));
        //        ticketPriceGroup.setLayout(new GridLayout(1, false));

        //        ticketLabel = new Label(ticketPriceGroup, SWT.NONE);
        //        ticketLabel.setText(MainWindow.getMessage("ticketLabel"));
        timeLabel = new Label(attackGroup, SWT.NONE);
        timeLabel.setText(MainWindow.getMessage("timeLabel"));
        resultLabel = new Label(attackGroup, SWT.NONE);
        resultLabel.setText(MainWindow.getMessage("resultLabel"));

        GridData gridData = new GridData();
        gridData.horizontalAlignment = GridData.FILL;
        gridData.horizontalSpan = 4;
        gridData.horizontalAlignment = SWT.LEFT;
        resultLabel.setLayoutData(gridData);

        timeLabelD = new Label(defenceGroup, SWT.NONE);
        timeLabelD.setText(MainWindow.getMessage("timeLabel"));
        resultLabelD = new Label(defenceGroup, SWT.NONE);
        resultLabelD.setText(MainWindow.getMessage("resultLabel"));

        gridData = new GridData();
        gridData.horizontalAlignment = GridData.FILL;
        gridData.horizontalSpan = 4;
        gridData.horizontalAlignment = SWT.LEFT;
        resultLabelD.setLayoutData(gridData);

        //        ticketPriceSpinner = new Spinner(ticketPriceGroup, SWT.BORDER);
        //        ticketPriceSpinner.setMinimum(0);
        //        ticketPriceSpinner.setMaximum(999);
        //        ticketPriceSpinner.setTextLimit(3);
        //        ticketPriceSpinner.setSelection(40);
        //        GameForm.getInstance().setPrice(ticketPriceSpinner.getSelection());
        //        ticketPriceSpinner.addModifyListener(new ModifyListener() {
        //
        //            public void modifyText(ModifyEvent e) {
        //                GameForm.getInstance().setPrice(ticketPriceSpinner.getSelection());
        //            }
        //        });

        attackTimeSpinner = new Spinner(attackGroup, SWT.BORDER);
        attackTimeSpinner.setMinimum(0);
        attackTimeSpinner.setMaximum(120);
        attackTimeSpinner.setTextLimit(3);
        attackTimeSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setAttackTime(attackTimeSpinner.getSelection());
            }
        });
        Label from = new Label(attackGroup, SWT.NONE);
        from.setText(MainWindow.getMessage("from"));
        attackMinSpinner = new Spinner(attackGroup, SWT.BORDER );
        attackMinSpinner.setMinimum(-20);
        attackMinSpinner.setMaximum(20);
        attackMinSpinner.setTextLimit(3);
        attackMinSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setAttackMin(attackMinSpinner.getSelection());
            }
        });
        Label to = new Label(attackGroup, SWT.NONE);
        to.setText(MainWindow.getMessage("to"));
        attackMaxSpinner = new Spinner(attackGroup, SWT.BORDER );
        attackMaxSpinner.setMinimum(-20);
        attackMaxSpinner.setMaximum(20);
        attackMaxSpinner.setTextLimit(3);
        attackMaxSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setAttackMax(attackMaxSpinner.getSelection());
            }
        });

        defenceTimeSpinner = new Spinner(defenceGroup, SWT.BORDER);
        defenceTimeSpinner.setMinimum(0);
        defenceTimeSpinner.setMaximum(120);
        defenceTimeSpinner.setTextLimit(3);
        defenceTimeSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setDefenceTime(defenceTimeSpinner.getSelection());
            }
        });

        Label fromDefence = new Label(defenceGroup, SWT.NONE);
        fromDefence.setText(MainWindow.getMessage("from"));
        defenceMinSpinner = new Spinner(defenceGroup, SWT.BORDER );
        defenceMinSpinner.setMinimum(-20);
        defenceMinSpinner.setMaximum(20);
        defenceMinSpinner.setTextLimit(3);
        defenceMinSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setDefenceMin(defenceMinSpinner.getSelection());
            }
        });

        Label toDefence = new Label(defenceGroup, SWT.NONE);
        toDefence.setText(MainWindow.getMessage("to"));
        defenceMaxSpinner = new Spinner(defenceGroup, SWT.BORDER );
        defenceMaxSpinner.setMinimum(-20);
        defenceMaxSpinner.setMaximum(20);
        defenceMaxSpinner.setTextLimit(3);
        defenceMaxSpinner.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                GameForm.getInstance().setDefenceMax(defenceMaxSpinner.getSelection());
            }
        });

        //ticketPriceGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, false, false));
        teamTacticsGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, false, false));
        attackGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, false, false));
        defenceGroup.setLayoutData(new GridData(SWT.BEGINNING, SWT.FILL, false, false));
        coachSettingsPanel.pack();
        coachSettingsScrolledPanel.setMinSize(coachSettingsPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT));

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, -coachSettingsScrolledPanel.getHorizontalBar().getSize().y
                - coachSettingsPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        fieldScrolledPanel.setLayoutData(data);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(fieldScrolledPanel, 0);
        data.bottom = new FormAttachment(100, 0);
        coachSettingsScrolledPanel.setLayoutData(data);

        Button btns[] = { attackButton, defenceButton, freeKickButton };

        attackButton.addSelectionListener(new ViewButtonListener(SchemeViewType.ATTACK, btns, 0));
        defenceButton.addSelectionListener(new ViewButtonListener(SchemeViewType.DEFENCE, btns, 1));
        freeKickButton.addSelectionListener(new ViewButtonListener(SchemeViewType.FREEKICK, btns, 2));
        defenceButton.setSelection(true);
        schemeType = SchemeViewType.DEFENCE;

        mainPlayersButton.setVisible(false);
        substitutionPlayersButton.setVisible(false);

        PlayerSpinnerModifyListener spinnerModifyListener = new PlayerSpinnerModifyListener(this);
        playerXSpinner.addModifyListener(spinnerModifyListener);
        playerYSpinner.addModifyListener(spinnerModifyListener);

        for (Button btn : btns) {
            btn.setSelection(false);
        }
        //deleteScheme.setEnabled(false);
        //playerTestLabel.setVisible(false);

        if (GameForm.getInstance().getStartTactics() == null) {
            GameForm.getInstance().setStartTactics(createStartTacticsFromSelectedValues());
        }

        playerNumberColor = playerImageType == PlayerImageType.Man ? new Color(parent.getDisplay(), 0, 255, 255) : new Color(parent.getDisplay(),
                240, 0, 0);
        int fontSize = playerImageType == PlayerImageType.Small ? 5 : 7;
        playerNumberFont = createFont(parent, fontSize, SWT.BOLD);
        fontSize = playerImageType == PlayerImageType.Standard ? 8 : 7;
        playerLabelsFont = createFont(parent, fontSize, SWT.BOLD | SWT.ITALIC);

    }

    private void loadImages() {

        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/shield_16x16.png"));
        defenseImage = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/sword_16x16.png"));
        attackImage = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/freekick_16x16.png"));
        freeKickImage = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/repair_16x16.png"));
        repairImage = new Image(parentInstance.getDisplay(), imgData);
        
        String imgPath;
        if (playerImageType == PlayerImageType.Small) {
            imgPath = "/com/fa13/build/resources/images/player2_small.png";
        } else if (playerImageType == PlayerImageType.Standard) {
            imgPath = "/com/fa13/build/resources/images/player2.png";
        } else {
            imgPath = "/com/fa13/build/resources/images/player_man2.png";
        }

        imgData = new ImageData(this.getClass().getResourceAsStream(imgPath));
        playerCoordSubstImage = new Image(parentInstance.getDisplay(), imgData);

    }

    private ScrolledComposite initFieldPanel(final Composite parent) {
        ScrolledComposite fieldScrolledPanel = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.H_SCROLL);
        fieldScrolledPanel.setExpandHorizontal(false);
        fieldScrolledPanel.setExpandVertical(false);
        fieldScrolledPanel.setMinHeight(620);

        fieldPanel = new Composite(fieldScrolledPanel, SWT.NONE);
        fieldScrolledPanel.setContent(fieldPanel);

        fieldPanel.setLayout(new FormLayout());
        fieldPanel.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_BLACK));

        fieldPanel.setSize(698, 460);
        fieldPanel.setLayout(new FormLayout());

        fieldImage = new Image(parent.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/images/field.jpg"));

        fieldCanvas = new Canvas(fieldPanel, SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE);
        fieldCanvas.setSize(698, 460);
        fieldCanvas.addPaintListener(new FieldPaintListener());
        fieldCanvas.setData("canvas");
        Listener listener = new FieldCanvasMouseListener(this);
        fieldCanvas.addListener(SWT.MouseMove, listener);
        fieldCanvas.addListener(SWT.MouseDown, listener);
        fieldCanvas.addListener(SWT.MouseUp, listener);
        FieldCanvasKeyListener fieldCanvasKeyListener = new FieldCanvasKeyListener(this);
        fieldCanvas.addKeyListener(fieldCanvasKeyListener);
        fieldCanvas.addMenuDetectListener(new MenuDetectListener() {
            
            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(fieldCanvas);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("FixImageProblems"));
                item.setImage(repairImage);
                item.addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        //fieldImage.dispose();
                        //fieldImage = new Image(parent.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/images/field.jpg"));
                        fieldCanvas.setSize(698, 460);
                        //fieldCanvas.redraw();
                        fieldCanvas.getParent().layout(true);
                        
                        fieldCanvas.redraw();
                        
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });
        //playerTestLabel = new Label(fieldCanvas, SWT.NO_BACKGROUND);
        String imgPath, imgPathH;
        if (playerImageType == PlayerImageType.Small) {
            imgPath = "/com/fa13/build/resources/images/player_small.png";
            imgPathH = "/com/fa13/build/resources/images/player_hl_small.png";
        } else if (playerImageType == PlayerImageType.Standard) {
            imgPath = "/com/fa13/build/resources/images/player.png";
            imgPathH = "/com/fa13/build/resources/images/player_hl.png";
        } else {
            imgPath = "/com/fa13/build/resources/images/player_man.png";
            imgPathH = "/com/fa13/build/resources/images/player_man_hl.png";
        }
        ImageData playerNormal = new ImageData(this.getClass().getResourceAsStream(imgPath));
        playerNormalImage = new Image(parent.getDisplay(), playerNormal);

        ImageData playerHighlighted = new ImageData(this.getClass().getResourceAsStream(imgPathH));
        playerHighlightedImage = new Image(parent.getDisplay(), playerHighlighted);

        return fieldScrolledPanel;
    }

    //TODO remove magic numbers!
    public static int mapVirtToRealX(int x) {
        return (int) ((678.0 * x / 600.0) + 10);
    }

    public static int mapVirtToRealY(int y) {
        return (int) ((440.0 * y / 450.0) + 10);
    }

    public static int mapRealToVirtX(int x) {
        int res = (int) ((double) (x - 10) * 600 / 678.0);
        if (res < 0) {
            res = 0;
        }
        if (res > 600) {
            res = 600;
        }
        return res;
    }

    public static int mapRealToVirtY(int y) {
        int res = (int) ((double) (y - 10) * 450 / 440.0);
        if (res < 0) {
            res = 0;
        }
        if (res > 450) {
            res = 450;
        }
        return res;
    }

    public void setCurrentPlayerPositionByIndex(int playerPositionIndex) {
        if (playerPositionIndex >= 0 || playerCoordSubstitutionMode) {

            PlayerPosition playerPos;
            if (playerCoordSubstitutionMode) {
                playerPos = GameForm.getInstance().getActiveCoordSubtition();
            } else {
                currentPlayerPositionIndex = playerPositionIndex;
                playerPos = GameForm.getInstance().getFirstTeam()[playerPositionIndex];
            }
            Player player = null;
            if (MainWindow.getAllInstance() != null) {
                player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(playerPos.getNumber());
            }

            playerXSpinner.setEnabled(true);
            playerYSpinner.setEnabled(true);

            playerXSpinner.setSelection(getPosX(playerPos));
            playerYSpinner.setSelection(getPosY(playerPos));
            if (player != null) {
                playerNameLabel.setText(player.getNumber() + " " + playerPos.getAmplua().toString() + " " + player.getName());
            } else {
                playerNameLabel.setText(MainWindow.getMessage("PositionPlayerChoose"));
            }

            if (playerPos.getActOnFreekick() == FreekickAction.FK_YES) {
                actOnFreekickButton.setGrayed(false);
                actOnFreekickButton.setSelection(true);
            }
            if (playerPos.getActOnFreekick() == FreekickAction.FK_DEFAULT) {
                actOnFreekickButton.setGrayed(true);
                actOnFreekickButton.setSelection(true);

            }
            if (playerPos.getActOnFreekick() == FreekickAction.FK_NO) {
                actOnFreekickButton.setGrayed(false);
                actOnFreekickButton.setSelection(false);
            }

            longShotButton.setSelection(playerPos.isLongShot());
            fantasistaButton.setSelection(playerPos.isFantasista());
            dispatcherButton.setSelection(playerPos.isDispatcher());
            if (playerPos.getPersonalDefence() > 0) {
                personalDefenceButton.setSelection(true);
            } else {
                personalDefenceButton.setSelection(false);
            }

            pressingButton.setSelection(playerPos.isPressing());
            keepBallButton.setSelection(playerPos.isKeepBall());
            defenderAttackButton.setSelection(playerPos.isDefenderAttack());

            if (playerPos.getAmplua() == PlayerAmplua.CD || playerPos.getAmplua() == PlayerAmplua.LD || playerPos.getAmplua() == PlayerAmplua.RD) {
                longShotButton.setEnabled(true);
                actOnFreekickButton.setEnabled(true);
                fantasistaButton.setEnabled(false);
                dispatcherButton.setEnabled(false);
                personalDefenceButton.setEnabled(true);
                pressingButton.setEnabled(false);
                keepBallButton.setEnabled(false);
                defenderAttackButton.setEnabled(true);

                fantasistaButton.setSelection(false);
                dispatcherButton.setSelection(false);
                pressingButton.setSelection(true);
                keepBallButton.setSelection(false);
            }

            if (playerPos.getAmplua() == PlayerAmplua.CM || playerPos.getAmplua() == PlayerAmplua.LM || playerPos.getAmplua() == PlayerAmplua.RM) {
                longShotButton.setEnabled(true);
                actOnFreekickButton.setEnabled(true);
                fantasistaButton.setEnabled(true);
                dispatcherButton.setEnabled(true);
                personalDefenceButton.setEnabled(true);
                pressingButton.setEnabled(true);
                keepBallButton.setEnabled(false);
                defenderAttackButton.setEnabled(false);

                keepBallButton.setSelection(false);
                defenderAttackButton.setSelection(false);
            }

            if (playerPos.getAmplua() == PlayerAmplua.CF || playerPos.getAmplua() == PlayerAmplua.LF || playerPos.getAmplua() == PlayerAmplua.RF) {
                longShotButton.setEnabled(true);
                actOnFreekickButton.setEnabled(false);
                fantasistaButton.setEnabled(true);
                dispatcherButton.setEnabled( playerPos.getAmplua() != PlayerAmplua.CF );
                personalDefenceButton.setEnabled(false);
                pressingButton.setEnabled(true);
                keepBallButton.setEnabled(true);
                defenderAttackButton.setEnabled(false);

                actOnFreekickButton.setSelection(true);
                if (playerPos.getAmplua() == PlayerAmplua.CF ) {
                    dispatcherButton.setSelection(false);
                }
                personalDefenceButton.setSelection(false);
                defenderAttackButton.setSelection(false);
            }

            if (personalDefenceButton.getSelection()) {
                personalDefenceSpinner.setEnabled(true);
                personalDefenceSpinner.setSelection(playerPos.getPersonalDefence());
            } else {
                personalDefenceSpinner.setEnabled(false);
                personalDefenceSpinner.setSelection(1);
            }
            GuiUtils.selectComboItem(passingStyleCombo, playerPos.getPassingStyle().getLocalizedString());
            GuiUtils.selectComboItem(hardnessCombo, playerPos.getHardness().getLocalizedString());
        } else {
            playerXSpinner.setEnabled(false);
            playerYSpinner.setEnabled(false);
            personalDefenceSpinner.setEnabled(false);
        }

    }

    public PlayerPosition getCurrentPlayerPosition() {
        if (playerCoordSubstitutionMode) {
            return GameForm.getInstance().getActiveCoordSubtition();
        } else {
            if (currentPlayerPositionIndex >= 0) {
                return GameForm.getInstance().getFirstTeam()[currentPlayerPositionIndex];
            } else {
                return null;
            }
        }
    }

    public void updateAll() {
        All all = MainWindow.getAllInstance();
        boolean useClubColors = MainWindow.getDefault().getSettings().getProperty(MainWindow.PROP_USE_CLUB_COLORS_IN_SCHEME, "0")
                .equalsIgnoreCase("1");
        if (useClubColors && all != null && all.getCurrentTeam() != null && all.getCurrentTeam() != currentTeam
                && playerImageType == PlayerImageType.Man) {
            currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            recreateManImagesWithNewColors();
        }
        setCurrentPlayerPositionByIndex(currentPlayerPositionIndex);
        fieldCanvas.redraw();
    }

    private void recreateManImagesWithNewColors() {
        if (currentTeam == null || playerImageType != PlayerImageType.Man) {
            return;
        }

        String imgPath = "/com/fa13/build/resources/images/player_man.png";
        String imgPathH = "/com/fa13/build/resources/images/player_man_hl.png";

        Map<RGB, RGB> mapTransformColors = new HashMap<RGB, RGB>();
        RGB homeTop = ClubInfoView.rgbColors[currentTeam.getHomeTop() - 1];
        RGB homeBottom = ClubInfoView.rgbColors[currentTeam.getHomeBottom() - 1];
        mapTransformColors.put(ClubInfoView.rgbColors[46], homeTop);//replace white
        mapTransformColors.put(ClubInfoView.rgbColors[5], homeBottom);//replace black

        ImageData playerNormalImageData = new ImageData(this.getClass().getResourceAsStream(imgPath));
        if (playerNormalImage != null) {
            playerNormalImage.dispose();
        }
        playerNormalImage = GuiUtils.transformImageColors(playerNormalImageData, mapTransformColors);

        ImageData playerHighlightedImageData = new ImageData(this.getClass().getResourceAsStream(imgPathH));
        if (playerHighlightedImage != null) {
            playerHighlightedImage.dispose();
        }
        playerHighlightedImage = GuiUtils.transformImageColors(playerHighlightedImageData, mapTransformColors);

    }

    public void setPosX(PlayerPosition playerPosition, int x) {
        if (playerPosition == null) {
            return;
        }
        switch (schemeType) {
            case ATTACK:
                playerPosition.setAttackX(x);
                break;
            case DEFENCE:
                playerPosition.setDefenceX(x);
                break;
            case FREEKICK:
                playerPosition.setFreekickX(x);
                break;
        }
    }

    public void setPosY(PlayerPosition playerPosition, int y) {
        if (playerPosition == null) {
            return;
        }
        switch (schemeType) {
            case ATTACK:
                playerPosition.setAttackY(y);
                break;
            case DEFENCE:
                playerPosition.setDefenceY(y);
                break;
            case FREEKICK:
                playerPosition.setFreekickY(y);
                break;
        }
    }

    public int getPosX(PlayerPosition playerPosition) {
        switch (schemeType) {
            case ATTACK:
                return playerPosition.getAttackX();
            case DEFENCE:
                return playerPosition.getDefenceX();
            case FREEKICK:
                return playerPosition.getFreekickX();
        }
        return 0;
    }

    public int getPosY(PlayerPosition playerPosition) {
        switch (schemeType) {
            case ATTACK:
                return playerPosition.getAttackY();
            case DEFENCE:
                return playerPosition.getDefenceY();
            case FREEKICK:
                return playerPosition.getFreekickY();
        }
        return 0;
    }

    public void onGameFormUpdate() {
        isSchemeUpdate = false;
        schemeControlPanel.getSchemeCombo().select(GameForm.getInstance().getSelectedSchemeIndex());
        isSchemeUpdate = true;
        updateAll();

        TeamTactics startTactics = GameForm.getInstance().getStartTactics();
        if (startTactics != null) {
            GuiUtils.selectComboItem(teamTacticCombo, startTactics.getTeamTactic().getLocalizedString());
            GuiUtils.selectComboItem(teamStyleCombo, startTactics.getTeamStyle().getLocalizedString());
            GuiUtils.selectComboItem(teamHardnessCombo, startTactics.getTeamHardness().getLocalizedString());
        }
        GuiUtils.selectComboItem(coachSettingsCombo, GameForm.getInstance().getTeamCoachSettings().getLocalizedString());
        defenceTimeSpinner.setSelection(GameForm.getInstance().getDefenceTime());
        defenceMaxSpinner.setSelection(GameForm.getInstance().getDefenceMax());
        defenceMinSpinner.setSelection(GameForm.getInstance().getDefenceMin());
        attackTimeSpinner.setSelection(GameForm.getInstance().getAttackTime());
        attackMaxSpinner.setSelection(GameForm.getInstance().getAttackMax());
        attackMinSpinner.setSelection(GameForm.getInstance().getAttackMin());
        //ticketPriceSpinner.setSelection(GameForm.getInstance().getPrice());
    }

    public void updateMessages() {
        parentInstance.setRedraw(false);

        //deleteScheme.setText(MainWindow.getMessage("DeleteSchemeButton"));
        attackButton.setText(MainWindow.getMessage("AttackViewButton"));
        defenceButton.setText(MainWindow.getMessage("DefenceViewButton"));
        freeKickButton.setText(MainWindow.getMessage("FreeKickViewButton"));
        mainPlayersButton.setText(MainWindow.getMessage("MainPlayersPositionsView"));
        substitutionPlayersButton.setText(MainWindow.getMessage("SubstitutionPlayersPositionsView"));
        playerGroup.setText(MainWindow.getMessage("PlayerSettingsGroupLabel"));

        String labelsNames[] = { MainWindow.getMessage("PositionPlayerX"), MainWindow.getMessage("PositionPlayerY"),
                MainWindow.getMessage("PositionLongShot"), MainWindow.getMessage("PositionActOnFreekick"),
                MainWindow.getMessage("PositionFantasista"), MainWindow.getMessage("PositionDispatcher"),
                MainWindow.getMessage("PositionPersonalDefence"), MainWindow.getMessage("PositionPressing"),
                MainWindow.getMessage("PositionKeepBall"), MainWindow.getMessage("PositionDefenderAttack"),
                MainWindow.getMessage("PositionPassingStyle"), MainWindow.getMessage("PositionHardness") };
        Label labels[] = { playerXLabel, playerYLabel, longShotLabel, actOnFreekickLabel, fantasistaLabel, dispatcherLabel, personalDefenceLabel,
                pressingLabel, keepBallLabel, defenderAttackLabel, passingStyleLabel, hardnessLabel };

        for (int i = 0; i < labelsNames.length; i++) {
            labels[i].setText(labelsNames[i]);
        }

        resetPassingStyleCombo();
        resetHardnessCombo();

        teamTacticsGroup.setText(MainWindow.getMessage("teamTacticsGroup"));

        coachSettingsLabel.setText(MainWindow.getMessage("coachSettingsLabel"));
        teamTacticLabel.setText(MainWindow.getMessage("teamTacticLabel"));
        teamHardnessLabel.setText(MainWindow.getMessage("teamHardnessLabel"));
        teamStyleLabel.setText(MainWindow.getMessage("teamStyleLabel"));

        resetTeamHardnessCombo();
        resetTeamTacticCombo();
        resetTeamStyleCombo();
        resetCoachSettingsCombo();

        attackGroup.setText(MainWindow.getMessage("attackGroup"));
        defenceGroup.setText(MainWindow.getMessage("defenceGroup"));
        //ticketPriceGroup.setText(MainWindow.getMessage("ticketPriceGroup"));
        //ticketLabel.setText(MainWindow.getMessage("ticketLabel"));
        timeLabel.setText(MainWindow.getMessage("timeLabel"));
        resultLabel.setText(MainWindow.getMessage("resultLabel"));
        timeLabelD.setText(MainWindow.getMessage("timeLabel"));
        resultLabelD.setText(MainWindow.getMessage("resultLabel"));

        playerGroup.layout();
        attackGroup.layout();
        defenceGroup.layout();
        teamTacticsGroup.layout();

        playerGroup.layout();

        Point spinnerSize = personalDefenceSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        FormData data = new FormData();
        data.right = new FormAttachment(100, -10);
        // data.left = new FormAttachment(100, -10
        // - passingStyleCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(passingStyleButton, 3 - spinnerSize.y);
        passingStyleCombo.setLayoutData(data);
        data = new FormData();
        data.right = new FormAttachment(100, -10);
        // data.left = new FormAttachment(100, -10
        // - hardnessCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(hardnessButton, 3 - spinnerSize.y);
        hardnessCombo.setLayoutData(data);

        int scrollWidth = rightPanelScrl.getVerticalBar().getSize().x;
        /*
                data = new FormData();
                data.left = new FormAttachment(deleteScheme, -deleteScheme.computeSize(
                        SWT.DEFAULT, SWT.DEFAULT).x);
                data.right = new FormAttachment(100, -scrollWidth);
                data.top = new FormAttachment(0, 0);
                deleteScheme.setLayoutData(data);
        */
        Button buttons[] = { playerXButton, playerYButton, longShotButton, actOnFreekickButton, fantasistaButton, dispatcherButton,
                personalDefenceButton, pressingButton, keepBallButton, defenderAttackButton, passingStyleButton, hardnessButton };

        int rightPanelWidth = 0;

        data = new FormData();
        data.left = new FormAttachment(buttons[0], 10);
        data.right = new FormAttachment(labels[0], labels[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        data.top = new FormAttachment(buttons[0], -buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        labels[0].setLayoutData(data);

        for (int i = 1; i < labels.length; i++) {
            data = new FormData();
            data.left = new FormAttachment(buttons[i], 10);
            data.right = new FormAttachment(labels[i], labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
            int dy1 = buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y - labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
            data.top = new FormAttachment(buttons[i], dy1 / 2 - buttons[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
            labels[i].setLayoutData(data);
        }

        int maxLabelsWidth = 0;
        for (int i = 0; i < labels.length - 2; i++) {
            maxLabelsWidth = Math.max(maxLabelsWidth, labels[i].computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        }

        int groupWidth = 0;
        groupWidth = 10;
        groupWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        groupWidth += 6;
        groupWidth += maxLabelsWidth;
        groupWidth += 6;
        groupWidth += spinnerSize.x;
        groupWidth += 10;
        groupWidth = Math.max(playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).x, groupWidth);

        maxLabelsWidth = 6;
        maxLabelsWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += passingStyleCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += passingStyleLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        groupWidth = Math.max(groupWidth, maxLabelsWidth);

        maxLabelsWidth = 6;
        maxLabelsWidth += buttons[0].computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += hardnessCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        maxLabelsWidth += hardnessLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        maxLabelsWidth += 10;
        groupWidth = Math.max(groupWidth, maxLabelsWidth);

        data = new FormData();
        data.left = new FormAttachment(0, 10);
        data.right = new FormAttachment(playerGroup, groupWidth);
        data.top = new FormAttachment(playerNameLabel, 6);
        data.bottom = new FormAttachment(playerGroup, playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).y + spinnerSize.y / 2);
        playerGroup.setLayoutData(data);

        rightPanelWidth = 2 * scrollWidth;
        rightPanelWidth += attackButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth += defenceButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth += freeKickButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        rightPanelWidth = Math.max(rightPanelWidth, playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
        rightPanelWidth = Math.max(rightPanelWidth, groupWidth + 20);

        data = (FormData) rightPanelScrl.getLayoutData();
        data.left = new FormAttachment(rightPanelScrl, -rightPanelWidth);

        int scrollHeight = 0;
        //scrollHeight = schemeCombo.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight = schemeControlPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight += 10;
        scrollHeight += freeKickButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight += 6;
        scrollHeight += playerNameLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
        scrollHeight += 6;
        scrollHeight += playerGroup.computeSize(SWT.DEFAULT, SWT.DEFAULT).y + spinnerSize.y / 2;

        rightPanelScrl.setMinWidth(rightPanelWidth);
        rightPanelScrl.setMinHeight(scrollHeight);

        playerGroup.layout();
        rightPanel.layout();
        parentInstance.layout();
        parentInstance.setRedraw(true);
    }

    public void updatePassword(String password) {
    }

    @Override
    public void redraw() {
    }

    private boolean needUpdate = true;
    private PlayerImageType playerImageType;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
    }

    @Override
    public TabItem asTabItem() {
        return null;
    }

    private TeamTactics createStartTacticsFromSelectedValues() {
        return new TeamTactics((Tactic) teamTacticCombo.getData(teamTacticCombo.getText()), (Hardness) teamHardnessCombo.getData(teamHardnessCombo
                .getText()), (Style) teamStyleCombo.getData(teamStyleCombo.getText()), -20, 20);
    }

    private void resetPassingStyleCombo() {
        //to survive localization change
        int wasSelected = passingStyleCombo.getSelectionIndex();
        passingStyleCombo.removeAll();
        for (PassingStyle passingStyle : PassingStyle.values()) {
            String localizedString = passingStyle.getLocalizedString();
            passingStyleCombo.add(localizedString);
            passingStyleCombo.setData(localizedString, passingStyle);
        }

        passingStyleCombo.select(wasSelected == -1 ? 0 : wasSelected);
    }

    private void resetHardnessCombo() {
        //to survive localization change
        int wasSelected = hardnessCombo.getSelectionIndex();
        hardnessCombo.removeAll();
        for (Hardness hardness : Hardness.values()) {
            String localizedString = hardness.getLocalizedString();
            hardnessCombo.add(localizedString);
            hardnessCombo.setData(localizedString, hardness);
        }
        hardnessCombo.select(wasSelected == -1 ? 0 : wasSelected);
    }

    private void resetTeamTacticCombo() {
        teamTacticCombo.removeAll();
        for (Tactic tactic : Tactic.values()) {
            String localizedString = tactic.getLocalizedString();
            teamTacticCombo.add(localizedString);
            teamTacticCombo.setData(localizedString, tactic);
        }
        teamTacticCombo.select(1);
    }

    private void resetTeamStyleCombo() {
        teamStyleCombo.removeAll();
        for (Style style : Style.values()) {
            String localizedString = style.getLocalizedString();
            teamStyleCombo.add(localizedString);
            teamStyleCombo.setData(localizedString, style);
        }
        teamStyleCombo.select(0);
        teamStyleCombo.setVisibleItemCount(Style.values().length);
    }

    private void resetTeamHardnessCombo() {
        teamHardnessCombo.removeAll();
        for (Hardness hardness : Hardness.values()) {
            String localizedString = hardness.getLocalizedString();
            teamHardnessCombo.add(localizedString);
            teamHardnessCombo.setData(localizedString, hardness);
        }
        teamHardnessCombo.select(0);
    }

    private void resetCoachSettingsCombo() {
        coachSettingsCombo.removeAll();
        for (CoachSettings coachSettings : CoachSettings.values()) {
            String localizedString = coachSettings.getLocalizedString();
            coachSettingsCombo.add(localizedString);
            coachSettingsCombo.setData(localizedString, coachSettings);
        }
        coachSettingsCombo.select(0);
    }

    private void redrawCurrentPlayer() {
        PlayerPosition pos = getCurrentPlayerPosition();
        if (pos == null) {
            return;
        }
        int w2 = playerNormalImage.getBounds().width / 2;
        int h2 = playerNormalImage.getBounds().height / 2;
        int x = mapVirtToRealX(getPosX(pos));
        int y = mapVirtToRealY(getPosY(pos));
        fieldCanvas.redraw(x, y - h2, w2 * 8, h2 * 2, true);
    }

    private class CheckButtonListener implements SelectionListener {
        int type;

        public CheckButtonListener(int type) {
            this.type = type;
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            PlayerPosition pos = getCurrentPlayerPosition();
            if (pos != null) {
                boolean select = ((Button) e.widget).getSelection();
                switch (type) {
                    case 0:
                        pos.setLongShot(select);
                        break;
                    case 1:
                        if (pos.getActOnFreekick() == FreekickAction.FK_DEFAULT) {
                            pos.setActOnFreekick(FreekickAction.FK_YES);
                            actOnFreekickButton.setSelection(true);
                            actOnFreekickButton.setGrayed(false);
                        } else if (pos.getActOnFreekick() == FreekickAction.FK_YES) {
                            pos.setActOnFreekick(FreekickAction.FK_NO);
                            actOnFreekickButton.setSelection(false);
                            actOnFreekickButton.setGrayed(false);
                        } else if (pos.getActOnFreekick() == FreekickAction.FK_NO) {
                            pos.setActOnFreekick(FreekickAction.FK_DEFAULT);
                            actOnFreekickButton.setSelection(true);
                            actOnFreekickButton.setGrayed(true);
                        }

                        break;
                    case 2:
                        pos.setFantasista(select);
                        break;
                    case 3:
                        pos.setDispatcher(select);
                        break;
                    case 4:
                        personalDefenceSpinner.setEnabled(select);
                        if (select) {
                            personalDefenceSpinner.setSelection(pos.getPersonalDefence());
                        } else {
                            personalDefenceSpinner.setSelection(1);
                            pos.setPersonalDefence(-1);
                        }
                        break;
                    case 5:
                        pos.setPressing(select);
                        break;
                    case 6:
                        pos.setKeepBall(select);
                        break;
                    case 7:
                        pos.setDefenderAttack(select);
                        break;
                    default:
                        break;
                }
                redrawCurrentPlayer();
            }
        }

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

    }

    private class PassingStyleComboListener implements ModifyListener {

        public void modifyText(ModifyEvent e) {
            PlayerPosition pos = getCurrentPlayerPosition();
            if (pos != null) {
                int selected = passingStyleCombo.getSelectionIndex();
                switch (selected) {
                    case 0:
                        pos.setPassingStyle(PassingStyle.PS_BOTH);
                        break;
                    case 1:
                        pos.setPassingStyle(PassingStyle.PS_EXACT);
                        break;
                    case 2:
                        pos.setPassingStyle(PassingStyle.PS_FORWARD);
                        break;
                }
                redrawCurrentPlayer();
            }
        }
    }

    private class HardnessComboListener implements ModifyListener {

        public void modifyText(ModifyEvent e) {
            PlayerPosition pos = getCurrentPlayerPosition();
            if (pos != null) {
                int selected = hardnessCombo.getSelectionIndex();
                switch (selected) {
                    case 0:
                        pos.setHardness(Hardness.DEFAULT);
                        break;
                    case 1:
                        pos.setHardness(Hardness.SOFT);
                        break;
                    case 2:
                        pos.setHardness(Hardness.NORMAL);
                        break;
                    case 3:
                        pos.setHardness(Hardness.HARD);
                        break;
                    case 4:
                        pos.setHardness(Hardness.MAXI_HARD);
                        break;
                }
                redrawCurrentPlayer();
            }
        }
    }

    private class ViewButtonListener implements SelectionListener {

        int index;
        Button btns[];
        SchemeViewType type;

        public ViewButtonListener(SchemeViewType type, Button[] btns, int index) {
            this.type = type;
            this.btns = btns;
            if (index < 0) {
                index = 0;
            }
            if (index > btns.length) {
                index = btns.length - 1;
            }
            this.index = index;
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            if (btns[index].getSelection()) {
                for (int i = 0; i < btns.length; i++) {
                    if (i != index) {
                        btns[i].setSelection(false);
                    } else {
                        schemeType = type;
                        updateAll();
                    }
                }
            }
            //never called
            //else {
            //    btns[index].setSelection(true);
            //}
        }

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

    }

    private String playerSettingsAsShortString(PlayerPosition position) {
        StringBuilder sb = new StringBuilder();
        if (position.isLongShot()) {
            sb.append(MainWindow.getMessage("PositionLongShotShort"));
        }
        if (position.isFantasista()) {
            sb.append(MainWindow.getMessage("PositionFantasistaShort"));
        }
        if (position.isDispatcher()) {
            sb.append(MainWindow.getMessage("PositionDispatcherShort"));
        }
        if (position.getPersonalDefence() > 0) {
            sb.append(MainWindow.getMessage("PositionPersonalDefenceShort"));
            sb.append(position.getPersonalDefence());
        }
        if (position.isPressing()) {
            sb.append(MainWindow.getMessage("PositionPressingShort"));
        }
        if (position.isKeepBall()) {
            sb.append(MainWindow.getMessage("PositionKeepBallShort"));
        }
        if (position.isDefenderAttack()) {
            sb.append(MainWindow.getMessage("PositionDefenderAttackShort"));
        }

        if (position.getPassingStyle() == PassingStyle.PS_EXACT) {
            sb.append(MainWindow.getMessage("PositionPassStyleExactShort"));

        } else if (position.getPassingStyle() == PassingStyle.PS_FORWARD) {
            sb.append(MainWindow.getMessage("PositionPassStyleDirectShort"));
        }

        String hardness;
        switch (position.getHardness()) {
            case SOFT:
                hardness = "!";
                break;
            case NORMAL:
                hardness = "!!";
                break;
            case HARD:
                hardness = "!!!";
                break;
            case MAXI_HARD:
                hardness = "!!!!";
                break;
            case DEFAULT:
                hardness = "";
                break;
            default:
                hardness = "";
        }
        sb.append(hardness);

        return sb.toString();
    }

    private class FieldPaintListener implements PaintListener {
        public void paintControl(PaintEvent e) {
            //System.out.println("paintControl...");
            GC gc = e.gc;
            Rectangle rect = fieldImage.getBounds();
            //System.out.println("e.width,heigth"+e.width+","+e.height);
            //System.out.println("rect.Width,Height="+rect.width+","+rect.height);
            //TODO remove magic numbers
            gc.drawImage(fieldImage, (rect.width - 698) / 2, (rect.height - 460) / 2, 698, 460, 0, 0, 698, 460);

            PlayerPosition positions[] = GameForm.getInstance().getFirstTeam();
            int playerImageWidth2 = playerNormalImage.getBounds().width / 2;
            int playerImageHeight2 = playerNormalImage.getBounds().height / 2;
            //draw all field players except gk and selected player
            for (int i = 1; i < positions.length; i++) {
                int x = mapVirtToRealX(getPosX(positions[i]));
                int y = mapVirtToRealY(getPosY(positions[i]));
                if (playerCoordSubstitutionMode || i != currentPlayerPositionIndex) {
                    gc.drawImage(playerNormalImage, -playerImageWidth2 + x, -playerImageHeight2 + y);
                }
            }

            for (int i = 1; i < positions.length; i++) {
                int x = mapVirtToRealX(getPosX(positions[i]));
                int y = mapVirtToRealY(getPosY(positions[i]));
                String numberStr = String.valueOf(positions[i].getNumber());
                if (positions[i].getNumber() > 0 && (playerCoordSubstitutionMode || i != currentPlayerPositionIndex)) {
                    
                    Player player = null;
                    if (MainWindow.getAllInstance() != null) {
                        player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(positions[i].getNumber());
                    }
                    
                    gc.setFont(playerNumberFont);
                    Color oldColor = gc.getForeground();
                    gc.setForeground(playerNumberColor);
                    Point extent = gc.stringExtent(numberStr);
                    int offsetX = useManIcon ? -playerImageWidth2 - extent.x - 2 : -extent.x / 2;
                    int offsetY = -playerImageHeight2 + (playerImageHeight2 - extent.y) / 2 + 1;
                    gc.drawString(numberStr, x + offsetX, y + offsetY, true);
                    gc.setForeground(oldColor);
                    
                    oldColor = gc.getForeground();
                    boolean isPosFined = player == null || positions[i].getAmplua() != player.getPosition();
                    gc.setForeground(isPosFined ? Display.getDefault().getSystemColor(SWT.COLOR_RED) : oldColor);
                    gc.setFont(playerLabelsFont);
                    String amplua = positions[i].getAmplua().toString();
                    extent = gc.stringExtent(amplua);
                    gc.drawString(amplua, -playerImageWidth2 + x - extent.x, y + playerImageHeight2 - extent.y, true);
                    gc.setForeground(oldColor);
                    
                    String specialRoles = playerSettingsAsShortString(positions[i]);
                    extent = gc.stringExtent(specialRoles);
                    gc.drawString(specialRoles, playerImageWidth2 + x, y + playerImageHeight2 - extent.y, true);

                    if (player != null) {
                        gc.setFont(playerLabelsFont);
                        Point extent2 = gc.stringExtent(player.getName());
                        int xx = extent2.x / 2;
                        gc.drawString(player.getName(), x - xx, playerImageHeight2 + y, true);
                        if (xx > playerImageWidth2) {
                            maxWidth = Math.max(maxWidth, xx - playerImageWidth2);
                        }
                        maxHeight = Math.max(maxHeight, extent2.y);
                    }
                    maxWidth = Math.max(maxWidth, extent.x);
                    maxHeight = Math.max(maxHeight, extent.y);
                }
            }
            //draw selected playerPosition
            if (playerCoordSubstitutionMode || (currentPlayerPositionIndex > 0 && currentPlayerPositionIndex < positions.length)) {
                PlayerPosition playerPos;
                if (playerCoordSubstitutionMode) {
                    playerPos = GameForm.getInstance().getActiveCoordSubtition();
                } else {
                    playerPos = positions[currentPlayerPositionIndex];
                }
                int x = mapVirtToRealX(getPosX(playerPos));
                int y = mapVirtToRealY(getPosY(playerPos));
                //int i = currentPlayerPositionIndex;
                if (playerCoordSubstitutionMode) {
                    gc.drawImage(playerCoordSubstImage, -playerImageWidth2 + x, -playerImageHeight2 + y);
                } else {
                    gc.drawImage(playerHighlightedImage, -playerImageWidth2 + x, -playerImageHeight2 + y);
                }
                String numberStr = String.valueOf(playerPos.getNumber());
                if (playerPos.getNumber() > 0) {
                    Player player = null;
                    if (MainWindow.getAllInstance() != null) {
                        player = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(playerPos.getNumber());
                    }

                    gc.setFont(playerNumberFont);
                    Color oldColor = gc.getForeground();
                    gc.setForeground(playerNumberColor);
                    Point extent = gc.stringExtent(numberStr);
                    //int offsetX = -extent.x / 2;
                    int offsetX = useManIcon ? -playerImageWidth2 - extent.x - 2 : -extent.x / 2;
                    int offsetY = -playerImageHeight2 + (playerImageHeight2 - extent.y) / 2 + 1;
                    gc.drawString(numberStr, x + offsetX, y + offsetY, true);
                    gc.setForeground(oldColor);

                    oldColor = gc.getForeground();
                    boolean isPosFined = player == null || playerPos.getAmplua() != player.getPosition();
                    gc.setForeground(isPosFined ? Display.getDefault().getSystemColor(SWT.COLOR_RED) : oldColor);
                    gc.setFont(playerLabelsFont);
                    String amplua = playerPos.getAmplua().toString();
                    extent = gc.stringExtent(amplua);
                    gc.drawString(amplua, -playerImageWidth2 + x - extent.x, y + playerImageHeight2 - extent.y, true);
                    gc.setForeground(oldColor);
                    
                    String specialRoles = playerSettingsAsShortString(playerPos);
                    extent = gc.stringExtent(specialRoles);
                    gc.drawString(specialRoles, playerImageWidth2 + x, y + playerImageHeight2 - extent.y, true);

                    if (player != null) {
                        gc.setFont(playerLabelsFont);
                        Point extent2 = gc.stringExtent(player.getName());
                        gc.drawString(player.getName(), x - extent2.x / 2, playerImageHeight2 + y, true);
                        maxWidth = Math.max(maxWidth, extent2.x / 2);
                        maxHeight = Math.max(maxHeight, extent2.y);
                    }
                    maxWidth = Math.max(maxWidth, extent.x);
                    maxHeight = Math.max(maxHeight, extent.y);
                }
            }

            rect = new Rectangle(0, 0, 698, 460);
            Rectangle client = fieldCanvas.getClientArea();
            int marginWidth = client.width - rect.width;
            if (marginWidth > 0) {
                gc.fillRectangle(rect.width, 0, marginWidth, client.height);
            }
            int marginHeight = client.height - rect.height;
            if (marginHeight > 0) {
                gc.fillRectangle(0, rect.height, client.width, marginHeight);
            }
            
            fieldCanvas.setSize(698, 460);
            fieldCanvas.update();
        }
    }

    private static class FieldCanvasMouseListener implements Listener {
        int playerPositionIndex = -1;
        PlayerPosition positions[];
        int offsetX;
        int offsetY;
        int oldX;
        int oldY;
        int width;
        int height;
        boolean mouseButtonPressed = false;
        boolean playerCatched = false;

        private GameSchemeEditView view;

        private FieldCanvasMouseListener(GameSchemeEditView view) {
            this.view = view;
        }

        public void handleEvent(Event event) {
            switch (event.type) {
                case SWT.MouseDown:
                    view.fieldCanvas.setFocus();
                    onMouseDown(event);
                    break;
                case SWT.MouseMove:
                    onMouseMove(event);
                    break;
                case SWT.MouseUp:
                    onMouseUp(event);
                    break;
            }

        }

        private void onMouseMove(Event event) {
            if (!mouseButtonPressed || !playerCatched) {
                return;
            }
            if (playerPositionIndex >= 0 || view.playerCoordSubstitutionMode) {
                view.setCurrentPlayerPositionByIndex(view.currentPlayerPositionIndex);
                positions = GameForm.getInstance().getFirstTeam();
                if (!view.playerCoordSubstitutionMode && playerPositionIndex > positions.length) {
                    return;
                }
                PlayerPosition position = view.getCurrentPlayerPosition();

                view.fieldCanvas.redraw(mapVirtToRealX(view.getPosX(position)) - 2 * width - view.maxWidth, mapVirtToRealY(view.getPosY(position))
                        - 2 * height, width * 4 + view.maxWidth, height * 4 + view.maxHeight + 5, true);

                view.setPosX(position, mapRealToVirtX(event.x - offsetX));
                view.setPosY(position, mapRealToVirtY(event.y - offsetY));

                view.playerXSpinner.setSelection(mapRealToVirtX(event.x - offsetX));
                view.playerYSpinner.setSelection(mapRealToVirtY(event.y - offsetY));

                view.fieldCanvas.redraw(mapVirtToRealX(view.getPosX(position)) - 2 * width - view.maxWidth, mapVirtToRealY(view.getPosY(position))
                        - 2 * height, width * 4 + view.maxWidth, height * 4 + view.maxHeight + 5, true);

                oldX = event.x;
                oldY = event.y;
            }

        }

        private void onMouseDown(Event event) {

            mouseButtonPressed = true;
            playerCatched = false;
            Image playerImage;
            if (view.playerCoordSubstitutionMode) {
                playerImage = view.playerCoordSubstImage;
            } else {
                playerImage = view.playerNormalImage;
            }
            width = playerImage.getBounds().width / 2;
            height = playerImage.getBounds().height / 2;
            offsetX = playerImage.getBounds().width / 2;
            offsetY = playerImage.getBounds().height / 2;
            PlayerPosition playerPosition = null;
            if (!view.playerCoordSubstitutionMode) {
                playerPositionIndex = -1;
                positions = GameForm.getInstance().getFirstTeam();
                for (int i = 1; i < positions.length; i++) {
                    int x = mapVirtToRealX(view.getPosX(positions[i]));
                    int y = mapVirtToRealY(view.getPosY(positions[i]));
                    if (x + offsetX >= event.x && x - offsetX <= event.x && y + offsetY >= event.y && y - offsetY <= event.y) {
                        playerPositionIndex = i;
                    }
                }
                if (playerPositionIndex != -1) {
                    playerPosition = positions[playerPositionIndex];
                }
            } else {
                playerPosition = GameForm.getInstance().getActiveCoordSubtition();
            }

            if (playerPosition == null) {
                return;
            }

            if (playerPositionIndex >= 0 || view.playerCoordSubstitutionMode) {
                int x = mapVirtToRealX(view.getPosX(playerPosition));
                int y = mapVirtToRealY(view.getPosY(playerPosition));

                if (view.playerCoordSubstitutionMode
                        && !(x + offsetX >= event.x && x - offsetX <= event.x && y + offsetY >= event.y && y - offsetY <= event.y)) {
                    return;//did't point on subst player
                }
                playerCatched = true;
                offsetX = event.x - x;
                offsetY = event.y - y;
                view.fieldCanvas.setCursor(event.display.getSystemCursor(SWT.CURSOR_HAND));
                view.setCurrentPlayerPositionByIndex(playerPositionIndex);
                view.fieldCanvas.redraw();
            }
        }

        private void onMouseUp(Event event) {
            mouseButtonPressed = false;
            playerCatched = false;
            playerPositionIndex = -1;
            view.fieldCanvas.setCursor(event.display.getSystemCursor(SWT.CURSOR_ARROW));
            view.fieldCanvas.redraw();
            view.setCurrentPlayerPositionByIndex(view.currentPlayerPositionIndex);
        }
    }

    private static class PlayerSpinnerModifyListener implements ModifyListener {
        private GameSchemeEditView view;

        private PlayerSpinnerModifyListener(GameSchemeEditView view) {
            this.view = view;
        }

        public void modifyText(ModifyEvent e) {

            boolean isXSpinner = (e.widget == view.playerXSpinner);
            //System.out.println("modifyText..."+isXSpinner);
            //System.out.println("max H, W  = "+view.maxWidth+","+view.maxHeight);
            PlayerPosition position = view.getCurrentPlayerPosition();
            int width = view.playerNormalImage.getBounds().width;
            int height = view.playerNormalImage.getBounds().height;
            /*
                        view.fieldCanvas.redraw(mapVirtToRealX( view.getPosX(position)) - 2 * width, mapVirtToRealY(view.getPosY(position)) - 2 * height, 
                                                                width * 4 + view.maxWidth, height * 4, true);
            */

            if (isXSpinner) {
                view.setPosX(position, view.playerXSpinner.getSelection());
            } else {
                view.setPosY(position, view.playerYSpinner.getSelection());
            }
            /*
                        view.fieldCanvas.redraw(mapVirtToRealX( view.getPosX(position)) - 2 * width, mapVirtToRealY(view.getPosY(position)) - 2 * height, 
                                                                width * 4 + view.maxWidth, height * 4, true);
            */
            view.fieldCanvas.redraw(mapVirtToRealX(view.getPosX(position)) - width - view.maxWidth, mapVirtToRealY(view.getPosY(position)) - height
                    - view.maxHeight - 5, width * 2 + 2 * view.maxWidth, height * 2 + view.maxHeight + 7, true);
            // fieldCanvas.redraw();
        }
    }

    private class FieldCanvasKeyListener implements KeyListener {
        private GameSchemeEditView view;

        public FieldCanvasKeyListener(GameSchemeEditView view) {
            this.view = view;
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.keyCode) {
                case SWT.ARROW_LEFT:
                    view.playerXSpinner.setSelection(view.playerXSpinner.getSelection() - 1);
                    break;
                case SWT.ARROW_RIGHT:
                    view.playerXSpinner.setSelection(view.playerXSpinner.getSelection() + 1);
                    break;
                case SWT.ARROW_UP:
                    view.playerYSpinner.setSelection(view.playerYSpinner.getSelection() - 1);
                    break;
                case SWT.ARROW_DOWN:
                    view.playerYSpinner.setSelection(view.playerYSpinner.getSelection() + 1);
                    break;
            }
        }
    };

    private Font createFont(Composite parent, int height, int style) {
        FontData fontData = parent.getFont().getFontData()[0];
        fontData.setHeight(height);
        fontData.setStyle(style);
        return new Font(parent.getDisplay(), fontData);
    }

    public void setPlayerCoordSubstitionMode(boolean flag) {
        playerCoordSubstitutionMode = flag;
        coachSettingsScrolledPanel.setVisible(!flag);
    }

    @Override
    public void dispose() {
        fieldCanvas.dispose();
        defenseImage.dispose();
        attackImage.dispose();
        freeKickImage.dispose();
        playerNormalImage.dispose();
        playerHighlightedImage.dispose();
        playerCoordSubstImage.dispose();
        fieldImage.dispose();
        repairImage.dispose();
        playerNumberFont.dispose();
        playerNameFont.dispose();
        playerNumberColor.dispose();
        playerLabelsFont.dispose();
    }

    public enum PlayerImageType {
        Standard, Small, Man
    }

    @Override
    public void load(Properties props) {
        //load coach settings
        String data = props.getProperty(MainWindow.PROP_TACTICS_COACH_SETTINGS, "");
        if (!data.isEmpty()) {
            GuiUtils.selectComboItem(coachSettingsCombo, data);
        }

        //load tactics settings
        data = props.getProperty(MainWindow.PROP_TACTICS_TACTICS, "");
        if (!data.isEmpty()) {
            GuiUtils.selectComboItem(teamTacticCombo, data);
        }

        //load agression
        data = props.getProperty(MainWindow.PROP_TACTICS_AGRESSION, "");
        if (!data.isEmpty()) {
            GuiUtils.selectComboItem(teamHardnessCombo, data);
        }

        //load style
        data = props.getProperty(MainWindow.PROP_TACTICS_STYLE, "");
        if (!data.isEmpty()) {
            GuiUtils.selectComboItem(teamStyleCombo, data);
        }

        //load all attack/defence setttings
        data = props.getProperty(MainWindow.PROP_ALL_ATTACK_DEFENCE, "");
        if (!data.isEmpty()) {
            try {
                String[] ss = data.split(";");
                //attack time
                int val = Integer.valueOf(ss[0]);
                attackTimeSpinner.setValues(val, 0, 120, 0, 1, 5);
                //attack min score difference
                val = Integer.valueOf(ss[1]);
                attackMinSpinner.setValues(val, -20, 20, 0, 1, 2);
                //attack max score difference
                val = Integer.valueOf(ss[2]);
                attackMaxSpinner.setValues(val, -20, 20, 0, 1, 2);

                //defence time
                val = Integer.valueOf(ss[3]);
                defenceTimeSpinner.setValues(val, 0, 120, 0, 1, 5);
                //defence min score difference
                val = Integer.valueOf(ss[4]);
                defenceMinSpinner.setValues(val, -20, 20, 0, 1, 2);
                //defence max score difference
                val = Integer.valueOf(ss[5]);
                defenceMaxSpinner.setValues(val, -20, 20, 0, 1, 2);

            } catch (Exception ex) {
                GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"),
                        MainWindow.getMessage("CantReadAllAttackDefenceSettings"));
            }
        }
    }

    @Override
    public void store(Properties props) {
        props.put(MainWindow.PROP_TACTICS_COACH_SETTINGS, GameForm.getInstance().getTeamCoachSettings().getLocalizedString());

        props.put(MainWindow.PROP_TACTICS_TACTICS, GameForm.getInstance().getStartTactics().getTeamTactic().getLocalizedString());

        props.put(MainWindow.PROP_TACTICS_AGRESSION, GameForm.getInstance().getStartTactics().getTeamHardness().getLocalizedString());

        props.put(MainWindow.PROP_TACTICS_STYLE, GameForm.getInstance().getStartTactics().getTeamStyle().getLocalizedString());

        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(GameForm.getInstance().getAttackTime())).append(";");
        sb.append(String.valueOf(GameForm.getInstance().getAttackMin())).append(";");
        sb.append(String.valueOf(GameForm.getInstance().getAttackMax())).append(";");

        sb.append(String.valueOf(GameForm.getInstance().getDefenceTime())).append(";");
        sb.append(String.valueOf(GameForm.getInstance().getDefenceMin())).append(";");
        sb.append(String.valueOf(GameForm.getInstance().getDefenceMax()));
        props.put(MainWindow.PROP_ALL_ATTACK_DEFENCE, sb.toString());

    }

}