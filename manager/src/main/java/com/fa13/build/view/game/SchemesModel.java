package com.fa13.build.view.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.fa13.build.controller.io.SchemeReader;
import com.fa13.build.controller.io.SchemeWriter;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.view.MainWindow;

public class SchemesModel {

    public static final String SCHEME_FILE_NAME = "Schemes.txt";

    private Map<String, PlayerPosition[]> schemesTemplates;

    private boolean changed = false;

    private Shell shell;

    public SchemesModel(Shell shell) {
        this.shell = shell;
    }

    public void addScheme(String schemeName, PlayerPosition[] schemeData) {
        //make copy of scheme data
        PlayerPosition[] schemeDataCopy = new PlayerPosition[schemeData.length];
        GameEditView.copyScheme(schemeDataCopy, schemeData);
        schemesTemplates.put(schemeName, schemeDataCopy);
        fireEvent(SchemeChangeType.ADDED, schemeName, schemeDataCopy);
        setChanged(true);
        selectScheme(schemeName);
    }

    public void removeScheme(String schemeName) {
        PlayerPosition[] schemeData = schemesTemplates.remove(schemeName);
        fireEvent(SchemeChangeType.REMOVED, schemeName, schemeData);
        setChanged(true);
    }

    public void selectScheme(String schemeName) {
        PlayerPosition[] schemeData = schemesTemplates.get(schemeName);
        fireEvent(SchemeChangeType.SELECTED, schemeName, schemeData);
    }

    public void fireEvent(SchemeChangeType eventType, String schemeName, PlayerPosition[] schemeData) {
        for (ISchemesModelListener l : listeners) {
            l.handeSchemeChanged(eventType, schemeName, schemeData);
        }
    }

    public Map<String, PlayerPosition[]> getSchemesTemplates() {
        return schemesTemplates;
    }

    public void setSchemesTemplates(Map<String, PlayerPosition[]> schemesTemplates) {
        if (this.schemesTemplates != schemesTemplates) {
            this.schemesTemplates = schemesTemplates;
            fireEvent(SchemeChangeType.RELOAD, null, null);
        }
    }

    private List<ISchemesModelListener> listeners = new ArrayList<ISchemesModelListener>();

    public void addChangeListener(ISchemesModelListener l) {
        listeners.add(l);
    }

    public void removeChangeListener(ISchemesModelListener l) {
        listeners.remove(l);
    }

    public enum SchemeChangeType {
        UPDATED, REMOVED, ADDED, RELOAD, SELECTED
    }

    public interface ISchemesModelListener {

        public void handeSchemeChanged(SchemeChangeType eventType, String schemeName, PlayerPosition[] schemeData);

    }

    public void loadSchemes() {

        if (getSchemesTemplates() == null) {

            File schemesFile = new File(SCHEME_FILE_NAME);

            if (schemesFile.exists()) {
                try {
                    setSchemesTemplates(SchemeReader.read(SCHEME_FILE_NAME));
                } catch (Exception ex) {
                    MessageBox errMsg = new MessageBox(shell, SWT.ERROR);
                    errMsg.setText(MainWindow.getMessage("error"));
                    errMsg.setMessage(MainWindow.getMessage("schemesReadError"));
                    errMsg.open();
                }
            } else {

                resetSchemes();

            }
        }
    }

    public static final String DEFAULT_SCHEME_RESOURCE = "/com/fa13/build/resources/game/DefaultSchemes.txt";

    public void resetSchemes() {
        try {
            setSchemesTemplates(SchemeReader.readScheme(new BufferedReader(
                    new InputStreamReader(this.getClass().getResourceAsStream(DEFAULT_SCHEME_RESOURCE), SchemeWriter.UTF8_CHARSET))));
            setChanged(true);
        } catch (Exception ex) {
            MessageBox errMsg = new MessageBox(shell, SWT.ERROR);
            errMsg.setText(MainWindow.getMessage("error"));
            errMsg.setMessage(MainWindow.getMessage("schemesReadError"));
            errMsg.open();
        }
    }

    public void saveSchemes() throws IOException {
        saveSchemes(SCHEME_FILE_NAME);
    }

    public void saveSchemes(String fileName) throws IOException {
        SchemeWriter.write(fileName, schemesTemplates);
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public void selectFirstScheme() {
        String firstSchemeName;
        if (schemesTemplates.get("4-4-2") != null) {
            firstSchemeName = "4-4-2";
        } else {
            firstSchemeName = schemesTemplates.keySet().iterator().next();
        }
        //System.out.println("firstSchemeName= "+firstSchemeName);
        selectScheme(firstSchemeName);
    }

}