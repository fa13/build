package com.fa13.build.view.game;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.api.IDisposable;
import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.Hardness;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.PlayerSubstitution;
import com.fa13.build.model.game.Style;
import com.fa13.build.model.game.SubstitutionPreferences;
import com.fa13.build.model.game.Tactic;
import com.fa13.build.model.game.TeamTactics;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.SpecialRoleEditor;
import com.fa13.build.view.UIItem;

public class GameChangeEditView implements UIItem, IDisposable {

    private static final String PROP_BY_COORD_EDITOR = "byCoordEditor";
    Composite leftPanel;
    ScrolledComposite rightPanelScrl;
    Composite rightPanel;
    Table substitutionTable;
    Table firstTeamTable;
    Table benchTeamTable;
    TableEditor editor;
    TableEditor matchPlayersTableEditor;
    static Color tableColors[] = null;
    static Font tableFonts[] = null;
    Button changeByStrengthButton;
    Button changeByPositionButton;
    Button changeByFutureSubButton;
    SpecialRoleEditor specialRoleEditor;
    SpecialRoleEditor specialRoleEditorSmall;

    Group teamTacticChangesGroup;
    Label teamTacticChangesDifferenceLabel;
    Label teamTacticChangesTacticLabel;
    Label teamTacticChangesHardnessLabel;
    Label teamTacticChangesStyleLabel;
    HalftimeTacticChangesRow[] halftimeTacticChangesRows;

    List<Listener> coordSubstitutionListeners = new ArrayList<Listener>();
    PlayerSubstitution activePlayerSubstitution;
    private Composite parentInstance;
    private int substSelectedRow = -1;
    private int substSelectedColumn = -1;
    
    
    public GameChangeEditView(Composite parent) {

        parentInstance = parent;

        if (tableColors == null) {
            tableColors = new Color[3];
            tableColors[0] = new Color(parent.getDisplay(), 0, 0, 0);
            tableColors[1] = new Color(parent.getDisplay(), 0, 0, 0);
            tableColors[2] = new Color(parent.getDisplay(), 50, 50, 50);
        }
        if (tableFonts == null) {
            tableFonts = new Font[3];
            FontData fontData = parent.getFont().getFontData()[0];
            tableFonts[0] = new Font(parent.getDisplay(), fontData);
            fontData.setStyle(SWT.BOLD);
            tableFonts[1] = new Font(parent.getDisplay(), fontData);
            fontData.setStyle(SWT.ITALIC | SWT.BOLD);
            tableFonts[2] = new Font(parent.getDisplay(), fontData);
        }

        parent.setLayout(new FormLayout());

        createLeftPanel(parent, GameForm.getInstance().getSubstitutions());
        createRightPanel(parent);

        updateAll();
    }

    private void createLeftPanel(Composite parent, PlayerSubstitution[] substitutions) {
        leftPanel = new Composite(parent, SWT.NONE);
        GridLayout gridLayout = new GridLayout(1, true);
        gridLayout.horizontalSpacing = 0;
        gridLayout.verticalSpacing = 0;
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        leftPanel.setLayout(gridLayout);

        substitutionTable = new Table(leftPanel, SWT.BORDER | SWT.FULL_SELECTION);
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.horizontalSpan = 2;
        substitutionTable.setLayoutData(gridData);
        substitutionTable.setHeaderVisible(true);
        substitutionTable.setLinesVisible(true);
        substitutionTable.setHeaderVisible(true);

        addColumn(substitutionTable, MainWindow.getMessage("ChangeTime"));
        addColumn(substitutionTable, MainWindow.getMessage("MinDifference"));
        addColumn(substitutionTable, MainWindow.getMessage("MaxDifference"));
        addColumn(substitutionTable, MainWindow.getMessage("PlayerNumber"));
        addColumn(substitutionTable, MainWindow.getMessage("OutgoingPlayer"), SWT.LEFT);
        addColumn(substitutionTable, MainWindow.getMessage("PlayerNumber"));
        addColumn(substitutionTable, MainWindow.getMessage("IngoingPlayer"), SWT.LEFT);
        addColumn(substitutionTable, MainWindow.getMessage("IngoingPosition"));
        addColumn(substitutionTable, MainWindow.getMessage("Standards"));
        addColumn(substitutionTable, MainWindow.getMessage("changeByCoord"));
        if (SystemUtils.IS_OS_LINUX) {
            //SWT-GTK bugfix - workaround with StackOverflowError with last column (TableEditor.setEditor was the matter)
            addColumn(substitutionTable, "");
        }

        GuiUtils.autoAdjustTableColumnsWidth(substitutionTable);

        editor = new TableEditor(substitutionTable);
        editor.horizontalAlignment = SWT.LEFT;
        editor.grabHorizontal = true;
        editor.minimumWidth = 35;

        substitutionTable.addListener(SWT.MouseDown, new SubstitutionTableEditListener());
        substitutionTable.addMenuDetectListener(new SubstitutionTableMouseListener());
        substitutionTable.setItemCount(substitutions.length);
        
        substitutionTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseDoubleClick(MouseEvent e) {
                onTableDoubleClick(substitutionTable, substitutionTable.getSelection());
            }
        });
        
        for (int i = 0; i < substitutions.length; i++) {
            substitutions[i] = new PlayerSubstitution();
        }

        Composite substitutionPreferencesPanel = new Composite(leftPanel, SWT.NONE);
        substitutionPreferencesPanel.setLayout(new GridLayout(3, false));

        changeByStrengthButton = new Button(substitutionPreferencesPanel, SWT.RADIO);
        gridData = new GridData(SWT.BEGINNING, SWT.END, false, false);
        changeByStrengthButton.setLayoutData(gridData);
        changeByStrengthButton.setText(MainWindow.getMessage("ForcedChangeByStrength"));
        changeByStrengthButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                if (changeByStrengthButton.getSelection()) {
                    GameForm.getInstance().setSubstitutionPreferences(SubstitutionPreferences.BY_STRENGTH);
                }
            }
        });

        changeByPositionButton = new Button(substitutionPreferencesPanel, SWT.RADIO);
        gridData = new GridData(SWT.BEGINNING, SWT.END, false, false);
        changeByPositionButton.setLayoutData(gridData);
        changeByPositionButton.setText(MainWindow.getMessage("ForcedChangeByPosition"));
        changeByPositionButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                if (changeByPositionButton.getSelection()) {
                    GameForm.getInstance().setSubstitutionPreferences(SubstitutionPreferences.BY_POSITION);
                }
            }
        });

        changeByFutureSubButton = new Button(substitutionPreferencesPanel, SWT.RADIO);
        gridData = new GridData(SWT.BEGINNING, SWT.END, false, false);
        changeByFutureSubButton.setLayoutData(gridData);
        changeByFutureSubButton.setText(MainWindow.getMessage("ForcedChangeByFutureSubstitution"));
        changeByFutureSubButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e) {
                if (changeByFutureSubButton.getSelection()) {
                    GameForm.getInstance().setSubstitutionPreferences(SubstitutionPreferences.BY_FUTURE_SUB);
                }
            }

        });

        Composite teamTacticChangesPanel = new Composite(leftPanel, SWT.NONE);
        FormLayout layout = new FormLayout();
        layout.marginLeft = 10;
        layout.marginRight = 10;
        layout.marginTop = 0;
        layout.marginBottom = 10;
        teamTacticChangesPanel.setLayout(layout);

        teamTacticChangesGroup = new Group(teamTacticChangesPanel, SWT.NONE);
        teamTacticChangesGroup.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        teamTacticChangesGroup.setLayout(new GridLayout(6, false));
        teamTacticChangesGroup.setText(MainWindow.getMessage("HalftimeTacticChangesLabel"));

        teamTacticChangesDifferenceLabel = new Label(teamTacticChangesGroup, SWT.NONE);
        teamTacticChangesDifferenceLabel.setText(MainWindow.getMessage("resultLabel"));
        teamTacticChangesDifferenceLabel.setLayoutData(new GridData(SWT.LEFT, GridData.BEGINNING, false, false, 2, 0));
        teamTacticChangesTacticLabel = new Label(teamTacticChangesGroup, SWT.NONE);
        teamTacticChangesTacticLabel.setText(MainWindow.getMessage("teamTacticLabel"));
        teamTacticChangesHardnessLabel = new Label(teamTacticChangesGroup, SWT.NONE);
        teamTacticChangesHardnessLabel.setText(MainWindow.getMessage("teamHardnessLabel"));
        teamTacticChangesStyleLabel = new Label(teamTacticChangesGroup, SWT.NONE);
        teamTacticChangesStyleLabel.setText(MainWindow.getMessage("teamStyleLabel"));
        Label fakeLabel = new Label(teamTacticChangesGroup, SWT.NONE);

        halftimeTacticChangesRows = new HalftimeTacticChangesRow[5];
        for (int i = 0; i < halftimeTacticChangesRows.length; i++) {
            halftimeTacticChangesRows[i] = new HalftimeTacticChangesRow(teamTacticChangesGroup, i);
        }
    }

    private void createRightPanel(Composite parent) {
        rightPanelScrl = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
        rightPanelScrl.setExpandHorizontal(false);
        rightPanelScrl.setExpandVertical(true);

        FormData rightPanelScrlData = new FormData();
        rightPanelScrlData.left = new FormAttachment(100, 0);
        rightPanelScrlData.top = new FormAttachment(0, 0);
        rightPanelScrlData.bottom = new FormAttachment(100, 0);
        rightPanelScrlData.right = new FormAttachment(100, 0);
        rightPanelScrl.setLayoutData(rightPanelScrlData);

        rightPanel = new Composite(rightPanelScrl, SWT.NONE);
        rightPanelScrl.setContent(rightPanel);

        FormData leftPanelData = new FormData();
        leftPanelData.left = new FormAttachment(0, 0);
        leftPanelData.top = new FormAttachment(0, 0);
        leftPanelData.bottom = new FormAttachment(100, 0);
        leftPanelData.right = new FormAttachment(rightPanelScrl, -5);
        leftPanel.setLayoutData(leftPanelData);

        rightPanel.setLayoutData(new FormData());
        rightPanel.setLayout(new FormLayout());

        firstTeamTable = createFormPlayersTable(rightPanel, 11, null);
        addColumn(firstTeamTable, MainWindow.getMessage("PlayerNumber"));
        addColumn(firstTeamTable, MainWindow.getMessage("PlayerPosition"));
        addColumn(firstTeamTable, MainWindow.getMessage("PlayerName"), SWT.LEFT);
        addColumn(firstTeamTable, MainWindow.getMessage("Standards"));

        benchTeamTable = createFormPlayersTable(rightPanel, 7, firstTeamTable);
        addColumn(benchTeamTable, MainWindow.getMessage("PlayerNumber"));
        addColumn(benchTeamTable, MainWindow.getMessage("PlayerPosition"));
        addColumn(benchTeamTable, MainWindow.getMessage("PlayerName"), SWT.LEFT);
        addColumn(benchTeamTable, "   ");

        matchPlayersTableEditor = new TableEditor(firstTeamTable);
        matchPlayersTableEditor.horizontalAlignment = SWT.LEFT;
        matchPlayersTableEditor.grabHorizontal = true;
        matchPlayersTableEditor.minimumWidth = 35;

        firstTeamTable.addListener(SWT.MouseDown, new MatchPlayersTableEditListener());
        firstTeamTable.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                benchTeamTable.deselectAll();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        
        benchTeamTable.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                firstTeamTable.deselectAll();
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        
        setDragDropSource(firstTeamTable);
        setDragDropSource(benchTeamTable);
        setDragDropTargetSubstTable(substitutionTable);
        setDragDropTargetFirstTeamTable(firstTeamTable);
        setDragDropTargetBenchTeamTable(benchTeamTable);

        specialRoleEditor = new SpecialRoleEditor(firstTeamTable.getDisplay(), firstTeamTable.getShell(), null, null, true);

        specialRoleEditorSmall = new SpecialRoleEditor(substitutionTable.getDisplay(), substitutionTable.getShell(), null, null, false);
    }

    private Table createFormPlayersTable(Composite parentComponent, int rowsCount, Control topComponent) {
        Table table = new Table(parentComponent, SWT.BORDER | SWT.FULL_SELECTION);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.setItemCount(rowsCount);

        FormData layoutData = new FormData();
        if (topComponent != null) {
            layoutData.top = new FormAttachment(topComponent, 0);
        } else {
            layoutData.top = new FormAttachment(0, 0);
        }
        layoutData.right = new FormAttachment(100, 0);
        layoutData.left = new FormAttachment(0, 0);
        table.setLayoutData(layoutData);
        return table;
    }

    private void addColumn(Table table, String title) {
        addColumn(table, title, SWT.CENTER);
    }

    private void addColumn(Table table, String title, int style) {
        TableColumn column = new TableColumn(table, style);
        column.setText(title);
        column.setResizable(true);
    }

    private void onTableDoubleClick(Table source, TableItem[] ti) {
        //System.out.println("table double click..." + (ti == null ? "empty" : ti[0].getText()));
        if (source == substitutionTable) {
            if (ti == null || ti.length == 0 || ti[0] == null) {
                return;
            }
            
            if (substSelectedColumn != 4 && substSelectedColumn != 6) {
                return;
            }
            boolean playerOut = (substSelectedColumn == 4);

            //check what is selected(firstteam/bench)
            TableItem[] selection = firstTeamTable.getSelection();
            TableItem[] selection2 = benchTeamTable.getSelection();
            int playerNumber = -1;
            try {
                if (selection != null && selection.length != 0) {
                  //from first team
                    playerNumber = Integer.valueOf(selection[0].getText(0));
                } else if (selection2 != null && selection2.length != 0) {
                    //from bench
                    playerNumber = Integer.valueOf(selection2[0].getText(0));
                } else {
                    return;
                }
            } catch (NumberFormatException ex) {
                return;
            }
            PlayerSubstitution sub = GameForm.getInstance().getSubstitutions()[substSelectedRow];
            
            if (playerOut) {
                sub.setSubstitutedPlayer(playerNumber);
            } else {
                sub.setNumber(playerNumber);
            }
            redraw();
        }
    }
    
    public void updateAll() {
        for (TableItem item : substitutionTable.getItems()) {
            TableEditor te = (TableEditor) item.getData(PROP_BY_COORD_EDITOR);
            if (te != null && te.getEditor() != null) {
                te.getEditor().dispose();
            }
        }
        substitutionTable.removeAll();
        substitutionTable.clearAll();
        substitutionTable.setItemCount(GameForm.getInstance().getSubstitutions().length);
        for (int i = 0; i < halftimeTacticChangesRows.length; i++) {
            halftimeTacticChangesRows[i].updateRow(GameForm.getInstance().getHalftimeChanges()[i]);
        }
        int val;
        if (GameForm.getInstance().getSubstitutionPreferences() == SubstitutionPreferences.BY_STRENGTH) {
            val = SubstitutionPreferences.BY_STRENGTH.getFormValue();
        } else if (GameForm.getInstance().getSubstitutionPreferences() == SubstitutionPreferences.BY_POSITION) {
            val = SubstitutionPreferences.BY_POSITION.getFormValue();
        } else {
            val = SubstitutionPreferences.BY_FUTURE_SUB.getFormValue();
        }
        changeByStrengthButton.setSelection(val == SubstitutionPreferences.BY_STRENGTH.getFormValue());
        changeByPositionButton.setSelection(val == SubstitutionPreferences.BY_POSITION.getFormValue());
        changeByFutureSubButton.setSelection(val == SubstitutionPreferences.BY_FUTURE_SUB.getFormValue());
        redraw();
    }

    private int getPlayerTypeGameForm(int playerNumber) {
        int res = 0;
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        for (PlayerPosition playerPosition : playerPositions) {
            if (playerPosition.getNumber() == playerNumber) {
                res = 1;
            }
        }
        int playerSubstitutions[] = GameForm.getInstance().getBench();
        for (int playerSubstitution : playerSubstitutions) {
            if (playerSubstitution == playerNumber) {
                res = 2;
            }
        }
        return res;
    }

    public void applyPlayerCoordChange() {
        if (activePlayerSubstitution != null) {
            activePlayerSubstitution.copyFrom(GameForm.getInstance().getActiveCoordSubtition());
        }
        //update position field of applied substitution
        PlayerSubstitution[] playerSubsts = GameForm.getInstance().getSubstitutions();
        if (playerSubsts.length != 0) {
            for (int i = 0; i < playerSubsts.length; i++) {
                final PlayerSubstitution subst = playerSubsts[i];
                if (subst == activePlayerSubstitution) {
                    final TableItem item = substitutionTable.getItem(i);
                    updateSubstitutionPositionField(subst, item);
                    break;
                }
            }
        }

    }

    public void redraw() {
        if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getCurrentTeam() != null) {
            substitutionTable.setRedraw(false);
            PlayerSubstitution[] playerSubsts = GameForm.getInstance().getSubstitutions();
            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (playerSubsts.length != 0) {
                for (int i = 0; i < playerSubsts.length; i++) {
                    final PlayerSubstitution subst = playerSubsts[i];
                    final TableItem item = substitutionTable.getItem(i);
                    if (subst != null && !subst.isEmpty()) {
                        String time = (subst.getTime() == -1) ? "" : String.valueOf(subst.getTime());
                        item.setText(0, time);
                        item.setText(1, String.valueOf(subst.getMinDifference()));
                        item.setText(2, String.valueOf(subst.getMaxDifference()));
                        int number = subst.getSubstitutedPlayer();
                        int type = getPlayerTypeGameForm(number);
                        Player player = curTeam.getPlayerByNumber(number);
                        String st = (player != null) ? String.valueOf(player.getNumber()) : "";
                        item.setText(3, st);
                        item.setForeground(4, tableColors[type]);
                        item.setFont(4, tableFonts[type]);
                        st = (player != null) ? player.getName() : "";
                        item.setText(4, st);
                        number = subst.getNumber();
                        type = getPlayerTypeGameForm(number);
                        player = curTeam.getPlayerByNumber(number);
                        st = (player != null) ? String.valueOf(player.getNumber()) : "";
                        item.setText(5, st);
                        item.setForeground(6, tableColors[type]);
                        item.setFont(6, tableFonts[type]);
                        st = (player != null) ? player.getName() : "";
                        item.setText(6, st);
                        updateSubstitutionPositionField(subst, item);
                        item.setText(8, printSRflags(subst));
                        TableEditor oldTableEditor = (TableEditor) item.getData(PROP_BY_COORD_EDITOR);
                        if (oldTableEditor != null && oldTableEditor.getEditor() != null) {
                            oldTableEditor.getEditor().dispose();
                        }
                        //add change by coord widget
                        if (subst.getSubstitutedPlayer() > 0 && subst.getNumber() > 0) {
                            final ChangeByCoordWidget byCoordEditor = new ChangeByCoordWidget(substitutionTable, SWT.NONE);

                            byCoordEditor.setChecked(subst.isUseCoordinates());
                            //don't allow coord subst if players are not set!
                            //byCoordEditor.setVisible(subst.getSubstitutedPlayer() > 0 && subst.getNumber() > 0);

                            byCoordEditor.addActionListener(new Listener() {

                                @Override
                                public void handleEvent(Event event) {
                                    if (byCoordEditor.isChecked()) {
                                        //send copy of substitution
                                        GameForm.getInstance().getActiveCoordSubtition().copyFrom(subst);
                                        //to return changes then apply clicked
                                        activePlayerSubstitution = subst;
                                        for (Listener l : coordSubstitutionListeners) {
                                            l.handleEvent(event);
                                        }
                                    }
                                }
                            });
                            byCoordEditor.addCheckedListener(new Listener() {

                                @Override
                                public void handleEvent(Event event) {
                                    subst.setUseCoordinates(byCoordEditor.isChecked());
                                    if (byCoordEditor.isChecked()) {

                                        MessageBox dialog = new MessageBox(parentInstance.getShell(), SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
                                        dialog.setText(MainWindow.getMessage("confirmation"));
                                        dialog.setMessage(MainWindow.getMessage("copyDataFromSubstitutedPlayerQuestion"));
                                        byCoordEditor.update();
                                        if (dialog.open() == SWT.OK) {
                                            //copy coords from substituted player
                                            for (PlayerPosition position : GameForm.getInstance().getFirstTeam()) {
                                                if (position.getNumber() == subst.getSubstitutedPlayer()) {

                                                    subst.setAttackX(position.getAttackX());
                                                    subst.setAttackY(position.getAttackY());
                                                    subst.setDefenceX(position.getDefenceX());
                                                    subst.setDefenceY(position.getDefenceY());
                                                    subst.setFreekickX(position.getFreekickX());
                                                    subst.setFreekickY(position.getFreekickY());
                                                    subst.setActOnFreekick(position.getActOnFreekick());
                                                    subst.setDefenderAttack(position.isDefenderAttack());
                                                    subst.setDispatcher(position.isDispatcher());
                                                    subst.setFantasista(position.isFantasista());
                                                    subst.setHardness(position.getHardness());
                                                    subst.setKeepBall(position.isKeepBall());
                                                    subst.setLongShot(position.isLongShot());
                                                    subst.setPassingStyle(position.getPassingStyle());
                                                    subst.setPressing(position.isPressing());
                                                    /* to avoid standarts reset on copy 
                                                    subst.setSpecialRole(position.getSpecialRole());
                                                    subst.setCaptainAssistant(false);
                                                    subst.setDirectFreekickAssistant(false);
                                                    subst.setIndirectFreekickAssistant(false);
                                                    subst.setLeftCornerAssistant(false);
                                                    subst.setPenaltyAssistant(false);
                                                    subst.setRightCornerAssistant(false);
                                                    */
                                                    break;

                                                }
                                            }
                                        }

                                    } else {
                                        //clear position as it is obsolete
                                        subst.setAmplua(null);
                                    }
                                    updateSubstitutionPositionField(subst, item);

                                }
                            });
                            TableEditor tableEditor;
                            byCoordEditor.pack();
                            if (oldTableEditor != null) {
                                tableEditor = oldTableEditor;
                            } else {
                                tableEditor = new TableEditor(substitutionTable);
                            }
                            if (tableEditor.getEditor() != null) {
                                tableEditor.getEditor().dispose();
                            }
                            tableEditor.grabHorizontal = tableEditor.grabVertical = true;
                            Point p = byCoordEditor.computeSize(SWT.DEFAULT, SWT.DEFAULT);
                            tableEditor.minimumHeight = p.y;
                            tableEditor.minimumWidth = p.x;
                            tableEditor.setEditor(byCoordEditor, item, 9);
                            item.setData(PROP_BY_COORD_EDITOR, tableEditor);
                        }
                    } else {
                        //substitution is empty - so clear row
                        for (int j = 0; j < substitutionTable.getColumnCount(); j++) {
                            item.setText(j, "");
                        }
                        TableEditor te = (TableEditor) item.getData(PROP_BY_COORD_EDITOR);
                        if (te != null && te.getEditor() != null) {
                            te.getEditor().dispose();
                        }
                    }
                }
            }

            //            for (int i = 0; i < substitutionTable.getColumnCount(); i++) {
            //                substitutionTable.getColumn(i).pack();
            //            }
            GuiUtils.autoAdjustTableColumnsWidth(substitutionTable);
            substitutionTable.getParent().layout();
            substitutionTable.setRedraw(true);
        }
        redrawForm();
    }

    private void updateSubstitutionPositionField(PlayerSubstitution subst, TableItem item) {
        PlayerAmplua amplua = subst.getAmplua();
        if (subst.isUseCoordinates()) {
            item.setFont(7, tableFonts[1]);
            item.setText(7, amplua != null ? "*" + amplua.toString() : "");
        } else {
            //position field is obsolete if byCood subst is not used
            subst.setAmplua(null);
            item.setText(7, "");
            //item.setText(7, amplua != null ? amplua.toString() : "");
        }
    }

    private void setDragDropSource(final Table table) {
        Transfer[] types = new Transfer[] { TextTransfer.getInstance() };
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        final DragSource source = new DragSource(table, operations);
        source.setTransfer(types);
        source.addDragListener(new DragSourceListener() {

            public void dragStart(DragSourceEvent event) {
                TableItem items[] = table.getSelection();
                TableItem item = items[0];
                Player player = (Player) item.getData();
                if (player == null) {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {

                TableItem items[] = table.getSelection();
                TableItem item = items[0];
                Player player = (Player) item.getData();
                if (player != null) {
                    event.data = String.valueOf(player.getNumber());
                } else {
                    event.doit = false;
                }
            }

            public void dragFinished(DragSourceEvent event) {
            }
        });
    }

    private void setDragDropTargetSubstTable(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        Transfer types[] = new Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    if (event.item instanceof TableItem) {
                        PlayerSubstitution[] substs = GameForm.getInstance().getSubstitutions();
                        if (event.data != null) {
                            String st = (String) event.data;
                            int playerNumber = Integer.valueOf(st);
                            for (int i = 0; i < table.getItemCount(); i++) {
                                TableItem targetItem = table.getItem(i);
                                Rectangle rect = targetItem.getDisplay().map(table, null, targetItem.getBounds(3));
                                Rectangle rect2 = targetItem.getDisplay().map(table, null, targetItem.getBounds(4));
                                if (rect.contains(event.x, event.y) || rect2.contains(event.x, event.y)) {
                                    substs[i].setSubstitutedPlayer(playerNumber);
                                    redraw();
                                }
                                rect = targetItem.getDisplay().map(table, null, targetItem.getBounds(5));
                                rect2 = targetItem.getDisplay().map(table, null, targetItem.getBounds(6));
                                if (rect.contains(event.x, event.y) || rect2.contains(event.x, event.y)) {
                                    substs[i].setNumber(playerNumber);
                                    redraw();
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void setDragDropTargetFirstTeamTable(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        Transfer types[] = new Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    if (event.item instanceof TableItem) {
                        PlayerPosition[] players = GameForm.getInstance().getFirstTeam();
                        if (event.data != null) {
                            String st = (String) event.data;
                            int i = (Integer) event.item.getData("number");
                            int j = 0;
                            players[i].setNumber(Integer.valueOf(st));
                            j = firstTeamTable.getSelectionIndex();
                            if (j != -1) {
                                players[j].setNumber(0);
                            }
                            j = benchTeamTable.getSelectionIndex();
                            if (j != -1) {
                                GameForm.getInstance().getBench()[j] = 0;
                            }
                            redraw();
                        }
                    }
                }
            }
        });
    }

    private void setDragDropTargetBenchTeamTable(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        Transfer types[] = new Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    if (event.item instanceof TableItem) {
                        int[] players = GameForm.getInstance().getBench();
                        if (event.data != null) {
                            String st = (String) event.data;
                            int i = (Integer) event.item.getData("number");
                            int j = 0;
                            players[i] = Integer.valueOf(st);
                            j = benchTeamTable.getSelectionIndex();
                            if (j != -1) {
                                players[j] = 0;
                            }
                            j = firstTeamTable.getSelectionIndex();
                            if (j != -1) {
                                GameForm.getInstance().getFirstTeam()[j].setNumber(0);
                            }
                            redraw();
                        }
                    }
                }
            }
        });
    }
/*
    private void setPlayer(TableItem item, String playerData) {
        String playerItems[] = playerData.split("/");
        Integer number = (Integer) item.getData("number");
        int playerNumber = Integer.valueOf(playerItems[0]);
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        for (int i = 0; i < playerPositions.length; i++) {
            if (playerPositions[i].getNumber() == playerNumber) {
                playerPositions[i].reset();
            }
        }
        int playerSubstitutions[] = GameForm.getInstance().getBench();
        for (int i = 0; i < playerSubstitutions.length; i++) {
            if (playerSubstitutions[i] == playerNumber) {
                playerSubstitutions[i] = 0;
            }
        }
        if (number < playerPositions.length && number >= 0) {
            playerPositions[number].setNumber(playerNumber);
        }
        redraw();
    }

    private void setPlayerSubstitution(TableItem item, String playerData) {
        String playerItems[] = playerData.split("/");
        Integer number = (Integer) item.getData("number");
        int playerNumber = Integer.valueOf(playerItems[0]);
        PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
        for (PlayerPosition playerPosition : playerPositions) {
            if (playerPosition.getNumber() == playerNumber) {
                playerPosition.reset();
            }
        }
        int playerSubstitutions[] = GameForm.getInstance().getBench();
        for (int i = 0; i < playerSubstitutions.length; i++) {
            if (playerSubstitutions[i] == playerNumber) {
                playerSubstitutions[i] = 0;
            }
        }
        if (number < playerSubstitutions.length && number >= 0) {
            playerSubstitutions[number] = playerNumber;
        }
        redraw();
    }
*/
    public void redrawForm() {
        if (MainWindow.getAllInstance() != null) {
            firstTeamTable.setRedraw(false);
            benchTeamTable.setRedraw(false);
            Team curTeam = MainWindow.getAllInstance().getCurrentTeam();
            PlayerPosition playerPositions[] = GameForm.getInstance().getFirstTeam();
            int playerSubstitution[] = GameForm.getInstance().getBench();
            int itemsCount = Math.max(11, playerPositions.length);
            firstTeamTable.setItemCount(itemsCount);
            benchTeamTable.setItemCount(7);
            for (int i = 0; i < itemsCount; i++) {
                TableItem item = firstTeamTable.getItem(i);
                item.setData("number", i);
                item.setData("type", 0);
                item.setData(null);
            }
            for (int i = 0; i < benchTeamTable.getItemCount(); i++) {
                TableItem item = benchTeamTable.getItem(i);
                item.setData("number", i);
                item.setData("type", 1);
                item.setData(null);
            }

            for (int i = 0; i < firstTeamTable.getItemCount(); i++) {
                TableItem item = firstTeamTable.getItem(i);
                for (int index = 0; index < firstTeamTable.getColumnCount(); index++) {
                    item.setText(index, "");
                }
            }
            for (int i = 0; i < benchTeamTable.getItemCount(); i++) {
                TableItem item = benchTeamTable.getItem(i);
                for (int index = 0; index < benchTeamTable.getColumnCount(); index++) {
                    item.setText(index, "");
                }
            }

            for (int i = 0; i < playerPositions.length; i++) {
                TableItem item = firstTeamTable.getItem(i);
                int index = 0;
                if (playerPositions[i] != null) {
                    Player player = curTeam.getPlayerByNumber(playerPositions[i].getNumber());
                    if (player != null) {
                        item.setText(index++, String.valueOf(player.getNumber()));
                    } else {
                        index++;
                    }
                    item.setText(index++, playerPositions[i].getAmplua().toString());
                    if (player != null) {
                        item.setText(index++, player.getName());
                        item.setData(player);
                    } else {
                        index++;
                    }
                    item.setText(index++, printSRflags(playerPositions[i]));
                }
            }
            for (int i = 0; i < playerSubstitution.length; i++) {
                TableItem item = benchTeamTable.getItem(i);
                int index = 0;
                Player player = curTeam.getPlayerByNumber(playerSubstitution[i]);
                if (player != null) {
                    item.setText(index++, String.valueOf(player.getNumber()));
                    item.setText(index++, player.getPosition().toString());
                    item.setText(index++, player.getName());
                    item.setData(player);
                }
            }

            int width = 0;
            for (int i = 0; i < firstTeamTable.getColumnCount(); i++) {
                int widthColum = 0;
                firstTeamTable.getColumn(i).pack();
                int dWidth = firstTeamTable.getColumn(i).getWidth();
                widthColum = dWidth;
                benchTeamTable.getColumn(i).pack();
                dWidth = benchTeamTable.getColumn(i).getWidth();
                widthColum = Math.max(widthColum, dWidth);
                width += widthColum;
                firstTeamTable.getColumn(i).setWidth(widthColum);
                benchTeamTable.getColumn(i).setWidth(widthColum);
            }

            int heightAll = 0;
            int widthAll = 0;
            FormData data = (FormData) firstTeamTable.getLayoutData();
            width += firstTeamTable.getBorderWidth() * 2;
            data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
            data.right = new FormAttachment(firstTeamTable, width);
            int height = firstTeamTable.getItemCount() * firstTeamTable.getItemHeight();
            height += firstTeamTable.getHeaderHeight();
            height += firstTeamTable.getBorderWidth() * 3;
            data.bottom = new FormAttachment(firstTeamTable, height);
            heightAll += height;
            widthAll = width;

            data = (FormData) benchTeamTable.getLayoutData();
            width -= firstTeamTable.getBorderWidth() * 2;
            width += benchTeamTable.getBorderWidth() * 2;
            data.left = new FormAttachment(0, rightPanelScrl.getVerticalBar().getSize().x);
            data.right = new FormAttachment(benchTeamTable, width);
            height = benchTeamTable.getItemCount() * firstTeamTable.getItemHeight();
            height += benchTeamTable.getHeaderHeight();
            height += benchTeamTable.getBorderWidth() * 3;
            data.bottom = new FormAttachment(benchTeamTable, height);
            heightAll += height;
            widthAll = Math.max(widthAll, width);
            firstTeamTable.getParent().layout();

            data = (FormData) rightPanelScrl.getLayoutData();
            int scrollWidth = rightPanelScrl.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
            data.left = new FormAttachment(rightPanelScrl, -scrollWidth);
            rightPanel.setSize(widthAll, heightAll);
            rightPanelScrl.setMinWidth(widthAll);
            rightPanelScrl.setExpandHorizontal(true);
            rightPanelScrl.setMinHeight(heightAll);
            rightPanelScrl.layout();
            rightPanelScrl.getParent().layout();

            firstTeamTable.setRedraw(true);
            benchTeamTable.setRedraw(true);
        }
    }

    private class SubstitutionTableEditListener implements Listener {

        public void handleEvent(Event event) {

            Rectangle clientArea = substitutionTable.getClientArea();
            Point pt = new Point(event.x, event.y);

            if (event.button == 1) {

                int index = substitutionTable.getTopIndex();

                while (index < substitutionTable.getItemCount()) {

                    boolean visible = false;
                    final TableItem item = substitutionTable.getItem(index);

                    for (int i = 0; i < substitutionTable.getColumnCount(); i++) {

                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            final int column = i;
                            final int row = index;
                            substSelectedColumn = i;
                            substSelectedRow = index;
                            if (i < 3) {
                                final Spinner spin = new Spinner(substitutionTable, SWT.NONE);
                                Listener textListener = new Listener() {
                                    public void handleEvent(final Event e) {
                                        int val = -1;
                                        PlayerSubstitution subst;

                                        val = spin.getSelection();
                                        switch (e.type) {
                                            case SWT.FocusOut:
                                                if (val > 0 || column == 1 || column == 2) {
                                                    if (val > 150) {
                                                        val = 150;
                                                    }
                                                    item.setText(column, String.valueOf(val));
                                                    subst = GameForm.getInstance().getSubstitutions()[row];
                                                    if (column == 0) {
                                                        subst.setTime(val);
                                                    } else if (column == 1) {
                                                        subst.setMinDifference(val);
                                                    } else {
                                                        subst.setMaxDifference(val);
                                                    }
                                                    redraw();
                                                }
                                                if (val != spin.getSelection()) {
                                                    spin.setSelection(val);
                                                }
                                                spin.dispose();
                                                break;
                                            case SWT.Traverse:
                                                switch (e.detail) {
                                                    case SWT.TRAVERSE_RETURN:
                                                        if (val > 0 || column == 1 || column == 2) {
                                                            if (val > 150) {
                                                                val = 150;
                                                            }
                                                            item.setText(column, String.valueOf(val));
                                                            subst = GameForm.getInstance().getSubstitutions()[row];
                                                            if (column == 0) {
                                                                subst.setTime(val);
                                                            } else if (column == 1) {
                                                                subst.setMinDifference(val);
                                                            } else {
                                                                subst.setMaxDifference(val);
                                                            }
                                                            redraw();
                                                        }
                                                        //FALL THROUGH
                                                    case SWT.TRAVERSE_ESCAPE:
                                                        spin.dispose();
                                                        e.doit = false;
                                                }
                                                break;
                                            case SWT.Verify:
                                                /*
                                                String string = e.text;
                                                char[] chars = new char[string.length()];
                                                string.getChars(0, chars.length, chars, 0);
                                                for (int i = 0; i < chars.length; i++) {
                                                    if (!(i == 0 && chars[i] == '-') && !('0' <= chars[i] && chars[i] <= '9')) {
                                                        e.doit = false;
                                                        return;
                                                    }
                                                }
                                                */
                                        }
                                    }
                                };
                                spin.addListener(SWT.FocusOut, textListener);
                                spin.addListener(SWT.Traverse, textListener);
                                spin.addListener(SWT.Verify, textListener);
                                spin.setTextLimit(5);
                                editor.setEditor(spin, item, i);
                                //text.setText(item.getText(i));
                                int min = 0;
                                int max = 150;
                                if (i > 0) {
                                    min = -20;
                                    max = 20;
                                }
                                spin.setValues(getIntegerValue(item.getText(i)), min, max, 0, 1, 1);
                                editor.minimumWidth = spin.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
                                spin.setFocus();
                                /*
                                final Text text = new Text(substitutionTable, SWT.NONE);
                                Listener textListener = new Listener() {
                                    public void handleEvent(final Event e) {
                                        int val = -1;
                                        PlayerSubstitution subst;
                                        try {
                                            val = Integer.valueOf(text.getText());
                                        } catch (NumberFormatException nfe) {
                                            val = -1;
                                        }
                                        switch (e.type) {
                                            case SWT.FocusOut:
                                                if (val > 0 || column == 1 || column == 2) {
                                                    if (val > 150) {
                                                        val = 150;
                                                    }
                                                    item.setText(column, String.valueOf(val));
                                                    subst = GameForm.getInstance().getSubstitutions()[row];
                                                    if (column == 0) {
                                                        subst.setTime(val);
                                                    } else if (column == 1) {
                                                        subst.setMinDifference(val);
                                                    } else {
                                                        subst.setMaxDifference(val);
                                                    }
                                                    redraw();
                                                }
                                                text.dispose();
                                                break;
                                            case SWT.Traverse:
                                                switch (e.detail) {
                                                    case SWT.TRAVERSE_RETURN:
                                                        if (val > 0) {
                                                            if (val > 150) {
                                                                val = 150;
                                                            }
                                                            item.setText(column, String.valueOf(val));
                                                            subst = GameForm.getInstance().getSubstitutions()[row];
                                                            if (column == 0) {
                                                                subst.setTime(val);
                                                            } else if (column == 1) {
                                                                subst.setMinDifference(val);
                                                            } else {
                                                                subst.setMaxDifference(val);
                                                            }
                                                            redraw();
                                                        }
                                                        //FALL THROUGH
                                                    case SWT.TRAVERSE_ESCAPE:
                                                        text.dispose();
                                                        e.doit = false;
                                                }
                                                break;
                                            case SWT.Verify:
                                                String string = e.text;
                                                char[] chars = new char[string.length()];
                                                string.getChars(0, chars.length, chars, 0);
                                                for (int i = 0; i < chars.length; i++) {
                                                    if (!(i == 0 && chars[i] == '-') && !('0' <= chars[i] && chars[i] <= '9')) {
                                                        e.doit = false;
                                                        return;
                                                    }
                                                }
                                        }
                                    }
                                };
                                text.addListener(SWT.FocusOut, textListener);
                                text.addListener(SWT.Traverse, textListener);
                                text.addListener(SWT.Verify, textListener);
                                text.setTextLimit(5);
                                editor.setEditor(text, item, i);
                                text.setText(item.getText(i));
                                text.selectAll();
                                text.setFocus();
                                */
                            } else {
                                if (i == 8) {
                                    Rectangle rect1 = substitutionTable.getDisplay().map(item.getParent(), null, rect);
                                    specialRoleEditorSmall.updatePlayerPosition(GameForm.getInstance().getSubstitutions()[row]);
                                    specialRoleEditorSmall.show(new Point(rect1.x + rect1.width, rect1.y));
                                    redraw();
                                }
                            }

                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        }

    }

    private int getIntegerValue(String st) {
        if (st == null || st.isEmpty()) {
            return 0;
        }
        return Integer.valueOf(st);
    }

    /* NOT USED - BUT DON'T REMOVE IT.
        private void setSRflag(String flg, PlayerSubstitution subst, boolean val) {
            if (flg.equals(MainWindow.getMessage("SRCaptain"))) {
                subst.setCaptain(val);
            } else if (flg.equals(MainWindow.getMessage("SRDirectFreekick"))) {
                subst.setDirectFreekick(val);
            } else if (flg.equals(MainWindow.getMessage("SRIndirectFreekick"))) {
                subst.setIndirectFreekick(val);
            } else if (flg.equals(MainWindow.getMessage("SRPenalty"))) {
                subst.setPenalty(val);
            } else if (flg.equals(MainWindow.getMessage("SRLeftCorner"))) {
                subst.setLeftCorner(val);
            } else if (flg.equals(MainWindow.getMessage("SRRightCorner"))) {
                subst.setRightCorner(val);
            } else if (flg.equals(MainWindow.getMessage("Clear"))) {
                clearAllSRflags(subst);
            }
        }
    
        private boolean getSRflag(String flg, PlayerSubstitution subst) {
            if (flg.equals(MainWindow.getMessage("SRCaptain"))) {
                return subst.isCaptain();
            } else if (flg.equals(MainWindow.getMessage("SRDirectFreekick"))) {
                return subst.isDirectFreekick();
            } else if (flg.equals(MainWindow.getMessage("SRIndirectFreekick"))) {
                return subst.isIndirectFreekick();
            } else if (flg.equals(MainWindow.getMessage("SRPenalty"))) {
                return subst.isPenalty();
            } else if (flg.equals(MainWindow.getMessage("SRLeftCorner"))) {
                return subst.isLeftCorner();
            } else if (flg.equals(MainWindow.getMessage("SRRightCorner"))) {
                return subst.isRightCorner();
            }
            return false;
        }
    
        private void clearAllSRflags(PlayerSubstitution subst) {
            subst.setCaptain(false);
            subst.setDirectFreekick(false);
            subst.setIndirectFreekick(false);
            subst.setPenalty(false);
            subst.setLeftCorner(false);
            subst.setRightCorner(false);
        }
    */
    public class SubstitutionTableMouseListener implements MenuDetectListener {

        public void menuDetected(MenuDetectEvent e) {
            Menu menu = new Menu(substitutionTable);
            MenuItem item = new MenuItem(menu, SWT.NONE);
            item.setText(MainWindow.getMessage("Clear"));
            item.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    int i = substitutionTable.getSelectionIndex();
                    if (i != -1) {
                        GameForm.getInstance().getSubstitutions()[i].markEmpty();
                    }
                    redraw();

                }
            });
            MenuItem miCopy = new MenuItem(menu, SWT.NONE);
            miCopy.setText(MainWindow.getMessage("Copy"));
            miCopy.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {

                    int i = substitutionTable.getSelectionIndex();
                    if (i < 0) {
                        return;
                    }
                    PlayerSubstitution subToCopy = GameForm.getInstance().getSubstitutions()[i];
                    if (subToCopy.isEmpty()) {
                        return;
                    }
                    //find next empty
                    PlayerSubstitution foundEmptySub = null;
                    for (int j = i + 1; j < GameForm.getInstance().getSubstitutions().length; j++) {
                        PlayerSubstitution pSub = GameForm.getInstance().getSubstitutions()[j];
                        if (pSub.isEmpty()) {
                            foundEmptySub = pSub;
                            break;
                        }
                    }
                    if (foundEmptySub != null) {
                        foundEmptySub.copyFrom(subToCopy);
                    } else {
                        //reverse sub search order
                        for (int j = i - 1; j >= 0; j--) {
                            PlayerSubstitution pSub = GameForm.getInstance().getSubstitutions()[j];
                            if (pSub.isEmpty()) {
                                foundEmptySub = pSub;
                                break;
                            }
                        }
                        if (foundEmptySub != null) {
                            foundEmptySub.copyFrom(subToCopy);
                        } 
                    }

                    redraw();
                }
            });
            menu.setLocation(e.x, e.y);
            menu.setVisible(true);
        }

    }

    public void onGameFormUpdate() {
        updateAll();
    }

    public void updatePassword(String password) {
        // TODO Auto-generated method stub

    }

    private String printSRflags(PlayerPosition subst) {
        StringBuilder st = new StringBuilder();
        if (subst.isCaptain()) {
            st.append(MainWindow.getMessage("SRCaptainShort"));
        }
        if (subst.isCaptainAssistant()) {
            st.append(MainWindow.getMessage("SRCaptainShort").toLowerCase());
        }
        if (subst.isDirectFreekick()) {
            st.append(MainWindow.getMessage("SRDirectFreekickShort"));
        }
        if (subst.isDirectFreekickAssistant()) {
            st.append(MainWindow.getMessage("SRDirectFreekickShort").toLowerCase());
        }
        if (subst.isIndirectFreekick()) {
            st.append(MainWindow.getMessage("SRIndirectFreekickShort"));
        }
        if (subst.isIndirectFreekickAssistant()) {
            st.append(MainWindow.getMessage("SRIndirectFreekickShort").toLowerCase());
        }
        if (subst.isPenalty()) {
            st.append(MainWindow.getMessage("SRPenaltyShort"));
        }
        if (subst.isPenaltyAssistant()) {
            st.append(MainWindow.getMessage("SRPenaltyShort").toLowerCase());
        }
        if (subst.isLeftCorner()) {
            st.append(MainWindow.getMessage("SRLeftCornerShort"));
        }
        if (subst.isLeftCornerAssistant()) {
            st.append(MainWindow.getMessage("SRLeftCornerShort").toLowerCase());
        }
        if (subst.isRightCorner()) {
            st.append(MainWindow.getMessage("SRRightCornerShort"));
        }
        if (subst.isRightCornerAssistant()) {
            st.append(MainWindow.getMessage("SRRightCornerShort").toLowerCase());
        }
        if (subst.getPenaltyOrder() > 0) {
            st.append(" ").append(subst.getPenaltyOrder());
        }
        return st.toString();
    }

    private class MatchPlayersTableEditListener implements Listener {

        public void handleEvent(Event event) {
            Rectangle clientArea = firstTeamTable.getClientArea();
            Point pt = new Point(event.x, event.y);
            if (event.button == 1) {
                int index = firstTeamTable.getTopIndex();
                while (index < firstTeamTable.getItemCount()) {
                    boolean visible = false;
                    final TableItem item = firstTeamTable.getItem(index);
                    for (int i = 0; i < firstTeamTable.getColumnCount(); i++) {
                        Rectangle rect = item.getBounds(i);
                        if (rect.contains(pt)) {
                            final int column = i;
                            final int row = index;
                            final PlayerPosition playerPos = GameForm.getInstance().getFirstTeam()[row];
                            if (column == 3) {
                                Rectangle rect1 = firstTeamTable.getDisplay().map(item.getParent(), null, rect);
                                specialRoleEditor.updatePlayerPosition(playerPos);
                                specialRoleEditor.show(new Point(rect1.x + rect1.width, rect1.y));
                                redraw();
                            }

                            return;
                        }
                        if (!visible && rect.intersects(clientArea)) {
                            visible = true;
                        }
                    }
                    if (!visible) {
                        return;
                    }
                    index++;
                }
            }
        }

    }

    public void updateDaysOfRest(int daysOfRest) {
        // TODO Auto-generated method stub

    }

    public void updateMessages() {
        substitutionTable.getColumn(0).setText(MainWindow.getMessage("ChangeTime"));
        substitutionTable.getColumn(1).setText(MainWindow.getMessage("MinDifference"));
        substitutionTable.getColumn(2).setText(MainWindow.getMessage("MaxDifference"));
        substitutionTable.getColumn(3).setText(MainWindow.getMessage("PlayerNumber"));
        substitutionTable.getColumn(4).setText(MainWindow.getMessage("OutgoingPlayer"));
        substitutionTable.getColumn(5).setText(MainWindow.getMessage("PlayerNumber"));
        substitutionTable.getColumn(6).setText(MainWindow.getMessage("IngoingPlayer"));
        substitutionTable.getColumn(7).setText(MainWindow.getMessage("IngoingPosition"));
        substitutionTable.getColumn(8).setText(MainWindow.getMessage("Standards"));

        changeByStrengthButton.setText(MainWindow.getMessage("ForcedChangeByStrength"));
        changeByPositionButton.setText(MainWindow.getMessage("ForcedChangeByPosition"));
        changeByFutureSubButton.setText(MainWindow.getMessage("ForcedChangeByFutureSubstitution"));
        
        firstTeamTable.getColumn(0).setText(MainWindow.getMessage("PlayerNumber"));
        firstTeamTable.getColumn(1).setText(MainWindow.getMessage("PlayerPosition"));
        firstTeamTable.getColumn(2).setText(MainWindow.getMessage("PlayerName"));
        firstTeamTable.getColumn(3).setText(MainWindow.getMessage("Standards"));

        benchTeamTable.getColumn(0).setText(MainWindow.getMessage("PlayerNumber"));
        benchTeamTable.getColumn(1).setText(MainWindow.getMessage("PlayerPosition"));
        benchTeamTable.getColumn(2).setText(MainWindow.getMessage("PlayerName"));

        teamTacticChangesGroup.setText(MainWindow.getMessage("HalftimeTacticChangesLabel"));
        teamTacticChangesDifferenceLabel.setText(MainWindow.getMessage("resultLabel"));
        teamTacticChangesTacticLabel.setText(MainWindow.getMessage("teamTacticLabel"));
        teamTacticChangesHardnessLabel.setText(MainWindow.getMessage("teamHardnessLabel"));
        teamTacticChangesStyleLabel.setText(MainWindow.getMessage("teamStyleLabel"));

        specialRoleEditor.updateMessages();
        specialRoleEditorSmall.updateMessages();

        redraw();
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
    }

    @Override
    public TabItem asTabItem() {
        return null;
    }

    private static class HalftimeTacticChangesRow {
        private Spinner minSpinner;
        private Spinner maxSpinner;
        private Combo teamTacticCombo;
        private Combo teamHardnessCombo;
        private Combo teamStyleCombo;
        private Button resetButton;

        public HalftimeTacticChangesRow(Composite parent, final int rowIndex) {
            minSpinner = createScoreSpinner(parent);
            minSpinner.addModifyListener(new ValueModifyListener(rowIndex, new Runnable() {
                @Override
                public void run() {
                    GameForm.getInstance().getHalftimeChanges()[rowIndex].setMinimumDifference(minSpinner.getSelection());
                }
            }));

            maxSpinner = createScoreSpinner(parent);
            maxSpinner.addModifyListener(new ValueModifyListener(rowIndex, new Runnable() {
                @Override
                public void run() {
                    GameForm.getInstance().getHalftimeChanges()[rowIndex].setMaximumDifference(maxSpinner.getSelection());
                }
            }));

            teamTacticCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
            if (!SystemUtils.IS_OS_LINUX) {
                teamTacticCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            }
            resetTeamTacticCombo();
            teamTacticCombo.addModifyListener(new ValueModifyListener(rowIndex, new Runnable() {
                @Override
                public void run() {
                    Tactic value = (Tactic) teamTacticCombo.getData(teamTacticCombo.getText());
                    GameForm.getInstance().getHalftimeChanges()[rowIndex].setTeamTactic(value);
                }
            }));

            teamHardnessCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
            if (!SystemUtils.IS_OS_LINUX) {
                teamHardnessCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            }
            resetTeamHardnessCombo();
            teamHardnessCombo.addModifyListener(new ValueModifyListener(rowIndex, new Runnable() {
                @Override
                public void run() {
                    Hardness value = (Hardness) teamHardnessCombo.getData(teamHardnessCombo.getText());
                    GameForm.getInstance().getHalftimeChanges()[rowIndex].setTeamHardness(value);
                }
            }));

            teamStyleCombo = new Combo(parent, SWT.DROP_DOWN | SWT.READ_ONLY);
            if (!SystemUtils.IS_OS_LINUX) {
                teamStyleCombo.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            }
            resetTeamStyleCombo();
            teamStyleCombo.addModifyListener(new ValueModifyListener(rowIndex, new Runnable() {
                @Override
                public void run() {
                    Style value = (Style) teamStyleCombo.getData(teamStyleCombo.getText());
                    GameForm.getInstance().getHalftimeChanges()[rowIndex].setTeamStyle(value);
                }
            }));

            resetButton = new Button(parent, SWT.PUSH);
            resetButton.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
            resetButton.addSelectionListener(new SelectionListener() {

                @Override
                public void widgetSelected(SelectionEvent e) {

                    resetRow();

                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                    widgetSelected(e);
                }
            });

        }

        public void updateRow(TeamTactics newTeamTactics) {
            if (newTeamTactics != null) {
                minSpinner.setSelection(newTeamTactics.getMinimumDifference());
                maxSpinner.setSelection(newTeamTactics.getMaximumDifference());
                Tactic tactic = newTeamTactics.getTeamTactic();
                if (tactic == null) {
                    tactic = Tactic.BALANCE;
                }
                selectComboItem(teamTacticCombo, tactic.getLocalizedString());
                Hardness hardness = newTeamTactics.getTeamHardness();
                if (hardness == null) {
                    hardness = Hardness.NORMAL;
                }
                selectComboItem(teamHardnessCombo, hardness.getLocalizedString());
                Style style = newTeamTactics.getTeamStyle();
                if (style == null) {
                    style = Style.WING_PLAY;
                }
                selectComboItem(teamStyleCombo, style.getLocalizedString());
            } else {
                resetRow();
            }
        }

        private void resetRow() {

            minSpinner.setSelection(1);
            maxSpinner.setSelection(0);
            resetTeamTacticCombo();
            resetTeamHardnessCombo();
            resetTeamStyleCombo();

        }

        private void resetTeamTacticCombo() {
            teamTacticCombo.removeAll();
            for (Tactic tactic : Tactic.values()) {
                String localizedString = tactic.getLocalizedString();
                teamTacticCombo.add(localizedString);
                teamTacticCombo.setData(localizedString, tactic);
            }
            teamTacticCombo.select(1);
        }

        private void resetTeamStyleCombo() {
            teamStyleCombo.removeAll();
            for (Style style : Style.values()) {
                String localizedString = style.getLocalizedString();
                teamStyleCombo.add(localizedString);
                teamStyleCombo.setData(localizedString, style);
            }
            teamStyleCombo.select(0);
            teamStyleCombo.setVisibleItemCount(Style.values().length);
        }

        private void resetTeamHardnessCombo() {
            teamHardnessCombo.removeAll();
            for (Hardness hardness : Hardness.values()) {
                String localizedString = hardness.getLocalizedString();
                teamHardnessCombo.add(localizedString);
                teamHardnessCombo.setData(localizedString, hardness);
            }
            teamHardnessCombo.select(0);
        }

        private void selectComboItem(Combo combo, String selectedItem) {
            String[] items = combo.getItems();
            for (int i = 0; i < items.length; i++) {
                if (items[i].equals(selectedItem)) {
                    combo.select(i);
                    return;
                }
            }
        }

        private Spinner createScoreSpinner(Composite parent) {
            Spinner spinner = new Spinner(parent, SWT.BORDER);
            spinner.setMinimum(-20);
            spinner.setMaximum(20);
            spinner.setTextLimit(3);
            return spinner;
        }

        private TeamTactics createStartTacticsFromSelectedValues() {
            return new TeamTactics((Tactic) teamTacticCombo.getData(teamTacticCombo.getText()),
                    (Hardness) teamHardnessCombo.getData(teamHardnessCombo.getText()), (Style) teamStyleCombo.getData(teamStyleCombo.getText()),
                    minSpinner.getSelection(), maxSpinner.getSelection());
        }

        private class ValueModifyListener implements ModifyListener {

            private final int rowIndex;
            private final Runnable updateTeamTactic;

            private ValueModifyListener(int rowIndex, Runnable updateRunnable) {
                this.rowIndex = rowIndex;
                this.updateTeamTactic = updateRunnable;
            }

            @Override
            public void modifyText(ModifyEvent modifyEvent) {
                TeamTactics tactics = GameForm.getInstance().getHalftimeChanges()[rowIndex];
                if (tactics == null) {
                    GameForm.getInstance().getHalftimeChanges()[rowIndex] = createStartTacticsFromSelectedValues();
                } else {
                    updateTeamTactic.run();
                }
            }
        }

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public void addCoordSubstitutionModeListener(Listener listener) {
        coordSubstitutionListeners.add(listener);
    }

    public void removeCoordSubstitutionModeListener(Listener listener) {
        coordSubstitutionListeners.remove(listener);
    }

    private class ChangeByCoordWidget extends Composite {

        Button checkButton;
        Button actionButton;
        List<Listener> actionListeners = new ArrayList<Listener>();
        List<Listener> checkedListeners = new ArrayList<Listener>();

        public ChangeByCoordWidget(Composite parent, int style) {
            super(parent, style);
            RowLayout rowLayout = new RowLayout();
            rowLayout.marginBottom = rowLayout.marginTop = 0;
            rowLayout.center = true;
            rowLayout.justify = true;
            setLayout(rowLayout);
            checkButton = new Button(this, SWT.CHECK);
            checkButton.setSelection(false);
            actionButton = new Button(this, SWT.PUSH);
            actionButton.setText("...");
            RowData data = new RowData();
            data.height = 16;
            data.width = 64;
            actionButton.setLayoutData(data);

            checkButton.addSelectionListener(new SelectionListener() {

                @Override
                public void widgetSelected(SelectionEvent e) {

                    actionButton.setEnabled(checkButton.getSelection());
                    for (Listener l : checkedListeners) {
                        l.handleEvent(null);
                    }
                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                    widgetSelected(e);
                }
            });
            actionButton.setEnabled(false);
            actionButton.addSelectionListener(new SelectionListener() {

                @Override
                public void widgetSelected(SelectionEvent e) {
                    for (Listener l : actionListeners) {
                        l.handleEvent(null);
                    }

                }

                @Override
                public void widgetDefaultSelected(SelectionEvent e) {
                    widgetSelected(e);
                }
            });
            checkButton.pack();
            actionButton.pack();
            pack();
        }

        public void addActionListener(Listener listener) {
            actionListeners.add(listener);
        }

        public void removeActionListener(Listener listener) {
            actionListeners.remove(listener);
        }

        public void addCheckedListener(Listener listener) {
            checkedListeners.add(listener);
        }

        public void removeCheckedListener(Listener listener) {
            checkedListeners.remove(listener);
        }

        public void setChecked(boolean flag) {
            checkButton.setSelection(flag);
            actionButton.setEnabled(flag);
        }

        public boolean isChecked() {
            return checkButton.getSelection();
        }

    }

}
