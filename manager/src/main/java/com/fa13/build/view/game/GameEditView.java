package com.fa13.build.view.game;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.ILoadable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.GameReader;
import com.fa13.build.controller.io.GameWriter;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.model.Competitions;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.PlayerSubstitution;
import com.fa13.build.model.game.SpecialRole;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.UIItem;
import com.fa13.build.view.game.GameSchemeEditView.PlayerImageType;

public class GameEditView implements UIItem, IDisposable, IStorable, ILoadable {

    private GameWriter writer = new GameWriter();

    String password;
    TabItem mainItem;
    Composite mainComposite;
    Composite topPanels[];
    Composite bottomPanel;
    Composite bottomPanels[];
    Slider restScale;
    Text restText;
    Label restLabel;
    Button viewBtns[];
    TabFolder parentInstance;
    GameFormEditView formEditView;
    GameSchemeEditView schemeEditView;
    GameChangeEditView changeEditView;
    Combo matchType;
    Combo matchRound;
    MainWindow mainWnd;
    Button openButton;
    Button saveButton;

    Image fieldImage;
    Image squardImage;
    Image substImage;
    //images for match bid check
    Image playerDisqImage;
    Image playerInjuredImage;
    Image playerSpecialRoleImage;

    Image gameImage;

    private SchemesModel schemesModel;
    private boolean playerCoordSubstitutionMode;

    private Button playerCoordSubstitutionButtonApply;
    private Button playerCoordSubstitutionButtonCancel;

    private Label labelChangeByCoord;
    private Label labelChangeByCoordDetails;
    private Font bigFont;

    //private Spinner ticketPriceSpinner;
    private Slider ticketSlider;
    
    private Label ticketLabel;

    private Label ticketLabel2;
    
    private static final int NEW_TICKETS_SLIDER_MIN = 4;
    private static final int NEW_TICKETS_SLIDER_MAX = 10;
    
    public GameEditView(TabFolder parent, MainWindow mainWnd) {

        this.mainWnd = mainWnd;
        parentInstance = parent;
        Properties properties = mainWnd.getSettings();

        FontData fontData = parent.getFont().getFontData()[0];
        fontData.setStyle(SWT.BOLD);
        fontData.setHeight(12);
        bigFont = new Font(parent.getDisplay(), fontData);

        loadImages();

        mainItem = new TabItem(parent, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("gameEditorTabName"));
        mainItem.setImage(gameImage);

        FormLayout formLayout = new FormLayout();
        mainComposite = new Composite(parent, SWT.NONE);
        mainItem.setControl(mainComposite);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(formLayout);

        bottomPanels = new Composite[2];

        bottomPanel = new Composite(mainComposite, SWT.BORDER);
        bottomPanel.setLayout(new FormLayout());
        FormData data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        bottomPanel.setLayoutData(data);

        bottomPanels[0] = new Composite(bottomPanel, SWT.NONE);
        bottomPanels[1] = new Composite(bottomPanel, SWT.NONE);
        topPanels = new Composite[3];

        topPanels[0] = new Composite(mainComposite, SWT.BORDER);
        topPanels[1] = new Composite(mainComposite, SWT.BORDER);
        topPanels[2] = new Composite(mainComposite, SWT.BORDER);

        //give it shell to allow showing error dialogs
        schemesModel = new SchemesModel(parent.getShell());
        schemesModel.loadSchemes();//!!! don forget !!!

        formEditView = new GameFormEditView(topPanels[0], schemesModel, properties);

        Object playerImageSize = properties.get(MainWindow.PROP_PLAYER_IMAGE_SIZE);
        PlayerImageType playerImageType;
        if (playerImageSize != null && playerImageSize.toString().equalsIgnoreCase("Small")) {
            playerImageType = PlayerImageType.Small;
        } else if (playerImageSize != null && playerImageSize.toString().equalsIgnoreCase("Man")) {
            playerImageType = PlayerImageType.Man;
        } else {
            playerImageType = PlayerImageType.Standard;
        }

        schemeEditView = new GameSchemeEditView(topPanels[1], schemesModel, playerImageType);

        changeEditView = new GameChangeEditView(topPanels[2]);
        changeEditView.addCoordSubstitutionModeListener(new Listener() {

            @Override
            public void handleEvent(Event event) {
                String stringPlayerChangeDetails = "";
                if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getCurrentTeam() != null) {
                    int number = GameForm.getInstance().getActiveCoordSubtition().getSubstitutedPlayer();
                    Player playerOut = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(number);
                    number = GameForm.getInstance().getActiveCoordSubtition().getNumber();
                    Player playerIn = MainWindow.getAllInstance().getCurrentTeam().getPlayerByNumber(number);
                    stringPlayerChangeDetails = GameForm.getInstance().getActiveCoordSubtition().getTime() + "'  ";
                    stringPlayerChangeDetails += "[" + GameForm.getInstance().getActiveCoordSubtition().getMinDifference() + " ,";
                    stringPlayerChangeDetails += GameForm.getInstance().getActiveCoordSubtition().getMaxDifference() + "]  ";
                    if (playerOut != null) {
                        stringPlayerChangeDetails += playerOut.getName();
                    }
                    stringPlayerChangeDetails += " -> ";
                    if (playerIn != null) {
                        stringPlayerChangeDetails += playerIn.getName();
                    }
                }

                labelChangeByCoordDetails.setText(stringPlayerChangeDetails);
                labelChangeByCoordDetails.setSize(labelChangeByCoordDetails.computeSize(SWT.DEFAULT, SWT.DEFAULT));
                setPlayerCoordSubstitionMode(true);
            }
        });

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.bottom = new FormAttachment(bottomPanel, -3);
        data.top = new FormAttachment(0, 0);

        topPanels[0].setLayoutData(data);
        topPanels[1].setLayoutData(data);
        topPanels[2].setLayoutData(data);

        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        bottomPanels[0].setLayoutData(data);
        bottomPanels[1].setLayoutData(data);

        bottomPanels[0].setLayout(new FormLayout());
        bottomPanels[1].setLayout(new FormLayout());

        topPanels[0].setVisible(true);
        topPanels[1].setVisible(false);
        topPanels[2].setVisible(false);

        viewBtns = new Button[3];
        viewBtns[0] = new Button(bottomPanels[0], SWT.TOGGLE);
        viewBtns[0].setFont(MainWindow.getSharedFont(FontType.BUTTON));
        viewBtns[0].setText(MainWindow.getMessage("fixturesPanelButtonName"));
        viewBtns[0].setSelection(true);
        viewBtns[0].setImage(squardImage);

        Point size = viewBtns[0].computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(50, -size.y / 2);
        viewBtns[0].setLayoutData(data);

        viewBtns[1] = new Button(bottomPanels[0], SWT.TOGGLE);
        viewBtns[1].setFont(MainWindow.getSharedFont(FontType.BUTTON));
        viewBtns[1].setText(MainWindow.getMessage("positionsPanelButtonName"));
        viewBtns[1].setImage(fieldImage);
        size = viewBtns[1].computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data = new FormData();
        data.left = new FormAttachment(viewBtns[0], 0);
        data.top = new FormAttachment(50, -size.y / 2);
        viewBtns[1].setLayoutData(data);

        viewBtns[2] = new Button(bottomPanels[0], SWT.TOGGLE);
        viewBtns[2].setFont(MainWindow.getSharedFont(FontType.BUTTON));
        viewBtns[2].setText(MainWindow.getMessage("substitutionsPanelButtonName"));
        viewBtns[2].setImage(substImage);
        size = viewBtns[2].computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data = new FormData();
        data.left = new FormAttachment(viewBtns[1], 0);
        data.top = new FormAttachment(50, -size.y / 2);
        viewBtns[2].setLayoutData(data);

        viewBtns[0].addSelectionListener(new ViewBtnListner(0, viewBtns, topPanels));
        viewBtns[1].addSelectionListener(new ViewBtnListner(1, viewBtns, topPanels));
        viewBtns[2].addSelectionListener(new ViewBtnListner(2, viewBtns, topPanels));

        matchType = new Combo(bottomPanels[0], SWT.DROP_DOWN | SWT.READ_ONLY);
        if (!SystemUtils.IS_OS_LINUX) { 
            matchType.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        matchType.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        matchType.addSelectionListener(new MatchTypeListner());
        size = matchType.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
        data = new FormData();
        data.top = new FormAttachment(50, -size.y / 2);
        data.left = new FormAttachment(viewBtns[2], 30);
        matchType.setLayoutData(data);
        matchType.select(0);

        matchRound = new Combo(bottomPanels[0], SWT.DROP_DOWN | SWT.READ_ONLY);
        if (!SystemUtils.IS_OS_LINUX) {
            matchRound.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        }
        matchRound.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        matchRound.addSelectionListener(new MatchRoundListner());

        data = new FormData();
        size = matchRound.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
        data.top = new FormAttachment(50, -size.y / 2);
        data.left = new FormAttachment(matchType, 5);
        matchRound.setLayoutData(data);
        for (int i = 0; i < Competitions.ROUND_TYPES.length; i++) {
            matchRound.add(Competitions.ROUND_TYPES[i]);
        }

        matchRound.select(0);
        matchRound.setVisibleItemCount(10);
/*
        String val = MainWindow.getDefault().getSettings().getProperty(MainWindow.PROP_USE_NEW_TICKETS, "0");
        if (val.equalsIgnoreCase("0")) {
        
            ticketLabel = new Label(bottomPanels[0], SWT.NONE);
            ticketLabel.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
            ticketLabel.setText(MainWindow.getMessage("ticketLabel"));
            data = new FormData();
            data.top = new FormAttachment(50, -ticketLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y / 2);
            data.left = new FormAttachment(matchRound, 10);
            ticketLabel.setLayoutData(data);
    
            ticketPriceSpinner = new Spinner(bottomPanels[0], SWT.BORDER);
            ticketPriceSpinner.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
            ticketPriceSpinner.setMinimum(0);
            ticketPriceSpinner.setMaximum(999);
            ticketPriceSpinner.setTextLimit(3);
            ticketPriceSpinner.setSelection(40);
            GameForm.getInstance().setPrice(ticketPriceSpinner.getSelection());
            ticketPriceSpinner.addModifyListener(new ModifyListener() {
    
                public void modifyText(ModifyEvent e) {
                    GameForm.getInstance().setPrice(ticketPriceSpinner.getSelection());
                }
            });
            
            size = ticketPriceSpinner.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
            
            data = new FormData();
            data.top = new FormAttachment(50, -size.y / 2);
            data.left = new FormAttachment(ticketLabel, 5);
            ticketPriceSpinner.setLayoutData(data);
        } else {
        */
            //new feature: new ticket's price
            
        ticketLabel = new Label(bottomPanels[0], SWT.NONE);
        ticketLabel.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        ticketLabel.setText(MainWindow.getMessage("label.maxIncomeBonus"));
        data = new FormData();
        data.top = new FormAttachment(50, -ticketLabel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y / 2);
        data.left = new FormAttachment(matchRound, 10);
        ticketLabel.setLayoutData(data);

        ticketSlider = new Slider(bottomPanels[0], SWT.HORIZONTAL);
        ticketSlider.setValues(4, NEW_TICKETS_SLIDER_MIN, NEW_TICKETS_SLIDER_MAX + 1, 1, 1, 1);

        size = ticketSlider.computeSize(150, SWT.DEFAULT, true);

        data = new FormData();
        data.top = new FormAttachment(50, -size.y / 2);
        data.left = new FormAttachment(ticketLabel, 5);
        //data.width = 100;
        ticketSlider.setLayoutData(data);

        GameForm.getInstance().setPrice(ticketSlider.getSelection());

        ticketSlider.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
//                String val = MainWindow.getDefault().getSettings().getProperty(MainWindow.PROP_USE_NEW_TICKETS, "0");
//                if (val.equalsIgnoreCase("1")) {
                GameForm.getInstance().setPrice(ticketSlider.getSelection());
                formEditView.onHomeBonusChanged(ticketSlider.getSelection());
//                }
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        ticketLabel2 = new Label(bottomPanels[0], SWT.NONE);
        ticketLabel2.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        ticketLabel2.setText(MainWindow.getMessage("label.maxOwnerBonus"));
        data = new FormData();
        data.top = new FormAttachment(50, -ticketLabel2.computeSize(SWT.DEFAULT, SWT.DEFAULT).y / 2);
        data.left = new FormAttachment(ticketSlider, 10);
        ticketLabel2.setLayoutData(data);
            
            //end new feature
        //}

        
        saveButton = new Button(bottomPanels[0], SWT.PUSH);
        saveButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saveButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        data = new FormData();
        size = saveButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.top = new FormAttachment(50, -size.y / 2);
        data.right = new FormAttachment(100, -5);
        saveButton.setLayoutData(data);
        saveButton.addSelectionListener(new SaveButtonListner());

        openButton = new Button(bottomPanels[0], SWT.PUSH);
        openButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openButton.setImage(MainWindow.getSharedImage(ImageType.OPEN));
        data = new FormData();
        size = openButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.top = new FormAttachment(50, -size.y / 2);
        data.right = new FormAttachment(saveButton, -5);
        openButton.setLayoutData(data);
        openButton.addSelectionListener(new OpenButtonListner());

        schemesModel.selectFirstScheme();

        labelChangeByCoord = new Label(bottomPanels[1], SWT.NONE);
        labelChangeByCoord.setFont(bigFont);
        labelChangeByCoord.setText(MainWindow.getMessage("changeByCoord"));
        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(0, 32);
        labelChangeByCoord.setLayoutData(data);

        playerCoordSubstitutionButtonApply = new Button(bottomPanels[1], SWT.PUSH);
        playerCoordSubstitutionButtonApply.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        playerCoordSubstitutionButtonApply.setImage(MainWindow.getSharedImage(ImageType.OK));
        playerCoordSubstitutionButtonApply.setText(MainWindow.getMessage("global.Apply"));
        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(labelChangeByCoord, 16);
        playerCoordSubstitutionButtonApply.setLayoutData(data);
        playerCoordSubstitutionButtonApply.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                setPlayerCoordSubstitionMode(false);
                schemeEditView.setPlayerCoordSubstitionMode(false);
                changeEditView.applyPlayerCoordChange();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        playerCoordSubstitutionButtonCancel = new Button(bottomPanels[1], SWT.PUSH);
        playerCoordSubstitutionButtonCancel.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        playerCoordSubstitutionButtonCancel.setImage(MainWindow.getSharedImage(ImageType.CANCEL));
        playerCoordSubstitutionButtonCancel.setText(MainWindow.getMessage("global.cancel"));
        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(playerCoordSubstitutionButtonApply, 16);
        playerCoordSubstitutionButtonCancel.setLayoutData(data);
        playerCoordSubstitutionButtonCancel.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                setPlayerCoordSubstitionMode(false);
                schemeEditView.setPlayerCoordSubstitionMode(false);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        labelChangeByCoordDetails = new Label(bottomPanels[1], SWT.NONE);
        labelChangeByCoordDetails.setFont(bigFont);
        data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(playerCoordSubstitutionButtonCancel, 32);
        data.right = new FormAttachment(100, 0);
        labelChangeByCoordDetails.setLayoutData(data);

        bottomPanels[0].setVisible(true);
        bottomPanels[1].setVisible(false);

        mainComposite.setRedraw(true);

    }

    private void loadImages() {

        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/squard_16x16.png"));
        squardImage = new Image(parentInstance.getDisplay(), imgData);
        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/ffield_16x16.png"));
        fieldImage = new Image(parentInstance.getDisplay(), imgData);
        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/subst_16x16.png"));
        substImage = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/red_card_16x16.png"));
        playerDisqImage = new Image(parentInstance.getDisplay(), imgData);
        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/medical_16x16.png"));
        playerInjuredImage = new Image(parentInstance.getDisplay(), imgData);
        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/freekick_16x16.png"));
        playerSpecialRoleImage = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/game_16x16.png"));
        gameImage = new Image(parentInstance.getDisplay(), imgData);

    }

    public void matchTypeChange() {
        int index = matchType.getSelectionIndex();
        if (index >= 0) {
            Properties properties = mainWnd.getSettings();
            String key = matchType.getItem(index);
            String value = (String) matchType.getData(key);
            properties.setProperty(MainWindow.PROP_MATCH_TYPE, key + "=" + value);
            mainWnd.saveSettings();
            GameForm.getInstance().setTournamentID(key);
        }
    }

    public void matchRoundChange() {
        int index = matchRound.getSelectionIndex();
        if (index >= 0) {
            Properties properties = mainWnd.getSettings();
            String value = matchRound.getItem(index);
            properties.setProperty(MainWindow.PROP_MATCH_ROUND, value);
            mainWnd.saveSettings();
            GameForm.getInstance().setGameType(value);
        }
    }

    public class MatchTypeListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }

        public void widgetSelected(SelectionEvent e) {
            matchTypeChange();
        }
    }

    public class MatchRoundListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }

        public void widgetSelected(SelectionEvent e) {
            matchRoundChange();
        }
    }

    public void updateAll() {
        if (!needUpdate) {
            return;
        }
        updateTeam();
        //init GameForm's match type and round
        matchTypeChange();
        matchRoundChange();
        needUpdate = false;
    }

    public void updateTeam() {
        matchType.removeAll();
        Properties properties = mainWnd.getSettings();
        String property = properties.getProperty(MainWindow.PROP_MATCH_TYPE);
        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                GameForm.getInstance().setTeamID(currentTeam.getId());

                Set<String> competitionsTeam = currentTeam.getCompetitions();
                Map<String, String> competitionsAll = MainWindow.getAllInstance().getCompetitions();
                int index = 0;
                int i = 0;
                for (String entry : competitionsTeam) {
                    String key = competitionsAll.get(entry);
                    if (key == null) {
                        continue;
                    }
                    String entryString = key + "=" + entry;
                    if (property != null && entryString.equals(property)) {
                        index = i;
                    }
                    i++;
                    matchType.add(key);
                    matchType.setData(key, entry);
                }
                matchType.select(index);
            }
            /*Point size = matchType.computeSize(SWT.DEFAULT, SWT.DEFAULT);
            FormData data = (FormData) matchType.getLayoutData();
            data.right.offset = size.x;
            matchType.setLayoutData(data);*/
            bottomPanels[0].layout();

            property = properties.getProperty(MainWindow.PROP_MATCH_ROUND);
            if (property != null) {
                int index = 0;
                for (int i = 0; i < Competitions.ROUND_TYPES.length; i++) {
                    if (Competitions.ROUND_TYPES[i].compareTo(property) == 0) {
                        index = i;
                    }
                }
                matchRound.select(index);
            }
            PlayerPosition[] firstTeam = GameForm.getInstance().getFirstTeam();
            for (int i = 0; i < firstTeam.length; i++) {
                firstTeam[i].setNumber(0);
            }
            GameForm.getInstance().setFirstTeam(firstTeam);
            formEditView.updateAll();
            changeEditView.updateAll();
            schemeEditView.updateAll();
        }
    }

    public static void copyScheme(PlayerPosition[] dstScheme, PlayerPosition[] srcScheme) {
        if (dstScheme == null || srcScheme == null) {
            return;
        }
        for (int i = 0; i < srcScheme.length; i++) {
            if (dstScheme[i] == null) {
                dstScheme[i] = new PlayerPosition();
            }
            if (srcScheme[i] == null) {
                continue;
            }
            dstScheme[i].setAttackX(srcScheme[i].getAttackX());
            dstScheme[i].setAttackY(srcScheme[i].getAttackY());
            dstScheme[i].setDefenceX(srcScheme[i].getDefenceX());
            dstScheme[i].setDefenceY(srcScheme[i].getDefenceY());
            dstScheme[i].setFreekickX(srcScheme[i].getFreekickX());
            dstScheme[i].setFreekickY(srcScheme[i].getFreekickY());
            dstScheme[i].setGoalkeeper(srcScheme[i].isGoalkeeper());
        }
    }

    public class ViewBtnListner implements SelectionListener {

        int index;
        Button btns[];
        Composite views[];

        public ViewBtnListner(int index, Button[] btns, Composite views[]) {
            this.btns = btns;
            if (index < 0) {
                index = 0;
            }
            if (index > btns.length) {
                index = btns.length - 1;
            }
            this.views = views;
            this.index = index;
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            if (btns[index].getSelection()) {
                for (int i = 0; i < btns.length; i++) {
                    if (i != index) {
                        btns[i].setSelection(false);
                        views[i].setVisible(false);
                    } else {
                        updateGameForm();
                        views[i].setVisible(true);
                    }
                }
            } else {
                btns[index].setSelection(true);
            }
        }

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

    }

    public class OpenButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            openGameFormDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            openGameFormDlg();
        }
    }

    public class SaveButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            saveGameFormDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            saveGameFormDlg();
        }
    }

    public void openGameForm(String fname) {
        if (fname == null) {
            return;
        }
        try {
            String teamBefore = GameForm.getInstance().getTeamID();
            GameReader.readGameFormFile(fname);
            String teamAfter = GameForm.getInstance().getTeamID();
            if (teamBefore != null && !teamBefore.equals(teamAfter)) {
                Team selTeam = MainWindow.getAllInstance().getCurrentTeam();
                if (selTeam != null) {
                    //replace teamId with current selected in combo
                    GameForm.getInstance().setTeamID(selTeam.getId());
                }
//                int index = mainWnd.getTeamCombo().getSelectionIndex();
//                String selTeamName = null;
//                if (index != -1) {
//                    selTeamName = mainWnd.getTeamCombo().getItem(index);
//                    Team selTeam = (Team) mainWnd.getTeamCombo().getData(selTeamName);
//                    //replace teamId with current selected in combo
//                    GameForm.getInstance().setTeamID(selTeam.getId());
//                }

            }
            GameForm.getInstance().clearIncorrectData(MainWindow.getAllInstance().getCurrentTeam());
        } catch (ReaderException e) {
            //e.printStackTrace();
        }
    }

    public void saveGameForm(String fname) {
        if (fname == null) {
            return;
        }
        try {
            GameForm.getInstance().setPassword(password);
            matchTypeChange();
            File f = new File(fname);
            if (f.exists()) {
                MessageBox mb = new MessageBox(parentInstance.getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
                mb.setMessage(String.format(MainWindow.getMessage("Ask.RewriteFile"), f.getName()));
                mb.setText(MainWindow.getMessage("FileExists"));
                if (mb.open() != SWT.YES) {
                    return;
                }
            }
            writer.write(fname, GameForm.getInstance());
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx.setMessage(MainWindow.getMessage("global.bidSaveSuccess"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        } catch (IOException e) {
            //e.printStackTrace();
            MessageBox mbx = new MessageBox(parentInstance.getShell(), SWT.OK);
            mbx.setMessage(MainWindow.getMessage("global.bidSaveError"));
            if (mbx.open() != SWT.OK) {
                return;
            }
        }
    }

    public void openGameFormDlg() {

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        String result = dlg.open();
        if (result != null) {
            this.openGameForm(result);
            updateGameForm();
        }
    }

    public void saveGameFormDlg() {

        MatchBidChecker.getDefault().doCheck();
        if (MatchBidChecker.getDefault().hasProblems()) {
            CheckMatchBidDialog confirmDialog = new CheckMatchBidDialog(parentInstance.getShell());
            confirmDialog.setText(MainWindow.getMessage("match.bid.err.dialog.title"));
            if (confirmDialog.open() == SWT.CANCEL) {
                return;
            }
        }

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.SAVE);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequestName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(getBidDefaultFileName());
        String result = dlg.open();
        if (result != null) {
            this.saveGameForm(result);
        }
    }

    private String getBidDefaultFileName() {
        String bidFileName = "";
        boolean correct = false;
        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                int index = matchType.getSelectionIndex();
                if (index >= 0) {
                    String competitionFullName = matchType.getItem(index);//full competition name
                    String competitionShortName = (String) matchType.getData(competitionFullName);//short name
                    bidFileName = competitionShortName;

                    index = matchRound.getSelectionIndex();
                    if (index >= 0) {

                        if (index < 30) {
                            bidFileName = bidFileName + 'c';
                        } else {
                            bidFileName = bidFileName + 'k';
                        }
                        String matchRoundName = matchRound.getItem(index);
                        Integer roundNum = Competitions.ROUND_TYPE_NUMBER.get(matchRoundName);
                        bidFileName = bidFileName + String.valueOf(roundNum.intValue()) + currentTeam.getId() + ".f13";

                        correct = true;
                    }

                }

            }
        }
        if (!correct) {
            bidFileName = "";//reset = we couldn't create correct fileName
        }

        return bidFileName;
    }

    public void updateGameForm() {
        formEditView.onGameFormUpdate();
        changeEditView.onGameFormUpdate();
        schemeEditView.onGameFormUpdate();
        //ticketPriceSpinner.setSelection(GameForm.getInstance().getPrice());
        ticketSlider.setValues(GameForm.getInstance().getPrice(), NEW_TICKETS_SLIDER_MIN, NEW_TICKETS_SLIDER_MAX + 1, 1, 1, 1);
        String id = GameForm.getInstance().getGameType();
        for (int i = 0; i < Competitions.ROUND_TYPES.length && id != null; i++) {
            if (id.equals(Competitions.ROUND_TYPES[i])) {
                matchRound.select(i);
                break;
            }
        }
        String type = GameForm.getInstance().getTournamentID();
        for (int i = 0; i < matchType.getItemCount() && type != null; i++) {
            if (type.equals(matchType.getItem(i))) {
                matchType.select(i);
                break;
            }
        }
    }

    public void updatePassword(String password) {
        this.password = password;
    }

    public void updateDaysOfRest(int daysOfRest) {
        formEditView.updateDaysOfRest(daysOfRest);
    }

    public void updateMessages() {
        mainItem.setText(MainWindow.getMessage("gameEditorTabName"));

        formEditView.updateMessages();
        changeEditView.updateMessages();
        schemeEditView.updateMessages();
        
        //String val = MainWindow.getDefault().getSettings().getProperty(MainWindow.PROP_USE_NEW_TICKETS, "0");
        //String key = (ticketPriceSpinner != null) ? "ticketLabel" : "label.maxIncomeBonus";
        String key = "label.maxIncomeBonus";
        ticketLabel.setText(MainWindow.getMessage(key));

        openButton.setText(MainWindow.getMessage("global.open"));
        saveButton.setText(MainWindow.getMessage("global.save"));

        viewBtns[0].setText(MainWindow.getMessage("fixturesPanelButtonName"));
        viewBtns[1].setText(MainWindow.getMessage("positionsPanelButtonName"));
        viewBtns[2].setText(MainWindow.getMessage("substitutionsPanelButtonName"));

        labelChangeByCoord.setText(MainWindow.getMessage("changeByCoord"));
        playerCoordSubstitutionButtonApply.setText(MainWindow.getMessage("global.Apply"));
        playerCoordSubstitutionButtonCancel.setText(MainWindow.getMessage("global.cancel"));

        bottomPanels[0].layout();

    }

    public void redraw() {
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
        if (!lazyUpdate) {
            updateAll();
        }
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    //release resources here!
    @Override
    public void dispose() {
        fieldImage.dispose();
        squardImage.dispose();
        substImage.dispose();
        schemeEditView.dispose();
        formEditView.dispose();
        changeEditView.dispose();
        playerDisqImage.dispose();
        playerInjuredImage.dispose();
        playerSpecialRoleImage.dispose();
        bigFont.dispose();
        gameImage.dispose();
    }

    @Override
    public void store(Properties props) {

        try {
            if (schemesModel.isChanged()) {
                schemesModel.saveSchemes();
            }
            props.put(MainWindow.PROP_TICKETS_PRICE, String.valueOf(GameForm.getInstance().getPrice()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        schemeEditView.store(props);
        formEditView.store(props);

    }

    public void setPlayerCoordSubstitionMode(boolean flag) {
        playerCoordSubstitutionMode = flag;
        bottomPanels[0].setVisible(!playerCoordSubstitutionMode);
        bottomPanels[1].setVisible(playerCoordSubstitutionMode);

        //switch to schemes
        schemeEditView.setPlayerCoordSubstitionMode(flag);
        if (flag) {
            topPanels[0].setVisible(false);//formEditView
            topPanels[1].setVisible(true);//schemeEditView
            topPanels[2].setVisible(false);//changeEditView
            updateGameForm();
        } else {
            //return to substition view
            topPanels[0].setVisible(false);//formEditView
            topPanels[1].setVisible(false);//schemeEditView
            topPanels[2].setVisible(true);//changeEditView
            updateGameForm();
        }

    }

    public class CheckMatchBidDialog extends Dialog {

        private int result = SWT.OK;
        Table table;

        public CheckMatchBidDialog(Shell parent) {
            this(parent, SWT.BORDER | SWT.APPLICATION_MODAL | SWT.TITLE);
        }

        public CheckMatchBidDialog(Shell parent, int style) {
            super(parent, style);
        }

        public int open() {
            Shell shell = new Shell(getParent(), getStyle());
            shell.setText(getText());
            createContents(shell);

            Rectangle ca = getParent().getShell().getClientArea();
            int dlgW = shell.getBounds().width;
            int dlgH = shell.getBounds().height;
            int x = (ca.width - dlgW) / 2;
            int y = (ca.height - dlgH) / 2;
            shell.setBounds(x, y, dlgW, dlgH);

            shell.pack();
            shell.open();
            Display display = getParent().getDisplay();
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
            return result;
        }

        private void createContents(final Shell shell) {
            shell.setLayout(new GridLayout(2, false));
            table = new Table(shell, SWT.BORDER);
            table.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
            table.setVisible(false);
            table.setCapture(false);
            table.setHeaderVisible(false);
            table.setLinesVisible(true);

            GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
            table.setLayoutData(gd);

            TableColumn tableColumn = new TableColumn(table, SWT.NONE);
            tableColumn.setResizable(true);
            tableColumn.setAlignment(SWT.FILL);

            tableColumn = new TableColumn(table, SWT.NONE);
            tableColumn.setResizable(true);
            tableColumn.setAlignment(SWT.CENTER);

            Button bOk = new Button(shell, SWT.PUSH);
            bOk.setFont(MainWindow.getSharedFont(FontType.BUTTON));
            bOk.setText(MainWindow.getMessage("global.continue"));
            gd = new GridData(GridData.FILL_HORIZONTAL);
            bOk.setLayoutData(gd);
            bOk.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent event) {
                    result = SWT.OK;
                    shell.close();
                }
            });

            Button bCancel = new Button(shell, SWT.PUSH);
            bCancel.setFont(MainWindow.getSharedFont(FontType.BUTTON));
            bCancel.setText(MainWindow.getMessage("global.cancel"));
            gd = new GridData(GridData.FILL_HORIZONTAL);
            bCancel.setLayoutData(gd);
            bCancel.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent event) {
                    result = SWT.CANCEL;
                    shell.close();
                }
            });

            shell.setDefaultButton(bOk);

            table.setFont(MainWindow.getSharedFont(FontType.VIEW_IMPORTANT));
            fillUiFromBid();
            shell.pack();
        }

        private void fillUiFromBid() {

            if (!MatchBidChecker.getDefault().isSquardIsFilled()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.squardNotFilled"));
                ti.setText(1, String.valueOf(MatchBidChecker.getDefault().getSquardNotFilledCount()));
                ti.setImage(0, squardImage);
            }

            for (Player player : MatchBidChecker.getDefault().getDisqPlayerList()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.disqPlayerChoosen"));
                ti.setText(1, player == null ? "???" : player.getName());
                ti.setImage(0, playerDisqImage);
            }

            for (Player player : MatchBidChecker.getDefault().getInjuredPlayerList()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.injuredPlayerChoosen"));
                ti.setText(1, player == null ? "???" : player.getName());
                ti.setImage(0, playerInjuredImage);
            }

            for (Player player : MatchBidChecker.getDefault().getDisqPlayerOnSubstList()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.disqPlayerOnSubstChoosen"));
                ti.setText(1, player == null ? "???" : player.getName());
                ti.setImage(0, substImage);
            }

            for (Player player : MatchBidChecker.getDefault().getAbsentSubstPlayerList()) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.substPlayerNotInSquard"));
                ti.setText(1, player == null ? "???" : player.getName());
                ti.setImage(0, substImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.CAPTAIN) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.captainNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.INDIRECT_FREEKICK) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.indirectFreeKickNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.FREEKICK) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.directFreeKickNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.PENALTY) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.penaltyNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.LEFT_CORNER) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.leftCornerNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRoles().get(SpecialRole.RIGHT_CORNER) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.rightCornerNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            // -------------------------  assistants --------------------------------
            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.CAPTAIN) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistCaptainNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.INDIRECT_FREEKICK) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistIndirectFreeKickNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.FREEKICK) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistDirectFreeKickNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.PENALTY) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistPenaltyNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.LEFT_CORNER) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistLeftCornerNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            if (MatchBidChecker.getDefault().getAbsentSpecialRolesAssist().get(SpecialRole.RIGHT_CORNER) == null) {
                TableItem ti = new TableItem(table, SWT.NONE);
                ti.setText(0, MainWindow.getMessage("match.bid.err.assistRightCornerNotChoosen"));
                ti.setText(1, "???");
                ti.setImage(0, playerSpecialRoleImage);
            }

            
            table.layout();
            table.getColumn(0).pack();
            table.getColumn(1).pack();
            table.setVisible(true);
        }

    }

    public static class MatchBidChecker {

        private static MatchBidChecker INSTANCE;

        private MatchBidChecker() {
        }

        public static MatchBidChecker getDefault() {
            if (INSTANCE == null) {
                INSTANCE = new MatchBidChecker();
            }
            return INSTANCE;
        }

        private List<Player> disqPlayerList = new ArrayList<Player>();
        private List<Player> disqPlayerOnSubstList = new ArrayList<Player>();
        private List<Player> injuredPlayerList = new ArrayList<Player>();
        private List<Player> absentSubstPlayerList = new ArrayList<Player>();
        private Map<SpecialRole, Player> specialRolesMap = new HashMap<SpecialRole, Player>();
        private Map<SpecialRole, Player> specialRolesAssistMap = new HashMap<SpecialRole, Player>();
        private boolean squardIsFilled = true;
        private int squardNotFilledCount = 0;

        public boolean isSquardIsFilled() {
            return squardIsFilled;
        }

        public int getSquardNotFilledCount() {
            return squardNotFilledCount;
        }

        public List<Player> getDisqPlayerList() {
            return disqPlayerList;
        }

        public List<Player> getInjuredPlayerList() {
            return injuredPlayerList;
        }

        public List<Player> getAbsentSubstPlayerList() {
            return absentSubstPlayerList;
        }

        public Map<SpecialRole, Player> getAbsentSpecialRoles() {
            return specialRolesMap;
        }
        
        public Map<SpecialRole, Player> getAbsentSpecialRolesAssist() {
            return specialRolesAssistMap;
        }

        public List<Player> getDisqPlayerOnSubstList() {
            return disqPlayerOnSubstList;
        }

        public boolean hasProblems() {
            boolean res = disqPlayerList.size() + injuredPlayerList.size() + absentSubstPlayerList.size() + disqPlayerOnSubstList.size() > 0;
            if (res) {
                return true;
            }
            for (Entry<SpecialRole, Player> entry : specialRolesMap.entrySet()) {
                if (entry.getValue() == null) {
                    return true;
                }
            }
            for (Entry<SpecialRole, Player> entry : specialRolesAssistMap.entrySet()) {
                if (entry.getValue() == null) {
                    return true;
                }
            }
            res = res || !squardIsFilled;
            return res;
        }

        public void doCheck() {
            disqPlayerList.clear();
            disqPlayerOnSubstList.clear();
            injuredPlayerList.clear();
            absentSubstPlayerList.clear();
            specialRolesMap.clear();
            squardIsFilled = true;
            squardNotFilledCount = 0;

            specialRolesMap.put(SpecialRole.CAPTAIN, null);
            specialRolesMap.put(SpecialRole.FREEKICK, null);
            specialRolesMap.put(SpecialRole.INDIRECT_FREEKICK, null);
            specialRolesMap.put(SpecialRole.LEFT_CORNER, null);
            specialRolesMap.put(SpecialRole.RIGHT_CORNER, null);
            specialRolesMap.put(SpecialRole.PENALTY, null);
            
            specialRolesAssistMap.put(SpecialRole.CAPTAIN, null);
            specialRolesAssistMap.put(SpecialRole.FREEKICK, null);
            specialRolesAssistMap.put(SpecialRole.INDIRECT_FREEKICK, null);
            specialRolesAssistMap.put(SpecialRole.LEFT_CORNER, null);
            specialRolesAssistMap.put(SpecialRole.RIGHT_CORNER, null);
            specialRolesAssistMap.put(SpecialRole.PENALTY, null);

            if (MainWindow.getAllInstance() == null) {
                return;
            }
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam == null) {
                return;
            }
            //find error in match bid

            for (PlayerPosition playerPostiton : GameForm.getInstance().getFirstTeam()) {
                Player player = currentTeam.getPlayerByNumber(playerPostiton.getNumber());
                if (player == null) {
                    squardIsFilled = false;
                    squardNotFilledCount++;
                    continue;
                } else {
                    if (player.getDisqualification() > 0) {
                        disqPlayerList.add(player);
                    }
                    if (player.getFitness(3) <= 65) {
                        injuredPlayerList.add(player);
                    }
                }

                if (playerPostiton.isCaptain()) {
                    specialRolesMap.put(SpecialRole.CAPTAIN, player);
                } else if (playerPostiton.isCaptainAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.CAPTAIN, player);
                }

                if (playerPostiton.isIndirectFreekick()) {
                    specialRolesMap.put(SpecialRole.INDIRECT_FREEKICK, player);
                } else if (playerPostiton.isIndirectFreekickAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.INDIRECT_FREEKICK, player);
                }

                if (playerPostiton.isDirectFreekick()) {
                    specialRolesMap.put(SpecialRole.FREEKICK, player);
                } else if (playerPostiton.isDirectFreekickAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.FREEKICK, player);
                }

                if (playerPostiton.isPenalty()) {
                    specialRolesMap.put(SpecialRole.PENALTY, player);
                } else if (playerPostiton.isPenaltyAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.PENALTY, player);
                }

                if (playerPostiton.isLeftCorner()) {
                    specialRolesMap.put(SpecialRole.LEFT_CORNER, player);
                } else if (playerPostiton.isLeftCornerAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.LEFT_CORNER, player);
                }

                if (playerPostiton.isRightCorner()) {
                    specialRolesMap.put(SpecialRole.RIGHT_CORNER, player);
                } else if (playerPostiton.isRightCornerAssistant()) {
                    specialRolesAssistMap.put(SpecialRole.RIGHT_CORNER, player);
                }

            }

            //check subst players are in squard (startup/bench)
            for (PlayerSubstitution playerSubstitution : GameForm.getInstance().getSubstitutions()) {

                if (playerSubstitution.isEmpty()) {
                    continue;
                }
                int playerInNumber = playerSubstitution.getNumber();
                Player player = currentTeam.getPlayerByNumber(playerInNumber);

                if (player != null && player.getDisqualification() > 0) {
                    disqPlayerOnSubstList.add(player);
                }

                boolean found = false;
                for (int benchPlayerNumber : GameForm.getInstance().getBench()) {
                    if (playerInNumber == benchPlayerNumber) {
                        found = true;
                        break;
                    }
                }
                //if not found on bench then search in startup squard in case of self substitution
                if (!found) {

                    for (PlayerPosition playerPosititon : GameForm.getInstance().getFirstTeam()) {
                        if (playerPosititon.getNumber() == playerInNumber) {
                            found = true;
                            break;
                        }
                    }

                }
                if (!found) {
                    //subst error!
                    player = currentTeam.getPlayerByNumber(playerInNumber);
                    absentSubstPlayerList.add(player);
                }
            }
        }
    }

    @Override
    public void load(Properties props) {

        schemeEditView.load(props);
        
        //boolean useNewTickets = !props.getProperty(MainWindow.PROP_USE_NEW_TICKETS, "0").equalsIgnoreCase("0");
        
        String data = props.getProperty(MainWindow.PROP_TICKETS_PRICE, "");
        if (!data.isEmpty()) {
            try {
                int val = Integer.valueOf(data);
                //if (useNewTickets) {
                //int avg = (NEW_TICKETS_SLIDER_MIN + NEW_TICKETS_SLIDER_MAX) / 2;
                int defVal = NEW_TICKETS_SLIDER_MAX;
                val = val > NEW_TICKETS_SLIDER_MAX ? defVal : val;
                val = val < NEW_TICKETS_SLIDER_MIN ? defVal : val;
                ticketSlider.setValues(val, NEW_TICKETS_SLIDER_MIN, NEW_TICKETS_SLIDER_MAX + 1, 1, 1, 1);
//                } else {
//                    ticketPriceSpinner.setValues(val, 0, 999, 0, 1, 5);
//                }
            } catch (Exception ex) {
                GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"),
                        MainWindow.getMessage("CantReadTicketsPriceSettings"));
            }
        }

    }

    /* never used
    private class SliderListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            restText.setText(String.valueOf(restScale.getSelection()));
            formEditView.updateDaysOfRest(restScale.getSelection());
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            restText.setText(String.valueOf(restScale.getSelection()));
            formEditView.updateDaysOfRest(restScale.getSelection());
        }
    }

    private class RestTextListener implements Listener {

        public void handleEvent(final Event e) {
            int val;
            try {
                val = Integer.valueOf(restText.getText());
            } catch (NumberFormatException nfe) {
                val = 0;
            }
            if (val > REST_SLIDER_MAX) {
                val = REST_SLIDER_MAX;
            }
            switch (e.type) {
                case SWT.Traverse:
                    if (e.detail == SWT.TRAVERSE_RETURN) {
                        restScale.setSelection(val);
                        restText.setText(String.valueOf(val));
                    }
                    break;
                case SWT.FocusOut:
                case SWT.Modify:
                    restScale.setSelection(val);
                    restText.setText(String.valueOf(val));
                    break;
                case SWT.Verify:
                    String string = e.text;
                    char[] chars = new char[string.length()];
                    string.getChars(0, chars.length, chars, 0);
                    for (char c : chars) {
                        if (!('0' <= c && c <= '9')) {
                            e.doit = false;
                            return;
                        }
                    }
            }
        }
    }
    */

}
