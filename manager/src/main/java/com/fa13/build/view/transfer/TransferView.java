package com.fa13.build.view.transfer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.FontDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.ReaderException;
import com.fa13.build.controller.io.TransferListReader;
import com.fa13.build.controller.io.TransferReader;
import com.fa13.build.controller.io.TransferWriter;
import com.fa13.build.model.Player;
import com.fa13.build.model.Team;
import com.fa13.build.model.Transfer;
import com.fa13.build.model.TransferBid;
import com.fa13.build.model.TransferList;
import com.fa13.build.model.TransferPlayer;
import com.fa13.build.model.TransferPlayerFilter;
import com.fa13.build.utils.GameUtils;
import com.fa13.build.utils.GuiLongTask;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.UIItem;
import com.fa13.build.view.game.GameFormEditView;

public class TransferView implements UIItem, IDisposable, IStorable {

    //public static final String TLIST_URL = "http://repository.fa13.info/site/build/Tlist13.b13";
    public static final String TLIST_URL = "https://fa13.info/transfer/TList";
    public static final String TLIST_DEFAULT_FILE_NAME = "Tlist13.b13";
    public static String PROP_COUNTRY_CODE = "countryCode";

    TabFolder parentInstance;
    TabItem mainItem;

    ScrolledComposite rightScrollComposite;
    Composite rightComposite;
    Composite mainComposite;
    Composite topPanel;
    Composite bottomPanel;
    Composite middlePanel;
    Group filterPanel;
    Group limitPlayersPanel;
    Composite buttonsPanel;

    Table tableBids;
    Table tableTList;
    TransferList tList = null;

    Button openTListButton;
    Button openTransferBidButton;
    Button saveTransferBidButton;

    Button buttonUseBankCoeff;

    Label labelGk, labelDc, labelDl, labelDr, labelMc, labelMl, labelMr, labelFc, labelFl, labelFr, labelTotals, labelLimitBalance,
            labelThousandsShort;
    Spinner spinnerGk, spinnerDc, spinnerDl, spinnerDr, spinnerMc, spinnerMl, spinnerMr, spinnerFc, spinnerFl, spinnerFr, spinnerTotals,
            spinnerLimitBalance;
    SimpleDateFormat dateFormat;
    Label dateLabel;
    List<TransferPlayer> playerList;
    Listener paintListener;
    Transfer transferBid;
    TransferBid[] bidList;
    private Font playersTableFont;
    private Image imgNoFlag;
    private Image imageOpenTList;
    private Image imageFilterWindowShow;
    private Image imageFilterWindowHide;
    private Image imgTransfer;

    private Button showHideFilterButton;
    private Label ageLabel;
    private Spinner ageSpinner1;
    private Label strengthLabel;
    private Spinner strengthSpinner1;
    private Label wageLabel;
    private Spinner wageSpinner1;
    private Label talentLabel;
    private Spinner talentSpinner1;
    private Label healthLabel;
    private Spinner healthSpinner1;
    private Label expLabel;
    private Spinner expSpinner1;
    private Spinner ageSpinner2;
    private Spinner strengthSpinner2;
    private Spinner wageSpinner2;
    private Spinner talentSpinner2;
    private Spinner healthSpinner2;
    private Spinner expSpinner2;
    private Label ticketsLabel;
    private Spinner ticketsSpinner1;
    private Spinner ticketsSpinner2;
    private Label positionLabel;
    private Text positionText;
    private Label priceLabel;
    private Spinner priceSpinner1;
    private Spinner priceSpinner2;
    private Button userChoiseButton;
    private Label playerLabel;
    private Text playerText;
    private Label countryLabel;
    private Text countryText;
    private Group abilityfilterPanel;
    private Button checkButtonUseFilter;
    private Button checkButtonUseAbilityFilter;
    private TransferPlayerFilter transferPlayerFilter = new TransferPlayerFilter();
    private Label speedLabel;
    private Spinner speedSpinner1;
    private Spinner speedSpinner2;
    private Label headingLabel;
    private Spinner headingSpinner1;
    private Spinner headingSpinner2;
    private Label fitnessLabel;
    private Spinner staminaSpinner1;
    private Spinner staminaSpinner2;
    private Label shootingLabel;
    private Spinner shootingSpinner1;
    private Spinner shootingSpinner2;
    private Label passingLabel;
    private Spinner passingSpinner1;
    private Spinner passingSpinner2;
    private Label dribblingLabel;
    private Spinner dribblingSpinner1;
    private Spinner dribblingSpinner2;
    private Label crossingLabel;
    private Spinner crossingSpinner1;
    private Spinner crossingSpinner2;
    private Label handlingLabel;
    private Spinner handlingSpinner1;
    private Label tacklingLabel;
    private Spinner tacklingSpinner1;
    private Spinner tacklingSpinner2;
    private Label reflexesLabel;
    private Spinner reflexesSpinner1;
    private Spinner reflexesSpinner2;
    private Spinner handlingSpinner2;
    private Button applyFiltersButton;

    FormData middlePanelFormData;
    private Button bDownloadTlist;
    private Image imgDownload;
    private boolean filterReseting = false;

    private String[] titlesTList;
    private String[] tooltipsTList;

    private List<Comparator<TransferPlayer>> comparators = new ArrayList<>();

    public TransferView(TabFolder parent) {
        parentInstance = parent;
        loadImages();
        transferBid = new Transfer();
        if (MainWindow.getAllInstance() != null) {
            updateAll();
        }
        mainItem = new TabItem(parentInstance, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("transferTabName"));
        mainItem.setImage(imgTransfer);

        FormLayout formLayout = new FormLayout();
        mainComposite = new Composite(parent, SWT.NONE);
        mainItem.setControl(mainComposite);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(formLayout);

        bottomPanel = new Composite(mainComposite, SWT.BORDER);
        topPanel = new Composite(mainComposite, SWT.BORDER);

        FormData data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.bottom = new FormAttachment(bottomPanel, -3);
        data.top = new FormAttachment(0, 0);
        topPanel.setLayoutData(data);

        data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        bottomPanel.setLayoutData(data);

        bottomPanel.setLayout(new GridLayout(2, false));

        limitPlayersPanel = createLimitBidPlayersGroupPanel();
        limitPlayersPanel.setVisible(false);
        topPanel.setLayout(new FormLayout());

        rightScrollComposite = new ScrolledComposite(topPanel, SWT.H_SCROLL | SWT.V_SCROLL);
        rightScrollComposite.setExpandHorizontal(false);
        rightScrollComposite.setExpandVertical(true);
        rightComposite = new Composite(rightScrollComposite, SWT.NONE);
        rightScrollComposite.setContent(rightComposite);
        data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        rightScrollComposite.setLayoutData(data);

        data = new FormData();
        rightComposite.setLayoutData(data);
        rightComposite.setLayout(new FormLayout());

        tableBids = new Table(rightComposite, SWT.BORDER);
        tableBids.setHeaderVisible(true);
        tableBids.setLinesVisible(true);

        String[] titles = { MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("tradein") };
        String[] tooltips = { "", MainWindow.getMessage("filter.price"), "" };
        GuiUtils.initTable(tableBids, titles, tooltips, SWT.LEFT, true);
        tableBids.setItemCount(20);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        //Point size = tableBids.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        //data.right = new FormAttachment(tableBids, size.x);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        tableBids.setLayoutData(data);

        tableBids.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {

                Menu menu = new Menu(tableBids);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("Clear"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        int i = tableBids.getSelectionIndex();
                        if (i > -1 && bidList != null && i < bidList.length) {
                            //clear bid
                            bidList[i] = null;

                        }
                        redrawBids();

                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        buttonUseBankCoeff = new Button(rightComposite, SWT.TOGGLE);
        data = new FormData();
        //data.bottom = new FormAttachment(100, 0);
        data.top = new FormAttachment(tableBids, 8);
        //data.right = new FormAttachment(100, 0);
        data.left = new FormAttachment(6, 0);
        buttonUseBankCoeff.setLayoutData(data);

        buttonUseBankCoeff.setSelection(false);
        buttonUseBankCoeff.setText(MainWindow.getMessage("trans.price_without_bank_coeff"));
        buttonUseBankCoeff.setToolTipText(MainWindow.getMessage("trans.bank_coeff"));
        buttonUseBankCoeff.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                doApplyUseBankCoeff(buttonUseBankCoeff.getSelection());

            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        applyFiltersButton = new Button(rightComposite, SWT.PUSH);
        applyFiltersButton.setToolTipText(MainWindow.getMessage("applyFilter"));
        //applyFiltersButton.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        data = new FormData();
        data.top = new FormAttachment(buttonUseBankCoeff, 2);
        data.left = new FormAttachment(6, 0);
        applyFiltersButton.setLayoutData(data);
        applyFiltersButton.setImage(MainWindow.getSharedImage(ImageType.OK));
        //applyFiltersButton.setText(MainWindow.getMessage("filter"));
        applyFiltersButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                redraw();
            }
        });

        showHideFilterButton = new Button(rightComposite, SWT.TOGGLE);
        showHideFilterButton.setSelection(true);
        data = new FormData();
        data.top = new FormAttachment(applyFiltersButton, 2);
        data.left = new FormAttachment(6, 0);
        showHideFilterButton.setLayoutData(data);
        showHideFilterButton.setImage(imageFilterWindowHide);
        //applyFiltersButton.setText(MainWindow.getMessage("filter"));
        showHideFilterButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                if (showHideFilterButton.getSelection()) {
                    showHideFilterButton.setImage(imageFilterWindowHide);
                    showHideFilterButton.setToolTipText(MainWindow.getMessage("hideFilter"));
                } else {
                    showHideFilterButton.setImage(imageFilterWindowShow);
                    showHideFilterButton.setToolTipText(MainWindow.getMessage("showFilter"));
                }
                showFilterPanel(showHideFilterButton.getSelection());
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        checkButtonUseFilter = new Button(rightComposite, SWT.CHECK);
        checkButtonUseFilter.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        data = new FormData();
        data.top = new FormAttachment(buttonUseBankCoeff, 8);
        data.left = new FormAttachment(applyFiltersButton, 8);
        checkButtonUseFilter.setLayoutData(data);
        checkButtonUseFilter.setText(MainWindow.getMessage("filter"));
        checkButtonUseFilter.setSelection(false);
        checkButtonUseFilter.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                boolean flag = checkButtonUseFilter.getSelection() || checkButtonUseAbilityFilter.getSelection();
                applyFiltersButton.setEnabled(flag);
                transferPlayerFilter.setUseMainFilter(checkButtonUseFilter.getSelection());
                redraw();

            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        checkButtonUseAbilityFilter = new Button(rightComposite, SWT.CHECK);
        checkButtonUseAbilityFilter.setFont(MainWindow.getSharedFont(FontType.EDIT_IMPORTANT));
        data = new FormData();
        data.top = new FormAttachment(checkButtonUseFilter, 12);
        data.left = new FormAttachment(showHideFilterButton, 8);
        checkButtonUseAbilityFilter.setLayoutData(data);
        checkButtonUseAbilityFilter.setText(MainWindow.getMessage("filter.ability"));
        checkButtonUseAbilityFilter.setSelection(false);
        checkButtonUseAbilityFilter.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                boolean flag = checkButtonUseFilter.getSelection() || checkButtonUseAbilityFilter.getSelection();
                applyFiltersButton.setEnabled(flag);
                transferPlayerFilter.setUseAbilityFilter(checkButtonUseAbilityFilter.getSelection());
                redraw();

            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        middlePanel = new Composite(topPanel, SWT.NONE);
        middlePanel.setLayout(new GridLayout(2, false));
        data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.right = new FormAttachment(rightScrollComposite, -5);
        data.left = new FormAttachment(0, 0);
        middlePanel.setLayoutData(data);
        middlePanelFormData = data;
        createFilterPanel();
        createPlayerAbilitiesFilterPanel();

        tableTList = new Table(topPanel, SWT.BORDER | SWT.FULL_SELECTION);
        tableTList.setHeaderVisible(true);
        tableTList.setLinesVisible(true);

        data = new FormData();
        data.bottom = new FormAttachment(middlePanel, 0);
        data.top = new FormAttachment(0, 0);
        data.right = new FormAttachment(rightScrollComposite, -5);
        data.left = new FormAttachment(0, 0);
        tableTList.setLayoutData(data);

        playersTableFont = tableTList.getFont();
        tableTList.addMenuDetectListener(new MenuDetectListener() {

            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(tableTList);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setText(MainWindow.getMessage("Font"));
                item.addSelectionListener(new SelectionListener() {

                    public void widgetSelected(SelectionEvent e) {
                        widgetDefaultSelected(e);

                    }

                    public void widgetDefaultSelected(SelectionEvent e) {
                        //change font
                        FontDialog dlg = new FontDialog(parentInstance.getShell());
                        dlg.setFontList(tableTList.getFont().getFontData());
                        FontData fontData = dlg.open();
                        if (fontData != null) {
                            playersTableFont = new Font(parentInstance.getShell().getDisplay(), fontData);

                        }
                        redraw();

                    }
                });

                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        String[] t1 = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerAge"),
                MainWindow.getMessage("PlayerTalant"), MainWindow.getMessage("PlayerStrength"), MainWindow.getMessage("PlayerHealth"),
                MainWindow.getMessage("PlayerAbilities"), MainWindow.getMessage("PlayerSalary"), MainWindow.getMessage("PlayerPrice"),
                MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("seller"),
                MainWindow.getMessage("PlayerBirthTour") };

        String[] t2 = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"), MainWindow.getMessage("tooltip.Age"),
                MainWindow.getMessage("tooltip.Talent"), MainWindow.getMessage("tooltip.Strength"), MainWindow.getMessage("tooltip.Health"),
                MainWindow.getMessage("tooltip.Skills"), MainWindow.getMessage("tooltip.Salary"), MainWindow.getMessage("filter.price"),
                MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("seller"),
                MainWindow.getMessage("PlayerBirthTour") };
        titlesTList = t1;
        tooltipsTList = t2;

        GuiUtils.initTable(tableTList, titlesTList, tooltipsTList, SWT.LEFT, true);

        buttonsPanel = new Composite(bottomPanel, SWT.NONE);
        GridData gridData = new GridData(SWT.END, SWT.CENTER, false, false);
        buttonsPanel.setLayoutData(gridData);
        buttonsPanel.setLayout(new GridLayout(3, false));

        openTListButton = new Button(buttonsPanel, SWT.PUSH);
        openTListButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openTListButton.setImage(imageOpenTList);
        openTListButton.setText(MainWindow.getMessage("openTListButton"));

        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        gridData.horizontalSpan = 2;
        openTListButton.setLayoutData(gridData);
        openTListButton.addSelectionListener(new OpenTlistSelectionListner());

        openTransferBidButton = new Button(buttonsPanel, SWT.PUSH);
        openTransferBidButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        openTransferBidButton.setImage(MainWindow.getSharedImage(ImageType.OPEN));
        openTransferBidButton.setText("  " + MainWindow.getMessage("global.open_bid") + "  ");

        gridData = new GridData(SWT.END, SWT.CENTER, false, false);
        openTransferBidButton.setLayoutData(gridData);
        openTransferBidButton.addSelectionListener(new OpenTransferBidSelectionListener());

        bDownloadTlist = new Button(buttonsPanel, SWT.PUSH);
        bDownloadTlist.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        bDownloadTlist.setText(MainWindow.getMessage("Download"));
        bDownloadTlist.setToolTipText(MainWindow.getMessage("DownloadingTlist"));
        bDownloadTlist.setImage(imgDownload);
        bDownloadTlist.addSelectionListener(slDownload);
        gridData = new GridData(SWT.END, SWT.CENTER, false, false);
        bDownloadTlist.setLayoutData(gridData);

        dateLabel = new Label(buttonsPanel, SWT.NONE);
        dateLabel.setFont(MainWindow.getSharedFont(FontType.TITLE_BIG));
        dateLabel.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        dateLabel.setText("Date");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        dateLabel.setLayoutData(gridData);

        dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        saveTransferBidButton = new Button(buttonsPanel, SWT.PUSH);
        saveTransferBidButton.setFont(MainWindow.getSharedFont(FontType.BUTTON));
        saveTransferBidButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        saveTransferBidButton.setText(MainWindow.getMessage("global.save_bid"));

        gridData = new GridData(SWT.END, SWT.CENTER, false, false);
        saveTransferBidButton.setLayoutData(gridData);
        saveTransferBidButton.addSelectionListener(new SaveTransferBidSelectionListener());

        paintListener = new Listener() {
            public void handleEvent(Event event) {
                switch (event.type) {
                    case SWT.MeasureItem: {
                        if (event.index == 10) {
                            Rectangle rect = imgNoFlag.getBounds();
                            event.width = rect.width;
                            event.height = Math.max(event.height, rect.height + 2);
                        } else {
                            TableItem item = (TableItem) event.item;
                            String text = item.getText(event.index);
                            int size = text.length();
                            text = "";
                            for (int i = 0; i < size + 1; i++) {
                                text += "A";
                            }
                            Point extent = event.gc.stringExtent(text);
                            // TODO: Remove Magic number 12 !!!
                            if (event.index != 6) {
                                event.width = Math.max(event.width, extent.x);
                            }
                        }
                        break;
                    }
                    case SWT.PaintItem: {
                        if (event.index == 9) {
                            int x = event.x;
                            Rectangle rect = imgNoFlag.getBounds();
                            int yoffset = Math.max(0, (event.height - rect.height) / 2);
                            Image flagImage = event.item.getData() == null ? imgNoFlag : (Image) event.item.getData();
                            event.gc.drawImage(flagImage, x + (tableTList.getColumn(9).getWidth() - rect.width) / 2, event.y + yoffset);
                        }
                        break;
                    }
                }
            }
        };

        tableTList.addListener(SWT.PaintItem, paintListener);
        tableTList.addListener(SWT.MeasureItem, paintListener);
        tableTList.addListener(SWT.MouseHover, new Listener() {
            @Override
            public void handleEvent(Event event) {
                onTlistMouseHover(event);
            }
        });

        tableBids.addListener(SWT.MouseDown, new TBidsTableListener(this.tableBids, this));

        mainComposite.setRedraw(true);
        redrawList();

        setDragDropSource(tableTList);
        setDragDropTarget(tableBids);
        openTransferBidButton.setVisible(false);
        saveTransferBidButton.setVisible(false);
        //viewBtns[0].setVisible(false);
        //viewBtns[1].setVisible(false);

        setupTableSorting();

    }

    private void setupTableSorting() {
        //to this moment tableTList MUST be inited
        //0 id
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getTransferID() - o2.getTransferID();
            }
        });
        //1 pos
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getOriginalPosition().compareTo(o2.getOriginalPosition());
            }
        });
        //2 age
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getAge() - o2.getAge();
            }
        });
        //3 talent
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getTalent() - o2.getTalent();
            }
        });
        //4 strength
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getStrength() - o2.getStrength();
            }
        });
        //5 health
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getHealth() - o2.getHealth();
            }
        });
        //6 abilities
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getSpeed() - o2.getSpeed();
            }
        });
        //7 salary
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getSalary() - o2.getSalary();
            }
        });
        //8 price
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getPrice() - o2.getPrice();
            }
        });
        //9 country
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getNationalityName().compareToIgnoreCase(o2.getNationalityName());
            }
        });
        //10 name
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        //11 previousTeam
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getPreviousTeam().compareToIgnoreCase(o2.getPreviousTeam());
            }
        });
        //12 birthTour
        comparators.add(new Comparator<TransferPlayer>() {

            @Override
            public int compare(TransferPlayer o1, TransferPlayer o2) {

                return o1.getBirthtour() - o2.getBirthtour();
            }
        });
        Listener sortListener = new Listener() {
            public void handleEvent(Event e) {
                tableTList.setRedraw(false);
                TableColumn sortColumn = tableTList.getSortColumn();
                TableColumn currentColumn = (TableColumn) e.widget;
                int dir = tableTList.getSortDirection();
                if (sortColumn == currentColumn) {
                    dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
                } else {
                    tableTList.setSortColumn(currentColumn);
                    dir = SWT.UP;
                }
                int index = tableTList.indexOf(currentColumn);
                Collections.sort(tList.getPlayers(), comparators.get(index));
                if (dir == SWT.DOWN) {
                    Collections.reverse(tList.getPlayers());
                }
                tableTList.setSortDirection(dir);
                redrawList();
                tableTList.setRedraw(true);
            }
        };

        for (int i = 0; i < tableTList.getColumnCount(); i++) {
            TableColumn column = tableTList.getColumn(i);

            column.addListener(SWT.Selection, sortListener);
        }

    }

    public void onTlistMouseHover(Event event) {
        Point point = new Point(event.x, event.y);
        TableItem item = tableTList.getItem(point);
        if (item == null) {
            return;
        }
        int column = -1;
        for (int i = 0; i < tableTList.getColumnCount(); i++) {
            if (item.getBounds(i) != null && item.getBounds(i).contains(point)) {
                column = i;
                break;
            }
        }
        if (column == 9) {
            String countryCode = (String) item.getData(PROP_COUNTRY_CODE);
            String countryName = countryCode == null ? "" : MainWindow.getMessage("country." + countryCode);
            tableTList.setToolTipText(countryName);
        } else {
            tableTList.setToolTipText("");
        }
    }

    private void showFilterPanel(boolean flag) {

        if (flag) {
            Point computedSize = middlePanel.computeSize(SWT.DEFAULT, SWT.DEFAULT);
            middlePanelFormData.height = computedSize.y;
        } else {
            middlePanelFormData.height = 0;
        }
        topPanel.layout();
    }

    private void loadImages() {

        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/open_tlist_16x16.png"));
        imageOpenTList = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/flags/not.png"));
        imgNoFlag = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_find_16x16.png"));
        imageFilterWindowShow = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_remove_16x16.png"));
        imageFilterWindowHide = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/download_16x16.png"));
        imgDownload = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/transfer_16x16.png"));
        imgTransfer = new Image(parentInstance.getDisplay(), imgData);

    }

    private Group createFilterPanel() {

        filterPanel = new Group(middlePanel, SWT.NONE);
        filterPanel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        filterPanel.setText(MainWindow.getMessage("filter"));
        filterPanel.setLayout(new GridLayout(6, false));

        GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
        filterPanel.setLayoutData(data);
        filterPanel.addMenuDetectListener(new MenuDetectListener() {
            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(filterPanel);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
                item.setText(MainWindow.getMessage("ResetFilter"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        resetFilter();
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });
        // age        
        ageLabel = new Label(filterPanel, SWT.NONE);
        ageLabel.setText(MainWindow.getMessage("filter.age"));
        GridData gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageLabel.setLayoutData(gridData);

        ageSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        ageSpinner1.setValues(15, 15, 99, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageSpinner1.setLayoutData(gridData);
        ageSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.AGE, 1));

        ageSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        ageSpinner2.setValues(99, 15, 99, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ageSpinner2.setLayoutData(gridData);
        ageSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.AGE, 2));
        //strength
        strengthLabel = new Label(filterPanel, SWT.NONE);
        strengthLabel.setText(MainWindow.getMessage("filter.strength"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthLabel.setLayoutData(gridData);

        strengthSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        strengthSpinner1.setValues(1, 1, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthSpinner1.setLayoutData(gridData);
        strengthSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STRENGTH, 1));

        strengthSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        strengthSpinner2.setValues(100, 1, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        strengthSpinner2.setLayoutData(gridData);
        strengthSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STRENGTH, 2));

        //wage
        wageLabel = new Label(filterPanel, SWT.NONE);
        wageLabel.setText(MainWindow.getMessage("filter.wage"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageLabel.setLayoutData(gridData);

        wageSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        wageSpinner1.setValues(0, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageSpinner1.setLayoutData(gridData);
        wageSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.WAGE, 1));

        wageSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        wageSpinner2.setValues(999, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        wageSpinner2.setLayoutData(gridData);
        wageSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.WAGE, 2));

        //talent
        talentLabel = new Label(filterPanel, SWT.NONE);
        talentLabel.setText(MainWindow.getMessage("filter.talent"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentLabel.setLayoutData(gridData);

        talentSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        talentSpinner1.setValues(20, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentSpinner1.setLayoutData(gridData);
        talentSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TALENT, 1));

        talentSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        talentSpinner2.setValues(100, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        talentSpinner2.setLayoutData(gridData);
        talentSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TALENT, 2));
        //health
        healthLabel = new Label(filterPanel, SWT.NONE);
        healthLabel.setText(MainWindow.getMessage("filter.health"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthLabel.setLayoutData(gridData);

        healthSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        healthSpinner1.setValues(20, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthSpinner1.setLayoutData(gridData);
        healthSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEALTH, 1));

        healthSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        healthSpinner2.setValues(100, 20, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        healthSpinner2.setLayoutData(gridData);
        healthSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEALTH, 2));
        //Experience
        expLabel = new Label(filterPanel, SWT.NONE);
        expLabel.setText(MainWindow.getMessage("filter.experiance"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expLabel.setLayoutData(gridData);

        expSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        expSpinner1.setValues(0, 0, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expSpinner1.setLayoutData(gridData);
        expSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.EXPERIANCE, 1));

        expSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        expSpinner2.setValues(100, 0, 100, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        expSpinner2.setLayoutData(gridData);
        expSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.EXPERIANCE, 2));

        //tickets
        ticketsLabel = new Label(filterPanel, SWT.NONE);
        ticketsLabel.setText(MainWindow.getMessage("filter.tickets"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ticketsLabel.setLayoutData(gridData);

        ticketsSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        ticketsSpinner1.setValues(0, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ticketsSpinner1.setLayoutData(gridData);
        ticketsSpinner1.setEnabled(false);
        ticketsSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TICKETS, 1));

        ticketsSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        ticketsSpinner2.setValues(999, 0, 999, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        ticketsSpinner2.setLayoutData(gridData);
        ticketsSpinner2.setEnabled(false);
        ticketsSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TICKETS, 2));

        //position
        positionLabel = new Label(filterPanel, SWT.NONE);
        positionLabel.setText(MainWindow.getMessage("filter.position"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        positionLabel.setLayoutData(gridData);

        positionText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        positionText.setLayoutData(gridData);
        positionText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setPositionNamePart(positionText.getText());
            }
        });
        //price
        priceLabel = new Label(filterPanel, SWT.NONE);
        priceLabel.setText(MainWindow.getMessage("filter.price"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        priceLabel.setLayoutData(gridData);

        priceSpinner1 = new Spinner(filterPanel, SWT.BORDER);
        priceSpinner1.setValues(0, 0, 99999, 0, 10, 100);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        priceSpinner1.setLayoutData(gridData);
        priceSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PRICE, 1));

        priceSpinner2 = new Spinner(filterPanel, SWT.BORDER);
        priceSpinner2.setValues(99999, 0, 99999, 0, 10, 100);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        priceSpinner2.setLayoutData(gridData);
        priceSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PRICE, 2));
        //userChoise
        userChoiseButton = new Button(filterPanel, SWT.CHECK);
        userChoiseButton.setText(MainWindow.getMessage("filter.userChoise"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        gridData.horizontalSpan = 3;
        userChoiseButton.setLayoutData(gridData);
        userChoiseButton.setEnabled(false);

        //player
        playerLabel = new Label(filterPanel, SWT.NONE);
        playerLabel.setText(MainWindow.getMessage("filter.player"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        playerLabel.setLayoutData(gridData);

        playerText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.FILL, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        playerText.setLayoutData(gridData);
        playerText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setPlayerNamePart(playerText.getText());
            }
        });
        //country
        countryLabel = new Label(filterPanel, SWT.NONE);
        countryLabel.setText(MainWindow.getMessage("filter.country"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        countryLabel.setLayoutData(gridData);

        countryText = new Text(filterPanel, SWT.BORDER);
        gridData = new GridData(SWT.FILL, SWT.LEFT, true, false);
        gridData.horizontalSpan = 2;
        countryText.setLayoutData(gridData);
        countryText.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {
                transferPlayerFilter.setCountryPart(countryText.getText());
            }
        });

        return filterPanel;
    }

    private Group createPlayerAbilitiesFilterPanel() {

        abilityfilterPanel = new Group(middlePanel, SWT.NONE);
        abilityfilterPanel.setText(MainWindow.getMessage("filter.ability"));
        abilityfilterPanel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        abilityfilterPanel.setLayout(new GridLayout(6, false));

        GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
        abilityfilterPanel.setLayoutData(data);
        abilityfilterPanel.addMenuDetectListener(new MenuDetectListener() {
            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(abilityfilterPanel);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
                item.setText(MainWindow.getMessage("ResetFilter"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        resetAbilityFilter();
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        // speed
        speedLabel = new Label(abilityfilterPanel, SWT.NONE);
        speedLabel.setText(MainWindow.getMessage("filter.speed"));
        GridData gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedLabel.setLayoutData(gridData);

        speedSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        speedSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedSpinner1.setLayoutData(gridData);
        speedSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SPEED, 1));

        speedSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        speedSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedSpinner2.setLayoutData(gridData);
        speedSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SPEED, 2));
        // heading
        headingLabel = new Label(abilityfilterPanel, SWT.NONE);
        headingLabel.setText(MainWindow.getMessage("filter.heading"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingLabel.setLayoutData(gridData);

        headingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        headingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingSpinner1.setLayoutData(gridData);
        headingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEAD, 1));

        headingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        headingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingSpinner2.setLayoutData(gridData);
        headingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEAD, 2));
        // fitness
        fitnessLabel = new Label(abilityfilterPanel, SWT.NONE);
        fitnessLabel.setText(MainWindow.getMessage("filter.fitness"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        fitnessLabel.setLayoutData(gridData);

        staminaSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        staminaSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        staminaSpinner1.setLayoutData(gridData);
        staminaSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STAMINA, 1));

        staminaSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        staminaSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        staminaSpinner2.setLayoutData(gridData);
        staminaSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STAMINA, 2));
        // shooting
        shootingLabel = new Label(abilityfilterPanel, SWT.NONE);
        shootingLabel.setText(MainWindow.getMessage("filter.shooting"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingLabel.setLayoutData(gridData);

        shootingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        shootingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingSpinner1.setLayoutData(gridData);
        shootingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SHOOT, 1));

        shootingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        shootingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingSpinner2.setLayoutData(gridData);
        shootingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SHOOT, 2));

        // passing
        passingLabel = new Label(abilityfilterPanel, SWT.NONE);
        passingLabel.setText(MainWindow.getMessage("filter.passing"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingLabel.setLayoutData(gridData);

        passingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        passingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingSpinner1.setLayoutData(gridData);
        passingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PASS, 1));

        passingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        passingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingSpinner2.setLayoutData(gridData);
        passingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PASS, 2));

        // dribbling
        dribblingLabel = new Label(abilityfilterPanel, SWT.NONE);
        dribblingLabel.setText(MainWindow.getMessage("filter.dribbling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingLabel.setLayoutData(gridData);

        dribblingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        dribblingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingSpinner1.setLayoutData(gridData);
        dribblingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.DRIBBLE, 1));

        dribblingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        dribblingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingSpinner2.setLayoutData(gridData);
        dribblingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.DRIBBLE, 2));
        // crossing
        crossingLabel = new Label(abilityfilterPanel, SWT.NONE);
        crossingLabel.setText(MainWindow.getMessage("filter.crossing"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingLabel.setLayoutData(gridData);

        crossingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        crossingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingSpinner1.setLayoutData(gridData);
        crossingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.CROSS, 1));

        crossingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        crossingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingSpinner2.setLayoutData(gridData);
        crossingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.CROSS, 2));
        // handling
        handlingLabel = new Label(abilityfilterPanel, SWT.NONE);
        handlingLabel.setText(MainWindow.getMessage("filter.handling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingLabel.setLayoutData(gridData);

        handlingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        handlingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingSpinner1.setLayoutData(gridData);
        handlingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HANDLING, 1));

        handlingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        handlingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingSpinner2.setLayoutData(gridData);
        handlingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HANDLING, 2));

        // tackling
        tacklingLabel = new Label(abilityfilterPanel, SWT.NONE);
        tacklingLabel.setText(MainWindow.getMessage("filter.tackling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingLabel.setLayoutData(gridData);

        tacklingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        tacklingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingSpinner1.setLayoutData(gridData);
        tacklingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TACKLE, 1));

        tacklingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        tacklingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingSpinner2.setLayoutData(gridData);
        tacklingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TACKLE, 2));
        // reflexes
        reflexesLabel = new Label(abilityfilterPanel, SWT.NONE);
        reflexesLabel.setText(MainWindow.getMessage("filter.reflexes"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesLabel.setLayoutData(gridData);

        reflexesSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        reflexesSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesSpinner1.setLayoutData(gridData);
        reflexesSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.REFLEXES, 1));

        reflexesSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        reflexesSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesSpinner2.setLayoutData(gridData);
        reflexesSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.REFLEXES, 2));

        return abilityfilterPanel;
    }

    private Group createLimitBidPlayersGroupPanel() {

        limitPlayersPanel = new Group(bottomPanel, SWT.NONE);
        limitPlayersPanel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        limitPlayersPanel.setText(MainWindow.getMessage("trans.limit_team_players"));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
        limitPlayersPanel.setLayoutData(gridData);
        limitPlayersPanel.setLayout(new GridLayout(13, false));
        //gk limit
        labelGk = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelGk.setLayoutData(gridData);

        spinnerGk = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerGk.setLayoutData(gridData);
        //dc limit
        labelDc = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelDc.setLayoutData(gridData);

        spinnerDc = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerDc.setLayoutData(gridData);

        //ml limit
        labelMl = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelMl.setLayoutData(gridData);

        spinnerMl = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerMl.setLayoutData(gridData);

        //mr limit
        labelMr = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelMr.setLayoutData(gridData);

        spinnerMr = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerMr.setLayoutData(gridData);

        //fc limit
        labelFc = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelFc.setLayoutData(gridData);

        spinnerFc = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerFc.setLayoutData(gridData);

        //limit totals
        labelTotals = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelTotals.setLayoutData(gridData);

        spinnerTotals = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        spinnerTotals.setLayoutData(gridData);

        //fake label
        Label fakeLabel2 = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        fakeLabel2.setLayoutData(gridData);

        //dl limit
        labelDl = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelDl.setLayoutData(gridData);

        spinnerDl = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerDl.setLayoutData(gridData);

        //dr limit
        labelDr = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelDr.setLayoutData(gridData);

        spinnerDr = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerDr.setLayoutData(gridData);

        //mc limit
        labelMc = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelMc.setLayoutData(gridData);

        spinnerMc = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerMc.setLayoutData(gridData);

        //fl limit
        labelFl = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelFl.setLayoutData(gridData);

        spinnerFl = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerFl.setLayoutData(gridData);

        //fr limit
        labelFr = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelFr.setLayoutData(gridData);

        spinnerFr = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
        spinnerFr.setLayoutData(gridData);

        //balance limit
        labelLimitBalance = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelLimitBalance.setLayoutData(gridData);

        spinnerLimitBalance = new Spinner(limitPlayersPanel, SWT.BORDER);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        spinnerLimitBalance.setLayoutData(gridData);
        spinnerLimitBalance.setMinimum(-999999);
        spinnerLimitBalance.setMaximum(999999);

        //label thousands
        labelThousandsShort = new Label(limitPlayersPanel, SWT.NONE);
        gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
        labelThousandsShort.setLayoutData(gridData);
        //set default values
        spinnerGk.setSelection(25);
        spinnerDc.setSelection(25);
        spinnerDl.setSelection(25);
        spinnerDr.setSelection(25);
        spinnerMc.setSelection(25);
        spinnerMl.setSelection(25);
        spinnerMr.setSelection(25);
        spinnerFc.setSelection(25);
        spinnerFl.setSelection(25);
        spinnerFr.setSelection(25);
        //TODO check if totals = sum of others ?
        spinnerTotals.setSelection(25);
        spinnerLimitBalance.setSelection(0);

        //set labels text
        labelGk.setText(MainWindow.getMessage("pos_gk"));
        labelDc.setText(MainWindow.getMessage("pos_dc"));
        labelDl.setText(MainWindow.getMessage("pos_dl"));
        labelDr.setText(MainWindow.getMessage("pos_dr"));
        labelMc.setText(MainWindow.getMessage("pos_mc"));
        labelMl.setText(MainWindow.getMessage("pos_ml"));
        labelMr.setText(MainWindow.getMessage("pos_mr"));
        labelFc.setText(MainWindow.getMessage("pos_fc"));
        labelFl.setText(MainWindow.getMessage("pos_fl"));
        labelFr.setText(MainWindow.getMessage("pos_fr"));
        labelTotals.setText(MainWindow.getMessage("trans.totals"));
        labelLimitBalance.setText(MainWindow.getMessage("trans.limit_balance"));
        labelThousandsShort.setText(MainWindow.getMessage("global.thousand_short_FC"));

        return limitPlayersPanel;
    }

    private void resetAbilityFilter() {
        //to avoid reaction on spinner's selection events
        filterReseting = true;
        transferPlayerFilter.resetAbilitiesFilter();

        speedSpinner1.setValues(transferPlayerFilter.getSpeed1(), 20, 299, 0, 1, 10);
        speedSpinner2.setValues(transferPlayerFilter.getSpeed2(), 20, 299, 0, 1, 10);

        staminaSpinner1.setValues(transferPlayerFilter.getStamina1(), 20, 299, 0, 1, 10);
        staminaSpinner2.setValues(transferPlayerFilter.getStamina2(), 20, 299, 0, 1, 10);

        passingSpinner1.setValues(transferPlayerFilter.getPassing1(), 20, 299, 0, 1, 10);
        passingSpinner2.setValues(transferPlayerFilter.getPassing2(), 20, 299, 0, 1, 10);

        crossingSpinner1.setValues(transferPlayerFilter.getCrossing1(), 20, 299, 0, 1, 10);
        crossingSpinner2.setValues(transferPlayerFilter.getCrossing2(), 20, 299, 0, 1, 10);

        tacklingSpinner1.setValues(transferPlayerFilter.getTackling1(), 20, 299, 0, 1, 10);
        tacklingSpinner2.setValues(transferPlayerFilter.getTackling2(), 20, 299, 0, 1, 10);

        headingSpinner1.setValues(transferPlayerFilter.getHeading1(), 20, 299, 0, 1, 10);
        headingSpinner2.setValues(transferPlayerFilter.getHeading2(), 20, 299, 0, 1, 10);

        shootingSpinner1.setValues(transferPlayerFilter.getShooting1(), 20, 299, 0, 1, 10);
        shootingSpinner2.setValues(transferPlayerFilter.getShooting2(), 20, 299, 0, 1, 10);

        dribblingSpinner1.setValues(transferPlayerFilter.getDribbling1(), 20, 299, 0, 1, 10);
        dribblingSpinner2.setValues(transferPlayerFilter.getDribbling2(), 20, 299, 0, 1, 10);

        handlingSpinner1.setValues(transferPlayerFilter.getHandling1(), 20, 299, 0, 1, 10);
        handlingSpinner2.setValues(transferPlayerFilter.getHandling2(), 20, 299, 0, 1, 10);

        reflexesSpinner1.setValues(transferPlayerFilter.getReflexes1(), 20, 299, 0, 1, 10);
        reflexesSpinner2.setValues(transferPlayerFilter.getReflexes2(), 20, 299, 0, 1, 10);

        filterReseting = false;
        redraw();
    }

    private void resetFilter() {
        //to avoid reaction on spinner's selection events
        filterReseting = true;
        transferPlayerFilter.resetMainFilter();

        ageSpinner1.setValues(transferPlayerFilter.getAge1(), 15, 99, 0, 1, 10);
        ageSpinner2.setValues(transferPlayerFilter.getAge2(), 15, 99, 0, 1, 10);

        wageSpinner1.setValues(transferPlayerFilter.getWage1(), 0, 999, 0, 1, 10);
        wageSpinner2.setValues(transferPlayerFilter.getWage2(), 0, 999, 0, 1, 10);

        healthSpinner1.setValues(transferPlayerFilter.getHealth1(), 20, 100, 0, 1, 10);
        healthSpinner2.setValues(transferPlayerFilter.getHealth2(), 20, 100, 0, 1, 10);

        ticketsSpinner1.setValues(transferPlayerFilter.getTickets1(), 0, 999, 0, 1, 10);
        ticketsSpinner2.setValues(transferPlayerFilter.getTickets2(), 0, 999, 0, 1, 10);

        priceSpinner1.setValues(transferPlayerFilter.getPrice1(), 0, 99999, 0, 10, 100);
        priceSpinner2.setValues(transferPlayerFilter.getPrice2(), 0, 99999, 0, 10, 100);

        strengthSpinner1.setValues(transferPlayerFilter.getStrength1(), 1, 100, 0, 1, 10);
        strengthSpinner2.setValues(transferPlayerFilter.getStrength2(), 1, 100, 0, 1, 10);

        talentSpinner1.setValues(transferPlayerFilter.getTalent1(), 20, 100, 0, 1, 10);
        talentSpinner2.setValues(transferPlayerFilter.getTalent2(), 20, 100, 0, 1, 10);

        expSpinner1.setValues(transferPlayerFilter.getExperiance1(), 1, 100, 0, 1, 10);
        expSpinner2.setValues(transferPlayerFilter.getExperiance2(), 1, 100, 0, 1, 10);

        playerText.setText(transferPlayerFilter.getPlayerNamePart());
        positionText.setText(transferPlayerFilter.getPositionNamePart());
        countryText.setText(transferPlayerFilter.getCountryPart());

        filterReseting = false;
        redraw();
    }

    protected void doApplyUseBankCoeff(boolean selected) {

        if (selected)
            buttonUseBankCoeff.setText(MainWindow.getMessage("trans.price_with_bank_coeff"));
        else
            buttonUseBankCoeff.setText(MainWindow.getMessage("trans.price_without_bank_coeff"));

        GuiLongTask task = new GuiLongTask() {

            @Override
            protected void execute() {
                redrawList();
            }
        };
        GuiUtils.showModalProgressWindow(parentInstance.getShell(), task, MainWindow.getMessage("global.pleaseWait"), false);

    }

    private void downloadTlist() {
        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.SAVE);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionTlist");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionTlistName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(TLIST_DEFAULT_FILE_NAME);
        final String tListPath = dlg.open();
        if (tListPath == null) {
            return;
        }
        GuiLongTask downloadTask = new GuiLongTask() {

            @Override
            protected void execute() {

                try {
                    int timeout = 9;//secs
                    RequestConfig config = RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000)
                            .setSocketTimeout(timeout * 1000).build();

                    CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

                    HttpHead head = new HttpHead(TLIST_URL);
                    CloseableHttpResponse response = httpClient.execute(head);
                    int status = response.getStatusLine().getStatusCode();

                    head.releaseConnection();
                    httpClient.close();
                    if (status == HttpStatus.SC_OK) {

                        GuiUtils.downloadUrlResource(TLIST_URL, tListPath);
                        openTList(tListPath);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"),
                            MainWindow.getMessage("FailedToDownloadTList"));
                }

            }
        };
        GuiUtils.showModalProgressWindow(this.parentInstance.getShell(), downloadTask, MainWindow.getMessage("DownloadingTlist"), false);

    }

    public void openTlistDlg() {
        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionTlist");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionTlistName");
        dlg.setFilterNames(extNames);
        dlg.setFileName(TLIST_DEFAULT_FILE_NAME);
        String result = dlg.open();
        if (result != null) {
            openTList(result);
        }
    }

    public void openTList(final String fname) {
        if (fname == null) {
            return;
        }

        GuiLongTask task = new GuiLongTask() {

            @Override
            protected void execute() {
                try {
                    tList = TransferListReader.readTransferListFile(fname);
                    redrawList();
                    openTransferBidButton.setVisible(true);
                    saveTransferBidButton.setVisible(true);
                    limitPlayersPanel.setVisible(true);
                } catch (ReaderException e) {
                    e.printStackTrace();
                    GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"),
                            MainWindow.getMessage("global.open_bid_error"));
                }
            }
        };
        GuiUtils.showModalProgressWindow(parentInstance.getShell(), task, MainWindow.getMessage("global.pleaseWait"), false);
    }

    private String bidFileName = null;

    public void openTransferBidDlg() {
        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterNames(extNames);
        dlg.setFileName(bidFileName == null ? getBidDefaultFileName() : bidFileName);
        String result = dlg.open();
        if (result != null) {
            openTransferBid(result);
        }
    }

    public void openTransferBid(String fname) {
        if (fname == null) {
            return;
        }
        try {

            transferBid = TransferReader.readTransferFile(fname);
            redrawBids();
            redrawBidLimits();
            redrawList();
            bidFileName = fname;
        } catch (ReaderException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.open_bid_error"));
        }
    }

    public void saveTransferBidDlg() {
        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.SAVE);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionRequest");
        dlg.setFilterNames(extNames);
        dlg.setFileName(getBidDefaultFileName());
        String result = dlg.open();
        if (result != null) {
            saveTransferBid(result);
        }
    }

    public void saveTransferBid(String fname) {
        if (fname == null) {
            return;
        }
        try {
            updateBidLimitsFromUI();
            if (MainWindow.getAllInstance() != null) {
                Team team = MainWindow.getAllInstance().getCurrentTeam();
                if (team != null)
                    transferBid.setTeamID(team.getId());
            }
            TransferWriter.writeTransferFile(fname, transferBid);
            bidFileName = fname;
            GuiUtils.showSuccessMessage(parentInstance.getShell(), MainWindow.getMessage("info"), MainWindow.getMessage("global.bidSaveSuccess"));
            //redrawBids();
        } catch (IOException e) {
            e.printStackTrace();
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("global.bidSaveError"));
        }
    }

    private String getBidDefaultFileName() {
        String bidFileName = "";

        if (MainWindow.getAllInstance() != null) {
            Team currentTeam = MainWindow.getAllInstance().getCurrentTeam();
            if (currentTeam != null) {
                bidFileName = "Trans" + currentTeam.getId() + ".f13";
            }
        }

        return bidFileName;
    }

    private void redrawList() {
        if (tList == null) {
            topPanel.setVisible(false);
            tableTList.setVisible(false);
            tableTList.setEnabled(false);
            dateLabel.setVisible(false);
            return;
        }

        Date date = tList.getDate();
        dateLabel.setText(dateFormat.format(date));
        dateLabel.pack();
        dateLabel.setVisible(true);
        bottomPanel.layout();
        playerList = filterTransferPlayers(tList.getPlayers());
        tableTList.setRedraw(false);
        tableTList.setFont(playersTableFont);
        tableTList.setItemCount(playerList.size());
        int curr = 0;
        int price = 0;
        for (TransferPlayer player : playerList) {
            TableItem item = tableTList.getItem(curr++);
            int index = 0;
            item.setText(index++, String.valueOf(player.getTransferID()));
            item.setText(index++, player.getPosition().toString());
            item.setText(index++, String.valueOf(player.getAge()));
            item.setText(index++, String.valueOf(player.getTalent()));
            item.setText(index++, String.valueOf(player.getStrength()));
            item.setText(index++, String.valueOf(player.getHealth()));
            item.setText(index++, GameUtils.getPlayerAbilitiesAsString(player));
            item.setText(index++, String.valueOf(player.getSalary()));

            price = player.getPrice();
            if (buttonUseBankCoeff.getSelection())
                price = (int) Math.round(price * 0.9);
            item.setText(index++, String.valueOf(price));
            item.setText(index++, "");
            item.setData(PROP_COUNTRY_CODE, player.getNationalityCode());
            item.setData(GameFormEditView.flags.get(player.getNationalityCode()));
            item.setText(index++, player.getName());
            item.setText(index++, player.getPreviousTeam());
            item.setText(index++, String.valueOf(player.getBirthtour()));
            colorizeTransferPlayer(player, item);
        }

        GuiUtils.autoAdjustTableColumnsWidth(tableTList);
        tableTList.setRedraw(true);
        tableTList.setEnabled(true);
        tableTList.setVisible(true);
        //dateLabel.setVisible(false);

        redrawBids();
        topPanel.setVisible(true);

    }

    private void colorizeTransferPlayer(TransferPlayer player, TableItem item) {
        bidList = transferBid.getBids();
        if (bidList == null || bidList.length == 0 || player == null || item == null) {
            item.setBackground(tableTList.getBackground());
            return;
        }
        for (TransferBid transferBid : bidList) {
            if (transferBid == null) {
                continue;
            }
            if (transferBid.getId() == player.getId() && transferBid.getName().equalsIgnoreCase(player.getName())) {
                item.setBackground(MainWindow.getSharedColorResource(ColorResourceType.SQUARD));
                return;
            }
        }
        item.setBackground(tableTList.getBackground());
    }

    private List<TransferPlayer> filterTransferPlayers(List<TransferPlayer> players) {

        List<TransferPlayer> filteredList = new ArrayList<TransferPlayer>();

        for (TransferPlayer transferPlayer : players) {

            if (transferPlayerFilter.validate(transferPlayer)) {
                filteredList.add(transferPlayer);
            }

        }

        return filteredList;
    }

    private void updateBidLimitsFromUI() {
        transferBid.setMaxGK(spinnerGk.getSelection());
        transferBid.setMaxCD(spinnerDc.getSelection());
        transferBid.setMaxLD(spinnerDl.getSelection());
        transferBid.setMaxRD(spinnerDr.getSelection());
        transferBid.setMaxCM(spinnerMc.getSelection());
        transferBid.setMaxLM(spinnerMl.getSelection());
        transferBid.setMaxRM(spinnerMr.getSelection());
        transferBid.setMaxCF(spinnerFc.getSelection());
        transferBid.setMaxLF(spinnerFl.getSelection());
        transferBid.setMaxRF(spinnerFr.getSelection());
        transferBid.setMaxPlayers(spinnerTotals.getSelection());
        transferBid.setMinCash(spinnerLimitBalance.getSelection());
    }

    private void redrawBidLimits() {

        if (transferBid == null) {
            limitPlayersPanel.setVisible(false);
            return;
        }

        spinnerGk.setSelection(transferBid.getMaxGK());
        spinnerDc.setSelection(transferBid.getMaxCD());
        spinnerDl.setSelection(transferBid.getMaxLD());
        spinnerDr.setSelection(transferBid.getMaxRD());
        spinnerMc.setSelection(transferBid.getMaxCM());
        spinnerMl.setSelection(transferBid.getMaxLM());
        spinnerMr.setSelection(transferBid.getMaxRM());
        spinnerFc.setSelection(transferBid.getMaxCF());
        spinnerFl.setSelection(transferBid.getMaxLF());
        spinnerFr.setSelection(transferBid.getMaxRF());
        spinnerTotals.setSelection(transferBid.getMaxPlayers());
        spinnerLimitBalance.setSelection(transferBid.getMinCash());

        limitPlayersPanel.setVisible(true);

    }

    void redrawBids() {
        if (transferBid == null) {
            tableBids.setVisible(false);
            return;
        }
        tableBids.setVisible(false);
        bidList = transferBid.getBids();
        if (bidList == null) {
            return;
        }
        tableBids.setRedraw(false);

        for (int i = 0; i < bidList.length; i++) {
            TransferBid bid = bidList[i];
            TableItem item = tableBids.getItem(i);
            if (bid == null) {
                item.setText(0, "");
                item.setText(1, "");
                item.setText(2, "");

            } else {
                item.setText(0, bid.getName());
                item.setText(1, String.valueOf(bid.getPrice()));
                Player player = MainWindow.getCurrentTeamPlayerByNumber(bid.getTradeIn());
                item.setText(2, player != null ? player.getName() : "");
            }
            item.setData("bidNumber", i);
        }
        tableBids.setRedraw(true);

        int width = 0;
        for (int i = 0; i < tableBids.getColumnCount(); i++) {
            GuiUtils.autoAdjustTableColumnWidth(tableBids, i);
            int widthColum = tableBids.getColumn(i).getWidth();
            width += widthColum;
        }

        int heightAll = 0;
        int widthAll = 0;
        FormData data = (FormData) tableBids.getLayoutData();
        width += tableBids.getBorderWidth() * 2;
        data.left = new FormAttachment(0, rightScrollComposite.getVerticalBar().getSize().x);
        //data.right = new FormAttachment(tableBids, width);
        data.right = new FormAttachment(100, 0);
        int height = tableBids.getItemCount() * tableBids.getItemHeight();
        height += tableBids.getHeaderHeight();
        height += tableBids.getBorderWidth() * 3;
        data.bottom = new FormAttachment(tableBids, height);
        heightAll += height;
        widthAll = width;

        widthAll = Math.max(widthAll, width);
        tableBids.getParent().layout();

        //rightComposite.setSize(widthAll + rightPanel.getVerticalBar().getSize().x, heightAll);

        rightScrollComposite.setMinWidth(widthAll);
        rightScrollComposite.setExpandHorizontal(true);
        Point computeSize = rightComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        rightComposite.setSize(computeSize);
        rightScrollComposite.setMinHeight(computeSize.y);

        //rightPanel.setMinHeight(heightAll);
        rightScrollComposite.layout();

        data = (FormData) rightScrollComposite.getLayoutData();
        int scrollWidth = rightScrollComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
        data.left = new FormAttachment(100, -scrollWidth);

        rightScrollComposite.getParent().layout();
        tableBids.setVisible(true);

    }

    public class OpenTlistSelectionListner implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            openTlistDlg();
        }
    }

    public class OpenTransferBidSelectionListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            openTransferBidDlg();
        }
    }

    public class SaveTransferBidSelectionListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            widgetDefaultSelected(e);
        }

        public void widgetDefaultSelected(SelectionEvent e) {
            saveTransferBidDlg();
        }
    }

    public void updateAll() {
        if (!needUpdate)
            return;
        redrawBids();
        needUpdate = false;
    }

    public void updatePassword(String password) {
        transferBid.setPassword(password);
    }

    public void updateMessages() {

        mainItem.setText(MainWindow.getMessage("transferTabName"));
        openTListButton.setText(MainWindow.getMessage("openTListButton"));
        openTransferBidButton.setText("  " + MainWindow.getMessage("global.open_bid") + "  ");
        saveTransferBidButton.setText(MainWindow.getMessage("global.save_bid"));
        limitPlayersPanel.setText(MainWindow.getMessage("trans.limit_team_players"));
        bDownloadTlist.setText(MainWindow.getMessage("Download"));
        bDownloadTlist.setToolTipText(MainWindow.getMessage("DownloadingTlist"));

        labelGk.setText(MainWindow.getMessage("pos_gk"));
        labelDc.setText(MainWindow.getMessage("pos_dc"));
        labelDl.setText(MainWindow.getMessage("pos_dl"));
        labelDr.setText(MainWindow.getMessage("pos_dr"));
        labelMc.setText(MainWindow.getMessage("pos_mc"));
        labelMl.setText(MainWindow.getMessage("pos_ml"));
        labelMr.setText(MainWindow.getMessage("pos_mr"));
        labelFc.setText(MainWindow.getMessage("pos_fc"));
        labelFl.setText(MainWindow.getMessage("pos_fl"));
        labelFr.setText(MainWindow.getMessage("pos_fr"));
        labelTotals.setText(MainWindow.getMessage("trans.totals"));
        labelLimitBalance.setText(MainWindow.getMessage("trans.limit_balance"));
        labelThousandsShort.setText(MainWindow.getMessage("global.thousand_short_FC"));

        buttonUseBankCoeff.setToolTipText(MainWindow.getMessage("trans.bank_coeff"));
        if (buttonUseBankCoeff.getSelection())
            buttonUseBankCoeff.setText(MainWindow.getMessage("trans.price_with_bank_coeff"));
        else
            buttonUseBankCoeff.setText(MainWindow.getMessage("trans.price_without_bank_coeff"));

        Point computeSize = buttonUseBankCoeff.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        buttonUseBankCoeff.setSize(computeSize);

        String[] titles = { MainWindow.getMessage("PlayerName"), MainWindow.getMessage("PlayerPrice"), MainWindow.getMessage("tradein") };
        String[] tooltips = { "", MainWindow.getMessage("filter.price"), "" };
        for (int i = 0; i < titles.length; i++) {
            TableColumn column = tableBids.getColumn(i);
            if (column != null) {
                column.setText(titles[i]);
                column.setToolTipText(tooltips[i]);
            }
            GuiUtils.autoAdjustTableColumnWidth(tableBids, i);
        }

        String[] t1 = { MainWindow.getMessage("PlayerNumber"), MainWindow.getMessage("PlayerPosition"), MainWindow.getMessage("PlayerAge"),
                MainWindow.getMessage("PlayerTalant"), MainWindow.getMessage("PlayerStrength"), MainWindow.getMessage("PlayerHealth"),
                MainWindow.getMessage("PlayerAbilities"), MainWindow.getMessage("PlayerSalary"), MainWindow.getMessage("PlayerPrice"),
                MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("seller"),
                MainWindow.getMessage("PlayerBirthTour") };

        String[] t2 = { MainWindow.getMessage("tooltip.Number"), MainWindow.getMessage("tooltip.Position"), MainWindow.getMessage("tooltip.Age"),
                MainWindow.getMessage("tooltip.Talent"), MainWindow.getMessage("tooltip.Strength"), MainWindow.getMessage("tooltip.Health"),
                MainWindow.getMessage("tooltip.Skills"), MainWindow.getMessage("tooltip.Salary"), MainWindow.getMessage("filter.price"),
                MainWindow.getMessage("PlayerCountry"), MainWindow.getMessage("PlayerName"), MainWindow.getMessage("seller"),
                MainWindow.getMessage("PlayerBirthTour") };
        titlesTList = t1;
        tooltipsTList = t2;
        tableTList.setRedraw(false);
        for (int i = 0; i < titlesTList.length; i++) {
            TableColumn column = tableTList.getColumn(i);
            if (column != null) {
                column.setText(titlesTList[i]);
                column.setToolTipText(tooltipsTList[i]);
            }
        }
        GuiUtils.autoAdjustTableColumnsWidth(tableTList);
        tableTList.setRedraw(true);

        ageLabel.setText(MainWindow.getMessage("filter.age"));
        strengthLabel.setText(MainWindow.getMessage("filter.strength"));
        wageLabel.setText(MainWindow.getMessage("filter.wage"));
        talentLabel.setText(MainWindow.getMessage("filter.talent"));
        healthLabel.setText(MainWindow.getMessage("filter.health"));
        expLabel.setText(MainWindow.getMessage("filter.experiance"));
        ticketsLabel.setText(MainWindow.getMessage("filter.tickets"));
        positionLabel.setText(MainWindow.getMessage("filter.position"));
        priceLabel.setText(MainWindow.getMessage("filter.price"));
        userChoiseButton.setText(MainWindow.getMessage("filter.userChoise"));
        playerLabel.setText(MainWindow.getMessage("filter.player"));
        countryLabel.setText(MainWindow.getMessage("filter.country"));

        speedLabel.setText(MainWindow.getMessage("filter.speed"));
        headingLabel.setText(MainWindow.getMessage("filter.heading"));
        fitnessLabel.setText(MainWindow.getMessage("filter.fitness"));
        shootingLabel.setText(MainWindow.getMessage("filter.shooting"));
        passingLabel.setText(MainWindow.getMessage("filter.passing"));
        dribblingLabel.setText(MainWindow.getMessage("filter.dribbling"));
        crossingLabel.setText(MainWindow.getMessage("filter.crossing"));
        handlingLabel.setText(MainWindow.getMessage("filter.handling"));
        tacklingLabel.setText(MainWindow.getMessage("filter.tackling"));
        reflexesLabel.setText(MainWindow.getMessage("filter.reflexes"));

        filterPanel.setText(MainWindow.getMessage("filter"));
        abilityfilterPanel.setText(MainWindow.getMessage("filter.ability"));
        applyFiltersButton.setToolTipText(MainWindow.getMessage("applyFilter"));
        String text = showHideFilterButton.getSelection() ? MainWindow.getMessage("hideFilter") : MainWindow.getMessage("showFilter");
        showHideFilterButton.setToolTipText(text);
        checkButtonUseFilter.setText(MainWindow.getMessage("filter"));
        checkButtonUseAbilityFilter.setText(MainWindow.getMessage("filter.ability"));
        computeSize = checkButtonUseFilter.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        checkButtonUseFilter.setSize(computeSize);
        computeSize = checkButtonUseAbilityFilter.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        checkButtonUseAbilityFilter.setSize(computeSize);
        rightComposite.layout();
        rightScrollComposite.layout();
        topPanel.layout();
        bottomPanel.layout();

        //need to call redraw because some layout done there
        redraw();

    }

    public void setDragDropSource(final Table table) {
        org.eclipse.swt.dnd.Transfer[] types = new org.eclipse.swt.dnd.Transfer[] { TextTransfer.getInstance() };
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        final DragSource source = new DragSource(table, operations);
        source.setTransfer(types);
        source.addDragListener(new DragSourceListener() {
            public void dragStart(DragSourceEvent event) {
                TableItem items[] = table.getSelection();

                if (items.length <= 0) {
                    event.doit = false;
                    return;
                }
                if (items[0].getText().length() == 0) {
                    event.doit = false;
                }
            }

            public void dragSetData(DragSourceEvent event) {
                TableItem items[] = table.getSelection();
                StringBuffer stringBuffer = new StringBuffer();
                for (int i = 0; i < items.length; i++) {
                    for (int j = 0; j < table.getColumnCount(); j++) {
                        stringBuffer.append(items[i].getText(j));
                        stringBuffer.append("/");
                    }

                }
                int selIndex = table.getSelectionIndex();
                lastDNDTlistSelectedIndex = selIndex;
                stringBuffer.append(String.valueOf(playerList.get(selIndex).getId()));
                stringBuffer.append("/");
                stringBuffer.append("\n");
                event.data = stringBuffer.toString();
            }

            public void dragFinished(DragSourceEvent event) {

            }
        });
    }

    private int lastDNDTlistSelectedIndex;

    public void setDragDropTarget(final Table table) {
        int operations = DND.DROP_MOVE | DND.DROP_COPY | DND.DROP_DEFAULT;
        DropTarget target = new DropTarget(table, operations);

        final TextTransfer textTransfer = TextTransfer.getInstance();
        org.eclipse.swt.dnd.Transfer types[] = new org.eclipse.swt.dnd.Transfer[] { textTransfer };
        target.setTransfer(types);

        target.addDropListener(new DropTargetAdapter() {

            public void drop(DropTargetEvent event) {
                if (textTransfer.isSupportedType(event.currentDataType)) {
                    TableItem item = null;
                    if (event.item instanceof TableItem) {
                        String text = (String) event.data;
                        item = (TableItem) event.item;
                        setPlayer(item, text);
                    }
                }
            }
        });

    }

    public void setPlayer(TableItem item, String playerData) {
        if (playerData == null) {
            return;
        }
        String playerItems[] = playerData.split("/");
        Integer number = (Integer) item.getData("bidNumber");
        if (number != null) {
            if (0 <= number && number <= 19) {
                bidList[number] = new TransferBid();
                bidList[number].setName(playerItems[10]);
                bidList[number].setId(Integer.valueOf(playerItems[13]));//12
                bidList[number].setPreviousTeam(playerItems[11]);
                redrawList();
                if (lastDNDTlistSelectedIndex != -1) {
                    tableTList.setSelection(lastDNDTlistSelectedIndex);
                }

            }
        }
        redrawBids();
    }

    @Override
    public void redraw() {
        redrawBids();
        redrawList();
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
        if (!lazyUpdate) {
            updateAll();
        }
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    @Override
    public void store(Properties props) {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        imgNoFlag.dispose();
        imageOpenTList.dispose();
        imageFilterWindowHide.dispose();
        imageFilterWindowShow.dispose();
        imgDownload.dispose();
        imgTransfer.dispose();
    }

    public enum FilterSpinner {
        AGE, WAGE, HEALTH, TICKETS, PRICE, STRENGTH, TALENT, EXPERIANCE, SPEED, STAMINA, PASS, CROSS, TACKLE, HEAD, SHOOT, DRIBBLE, HANDLING, REFLEXES;
    }

    private class FilterSpinnerSelectionListener implements SelectionListener {

        private FilterSpinner filterSpinner;
        private int spinnerNumber;

        public FilterSpinnerSelectionListener(FilterSpinner filterSpinner, int spinnerNumber) {
            this.filterSpinner = filterSpinner;
            this.spinnerNumber = spinnerNumber;
        }

        @Override
        public void widgetSelected(SelectionEvent e) {
            if (filterReseting) {
                return;
            }
            switch (filterSpinner) {
                case AGE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setAge1(ageSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setAge2(ageSpinner2.getSelection());
                    }
                    break;
                case CROSS:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setCrossing1(crossingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setCrossing2(crossingSpinner2.getSelection());
                    }
                    break;
                case DRIBBLE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setDribbling1(dribblingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setDribbling2(dribblingSpinner2.getSelection());
                    }
                    break;
                case EXPERIANCE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setExperiance1(expSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setExperiance2(expSpinner2.getSelection());
                    }
                    break;
                case HANDLING:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setHandling1(handlingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setHandling2(handlingSpinner2.getSelection());
                    }
                    break;
                case HEAD:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setHeading1(headingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setHeading2(headingSpinner2.getSelection());
                    }
                    break;
                case HEALTH:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setHealth1(healthSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setHealth2(healthSpinner2.getSelection());
                    }
                    break;
                case PASS:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setPassing1(passingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setPassing2(passingSpinner2.getSelection());
                    }
                    break;
                case PRICE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setPrice1(priceSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setPrice2(priceSpinner2.getSelection());
                    }
                    break;
                case REFLEXES:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setReflexes1(reflexesSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setReflexes2(reflexesSpinner2.getSelection());
                    }
                    break;
                case SHOOT:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setShooting1(shootingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setShooting2(shootingSpinner2.getSelection());
                    }
                    break;
                case SPEED:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setSpeed1(speedSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setSpeed2(speedSpinner2.getSelection());
                    }
                    break;
                case STAMINA:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setStamina1(staminaSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setStamina2(staminaSpinner2.getSelection());
                    }
                    break;
                case STRENGTH:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setStrength1(strengthSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setStrength2(strengthSpinner2.getSelection());
                    }
                    break;
                case TACKLE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setTackling1(tacklingSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setTackling2(tacklingSpinner2.getSelection());
                    }
                    break;
                case TALENT:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setTalent1(talentSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setTalent2(talentSpinner2.getSelection());
                    }
                    break;
                case TICKETS:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setTickets1(ticketsSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setTickets2(ticketsSpinner2.getSelection());
                    }
                    break;
                case WAGE:
                    if (spinnerNumber == 1) {
                        transferPlayerFilter.setWage1(wageSpinner1.getSelection());
                    } else {
                        transferPlayerFilter.setWage2(wageSpinner2.getSelection());
                    }
                    break;
                default:
                    break;
            }

        }

        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }

    }

    private SelectionAdapter slDownload = new SelectionAdapter() {

        @Override
        public void widgetSelected(SelectionEvent e) {

            downloadTlist();

        }

    };

}
