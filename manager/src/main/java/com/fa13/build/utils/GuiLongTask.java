package com.fa13.build.utils;

public abstract class GuiLongTask {
    private Runnable run = new Runnable() {

        @Override
        public void run() {

            execute();
            if (getOnFinishExecute() != null) {
                getOnFinishExecute().execute();
            }
        }
    };

    public interface IOnFinish {
        public void execute();
    }

    public interface IOnProgress {
        public void progress(int percent, String message);
    }

    private IOnFinish onFinishExecute;
    private IOnProgress onProgressExecute;
    /**
     * body of work to do.
     * Here we can notify about progress of work with {@link #progress(int,String)}
     */
    protected abstract void execute();

    public void progress(int percent, String message) {
        if (getOnProgressExecute() != null) {
            getOnProgressExecute().progress(percent, message);
        }
    }

    public void start() {
        run.run();
    }

    public IOnFinish getOnFinishExecute() {
        return onFinishExecute;
    }

    public void setOnFinishExecute(IOnFinish onFinishExecute) {
        this.onFinishExecute = onFinishExecute;
    }

    public IOnProgress getOnProgressExecute() {
        return onProgressExecute;
    }

    public void setOnProgressExecute(IOnProgress onProgressExecute) {
        this.onProgressExecute = onProgressExecute;
    }

}
