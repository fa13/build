package com.fa13.build.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class HttpUtils {
    /*
    private static final HttpConnectionManager CONNECTION_MANAGER = new MultiThreadedHttpConnectionManager();
    private static final HttpClient HTTP_CLIENT = new HttpClient(CONNECTION_MANAGER);
    
    public static String getRemoteContent(String uri) throws IOException {
        GetMethod method = new GetMethod(uri);
        int statusCode = HTTP_CLIENT.executeMethod(method);
        if (statusCode >= 300 || statusCode < 200) {
            throw new IOException("Could not get content from " + uri + ". Remote host answered HTTP " + statusCode);
        }
        return method.getResponseBodyAsString();
    }
    */

    private static String USER_AGENT = "java_build";

    public static String getRemoteContent(String uri) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createSystem();
        HttpGet httpGet = new HttpGet(uri);
        httpGet.addHeader("User-Agent", USER_AGENT);
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode >= 300 || statusCode < 200) {
            throw new IOException("Could not get content from " + uri + ". Remote host answered HTTP " + statusCode);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine).append(System.lineSeparator());
        }
        reader.close();

        //System.out.println(response.toString());
        httpClient.close();

        return response.toString();
    }

    /**
     * Method that builds the multi-part form data request. 
     * File objects are also supported as params.
     * @param urlString the urlString to which the file needs to be uploaded
     * @return server response as <code>String</code>
     */
    public static String executeMultiPartRequest(String uri, Map<String, Object> params) {

        HttpPost postRequest = new HttpPost(uri);
        MultipartEntityBuilder mpeBuilder = MultipartEntityBuilder.create();
        if (params != null) {
            for (Map.Entry<String, Object> paramEntry : params.entrySet()) {
                if (paramEntry.getValue() instanceof String) {
                    mpeBuilder.addTextBody(paramEntry.getKey(), (String) paramEntry.getValue());
                } else if (paramEntry.getValue() instanceof File) {
                    mpeBuilder.addBinaryBody(paramEntry.getKey(), (File) paramEntry.getValue());
                }
            }
        }
        postRequest.setEntity(mpeBuilder.build());
        CloseableHttpClient httpClient = HttpClients.createSystem();
        try {
            CloseableHttpResponse httpResponse = httpClient.execute(postRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            if (statusCode >= 300 || statusCode < 200) {
                throw new IOException("Could not get content from " + uri + ". Remote host answered HTTP " + statusCode);
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            reader.close();

            //System.out.println(response.toString());
            httpClient.close();

            return response.toString();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

}