package com.fa13.build.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.SystemUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.fa13.build.view.MainWindow;

public class GuiUtils {

    public static final String LINE_BREAK = "\r\n";
    public static final Charset WIN_CHARSET = Charset.forName("Cp1251");
    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    static {
        //fix need to avoid infinite redirecting loop on tlist download
        java.net.CookieManager cm = new java.net.CookieManager();
        java.net.CookieHandler.setDefault(cm);
    }

    public static void showModalProgressWindow(final Shell parentShell, final GuiLongTask task, String text, boolean useProgressBar) {
        final Shell dialog = new Shell(parentShell, SWT.BORDER | SWT.APPLICATION_MODAL);
        Rectangle ca = parentShell.getClientArea();
        int dlgW = 400;
        int dlgH = 80;
        int x = (ca.width - dlgW) / 2;
        int y = (ca.height - dlgH) / 2;
        dialog.setBounds(x, y, dlgW, dlgH);

        dialog.setLayout(new GridLayout(1, true));
        final Composite contents = new Composite(dialog, SWT.FILL | SWT.BORDER);
        contents.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout gl = new GridLayout(1, true);
        contents.setLayout(gl);
        final Label l = new Label(contents, SWT.CENTER);
        l.setText(text);
        l.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        ProgressBar pb = null;
        if (useProgressBar) {
            pb = new ProgressBar(contents, SWT.HORIZONTAL | SWT.FILL / SWT.INDETERMINATE);
            pb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
            pb.setMinimum(0);
            pb.setMaximum(100);
        }
        contents.pack();
        contents.layout();
        // dialog.pack();
        dialog.open();

        // dialog.layout();
        task.setOnFinishExecute(new GuiLongTask.IOnFinish() {

            @Override
            public void execute() {
                dialog.close();
                dialog.dispose();
            }
        });
        if (useProgressBar) {
            final ProgressBar pb2 = pb;
            task.setOnProgressExecute(new GuiLongTask.IOnProgress() {

                @Override
                public void progress(final int percent, final String message) {

                    if (!pb2.isDisposed()) {
                        pb2.setSelection(percent);
                        pb2.update();
                        pb2.redraw();
                    }
                    if (!l.isDisposed()) {
                        l.setText(message);
                        l.update();
                    }
                    if (!dialog.isDisposed()) {
                        dialog.layout();
                        dialog.update();
                    }

                }
            });
        } else {
            //only show progress text changed
            task.setOnProgressExecute(new GuiLongTask.IOnProgress() {

                @Override
                public void progress(final int percent, final String message) {

                    if (!l.isDisposed()) {
                        l.setText(message);
                        l.update();
                    }
                    if (!dialog.isDisposed()) {
                        dialog.layout();
                        dialog.update();
                    }

                }
            });

        }
        task.start();

    }

    public static void showErrorMessage(Shell shell, String title, String text) {
        MessageBox dlg = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
        dlg.setText(title);
        dlg.setMessage(text);
        dlg.open();
    }

    public static void showSuccessMessage(Shell shell, String title, String text) {
        MessageBox dlg = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
        dlg.setText(title);
        dlg.setMessage(text);
        dlg.open();
    }

    // to open URI in external browser
    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // to open URL in external browser
    public static void openWebpage(URL url) {
        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void openWebpage(String urlString) {
        try {
            openWebpage(new URL(urlString));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static final int DOWNLOAD_CONNECTION_TIMEOUT = 10 * 1000;

    public static void downloadUrlResource(String imageUrl, String destinationFile) throws Exception {

        File fileToSave = new File(destinationFile);
        if (!fileToSave.getParentFile().exists()) {
            fileToSave.getParentFile().mkdirs();
        }
        fileToSave.createNewFile();
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(fileToSave);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    /**
     * Updates various text controls such as Label, Button etc
     * 
     */
    public static void updateTextControls(Map<Object, String> textControlsToUpdate) {

        if (textControlsToUpdate != null && textControlsToUpdate.entrySet() != null)
            for (Map.Entry<Object, String> entry : textControlsToUpdate.entrySet()) {
                String text = MainWindow.getMessage(entry.getValue());
                if (entry.getKey() instanceof Label) {
                    ((Label) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof Button) {
                    ((Button) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof TabItem) {
                    ((TabItem) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof Group) {
                    ((Group) entry.getKey()).setText(text);
                }

            }

    }

    public static int getComboItemIndex(Combo combo, String itemName) {

        for (int i = 0; i < combo.getItemCount(); i++) {
            if (combo.getItem(i).equals(itemName)) {
                return i;
            }
        }
        return -1;
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Font createFont(Font parentFont, int height, int style) {
        FontData fontData = parentFont.getFontData()[0];
        fontData.setHeight(height);
        fontData.setStyle(style);
        return new Font(parentFont.getDevice(), fontData);
    }

    public static void selectComboItem(Combo combo, String selectedItem) {
        String[] items = combo.getItems();
        for (int i = 0; i < items.length; i++) {
            if (items[i].equals(selectedItem)) {
                combo.select(i);
                return;
            }
        }
    }

    public static Image transformImageColors(ImageData srcImageData, Map<RGB, RGB> mapTranformColors) {

        ImageData imgData = (ImageData) srcImageData.clone();
        PaletteData palette = new PaletteData(0xff0000, 0x00ff00, 0x0000ff);
        for (int x = 0; x < imgData.width; x++) {
            for (int y = 0; y < imgData.height; y++) {

                RGB rgb = palette.getRGB(imgData.getPixel(x, y));
                RGB newRgb = rgb;
                for (Map.Entry<RGB, RGB> entry : mapTranformColors.entrySet()) {
                    if (entry.getKey().equals(rgb)) {
                        newRgb = entry.getValue();
                    }
                }

                imgData.setPixel(x, y, palette.getPixel(newRgb));

            }
        }
        return new Image(Display.getDefault(), imgData);

    }

    public static Point getTextExtent(String string, Font font) {
        GC gc = new GC(Display.getDefault());
        gc.setAdvanced(true);
        gc.setTextAntialias(SWT.ON);
        gc.setFont(font);
        final Point textExtent = gc.textExtent(string, SWT.DRAW_DELIMITER);
        gc.dispose();
        return textExtent;
    }

    public static int getColumnMinWidth(Table table, int col) {

        String maxS = table.getColumn(col).getText();
        int maxWidth = GuiUtils.getTextExtent(maxS + "W", table.getFont()).x;
        for (int i = 0; i < table.getItemCount(); i++) {
            TableItem ti = table.getItem(i);
            String s = ti.getText(col);
            if (s == null || s.isEmpty()) {
                s = ".";
            } else {
                s += "WW";
            }

            int w = GuiUtils.getTextExtent(s, ti.getFont(i)).x;
            if (w > maxWidth) {
                maxWidth = w;
            }
        }

        return maxWidth;
    }

    public static void autoAdjustTableColumnsWidth(Table table) {
        table.setRedraw(false);
        for (int i = 0; i < table.getColumnCount(); i++) {
            autoAdjustTableColumnWidth(table, i);
        }
        table.setRedraw(true);
    }

    public static void autoAdjustTableColumnWidth(Table table, int columnIndex) {
        TableColumn column = table.getColumn(columnIndex);
        if (SystemUtils.IS_OS_WINDOWS) {
            column.pack();
        } else {
            column.setWidth(getColumnMinWidth(table, columnIndex));
        }
    }

    public static void initTable(Table table, String[] columnTitles, String[] columnTooltips, int columnAlign, boolean isColumResize) {

        table.setRedraw(false);

        while (table.getColumnCount() > 0) {
            table.getColumns()[0].dispose();
        }

        for (int i = 0; i < columnTitles.length; i++) {
            TableColumn column = new TableColumn(table, columnAlign);
            column.setText(columnTitles[i]);
            if (columnTooltips != null && i < columnTooltips.length) {
                column.setToolTipText(columnTooltips[i]);
            }
            column.setResizable(isColumResize);
            autoAdjustTableColumnWidth(table, i);
        }

        table.setRedraw(true);

    }

    public static void addAutoCompleteFeature(Combo combo) {
        // Add a key listener
        combo.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent keyEvent) {
                Combo cmb = ((Combo) keyEvent.getSource());
                setClosestMatch(cmb);
            }

            // Move the highlight back by one character for backspace
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.keyCode == SWT.BS) {
                    Combo cmb = ((Combo) keyEvent.getSource());
                    Point pt = cmb.getSelection();
                    cmb.setSelection(new Point(Math.max(0, pt.x - 1), pt.y));
                }
            }

            private void setClosestMatch(Combo combo) {
                String str = combo.getText();
                String[] cItems = combo.getItems();
                // Find Item in Combo Items. If full match returns index
                int index = -1;
                for (int i = 0; i < cItems.length; i++) {
                    if (cItems[i].toLowerCase().startsWith(str.toLowerCase())) {
                        index = i;
                        break;
                    }
                }

                if (index != -1) {
                    Point pt = combo.getSelection();
                    combo.select(index);
                    combo.setText(cItems[index]);
                    combo.setSelection(new Point(pt.x, cItems[index].length()));
                } else {
                    combo.setText("");
                }
            }
        });
    }

}