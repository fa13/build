package com.fa13.build.model.calc;

import java.util.ArrayList;
import java.util.List;

import com.fa13.build.model.All;
import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.Team;

public class PlayerPrice {

    public static final boolean isPlayersAreSimilar(Player p1, Player p2, int ageDiff, int talentDiff, int strengthDiff, int expDiff) {

        boolean res = (Math.abs(p1.getAge() - p2.getAge()) <= ageDiff) && (Math.abs(p1.getTalent() - p2.getTalent()) <= talentDiff)
                && (Math.abs(p1.getStrength() - p2.getStrength()) <= strengthDiff) && (Math.abs(p1.getExperience() - p2.getExperience()) <= expDiff);
        res = res && p2.getPosition().toString().substring(1).equalsIgnoreCase(p1.getPosition().toString().substring(1));

        return res;

    }

    /**
     * 
     * @param p1 player that price we want to calculate
     * @param p2 base player
     * @return p1 player's price based on price of p2 player
     */
    public static final double getPriceByPlayer(Player p1, Player p2) {
        double k = ((double) p1.getHealth() / (double) p2.getHealth())
                * ((double) getPlayerAbilitiesTotal(p1) / (double) getPlayerAbilitiesTotal(p2));
        k = (double) p2.getPrice() * k * (p2.getPosition().equals(p1.getPosition()) ? 1.0 : 0.9);
        return k;
    }

    public static int getPlayerAbilitiesTotal(Player p) {

        int res = p.getDribbling() + p.getCross() + p.getHandling() + p.getHeading();
        res += p.getPassing() + p.getReflexes() + p.getShooting() + p.getSpeed() + p.getStamina() + p.getTackling();

        return res;
    }

    public static int getPlayerAbilitiesTotalMinus20(Player p) {

        int res = getPlayerAbilitiesTotal(p) - 10 * 20;

        return res;
    }

    public static double getPredictionPrice(PlayerCalcInfo playerCalcInfo, List<Player> calcList) {
        if (playerCalcInfo == null || calcList == null || calcList.isEmpty()) {
            return 0;
        }
        double sum1 = 0, sum2 = 0, pos_weight = 1;
        Player p = playerCalcInfo.getPlayer();
        for (Player p2 : calcList) {
            pos_weight = (p2.getPosition().equals(p.getPosition()) ? 1.0 : 0.9);
            sum1 = sum1 + getPriceByPlayer(p, p2) * pos_weight;
            sum2 = sum2 + pos_weight;
        }
        if (Math.abs(sum2) < 0.0001) {
            //System.out.println(p.getName() + " : " + getPlayerAbilitiesTotal(p));
        }
        return sum1 / sum2;
    }

    /**
     * Fa13 Official Algorithm
     * @param choosenPlayerInfo
     * @param all
     * @return
     */
    public static double[] getPredictionPriceFa13(PlayerCalcInfo choosenPlayerInfo, All all, int likePlayersCountToFinishCalc) {
        if (all == null) {
            return null;
        }
        Player player = choosenPlayerInfo.getPlayer();

        //List<CalcStepInfo> stepInfoList = new ArrayList<CalcStepInfo>();
        for (int step = 1; step <= 12; step++) {
            double ageDiff = 0.25 * step;
            double talentDiff = 2 * step;
            double strengthDiff = step;

            CalcStepInfo stepInfo = new CalcStepInfo(choosenPlayerInfo);
            stepInfo.step = step;
            //stepInfoList.add(stepInfo);
            for (Team t : all.getTeams()) {
                for (Player p : t.getPlayers()) {
                    if (p != player && Math.abs(p.getAge() - player.getAge()) < ageDiff && Math.abs(p.getTalent() - player.getTalent()) < talentDiff
                            && Math.abs(p.getStrength() - player.getStrength()) < strengthDiff
                            && p.getPosition().toString().substring(1).equalsIgnoreCase(player.getPosition().toString().substring(1))) {
                        PlayerPriceFa13Calc pp = new PlayerPriceFa13Calc(p);
                        stepInfo.players.add(pp);
                    }
                }
            }
            stepInfo.calc();
            if (stepInfo.players.size() >= likePlayersCountToFinishCalc) {
                //System.out.println("Player: " + player.getName() + "[" + player.getAge() + "/" + player.getStrength() + "/"
                //        + choosenPlayerInfo.getAbilitiesTotal() + "]" + " price calculated on " + step + " step");

                double[] res = new double[2];
                res[0] = stepInfo.price;
                res[1] = stepInfo.players.size();
                return res;
            }
        }

        return null;
    }

    public static class CalcStepInfo {
        public double bankPercent = 10;
        public int step = 1;
        public int transAgo = 0;
        public double avgAge, avgTalent, avgStrength, avgAbilities, avgHealth, avgPrice, price;
        public List<PlayerPriceFa13Calc> players = new ArrayList<PlayerPriceFa13Calc>();
        public double ageWeight, talentWeight, strengthWeight, healthWeight, abilitiesWeight;

        private PlayerCalcInfo player;
        private static final String L = PlayerAmplua.LD.toString().substring(0, 1);
        private static final String R = PlayerAmplua.RD.toString().substring(0, 1);

        public CalcStepInfo(PlayerCalcInfo player) {
            this.player = player;
        }

        public boolean positionsAreSimilar(PlayerAmplua pos1, PlayerAmplua pos2) {
            boolean res = pos1.equals(pos2);
            if (res) {
                return res;
            } else {
                if (pos1.equals(PlayerAmplua.GK) || pos2.equals(PlayerAmplua.GK)) {
                    return false;
                }
                //check
                String s1 = pos1.toString().substring(1);
                String s2 = pos2.toString().substring(1);
                if (!s1.equalsIgnoreCase(s2)) {
                    return false;
                } else {
                    s1 = pos1.toString().substring(0, 1);
                    s2 = pos2.toString().substring(0, 1);
                    return (s1.equalsIgnoreCase(L) && s2.equalsIgnoreCase(R)) || (s1.equalsIgnoreCase(R) && s2.equalsIgnoreCase(L));
                }
            }
        }
/**
 * transAgo is ALWAYS 0 because of its absent in ALL file.
 * ALL file doesm't have ALL data from last 12 trans.
 * So the calc will not give the same PRICE.
 */
        public void calc() {
            if (players.size() < 1) {
                return;
            }
            //1. calc avgs
            double ageSum = 0;
            double ageWeightSum = 0;
            double talentSum = 0;
            double talentWeightSum = 0;
            double strengthSum = 0;
            double strengthWeightSum = 0;
            double abilitiesSum = 0;
            double abilitiesWeightSum = 0;
            double healthSum = 0;
            double healthWeightSum = 0;
            double priceSum = 0;
            double priceWeightSum = 0;

            for (PlayerPriceFa13Calc pp : players) {

                double posWeight = pp.getPlayer().getPosition().toString().substring(1)
                        .equalsIgnoreCase(player.getPlayer().getPosition().toString().substring(1)) ? 1 : 0;
                //posWeight *= (pp.getPlayer().getPosition().equals(player.getPlayer().getPosition()) ? 1.0 : 0.9);
                posWeight *= (positionsAreSimilar(pp.getPlayer().getPosition(), player.getPlayer().getPosition()) ? 1.0 : 0.9);
                double weight = (1 - transAgo * 0.05) * (1.01 - step * 0.01) * posWeight;
                if (weight < 0.0001) {
                    continue;
                }
                ageSum += pp.getPlayer().getAge() * weight;
                ageWeightSum += weight;

                talentSum += pp.getPlayer().getTalent() * weight;
                talentWeightSum += weight;

                strengthSum += pp.getPlayer().getStrength() * weight;
                strengthWeightSum += weight;

                abilitiesSum += PlayerPrice.getPlayerAbilitiesTotalMinus20(pp.getPlayer()) * weight;
                abilitiesWeightSum += weight;

                healthSum += pp.getPlayer().getHealth() * weight;
                healthWeightSum += weight;

                priceSum += pp.getPlayer().getPrice() * weight;
                priceWeightSum += weight;
            }
            avgAge = ageSum / ageWeightSum;
            avgTalent = talentSum / talentWeightSum;
            avgStrength = strengthSum / strengthWeightSum;
            avgAbilities = abilitiesSum / abilitiesWeightSum;
            avgHealth = healthSum / healthWeightSum;
            avgPrice = priceSum / priceWeightSum;

            ageWeight = Math.pow((avgAge - 6) / (player.getPlayer().getAge() - 6), 0.75);
            talentWeight = Math.pow((avgTalent - 15) / (player.getPlayer().getTalent() - 15), 0.5);
            strengthWeight = Math.pow((avgStrength + 10) / (player.getPlayer().getStrength() + 10), 1.75);
            healthWeight = Math.pow((avgHealth - 69) / (player.getPlayer().getHealth() - 69), 0.5);
            abilitiesWeight = Math.pow((avgAbilities + 20) / (player.getAbilitiesTotal() + 20), 0.25);
            price = avgPrice * (1 - bankPercent / 100) / (ageWeight * talentWeight * strengthWeight * healthWeight * abilitiesWeight);
            //            //2. calc player weights
            //            for (PlayerPriceFa13Calc pp : players) {
            //                pp.ageWeight = Math.pow((avgAge - 6) / (pp.getPlayer().getAge() - 6), 0.75);
            //                pp.talentWeight = Math.pow((avgTalent - 15) / (pp.getPlayer().getTalent() - 15), 0.5);
            //                pp.strengthWeight = Math.pow((avgStrength + 10) / (pp.getPlayer().getStrength() + 10), 1.75);
            //                pp.healthWeight = Math.pow((avgHealth - 69) / (pp.getPlayer().getHealth() - 69), 0.5);
            //                pp.abilitiesWeight = Math.pow((avgAbilities + 20) / (PlayerPrice.getPlayerAbilitiesTotalMinus20(pp.getPlayer()) + 20), 0.25);
            //            }
        }
    }

    public static class PlayerPriceFa13Calc extends PlayerCalcInfo {

        public double stepWeight;
        public double transWeight = 1;
        public double ageWeight;
        public double talentWeight;
        public double strengthWeight;
        public double healthWeight;
        public double abilitiesWeight;
        private double price;

        public PlayerPriceFa13Calc(Player player) {
            super(player);
        }

        public double calcPrice(double avgPrice, double bankPercent) {

            price = avgPrice * (1 - bankPercent / 100) / (ageWeight * talentWeight * strengthWeight * healthWeight * abilitiesWeight);

            return price;
        }

    }

}