package com.fa13.build.model;

import org.apache.commons.lang.builder.ToStringBuilder;

public abstract class BasicPlayer {

    protected int number;
    protected String name;
    protected PlayerAmplua position;
    protected final PlayerAmplua originalPosition;
    protected int id;

    protected BasicPlayer(PlayerAmplua position) {
        originalPosition = position;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
