package com.fa13.build.model.game;


import com.fa13.build.view.MainWindow;

public enum Hardness {
    DEFAULT  ("0", "агрессивность",      "Hardness.default"),
    SOFT     ("1", "аккуратно",          "Hardness.soft"),
    NORMAL   ("2", "нормально",          "Hardness.normal"),
    HARD     ("3", "жёстко",             "Hardness.hard"),
    MAXI_HARD("4", "максимально жёстко", "Hardness.maxiHard");

    private final String formValue;
    private final String playerFormValue;
    private final String localizationKey;

    private Hardness(String playerFormValue, String formValue, String localizationKey) {
        this.playerFormValue = playerFormValue;
        this.formValue = formValue;
        this.localizationKey = localizationKey;
    }

    public static Hardness resolveByFormValue(String formValue) {
        for (Hardness h : values()) {
            if (h.formValue.equals(formValue)) {
                return h;
            }
        }
        return null;
    }

    public static Hardness resolveByPlayerFormValue(String playerFormValue) {
        for (Hardness h : values()) {
            if (h.playerFormValue.equals(playerFormValue)) {
                return h;
            }
        }
        return null;
    }

    public String getPlayerFormValue() {
        return playerFormValue;
    }

    public String getLocalizedString() {
        return MainWindow.getMessage(localizationKey);
    }

    public String toString() {
        return formValue;
    }
}