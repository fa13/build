package com.fa13.build.model.calc;

import com.fa13.build.model.Player;

public class PlayerCalcInfo {

    private Player player;
    private int abilitiesTotal;

    public PlayerCalcInfo(Player player) {
        this(player, PlayerPrice.getPlayerAbilitiesTotal(player));
    }

    public PlayerCalcInfo(Player player, int abilitiesTotal) {
        this.player = player;
        this.abilitiesTotal = abilitiesTotal;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        if (this.player != player) {
            this.player = player;

            if (player == null) {
                abilitiesTotal = 0;
            } else {
                abilitiesTotal = PlayerPrice.getPlayerAbilitiesTotal(player);
            }
        }
    }

    public int getAbilitiesTotal() {
        return abilitiesTotal;
    }

    public void setAbilitiesTotal(int abilitiesTotal) {
        this.abilitiesTotal = abilitiesTotal;
    }

    public double getPriceByPlayer(Player otherPlayer) {

        double k = ((double) otherPlayer.getHealth() / (double) player.getHealth())
                * ((double) PlayerPrice.getPlayerAbilitiesTotal(otherPlayer) / (double) abilitiesTotal);
        k = (double) otherPlayer.getPrice() * k * (otherPlayer.getPosition().equals(player.getPosition()) ? 1.0 : 0.9);
        return k;
    }

}