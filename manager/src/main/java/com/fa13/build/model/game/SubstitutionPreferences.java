package com.fa13.build.model.game;


public enum SubstitutionPreferences {
    BY_POSITION(0),
    BY_STRENGTH(1),
    BY_FUTURE_SUB(2);

    private final int formValue;

    private SubstitutionPreferences(int formValue) {
        this.formValue = formValue;
    }

    public static SubstitutionPreferences resolveByFormValue(int formValue) {
        for (SubstitutionPreferences substitutionPreferences : values()) {
            if (substitutionPreferences.formValue == formValue) {
                return substitutionPreferences;
            }
        }
        return null;
    }

    public int getFormValue() {
        return formValue;
    }

    @Override
    public String toString() {
        return name();
    }
}