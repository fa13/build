package com.fa13.build.model;

public class TransferPlayerFilter {

    private boolean useMainFilter;
    private boolean useAbilityFilter;

    private int age1 = 0;
    private int age2 = 99;

    private int wage1 = 0;
    private int wage2 = 999;

    private int health1 = 0;
    private int health2 = 100;

    private int tickets1 = 0;
    private int tickets2 = 99999;

    private int price1 = 0;
    private int price2 = 999999;

    private int strength1 = 0;
    private int strength2 = 999;

    private int talent1 = 0;
    private int talent2 = 999;

    private int experiance1 = 0;
    private int experiance2 = 999;

    private int speed1 = 0;
    private int speed2 = 299;

    private int stamina1 = 0;
    private int stamina2 = 299;

    private int passing1 = 0;
    private int passing2 = 299;

    private int crossing1 = 0;
    private int crossing2 = 299;

    private int tackling1 = 0;
    private int tackling2 = 299;

    private int heading1 = 0;
    private int heading2 = 299;

    private int shooting1 = 0;
    private int shooting2 = 299;

    private int dribbling1 = 0;
    private int dribbling2 = 299;

    private int handling1 = 0;
    private int handling2 = 299;

    private int reflexes1 = 0;
    private int reflexes2 = 299;

    private String playerNamePart;
    private String positionNamePart;

    private String countryPart;
    private boolean userChoise;

    private String teamPart;
    private boolean teamsWithManagerOnly;

    private String noManagerName;

    public void resetMainFilter() {
        age1 = 15;
        age2 = 99;

        wage1 = 0;
        wage2 = 999;

        health1 = 20;
        health2 = 100;

        tickets1 = 0;
        tickets2 = 999;

        price1 = 0;
        price2 = 99999;

        strength1 = 0;
        strength2 = 100;

        talent1 = 20;
        talent2 = 100;

        experiance1 = 0;
        experiance2 = 100;

        playerNamePart = "";
        positionNamePart = "";

        countryPart = "";
        userChoise = false;

        teamPart = "";
        setTeamsWithManagerOnly(false);
    }

    public void resetAbilitiesFilter() {
        speed1 = 20;
        speed2 = 299;

        stamina1 = 20;
        stamina2 = 299;

        passing1 = 20;
        passing2 = 299;

        crossing1 = 20;
        crossing2 = 299;

        tackling1 = 20;
        tackling2 = 299;

        heading1 = 20;
        heading2 = 299;

        shooting1 = 20;
        shooting2 = 299;

        dribbling1 = 20;
        dribbling2 = 299;

        handling1 = 20;
        handling2 = 299;

        reflexes1 = 20;
        reflexes2 = 299;

    }

    public boolean validate(TransferPlayer transferPlayer) {
        if (useMainFilter) {
            if (transferPlayer.getAge() < age1 || transferPlayer.getAge() > age2) {
                return false;
            } else if (transferPlayer.getSalary() < wage1 || transferPlayer.getSalary() > wage2) {
                return false;
            } else if (transferPlayer.getHealth() < health1 || transferPlayer.getHealth() > health2) {
                return false;
            } else if (transferPlayer.getBids() < tickets1 || transferPlayer.getBids() > tickets2) {
                return false;
            } else if (transferPlayer.getPrice() < price1 || transferPlayer.getPrice() > price2) {
                return false;
            } else if (transferPlayer.getStrength() < strength1 || transferPlayer.getStrength() > strength2) {
                return false;
            } else if (transferPlayer.getTalent() < talent1 || transferPlayer.getTalent() > talent2) {
                return false;
            } else if (transferPlayer.getExperience() < experiance1 || transferPlayer.getExperience() > experiance2) {
                return false;
            } else if (positionNamePart != null && !positionNamePart.isEmpty()
                    && !transferPlayer.getOriginalPosition().getFormValue().toUpperCase().contains(positionNamePart.toUpperCase())) {
                return false;
            } else if (playerNamePart != null && !playerNamePart.isEmpty()
                    && !transferPlayer.getName().toUpperCase().contains(playerNamePart.toUpperCase())) {
                return false;
            } else if (countryPart != null && !countryPart.isEmpty()) {
                String playerCountry = Player.getNationalityNameByCode(transferPlayer.getNationalityCode());
                if (!playerCountry.isEmpty() && !playerCountry.toUpperCase().contains(countryPart.toUpperCase())) {
                    return false;
                }
            }
        }
        if (!validateAbilities((Player) transferPlayer)) {
            return false;
        }
        return true;
    }

    public boolean validate(Team team, Player player) {
        if (useMainFilter) {
            if (player.getAge() < age1 || player.getAge() > age2) {
                return false;
            } else if (player.getSalary() < wage1 || player.getSalary() > wage2) {
                return false;
            } else if (player.getHealth() < health1 || player.getHealth() > health2) {
                return false;
            } else if (player.getPrice() < price1 || player.getPrice() > price2) {
                return false;
            } else if (player.getStrength() < strength1 || player.getStrength() > strength2) {
                return false;
            } else if (player.getTalent() < talent1 || player.getTalent() > talent2) {
                return false;
            } else if (player.getExperience() < experiance1 || player.getExperience() > experiance2) {
                return false;
            } else if (positionNamePart != null && !positionNamePart.isEmpty()
                    && !player.getOriginalPosition().getFormValue().toUpperCase().contains(positionNamePart.toUpperCase())) {
                return false;
            } else if (playerNamePart != null && !playerNamePart.isEmpty() && !player.getName().toUpperCase().contains(playerNamePart.toUpperCase())) {
                return false;
            } else if (countryPart != null && !countryPart.isEmpty()) {
                String playerCountry = Player.getNationalityNameByCode(player.getNationalityCode());
                if (!playerCountry.isEmpty() && !playerCountry.toUpperCase().contains(countryPart.toUpperCase())) {
                    return false;
                }
                ///!!! if this will not return we need to check teamPart and teamsWithManagerOnly options too
            }

            if (teamPart != null && !teamPart.isEmpty() && !team.getName().toUpperCase().contains(teamPart.toUpperCase())) {
                return false;
            } else if (teamsWithManagerOnly && team.getManagerName().toUpperCase().equalsIgnoreCase(noManagerName.toUpperCase())) {
                return false;
            }
        }
        if (!validateAbilities(player)) {
            return false;
        }
        return true;
    }

    public boolean validateAbilities(Player player) {
        if (useAbilityFilter) {
            if (player.getSpeed() < speed1 || player.getSpeed() > speed2) {
                return false;
            } else if (player.getStamina() < stamina1 || player.getStamina() > stamina2) {
                return false;
            } else if (player.getPassing() < passing1 || player.getPassing() > passing2) {
                return false;
            } else if (player.getCross() < crossing1 || player.getCross() > crossing2) {
                return false;
            } else if (player.getTackling() < tackling1 || player.getTackling() > tackling2) {
                return false;
            } else if (player.getHeading() < heading1 || player.getHeading() > heading2) {
                return false;
            } else if (player.getShooting() < shooting1 || player.getShooting() > shooting2) {
                return false;
            } else if (player.getDribbling() < dribbling1 || player.getDribbling() > dribbling2) {
                return false;
            } else if (player.getHandling() < handling1 || player.getHandling() > handling2) {
                return false;
            } else if (player.getReflexes() < reflexes1 || player.getReflexes() > reflexes2) {
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public boolean isUseMainFilter() {
        return useMainFilter;
    }

    public void setUseMainFilter(boolean useMainFilter) {
        this.useMainFilter = useMainFilter;
    }

    public boolean isUseAbilityFilter() {
        return useAbilityFilter;
    }

    public void setUseAbilityFilter(boolean useAbilityFilter) {
        this.useAbilityFilter = useAbilityFilter;
    }

    public int getAge1() {
        return age1;
    }

    public void setAge1(int age1) {
        this.age1 = age1;
    }

    public int getAge2() {
        return age2;
    }

    public void setAge2(int age2) {
        this.age2 = age2;
    }

    public int getWage1() {
        return wage1;
    }

    public void setWage1(int wage1) {
        this.wage1 = wage1;
    }

    public int getWage2() {
        return wage2;
    }

    public void setWage2(int wage2) {
        this.wage2 = wage2;
    }

    public int getHealth1() {
        return health1;
    }

    public void setHealth1(int health1) {
        this.health1 = health1;
    }

    public int getHealth2() {
        return health2;
    }

    public void setHealth2(int health2) {
        this.health2 = health2;
    }

    public int getTickets1() {
        return tickets1;
    }

    public void setTickets1(int tickets1) {
        this.tickets1 = tickets1;
    }

    public int getTickets2() {
        return tickets2;
    }

    public void setTickets2(int tickets2) {
        this.tickets2 = tickets2;
    }

    public int getPrice1() {
        return price1;
    }

    public void setPrice1(int price1) {
        this.price1 = price1;
    }

    public int getPrice2() {
        return price2;
    }

    public void setPrice2(int price2) {
        this.price2 = price2;
    }

    public int getStrength1() {
        return strength1;
    }

    public void setStrength1(int strength1) {
        this.strength1 = strength1;
    }

    public int getStrength2() {
        return strength2;
    }

    public void setStrength2(int strength2) {
        this.strength2 = strength2;
    }

    public int getTalent1() {
        return talent1;
    }

    public void setTalent1(int talent1) {
        this.talent1 = talent1;
    }

    public int getTalent2() {
        return talent2;
    }

    public void setTalent2(int talent2) {
        this.talent2 = talent2;
    }

    public int getExperiance1() {
        return experiance1;
    }

    public void setExperiance1(int experiance1) {
        this.experiance1 = experiance1;
    }

    public int getExperiance2() {
        return experiance2;
    }

    public void setExperiance2(int experiance2) {
        this.experiance2 = experiance2;
    }

    public int getSpeed1() {
        return speed1;
    }

    public void setSpeed1(int speed1) {
        this.speed1 = speed1;
    }

    public int getSpeed2() {
        return speed2;
    }

    public void setSpeed2(int speed2) {
        this.speed2 = speed2;
    }

    public int getStamina1() {
        return stamina1;
    }

    public void setStamina1(int stamina1) {
        this.stamina1 = stamina1;
    }

    public int getStamina2() {
        return stamina2;
    }

    public void setStamina2(int stamina2) {
        this.stamina2 = stamina2;
    }

    public int getPassing1() {
        return passing1;
    }

    public void setPassing1(int passing1) {
        this.passing1 = passing1;
    }

    public int getPassing2() {
        return passing2;
    }

    public void setPassing2(int passing2) {
        this.passing2 = passing2;
    }

    public int getCrossing1() {
        return crossing1;
    }

    public void setCrossing1(int crossing1) {
        this.crossing1 = crossing1;
    }

    public int getCrossing2() {
        return crossing2;
    }

    public void setCrossing2(int crossing2) {
        this.crossing2 = crossing2;
    }

    public int getTackling1() {
        return tackling1;
    }

    public void setTackling1(int tackling1) {
        this.tackling1 = tackling1;
    }

    public int getTackling2() {
        return tackling2;
    }

    public void setTackling2(int tackling2) {
        this.tackling2 = tackling2;
    }

    public int getHeading1() {
        return heading1;
    }

    public void setHeading1(int heading1) {
        this.heading1 = heading1;
    }

    public int getHeading2() {
        return heading2;
    }

    public void setHeading2(int heading2) {
        this.heading2 = heading2;
    }

    public int getShooting1() {
        return shooting1;
    }

    public void setShooting1(int shooting1) {
        this.shooting1 = shooting1;
    }

    public int getShooting2() {
        return shooting2;
    }

    public void setShooting2(int shooting2) {
        this.shooting2 = shooting2;
    }

    public int getDribbling1() {
        return dribbling1;
    }

    public void setDribbling1(int dribbling1) {
        this.dribbling1 = dribbling1;
    }

    public int getDribbling2() {
        return dribbling2;
    }

    public void setDribbling2(int dribbling2) {
        this.dribbling2 = dribbling2;
    }

    public int getHandling1() {
        return handling1;
    }

    public void setHandling1(int handling1) {
        this.handling1 = handling1;
    }

    public int getHandling2() {
        return handling2;
    }

    public void setHandling2(int handling2) {
        this.handling2 = handling2;
    }

    public int getReflexes1() {
        return reflexes1;
    }

    public void setReflexes1(int reflexes1) {
        this.reflexes1 = reflexes1;
    }

    public int getReflexes2() {
        return reflexes2;
    }

    public void setReflexes2(int reflexes2) {
        this.reflexes2 = reflexes2;
    }

    public String getPlayerNamePart() {
        return playerNamePart;
    }

    public void setPlayerNamePart(String playerNamePart) {
        this.playerNamePart = playerNamePart;
    }

    public String getPositionNamePart() {
        return positionNamePart;
    }

    public void setPositionNamePart(String positionNamePart) {
        this.positionNamePart = positionNamePart;
    }

    public String getCountryPart() {
        return countryPart;
    }

    public void setCountryPart(String countryPart) {
        this.countryPart = countryPart;
    }

    public boolean isUserChoise() {
        return userChoise;
    }

    public void setUserChoise(boolean userChoise) {
        this.userChoise = userChoise;
    }

    public String getTeamPart() {
        return teamPart;
    }

    public void setTeamPart(String teamPart) {
        this.teamPart = teamPart;
    }

    public boolean isTeamsWithManagerOnly() {
        return teamsWithManagerOnly;
    }

    public void setTeamsWithManagerOnly(boolean teamsWithManagerOnly) {
        this.teamsWithManagerOnly = teamsWithManagerOnly;
    }

    public String getNoManagerName() {
        return noManagerName;
    }

    public void setNoManagerName(String noManagerName) {
        this.noManagerName = noManagerName;
    }

}