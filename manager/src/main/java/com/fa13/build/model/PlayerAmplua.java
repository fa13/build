package com.fa13.build.model;


import java.util.Arrays;

public enum PlayerAmplua {
    GK("ВР"),
    LD("ЛЗ"), CD("ЦЗ"), RD("ПЗ"),
    LM("ЛП"), CM("ЦП"), RM("ПП"),
    LF("ЛФ"), CF("ЦФ"), RF("ПФ");

    private String formValue;

    private PlayerAmplua(String s) {
        formValue = s;
    }

    public static PlayerAmplua resolveByFormValue(String formValue) {
        for (PlayerAmplua amplua : values()) {
            if (amplua.formValue.equals(formValue)) {
                return amplua;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    public String toString() {
        return formValue;
    }

    public static final double[][] POSITION_COEFFICIENTS;

    static {
        POSITION_COEFFICIENTS = new double[10][10];
        for (int i = 0; i < 10; i++) {
            Arrays.fill(POSITION_COEFFICIENTS[i], 0.3);
            POSITION_COEFFICIENTS[i][i] = 1;
        }
        POSITION_COEFFICIENTS[PlayerAmplua.LD.ordinal()][PlayerAmplua.CD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LD.ordinal()][PlayerAmplua.RD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LD.ordinal()][PlayerAmplua.LM.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.LD.ordinal()][PlayerAmplua.CM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.LD.ordinal()][PlayerAmplua.RM.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.CD.ordinal()][PlayerAmplua.LD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CD.ordinal()][PlayerAmplua.RD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CD.ordinal()][PlayerAmplua.LM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.CD.ordinal()][PlayerAmplua.CM.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.CD.ordinal()][PlayerAmplua.RM.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.RD.ordinal()][PlayerAmplua.LD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RD.ordinal()][PlayerAmplua.CD.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RD.ordinal()][PlayerAmplua.LM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RD.ordinal()][PlayerAmplua.CM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RD.ordinal()][PlayerAmplua.RM.ordinal()] = 0.75;

        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.CM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.RM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.LD.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.CD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.RD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.LF.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.CF.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.LM.ordinal()][PlayerAmplua.RF.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.LM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.RM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.LD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.CD.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.RD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.LF.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.CF.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.CM.ordinal()][PlayerAmplua.RF.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.LM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.CM.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.LD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.CD.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.RD.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.LF.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.CF.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RM.ordinal()][PlayerAmplua.RF.ordinal()] = 0.75;

        POSITION_COEFFICIENTS[PlayerAmplua.LF.ordinal()][PlayerAmplua.CF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LF.ordinal()][PlayerAmplua.RF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.LF.ordinal()][PlayerAmplua.LM.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.LF.ordinal()][PlayerAmplua.CM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.LF.ordinal()][PlayerAmplua.RM.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.CF.ordinal()][PlayerAmplua.LF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CF.ordinal()][PlayerAmplua.RF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.CF.ordinal()][PlayerAmplua.LM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.CF.ordinal()][PlayerAmplua.CM.ordinal()] = 0.75;
        POSITION_COEFFICIENTS[PlayerAmplua.CF.ordinal()][PlayerAmplua.RM.ordinal()] = 0.5;

        POSITION_COEFFICIENTS[PlayerAmplua.RF.ordinal()][PlayerAmplua.LF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RF.ordinal()][PlayerAmplua.CF.ordinal()] = 0.9;
        POSITION_COEFFICIENTS[PlayerAmplua.RF.ordinal()][PlayerAmplua.LM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RF.ordinal()][PlayerAmplua.CM.ordinal()] = 0.5;
        POSITION_COEFFICIENTS[PlayerAmplua.RF.ordinal()][PlayerAmplua.RM.ordinal()] = 0.75;
    }
}