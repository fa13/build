package com.fa13.build.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class All {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public All(Date date, Map<String, String> competitions, int bankRate, List<Team> teams) {
        this.date = date;
        this.competitions = competitions;
        this.bankRate = bankRate;
        this.teams = teams;
        this.currentTeam = 0;
    }

    Date date;
    Map<String, String> competitions;
    int bankRate;
    List<Team> teams;
    int currentTeam;
    private String fileName;

    public Date getDate() {
        return date;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public Team getCurrentTeam() {
        if (currentTeam >= 0 && teams.size() > currentTeam) {
            return teams.get(currentTeam);
        } else {
            return null;
        }
    }

    public void setCurrentTeam(int currentTeam) {
        this.currentTeam = currentTeam;
    }

    public Map<String, String> getCompetitions() {
        return competitions;
    }

    public Team getTeamByName(String name) {
        for (Team curr : teams) {
            if (curr.getName().equals(name)) {
                return curr;
            }
        }
        return null;
    }

    public Team getTeamById(String id) {
        for (Team curr : teams) {
            if (curr.getId().equals(id)) {
                return curr;
            }
        }
        return null;
    }

    public static String getAllUrlByName(String allFileNameBase) {

        if (allFileNameBase.equalsIgnoreCase("all13")) {
            return "https://repository.fa13.info/site/build/all13.zip";
        }

        //Map<String,String> competitions = Competitions.getDefaultCompetitions();
        String lastPart = allFileNameBase.replaceAll("all13", "");
        //if (competitions.containsValue(lastPart)) {
        if (lastPart.length() == 2) {
            //found championat
            return "https://repository.fa13.info/site/build/" + allFileNameBase + ".zip";
        } else {
            //club 3 symbols
            return "https://repository.fa13.info/site/build/mini/" + allFileNameBase + ".zip";
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getShortDescription() {
        String res = (fileName == null ? "" : fileName.substring(fileName.lastIndexOf(File.separator) + 1));

        res += (date == null ? "" : " [" + dateFormat.format(date) + "]");

        return res;
    }

}