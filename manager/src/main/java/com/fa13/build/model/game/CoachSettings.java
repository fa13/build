package com.fa13.build.model.game;

import com.fa13.build.view.MainWindow;

public enum CoachSettings {
    NONE             ("нет установки",         "CoachSettings.none"),
    PRESSING         ("прессинг",              "CoachSettings.pressing"),
    ARRHYTHMIA       ("аритмия",               "CoachSettings.arrhythmia"),
    PERSONAL_DEFENCE ("персональная опека",    "CoachSettings.personalDefence"),
    COUNTER_ATTACK   ("контратака",            "CoachSettings.counterAttack"),
    CATENACCIO       ("каттеначо",             "CoachSettings.catenaccio"),
    STRAIGHT_PRESSURE("прямое давление",       "CoachSettings.straightPressure"),
    POSITION_ATTACK  ("позиционное нападение", "CoachSettings.positionAttack");

    private final String formValue;
    private final String localizationKey;

    private CoachSettings(String formValue, String localizationKey) {
        this.formValue = formValue;
        this.localizationKey = localizationKey;
    }

    public static CoachSettings resolveByFormValue(String formValue) {
        for (CoachSettings cs : values()) {
            if (cs.formValue.equals(formValue)) {
                return cs;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    public String getLocalizedString() {
        return MainWindow.getMessage(localizationKey);
    }

    public String toString() {
        return formValue;
    }

}
