package com.fa13.build.model.game;

import com.fa13.build.model.PlayerAmplua;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PlayerPosition {

    public static final int SR_CAPTAIN = 32;
    public static final int SR_PENALTY = 16;
    public static final int SR_DIRECT_FREEKICK = 8;
    public static final int SR_INDIRECT_FREEKICK = 4;
    public static final int SR_LEFT_CORNER = 2;
    public static final int SR_RIGHT_CORNER = 1;
    public static final int SR_ASSISTANT = 6;

    private int defenceX = 10;
    private int defenceY = 222;
    private int attackX = 10;
    private int attackY = 222;
    private int freekickX = 10;
    private int freekickY = 222;
    private boolean goalkeeper = false;

    private int number;
    private int specialRole;
    private int penaltyOrder;

    private boolean longShot;
    private FreekickAction actOnFreekick;
    private boolean fantasista;
    private boolean dispatcher;
    private int personalDefence;
    private boolean pressing;
    private boolean keepBall;
    private boolean defenderAttack;
    private Hardness hardness;
    private PassingStyle passingStyle;

    public PlayerPosition(int defenceX, int defenceY, int attackX, int attackY,
                          int freekickX, int freekickY, int number, int specialRole,
                          int penaltyOrder, boolean longShot, FreekickAction actOnFreekick,
                          boolean fantasista, boolean dispatcher, int personalDefence,
                          boolean pressing, boolean keepBall, boolean defenderAttack,
                          Hardness hardness, PassingStyle passingStyle, boolean goalkeeper) {
        this.defenceX = defenceX;
        this.defenceY = defenceY;
        this.attackX = attackX;
        this.attackY = attackY;
        this.freekickX = freekickX;
        this.freekickY = freekickY;
        this.number = number;
        this.specialRole = specialRole;
        this.penaltyOrder = penaltyOrder;
        this.longShot = longShot;
        this.actOnFreekick = actOnFreekick;
        this.fantasista = fantasista;
        this.dispatcher = dispatcher;
        this.personalDefence = personalDefence;
        this.pressing = pressing;
        this.keepBall = keepBall;
        this.defenderAttack = defenderAttack;
        this.hardness = hardness;
        this.passingStyle = passingStyle;
        this.goalkeeper = goalkeeper;
    }

    public PlayerPosition(int defenceX, int defenceY, int attackX, int attackY,
                          int freekickX, int freekickY, int number, int specialRole,
                          int penaltyOrder, boolean longShot, FreekickAction actOnFreekick,
                          boolean fantasista, boolean dispatcher, int personalDefence,
                          boolean pressing, boolean keepBall, boolean defenderAttack,
                          Hardness hardness, PassingStyle passingStyle) {
        this.defenceX = defenceX;
        this.defenceY = defenceY;
        this.attackX = attackX;
        this.attackY = attackY;
        this.freekickX = freekickX;
        this.freekickY = freekickY;
        this.number = number;
        this.specialRole = specialRole;
        this.penaltyOrder = penaltyOrder;
        this.longShot = longShot;
        this.actOnFreekick = actOnFreekick;
        this.fantasista = fantasista;
        this.dispatcher = dispatcher;
        this.personalDefence = personalDefence;
        this.pressing = pressing;
        this.keepBall = keepBall;
        this.defenderAttack = defenderAttack;
        this.hardness = hardness;
        this.passingStyle = passingStyle;
        this.goalkeeper = false;
    }

    public PlayerPosition validatePersonalSettings() {
        if (this.getAmplua() != null) {
            switch (this.getAmplua()) {
                case LD:
                case CD:
                case RD:
                {
                    this.fantasista = false;
                    this.dispatcher = false;
                    this.pressing = true;
                    this.keepBall = false;
                    break;
                }
                case LM:
                case CM:
                case RM:
                {
                    this.keepBall = false;
                    this.defenderAttack = false;
                    break;
                }
                case LF:
                case RF:
                {
                    this.actOnFreekick = FreekickAction.FK_YES;
                    this.defenderAttack = false;
                    this.personalDefence = 0;
                    break;
                }
                case CF:
                {
                    this.actOnFreekick = FreekickAction.FK_YES;
                    this.dispatcher = false;
                    this.defenderAttack = false;
                    this.personalDefence = 0;
                    break;
                }
            }
        }
        //hotfix for invalid personalDefence occurring sometime somehow
        if (personalDefence < 0) {
            personalDefence = 0;
        }
        return this;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void reset() {
        this.number = -1;
    }

    public int getDefenceX() {
        return defenceX;
    }

    public void setDefenceX(int defenceX) {
        this.defenceX = defenceX;
    }

    public int getDefenceY() {
        return defenceY;
    }

    public void setDefenceY(int defenceY) {
        this.defenceY = defenceY;
    }

    public int getAttackX() {
        return attackX;
    }

    public void setAttackX(int attackX) {
        this.attackX = attackX;
    }

    public int getAttackY() {
        return attackY;
    }

    public void setAttackY(int attackY) {
        this.attackY = attackY;
    }

    public int getFreekickX() {
        return freekickX;
    }

    public void setFreekickX(int freekickX) {
        this.freekickX = freekickX;
    }

    public int getFreekickY() {
        return freekickY;
    }

    public void setFreekickY(int freekickY) {
        this.freekickY = freekickY;
    }

    public int getSpecialRole() {
        return specialRole;
    }

    public void setSpecialRole(int specialRole) {
        this.specialRole = specialRole;
    }

    public int getPenaltyOrder() {
        return penaltyOrder;
    }

    public void setPenaltyOrder(int penaltyOrder) {
        this.penaltyOrder = penaltyOrder;
    }

    public boolean isCaptain() {
        return (this.specialRole & SR_CAPTAIN) != 0;
    }

    public void setCaptain(boolean value) {
        if (value) {
            this.specialRole |= SR_CAPTAIN;
        } else {
            this.specialRole &= ~SR_CAPTAIN;
        }
    }

    public boolean isCaptainAssistant () {
        return (this.specialRole & (SR_CAPTAIN << SR_ASSISTANT)) != 0;
    }

    public void setCaptainAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_CAPTAIN << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_CAPTAIN << SR_ASSISTANT);
        }
    }

    public boolean isDirectFreekick() {
        return (this.specialRole & SR_DIRECT_FREEKICK) != 0;
    }

    public void setDirectFreekick(boolean value) {
        if (value) {
            this.specialRole |= SR_DIRECT_FREEKICK;
        } else {
            this.specialRole &= ~SR_DIRECT_FREEKICK;
        }
    }

    public boolean isDirectFreekickAssistant () {
        return (this.specialRole & (SR_DIRECT_FREEKICK << SR_ASSISTANT)) != 0;
    }

    public void setDirectFreekickAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_DIRECT_FREEKICK << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_DIRECT_FREEKICK << SR_ASSISTANT);
        }
    }

    public boolean isIndirectFreekick() {
        return (this.specialRole & SR_INDIRECT_FREEKICK) != 0;
    }

    public void setIndirectFreekick(boolean value) {
        if (value) {
            this.specialRole |= SR_INDIRECT_FREEKICK;
        } else {
            this.specialRole &= ~SR_INDIRECT_FREEKICK;
        }
    }

    public boolean isIndirectFreekickAssistant () {
        return (this.specialRole & (SR_INDIRECT_FREEKICK << SR_ASSISTANT)) != 0;
    }

    public void setIndirectFreekickAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_INDIRECT_FREEKICK << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_INDIRECT_FREEKICK << SR_ASSISTANT);
        }
    }

    public boolean isPenalty() {
        return (this.specialRole & SR_PENALTY) != 0;
    }

    public void setPenalty(boolean value) {
        if (value) {
            this.specialRole |= SR_PENALTY;
        } else {
            this.specialRole &= ~SR_PENALTY;
        }
    }

    public boolean isPenaltyAssistant () {
        return (this.specialRole & (SR_PENALTY << SR_ASSISTANT)) != 0;
    }

    public void setPenaltyAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_PENALTY << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_PENALTY << SR_ASSISTANT);
        }
    }

    public boolean isLeftCorner() {
        return (this.specialRole & SR_LEFT_CORNER) != 0;
    }

    public void setLeftCorner(boolean value) {
        if (value) {
            this.specialRole |= SR_LEFT_CORNER;
        } else {
            this.specialRole &= ~SR_LEFT_CORNER;
        }
    }

    public boolean isLeftCornerAssistant () {
        return (this.specialRole & (SR_LEFT_CORNER << SR_ASSISTANT)) != 0;
    }

    public void setLeftCornerAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_LEFT_CORNER << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_LEFT_CORNER << SR_ASSISTANT);
        }
    }

    public boolean isRightCorner() {
        return (this.specialRole & SR_RIGHT_CORNER) != 0;
    }

    public void setRightCorner(boolean value) {
        if (value) {
            this.specialRole |= SR_RIGHT_CORNER;
        } else {
            this.specialRole &= ~SR_RIGHT_CORNER;
        }
    }

    public boolean isRightCornerAssistant () {
        return (this.specialRole & (SR_RIGHT_CORNER << SR_ASSISTANT)) != 0;
    }

    public void setRightCornerAssistant (boolean value) {
        if (value) {
            this.specialRole |= (SR_RIGHT_CORNER << SR_ASSISTANT);
        } else {
            this.specialRole &= ~(SR_RIGHT_CORNER << SR_ASSISTANT);
        }
    }

    public boolean isLongShot() {
        return longShot;
    }

    public void setLongShot(boolean longShot) {
        this.longShot = longShot;
    }

    public FreekickAction getActOnFreekick() {
        return actOnFreekick;
    }

    public void setActOnFreekick(FreekickAction actOnFreekick) {
        this.actOnFreekick = actOnFreekick;
    }

    public boolean isFantasista() {
        return fantasista;
    }

    public void setFantasista(boolean fantasista) {
        this.fantasista = fantasista;
    }

    public boolean isDispatcher() {
        return dispatcher;
    }

    public void setDispatcher(boolean dispatcher) {
        this.dispatcher = dispatcher;
    }

    public int getPersonalDefence() {
        return personalDefence;
    }

    public void setPersonalDefence(int personalDefence) {
        this.personalDefence = personalDefence;
    }

    public boolean isPressing() {
        return pressing;
    }

    public void setPressing(boolean pressing) {
        this.pressing = pressing;
    }

    public boolean isKeepBall() {
        return keepBall;
    }

    public void setKeepBall(boolean keepBall) {
        this.keepBall = keepBall;
    }

    public boolean isDefenderAttack() {
        return defenderAttack;
    }

    public void setDefenderAttack(boolean defenderAttack) {
        this.defenderAttack = defenderAttack;
    }

    public Hardness getHardness() {
        return hardness;
    }

    public void setHardness(Hardness hardness) {
        this.hardness = hardness;
    }

    public PassingStyle getPassingStyle() {
        return passingStyle;
    }

    public void setPassingStyle(PassingStyle passingStyle) {
        this.passingStyle = passingStyle;
    }

    public PlayerPosition() {
        this.actOnFreekick = FreekickAction.FK_DEFAULT;
        this.passingStyle = PassingStyle.PS_BOTH;
        this.hardness = Hardness.DEFAULT;
    }

    public boolean isGoalkeeper() {
        return goalkeeper;
    }

    public void setGoalkeeper(boolean goalkeeper) {
        this.goalkeeper = goalkeeper;
    }

    public PlayerAmplua getAmplua () {
        if (this.goalkeeper) {
            return PlayerAmplua.GK;
        }

        String s = "";
        if (defenceY + attackY <= 295) {
            s += 'Л';
        } else if (defenceY + attackY >= 605) {
            s += 'П';
        } else {
            s += 'Ц';
        }

        if (defenceX + attackX < 500) {
            s += 'З';
        } else if (defenceX + attackX >= 780) {
            s += 'Ф';
        } else {
            s += 'П';
        }

        return PlayerAmplua.resolveByFormValue(s);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
