package com.fa13.build.model.calc;

import com.fa13.build.model.Player;
import com.fa13.build.model.Team;

public class PlayersExchangeVariant {

    private Player player1;
    private Player player2;
    private Team team1;
    private Team team2;

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof PlayersExchangeVariant)) {
            return false;
        }

        PlayersExchangeVariant pev = (PlayersExchangeVariant) other;

        if ((getPlayer1() == pev.getPlayer1() && getPlayer2() == pev.getPlayer2())
                || (getPlayer2() == pev.getPlayer1() && getPlayer1() == pev.getPlayer2())) {
            return true;
        } else {
            return false;
        }

    }

}