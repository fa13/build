package com.fa13.build.controller.io;

public class ReaderException extends Exception {

    private static final long serialVersionUID = 1119215946699943578L;

    public ReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReaderException(String message) {
        super(message);
    }
}
