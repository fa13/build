package com.fa13.build.controller.io;


import com.fa13.build.protect.ProtectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Generic class for all types of FormWriters.
 * Provides common parts, which are applicable for all forms (aka requests).
 */
public abstract class AbstractFormWriter<T> {
    public static final String LINE_BREAK = "\r\n";

    public void write(String fileName, T form) throws IOException {
        OutputStream stream = new HashingOutputStream(new FileOutputStream(fileName));
        write(stream, form);
    }

    public void write(OutputStream stream, T form) throws IOException {
        StringBuilder contentBuilder = new StringBuilder();
        appendFormHeader(contentBuilder);
        appendFormData(contentBuilder, form);
        String content = contentBuilder.toString();

        String[] additionalDefenceValues = getAdditionalDefenceValues(form);

        stream.write(content.getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(("*****" + LINE_BREAK).getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(ProtectionUtils.getDefenceData(content, HashingOutputStream.WIN_CHARSET, additionalDefenceValues));

        stream.close();
    }

    protected abstract void appendFormData(StringBuilder contentBuilder, T form);

    protected void appendLine(StringBuilder contentBuilder, CharSequence line) {
        contentBuilder.append(line).append(LINE_BREAK);
    }

    protected void appendLine(StringBuilder contentBuilder, int value) {
        contentBuilder.append(value).append(LINE_BREAK);
    }

    protected void appendFormHeader(StringBuilder contentBuilder) {
        appendLine(contentBuilder, "Заявка составлена с помощью BuildJava-1.0");

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy H:m:s");
        appendLine(contentBuilder, "LocalTime = " + sdf.format(date));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        appendLine(contentBuilder, "UTCTime = " + sdf.format(date));

        contentBuilder.append(LINE_BREAK);
    }

    protected String[] getAdditionalDefenceValues(T form) {
        return new String[0];
    }
}