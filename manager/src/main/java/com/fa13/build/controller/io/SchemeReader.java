package com.fa13.build.controller.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fa13.build.model.game.FreekickAction;
import com.fa13.build.model.game.Hardness;
import com.fa13.build.model.game.PassingStyle;
import com.fa13.build.model.game.PlayerPosition;

public class SchemeReader {

    public static final Map<String, PlayerPosition[]> read(String fileName) throws ReaderException {

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), SchemeWriter.UTF8_CHARSET));

            Map<String, PlayerPosition[]> schemes = readScheme(reader);
            reader.close();
            return schemes;
        } catch (ReaderException e) {
            throw e;
        } catch (Exception e) {
            throw new ReaderException("Unable to open scheme file: " + fileName);
        }
    }

    public static Map<String, PlayerPosition[]> readScheme(BufferedReader reader) throws ReaderException {

        Map<String, PlayerPosition[]> result = new TreeMap<String, PlayerPosition[]>();
        try {
            String s = reader.readLine();
            String schemeName = null;
            PlayerPosition[] playerPositions = null;
            int posCount = 0;
            while (s != null) {
                if (!s.startsWith("User")) {
                    if (!isPlayerData(s)) {

                        schemeName = s;
                        playerPositions = new PlayerPosition[11];
                        //GK
                        playerPositions[0] = new PlayerPosition(10, 222, 10, 222, 0, 0, 10, 222, 0, false, FreekickAction.FK_NO, false, false, 0,
                                false, false, false, Hardness.DEFAULT, PassingStyle.PS_BOTH, true);
                        posCount = 1;

                        result.put(schemeName, playerPositions);

                    } else {
                        //read player data
                        String ss[] = s.split(" ");
                        //POS DEF_X DEF_Y ATT_X ATT_Y FK_X FK_Y
                        if (ss.length > 6) {

                            playerPositions[posCount++] = new PlayerPosition(Integer.parseInt(ss[1]), Integer.parseInt(ss[2]),
                                    Integer.parseInt(ss[3]), Integer.parseInt(ss[4]), Integer.parseInt(ss[5]), Integer.parseInt(ss[6]), 0, 0, 0,
                                    false, FreekickAction.FK_NO, false, false, 0, false, false, false, Hardness.DEFAULT, PassingStyle.PS_BOTH);

                        }
                    }
                }
                s = reader.readLine();
            }

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        return result;
    }

    public static String[] POS_RUS_NAMES = new String[] { "ЛЗ", "ЦЗ", "ПЗ", "ЛП", "ЦП", "ПП", "ЛФ", "ЦФ", "ПФ" };

    private static boolean isPlayerData(String s) {

        for (String posName : POS_RUS_NAMES) {
            if (s.startsWith(posName)) {
                return true;
            }
        }

        return false;
    }

}