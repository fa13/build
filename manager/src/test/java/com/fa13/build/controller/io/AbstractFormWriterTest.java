package com.fa13.build.controller.io;


import org.hamcrest.core.StringContains;
import org.hamcrest.core.StringStartsWith;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class AbstractFormWriterTest {
    private ByteArrayOutputStream resultStream;
    private AbstractFormWriter<Object> writer;

    @Before
    public void setUp() {
        resultStream = new ByteArrayOutputStream();
        writer = new AbstractFormWriter<Object>() {

            @Override
            protected void appendFormData(StringBuilder contentBuilder, Object form) {

            }

            @Override
            protected String[] getAdditionalDefenceValues(Object form) {
                return new String[0];
            }
        };
    }

    @Test
    public void write() throws Exception {
        writer.write(resultStream, new Object());

        String result = resultStream.toString(HashingOutputStream.WIN_CHARSET.name());
        String[] actualLines = result.split(AbstractFormWriter.LINE_BREAK, -1);

        assertThat(actualLines[0], new StringContains("BuildJava-1.0"));
        assertThat(actualLines[1], new StringStartsWith("LocalTime = "));
        assertThat(actualLines[2], new StringStartsWith("UTCTime = "));
        assertEquals("", actualLines[3]);
    }

}