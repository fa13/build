package com.fa13.build.utils;


import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.game.*;

import static com.fa13.build.model.game.PassingStyle.PS_EXACT;
import static com.fa13.build.model.game.PassingStyle.PS_FORWARD;
import static com.fa13.build.utils.PlayerPositionBuilder.*;
import static com.fa13.build.utils.PlayerSubstitutionBuilder.substitution;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public abstract class GameFormBuilder {

    public static GameForm createGameForm() {
        GameForm form = GameForm.newInstance();
        form.setTournamentID("чемпионат Страны-2");
        form.setGameType("тур-12");
        form.setTeamID("MY");
        form.setPassword("password");
        form.setStartTactics(new TeamTactics(Tactic.ATTACK, Hardness.HARD, Style.COMBINATION, 0, 0));
        form.setTeamCoachSettings(CoachSettings.ARRHYTHMIA);
        form.setPrice(10);

        form.setFirstTeam(new PlayerPosition[] {
            goalkeeper(1)     .captain().get(),
            leftDefence(2)    .freekickPosition(500, 0)  .penaltyOrder(10).personalDefence(7).assistantCaptain().get(),
            centralDefence(3) .freekickPosition(500, 100).penaltyOrder(9) .actOnFreeKick(TRUE).indirectFreeKick().get(),
            centralDefence(4) .freekickPosition(500, 200).penaltyOrder(8) .actOnFreeKick(FALSE).assistantIndirectFreeKick().get(),
            rightDefence(5)   .freekickPosition(500, 300).penaltyOrder(7) .personalSettings(new char[] {'У', 'З'}).assistantPenalty().get(),
            leftMidfield(6)   .freekickPosition(500, 400).penaltyOrder(6) .passingStyle(PS_FORWARD).leftCorner().get(),
            centralMidfield(7).freekickPosition(550, 0)  .penaltyOrder(5) .hardness(Hardness.MAXI_HARD).freeKick().get(),
            rightMidfield(8)  .freekickPosition(550, 100).penaltyOrder(4) .personalSettings(new char[] {'У', 'А', 'Х', 'Д', 'П'}).rightCorner().get(),
            leftForward(9)    .freekickPosition(550, 200).penaltyOrder(3) .passingStyle(PS_EXACT).assistantLeftCorner().get(),
            centralForward(10).freekickPosition(550, 300).penaltyOrder(2) .hardness(Hardness.SOFT).penalty().assistantFreeKick().get(),
            rightForward(11)  .freekickPosition(550, 400).penaltyOrder(1) .personalSettings(new char[] {'У', 'Х', 'М', 'П'}).assistantRightCorner().get()
        });
        form.setBench(new int[] {
            12, 13, 14, 15, 16, 17, 18
        });

        PlayerSubstitution[] playerSubstitutions = new PlayerSubstitution[25];
        playerSubstitutions[0] = substitution(11, -1, 2).out(1).in(abstractPlayer(12).personalSettings(new char[] {'П'}).get()).get();
        playerSubstitutions[1] = substitution(22, -2, 0).out(2).in(abstractPlayer(13).personalSettings(new char[] {'П'}).get()).get();
        playerSubstitutions[2] = substitution(77, -3, 3).out(7).in(abstractPlayer(14).personalSettings(new char[] {'П'}).penalty().freeKick().get()).get();
        playerSubstitutions[3] = substitution(33, 1, 3).out(3).in(15).get();
        playerSubstitutions[4] = substitution(44, 2, 3).out(4).in(16, PlayerAmplua.RF).get();
        for (int i = 5; i < 25; i++) {
            playerSubstitutions[i] = new PlayerSubstitution();
        }
        form.setSubstitutions(playerSubstitutions);
        form.setSubstitutionPreferences(SubstitutionPreferences.BY_STRENGTH);

        setForceToAttack(form, 67, -1, -23);
        setForceToDefence(form, 45, 1, 23);
        form.setHalftimeChanges(new TeamTactics[] {
            new TeamTactics(Tactic.ATTACK, Hardness.HARD, Style.CENTER, -1, 0),
            new TeamTactics(Tactic.ATTACK, Hardness.NORMAL, Style.CENTER, -3, -2),
            new TeamTactics(Tactic.DEFENCE, Hardness.DEFAULT, Style.CENTER, 1, 2),
            new TeamTactics(Tactic.BALANCE, Hardness.MAXI_HARD, Style.LONG_BALL, -20, -4),
            null
        });

        return form;
    }

    public static GameForm createLegacyGameForm() {
        GameForm form = GameForm.newInstance();
        form.setTournamentID("кубок Грузии");
        form.setGameType("тур-1");
        form.setTeamID("G25");
        form.setPassword("abcd");
        form.setStartTactics(new TeamTactics(Tactic.DEFENCE, Hardness.SOFT, Style.WING_PLAY, 0, 0));
        form.setTeamCoachSettings(CoachSettings.PRESSING);
        form.setPrice(40);

        form.setFirstTeam(new PlayerPosition[] {
                goalkeeper(15)    .captain().get(),
                leftDefence(24)   .freekickPosition(10, 90) .penalty().get(),
                centralDefence(4) .freekickPosition(10, 120).freeKick().get(),
                centralDefence(20).freekickPosition(10, 150).indirectFreeKick().get(),
                rightDefence(5)   .freekickPosition(10, 180).leftCorner().get(),
                leftMidfield(6)   .freekickPosition(10, 210).rightCorner().personalSettings(new char[] {'П'}).get(),
                centralMidfield(7).freekickPosition(10, 240).get(),
                centralMidfield(8).freekickPosition(10, 270).get(),
                rightMidfield(14) .freekickPosition(10, 300).get(),
                centralForward(19).freekickPosition(10, 330).get(),
                centralForward(21).freekickPosition(10, 360).get()
        });
        form.setBench(new int[] {
                17, 25, 18, 16, 22, 23, 0
        });

        PlayerSubstitution[] playerSubstitutions = new PlayerSubstitution[25];
        playerSubstitutions[0] = substitution(25, -20, 20).out(15).in(25).get();
        playerSubstitutions[1] = substitution(45, -20, 20).out(7).in(23, PlayerAmplua.CM).get();
        playerSubstitutions[2] = substitution(75, -20, 20).out(8).in(
            abstractPlayer(22)
                .defencePosition(200, 270)
                .attackPosition(450, 270)
                .freekickPosition(450, 270)
                .personalSettings(new char[] {'П'})
                .get()
        ).get();
        for (int i = 3; i < 25; i++) {
            playerSubstitutions[i] = new PlayerSubstitution();
        }
        form.setSubstitutions(playerSubstitutions);
        form.setSubstitutionPreferences(SubstitutionPreferences.BY_POSITION);

        setForceToAttack(form, 75, 1, 0);
        setForceToDefence(form, 75, -10, 0);
        form.setHalftimeChanges(new TeamTactics[] {
                new TeamTactics(Tactic.DEFENCE, Hardness.SOFT, Style.COMBINATION, 1, 3),
                new TeamTactics(Tactic.ATTACK, Hardness.MAXI_HARD, Style.WING_PLAY, -5, -1),
                new TeamTactics(Tactic.DEFENCE, Hardness.DEFAULT, Style.COMBINATION, 0, 1),
                null,
                null
        });

        return form;
    }


    private static void setForceToAttack(GameForm form, int time, int min, int max) {
        form.setAttackMin(min);
        form.setAttackMax(max);
        form.setAttackTime(time);
    }

    private static void setForceToDefence(GameForm form, int time, int min, int max) {
        form.setDefenceMin(min);
        form.setDefenceMax(max);
        form.setDefenceTime(time);
    }
}