FA13 Jogador
============

FA13 Jogador is a program for managing virtual football team in online game
[Football Manager FA13](http://www.fa13.info).


Downloading artifacts
---------------------

Program is available for download on the following page: [http://www.fa13.info/build_java.html](http://www.fa13.info/build_java.html).


How to build
------------

Program uses [Gradle](http://www.gradle.org) as a build system. You don't need to download it separately,
you can use `./gradlew` from the root folder of the source tree instead. 

### Prerequisites

* [Git](https://git-scm.com/)
* [JDK 7+](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

### Checkout and build

**NOTE**: for Windows use ``gradlew.bat`` instead of Linux version ``gradlew``.

    git clone git@gitlab.com:fa13/build.git fa13-build
    cd fa13-build
    ./gradlew clean build


### Building distributions

In order to build distributions (archives with all-in-one application) run the following command:

    ./gradlew clean buildDistributions

### Building using Docker container (like in Gitlab CI)

If you like to run the same build like it will be done on Gitlab server, run the following command:

    docker run -i -v `pwd`:/root/project -w /root/project frekele/java:jdk8 ./gradlew  --info buildDistributions

Make sure you use the same image, which is defined in `.gitlab-ci.yml`.

### Setup Eclipse IDE

	1) Install BuildShip plugin for Eclipse.
       Be sure to use latest version! Check here http://projects.eclipse.org/projects/tools.buildship
	2) run from terminal "./gradlew -x eclipse"

	3) After that do "Import Gradle Project" 

Licence
-------

FA13 Jogador is released under GPL (General Public License) v3.

Resources
---------

### Gradle

* [Gradle User Guide](http://gradle.org/docs/current/userguide/userguide_single.html)
* [Gradle DSL Guide](http://gradle.org/docs/current/dsl/index.html)

### SWT

* [SWT Documentation](http://www.eclipse.org/swt/docs.php)


Trouble shooting
----------------

**Problem:** I run `gradlew clean build` and get compilation errors. What's wrong?

**Solution:** Make sure you are using JDK 7 or higher. Run `gradlew -version` to see which environment do you have.
